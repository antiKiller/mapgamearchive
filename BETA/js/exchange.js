var mustUpdateExchange=null;

function exchange_init()
{
	closeMap();
	loadExchange();


	$('#exchangeTabs abbr').unbind('click');
	$('#exchangeTabs abbr').bind
	(
		'click',
		function()
		{
			var _this=$(this);
			if (!$(this).hasClass('selected'))
			{
				$('#exchangeTabs abbr').removeClass('selected');
				_this.addClass('selected');
				loadExchange();
			}
		}
	);

	$('#buyCreditsButton').unbind('click');
	$('#buyCreditsButton').bind
	(
		'click',
		function()
		{
			var buyCreditsContent='Вы можете купить <img class="moneyIndicator" src="images/baks.png"> за рубли. Цена 1 <img class="moneyIndicator" src="images/baks.png"> равняется 50 копейкам. Однако, <b>чем больше <img class="moneyIndicator" src="images/baks.png"> вы покупаете, тем ниже цена одного <img class="moneyIndicator" src="images/baks.png"></b>.<br>'+
				'Цена за 1 <img class="moneyIndicator" src="images/baks.png"> уменьшается на 1 копейку за каждые 1500 <img class="moneyIndicator" src="images/baks.png">. Минимальный курс - 20 копеек за 1 <img class="moneyIndicator" src="images/baks.png">.<br>'+
				'Передвигайте слайдер для получения суммы кредитов, которую вы хотите купить. Минимум - 10 <img class="moneyIndicator" src="images/baks.png">. Максимум - 50 000 <img class="moneyIndicator" src="images/baks.png">.<br><br>'+
				'<div id="getCreditsSlider"></div><br><br>'+
				'Вы хотите купить <input type="text" id="buyCredits" value="100"> <img class="moneyIndicator" src="images/baks.png"> по курсу <b id="kursCredits">0.5</b> руб. Итого: <b id="itogoBuyCreditsSumm">50.00</b> руб.<br><br>'+
				//'<b>C учётом акции вы получите X1.5 кредитов: <span id="allCreditsByAction">150</span> <img class="moneyIndicator" src="images/baks.png"></b><br><br>'+
				'<input type="button" value="Купить" id="confirmBuyCredits">';
			setMessage(buyCreditsContent);
			var nowSlider=$("#getCreditsSlider" ).slider
			(
				{
					value:100,
					min: 10,
					max: 50000,
					step: 1,
					slide: function( event, ui )
					{
						var kurse=0.5;
						var credits=parseInt(ui.value);
						var coeff=parseInt(credits/1500);
						if (coeff>0)
						{
							kurse=(kurse-(coeff)/100).toFixed(2);
						}
						if (kurse<0.2)
						{
							kurse=0.2;
						}
						var all=(credits*kurse).toFixed(2);
						$('#buyCredits').val(credits);
						$('#kursCredits').html(kurse);
						$('#itogoBuyCreditsSumm').html(all);
					//	$('#allCreditsByAction').html((credits*1.5).toFixed(0));
					}
				}
			);
			var timer=null;
			$('#buyCredits').unbind('keyup');
			$('#buyCredits').bind
			(
				'keyup',
				function()
				{
					clearTimeout(timer);
					var _this=$(this);
					var f=function()
					{
						var val=parseInt(_this.val());
						if (!isNaN(val) && val>0)
						{
							if (val<10)
							{
								val=10;
								_this.val(val);
							}
							nowSlider.slider('value', val);
							var kurse=0.5;
							var credits=val;
							var coeff=parseInt(credits/1500);
							if (coeff>0)
							{
								kurse=(kurse-(coeff)/100).toFixed(2);
							}
							if (kurse<0.2)
							{
								kurse=0.2;
							}
							var all=(credits*kurse).toFixed(2);
							$('#buyCredits').val(credits);
							$('#kursCredits').html(kurse);
							$('#itogoBuyCreditsSumm').html(all);
						//	$('#allCreditsByAction').html((credits*1.5).toFixed(0));
						}
						_this=null;
						delete _this;
					}
					timer=setTimeout(f,200);
				}
			);

			$('#confirmBuyCredits').unbind('click');
			$('#confirmBuyCredits').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/confirmBuyCredits.php',
						{
							creditsCount:nowSlider.slider('value')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							setMessage(data.content);
							
							$('.buyCreditsConfirmed').bind
							(
								'click',
								function()
								{
									$.post
									(
										'ajax/buyCreditsPayment.php',
										{
											service:$(this).attr('id')
										},
										function(data)
										{
											if (data.result!='ok')
											{
												errorHanding(data.result);
												return ;
											}
											setMessage(data.form);
											closeMessage();
										},
										'json'
									);
								}
							);
						},
						'json'
					);
				}
			);
		}
	);
}

function reLoadExchange()
{
	var type=$('#exchangeTabs .selected').attr('name');
	if (type=='credits')
	{
		$('#creditsExchangeBlock').show();
		$('#buildsExchangeBlock').hide();
		$("#buyerBlock").trigger('reloadGrid');
		$("#sellerBlock").trigger('reloadGrid');
	}
		else
		{
			$('#creditsExchangeBlock').hide();
			$('#buildsExchangeBlock').show();
		}
}

function loadExchange()
{
	var maxHeight=$('#mainPageContent').height()-310;
	$('#creditsExchangeBlock').css('height',maxHeight+75+'px');
	$('#buildsExchangeBlock').css('height',maxHeight+75+'px');

	var type=$('#exchangeTabs .selected').attr('name');
	if (type=='credits')
	{
		$('#creditsExchangeBlock').show();
		$('#buildsExchangeBlock').hide();
	}
		else
		{
			$('#creditsExchangeBlock').hide();
			$('#buildsExchangeBlock').show();
		}

	$("#buyerBlock").jqGrid
	(
		{
			url:'ajax/getExchange.php',
			postData: {type:'buyer'},
			datatype: "json",
			mtype: "POST",
			caption:'Покупатели кредитов',
			hidegrid:false,
			colNames: [
									'Покупатель',
									'<img class="moneyIndicator" src="images/money.png"> за 1 <img class="moneyIndicator" src="images/baks.png">',
									'Вы отдадите <img class="moneyIndicator" src="images/baks.png">',
									'Вы получите <img class="moneyIndicator" src="images/money.png">',
									'Осталось дней',
									'Продать <img class="moneyIndicator" src="images/baks.png">'
								],
			colModel:
			[
				{name:'name',index:'name', sortable:false},
				{name:'kurs',index:'kurs', sortable:false},
				{name:'credits',index:'credits', sortable:false},
				{name:'money',index:'money', sortable:false},
				{name:'endTime',index:'endTime', sortable:false},
				{name:'actions',index:'actions', sortable:false, align:"center"}
			],
			rowNum:50,
			pager: '#buyerBlockPager',
			width: $('#mainPageContent').width()/2-4,
			height: maxHeight,
			loadComplete: function()
			{
				$("#buyerBlock tr td").removeAttr("title");
			}
		}
	);
	$("#buyerBlock").jqGrid('navGrid','#buyerBlockPager',{edit:false,add:false,del:false,search:false});

	$("#sellerBlock").jqGrid
	(
		{
			url:'ajax/getExchange.php',
			postData: {type:'seller'},
			datatype: "json",
			mtype: "POST",
			caption:'Продавцы кредитов',
			hidegrid:false,
			colNames: [
									'Продавец',
									'<img class="moneyIndicator" src="images/money.png"> за 1 <img class="moneyIndicator" src="images/baks.png">',
									'Вы получите <img class="moneyIndicator" src="images/baks.png">',
									'Вы отдадите <img class="moneyIndicator" src="images/money.png">',
									'Осталось дней',
									'Купить <img class="moneyIndicator" src="images/baks.png">'
								],
			colModel:
			[
				{name:'name',index:'name', sortable:false},
				{name:'kurs',index:'kurs', sortable:false},
				{name:'credits',index:'credits', sortable:false},
				{name:'money',index:'money', sortable:false},
				{name:'endTime',index:'endTime', sortable:false},
				{name:'actions',index:'actions', sortable:false, align:"center"}
			],
			rowNum:50,
			pager: '#sellerBlockPager',
			width: $('#mainPageContent').width()/2-4,
			height: maxHeight,
			loadComplete: function()
			{
				$("#sellerBlock tr td").removeAttr("title");
			}
		}
	);
	$("#sellerBlock").jqGrid('navGrid','#sellerBlockPager',{edit:false,add:false,del:false,search:false});
	$("#gbox_sellerBlock").css('left',($('#mainPageContent').width()/2+2)+'px');
	$("#gbox_sellerBlock").css('top',(($('#mainPageContent').height()-234)*-1)+'px');

	$("#buildsBlock").jqGrid
	(
		{
			url:'ajax/getExchange.php',
			postData: {type:'builds'},
			datatype: "json",
			mtype: "POST",
			caption:'Постройки на продажу',
			hidegrid:false,
			colNames: [
									'Продавец',
									'Адрес',
									'Тип',
									'Уровень',
									'Клиенты',
									'Конкуренция',
									'Бонус',
									'Доход',
									'Цена <img class="moneyIndicator" src="images/money.png">' ,
									'Цена <img class="moneyIndicator" src="images/baks.png">',
									'Действия'
								],
			colModel:
			[
				{name:'user',index:'user', searchoptions:{sopt:['cn','nc','eq','ne','bw','bn','ew','en']}},
				{name:'address', width:300, index:'address', searchoptions:{sopt:['cn','nc','eq','ne','bw','bn','ew','en']}},
				{name:'name',index:'name', align:"center", stype:'select', searchoptions: { value: $('#listOfCostedHouseTypes').html(),sopt:['eq','ne'] }},
				{name:'level',index:'level', align:"center",searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'clients',index:'clients', align:"center", sorttype:"inteter", formatter:"integer", formatoptions:{decimalSeparator:'.'}, summaryType:'sum', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'competition',index:'competition', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.',suffix:'%'}, searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'bonus',index:'bonus', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.',suffix:'%'}, searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'doxod',index:'doxod', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.'}, searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'cost',index:'cost', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.'}, searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'costCredits',index:'costCredits', align:"center", sorttype:"integer", formatter:"integer", searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'actions',index:'actions', sortable:false, align:"center"}
			],
			rowNum:100,
			rowList:[50,100,500,1000],
			multiSort:true,
			multipleSearch:true,
			pager: '#buildsBlockPager',
			width: $('#mainPageContent').width()-5,
			height: maxHeight-25,
			loadComplete: function()
			{
				$("#buildsBlock tr td").removeAttr("title");
			}
		}
	);
	$("#buildsBlock").jqGrid('filterToolbar',{stringResult:true, searchOperators: true, operandTitle:"Кликните для выбора типа условия"});
	$("#buildsBlock").jqGrid('navGrid','#buildsBlockPager',{edit:false,add:false,del:false,search:false});

	//для биржи покупки кредитов
	$('#newCreditsBuy').unbind('click');
	$('#newCreditsBuy').bind
	(
		'click',
		function ()
		{
			var mess= 'Введите количество кредитов, которые вы хотите купить и курс:<br>'+
								'кредитов: <input type="text" id="creditsToBuy" value="1"><br>'+
								'по курсу: <input type="text" id="kursToBuy" value="100"> (минимальный курс - 100 монет, курс автоматически округляется до сотого значения)<br>'+
								'Вы должны будете отдать монет: <span id="resultSumm">100</span> + налог 5% <span id="nalogSumm">5</span>. Итого: <span id="itogSumm">105</span><br>'+
								'<input type="button" value="Разместить заявку" id="sendOrder">';
			setMessage(mess);

			$('#creditsToBuy').unbind('keyup');
			$('#creditsToBuy').bind
			(
				'keyup',
				function ()
				{
					var val=parseInt($(this).val());
					if (!isNaN(val))
					{
						$(this).val(val);
					}
					var kurse=parseFloat($('#kursToBuy').val()).toFixed(2);
					var res=parseFloat((kurse*val).toFixed(2));
					var nalog=parseFloat((res*0.05).toFixed(2));
					var itog=(res+nalog).toFixed(2);
					$('#resultSumm').html(res);
					$('#nalogSumm').html(nalog);
					$('#itogSumm').html(itog);
				}
			);

			$('#kursToBuy').unbind('keyup');
			$('#kursToBuy').bind
			(
				'keyup',
				function ()
				{
					var kurse=parseFloat($(this).val());
					if (!isNaN(kurse))
					{
						$(this).val(kurse);
					}
					var val=parseInt($('#creditsToBuy').val());
					var res=parseFloat((kurse*val).toFixed(2));
					var nalog=parseFloat((res*0.05).toFixed(2));
					var itog=(res+nalog).toFixed(2);
					$('#resultSumm').html(res);
					$('#nalogSumm').html(nalog);
					$('#itogSumm').html(itog);
				}
			);

			$('#sendOrder').unbind('click');
			$('#sendOrder').bind
			(
				'click',
				function ()
				{
					closeMessage();
					$.post
					(
						'ajax/newBuyCreditsOrder.php',
						{
							kurs:$('#kursToBuy').val(),
							credits:$('#creditsToBuy').val()
						},
						function (data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							setTimeout("$('#buyerBlock').trigger('reloadGrid');",1000);
							},
						'json'
					);
				}
			);
		}
	);

	//для биржи продажи кредитов
	$('#newCreditsSell').unbind('click');
	$('#newCreditsSell').bind
	(
		'click',
		function ()
		{
			var mess= 'Введите количество кредитов, которые вы хотите продать и курс:<br>'+
								'кредитов: <input type="text" id="creditsToBuy" value="1"><br>'+
								'по курсу: <input type="text" id="kursToBuy" value="100"> (минимальный курс - 100 монет, курс автоматически округляется до сотого значения)<br>'+
								'Вы должны будете получить монет: <span id="resultSumm">100</span><br>'+
								'<input type="button" value="Разместить заявку" id="sendOrder">';
			setMessage(mess);

			$('#creditsToBuy').unbind('keyup');
			$('#creditsToBuy').bind
			(
				'keyup',
				function ()
				{
					var val=parseInt($(this).val());
					if (!isNaN(val))
					{
						$(this).val(val);
					}
					var kurse=parseFloat($('#kursToBuy').val()).toFixed(2);
					var res=parseFloat((kurse*val).toFixed(2));
					$('#resultSumm').html(res);
				}
			);

			$('#kursToBuy').unbind('keyup');
			$('#kursToBuy').bind
			(
				'keyup',
				function ()
				{
					var kurse=parseFloat($(this).val());
					if (!isNaN(kurse))
					{
						$(this).val(kurse);
					}
					var val=parseInt($('#creditsToBuy').val());
					var res=parseFloat((kurse*val).toFixed(2));
					$('#resultSumm').html(res);
				}
			);

			$('#sendOrder').unbind('click');
			$('#sendOrder').bind
			(
				'click',
				function ()
				{
					closeMessage();
					$.post
					(
						'ajax/newSellCreditsOrder.php',
						{
							kurs:$('#kursToBuy').val(),
							credits:$('#creditsToBuy').val()
						},
						function (data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							setTimeout("$('#sellerBlock').trigger('reloadGrid');",1000);
						},
						'json'
					);
				}
			);
		}
	);

	$('#creditsExchangeBlock').undelegate('.sellCreds','click');
	$('#creditsExchangeBlock').delegate
	(
		'.sellCreds',
		'click',
		function ()
		{
			var ids=$(this).attr('ids');
			var kurse=parseFloat($(this).attr('kurs'));
			var maxCredits=$(this).attr('credits');
			var startCreds=1;
			if (maxCredits!='infinity')
			{
				maxCredits=parseInt(maxCredits);
				startCreds=maxCredits;
			}
				else
				{
					maxCredits='неограничего';
				}

			var summ=(kurse*startCreds).toFixed(2);
			var mess= 'Продать кредиты по курсу '+kurse+'.<br>'+
								'Выберите кол-во кредитов, которое вы хотите продать: <input type="text" id="userCreditsCount" value="'+startCreds+'"> (максимум: '+maxCredits+') <input type="button" id="sellMyCredits" value="Продать"><br>'+
								'Вы получите <span id="resultSetCost">'+summ+'</span> монет.';
			setMessage(mess);

			$('#userCreditsCount').unbind('keyup');
			$('#userCreditsCount').bind
			(
				'keyup',
				function ()
				{
					var val=parseInt($(this).val());
					if (!isNaN(val))
					{
						$(this).val(val);
					}
					var res=kurse*val;
					$('#resultSetCost').html(res.toFixed(2));
				}
			);

			$('#sellMyCredits').unbind('click');
			$('#sellMyCredits').bind
			(
				'click',
				function ()
				{
					$.post
					(
						'ajax/sellUserCredits.php',
						{
							sellId:ids,
							credits:$('#userCreditsCount').val()
						},
						function (data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							closeMessage();
							reLoadExchange();
						},
								'json'
					);
				}
			);
		}
	);


	$('#creditsExchangeBlock').undelegate('.buyCreds','click');
	$('#creditsExchangeBlock').delegate
	(
		'.buyCreds',
		'click',
		function ()
		{
			var ids=$(this).attr('ids');
			var kurse=parseFloat($(this).attr('kurs'));
			var maxCredits=$(this).attr('credits');
			var startCreds=0;
			if (maxCredits!='infinuty')
			{
				maxCredits=parseInt(maxCredits);
				startCreds=maxCredits;
			}

			var summ=(kurse*startCreds).toFixed(2);
			var nalog=(parseFloat(summ)*0.1).toFixed(2);
			var itog=(parseFloat(summ)+parseFloat(nalog)).toFixed(2);
			var mess= 'Купить кредиты по курсу '+kurse+'.<br>'+
								'Выберите кол-во кредитов, которое вы хотите купить: <input type="text" id="userCreditsCount" value="'+startCreds+'"> (максимум: '+maxCredits+') <input type="button" id="buyMyCredits" value="Купить"><br>'+
								'Вы должны будете отдать <span id="resultSetCost">'+summ+'</span> монет за сами кредиты + комиссия 10% <span id="resultSetNalog">'+nalog+'</span>. Итого <span id="resultSetItog">'+itog+'</span>';
			setMessage(mess);

			$('#userCreditsCount').unbind('keyup');
			$('#userCreditsCount').bind
			(
				'keyup',
				function ()
				{
					var val=parseInt($(this).val());
					if (!isNaN(val))
					{
						$(this).val(val);
					}
					var summ=(kurse*val).toFixed(2);
					var nalog=(parseFloat(summ)*0.1).toFixed(2);
					var itog=(parseFloat(summ)+parseFloat(nalog)).toFixed(2);
					$('#resultSetCost').html(summ);
					$('#resultSetNalog').html(nalog);
					$('#resultSetItog').html(itog);
				}
			);

			$('#buyMyCredits').unbind('click');
			$('#buyMyCredits').bind
			(
				'click',
				function ()
				{
					$.post
					(
						'ajax/buyUserCredits.php',
						{
							buyId:ids,
							credits:$('#userCreditsCount').val()
						},
						function (data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							closeMessage();
							reLoadExchange();
						},
						'json'
					);
				}
			);
		}
	);

	$('#creditsExchangeBlock').undelegate('.canselOrder','click');
	$('#creditsExchangeBlock').delegate
	(
		'.canselOrder',
		'click',
		function()
		{
			var idsVal=$(this).attr('ids');
			$.post
			(
				'ajax/getResultForCancellOrder.php',
				{
					ids:idsVal
				},
				function (data)
				{
					if (data.result!='ok')
					{
						errorHanding(data.result);
						return ;
					}
					setMessage(data.content);
					$('#yesCancellOrder').unbind('click');
					$('#yesCancellOrder').bind
					(
						'click',
						function()
						{
							$.post
							(
								'ajax/canselOrder.php',
								{
									ids:idsVal
								},
								function(data)
								{
									if (data.result!='ok')
									{
										errorHanding(data.result);
										return ;
									}
									closeMessage();
									reLoadExchange();
								},
								'json'
							);
						}
					);
				},
				'json'
			);
		}
	);

	//для биржи строений
	$('#buildsExchangeBlock').undelegate('.buyHouse','click');
	$('#buildsExchangeBlock').delegate
	(
		'.buyHouse',
		'click',
		function()
		{
			clearTimeout(mustUpdateExchange);
			$.post
			(
				'ajax/confirmBuyHouse.php',
				{
					ids:$(this).attr('ids')
				},
				function (data)
				{
					if (data.result!='ok')
					{
						errorHanding(data.result);
						return ;
					}
					setMessage(data.content);
					$('#confirmedByHouse').unbind('click');
					$('#confirmedByHouse').bind
					(
						'click',
						function ()
						{
							closeMessage();
							$.post
							(
								'ajax/buyHouseOnExchange.php',
								{
									ids:$(this).attr('ids')
								},
								function(data)
								{
									if (data.result!='ok')
									{
										errorHanding(data.result);
										return ;
									}
									mustUpdateExchange=setTimeout("$('#buildsBlock').trigger('reloadGrid');",1000);
								},
								'json'
							);
						}
					);
				},
				'json'
			);
		}
	);

	$('#buildsExchangeBlock').undelegate('.canselSellBuilding','click');
	$('#buildsExchangeBlock').delegate
	(//снятие с продажи
		'.canselSellBuilding',
		'click',
		function()
		{
			cancelSellHouse([$(this).attr('ids')]);
			setTimeout("$('#buildsBlock').trigger('reloadGrid');",1000);
		}
	);
}
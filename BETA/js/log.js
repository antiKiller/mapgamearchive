function log_init()
{
	closeMap();
	$("#logBlock").jqGrid
	(
		{
			url:'ajax/getLog.php',
			datatype: "json",
			mtype: "POST",
			colNames:['Дата', 'Тип', 'Игрок', 'Обьект', 'Описание', 'Монеты', 'Кредиты'],
			colModel:
 			[
 				{name:'date',index:'date', search:false},
				{name:'type',index:'type', stype:'select', searchoptions: { value: $('#listOfTypes').html(),sopt:['eq','ne'] }},
				{name:'userName',index:'userName', search:false},
				{name:'address',index:'address', searchoptions:{sopt:['cn','nc','eq','ne','bw','bn','ew','en']}},
				{name:'text',index:'text', searchoptions:{sopt:['cn','nc','eq','ne','bw','bn','ew','en']}},
				{name:'money',index:'money', sorttype:'integer', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'credits',index:'credits', sorttype:'integer', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}}
			],
 			rowNum:30,
			rowList:[15,30,50,80,100],
			viewrecords: true,
			gridview: true,
			pager: '#logBlockPager',
			grouping:false,
			multiSort:true,
			multipleSearch:true,
			sortname: 'date',
			sortorder: 'desc',
			width: $('#mainPageContent').width()-5,
			height: $('#mainPageContent').height()-75,
			loadComplete: function()
			{
				$("#logBlock tr td").removeAttr("title");
			}
		}
	);
	$("#logBlock").jqGrid('filterToolbar',{stringResult:true, searchOperators: true, operandTitle:"Кликните для выбора типа условия"});
	$("#logBlock").jqGrid('navGrid','#logBlockPager',{edit:false,add:false,del:false,search:false});
}


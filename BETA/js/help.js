function index_init()
{}

function about_init()
{}

function helpCalc_house()
{
	var level=parseInt($('#buildLevel').val());
	if (level<1)
	{
		$('#buildLevel').val('1');
		level=1;
	}
	var basicCost=parseInt($('#buildCost').val());
	var updateCost=parseFloat($('#updateCost').val());
	var basicTime=parseInt($('#buildTime').val());
	var updateTime=parseFloat($('#updateTime').val());
	var clients=parseInt($('#fixClients').val());
	var newClients=clients+Math.floor(level/10);
	var socials=$('input[name="bonusForHoses"]');
	var bonus=0;
	for (var i=0;i<socials.length;i++)
	{
		var elem=$(socials[i]);
		var socialLevel=parseInt(elem.val());
		if (socialLevel>0)
		{
			newClients=newClients+Math.floor(socialLevel/20);
			var basicBonus=parseFloat(elem.attr('bonus'));
			var updateBonus=parseFloat(elem.attr('update'));
			if (socialLevel>1)
			{
				bonus+=basicBonus*Math.pow(updateBonus,socialLevel-1).toFixed(2);
			}
				else
				{
					bonus+=basicBonus;
				}
		}
	}
	bonus=bonus.toFixed(2);
	$('#clientsCount').val(newClients);
	$('#profitBonus').val(bonus);
	updateCostAndTime(level,basicCost,basicTime,updateCost,updateTime);

	var summCost=parseFloat($('#allCost').val());
	updatePaybackTimeAndProfit(summCost,newClients,level,bonus);
}
function helpCalcAct_house()
{
	$('#buildLevel').bind
	(
		'change',
		helpCalc_house
	);

	$('input[name="bonusForHoses"]').bind
	(
		'change',
		helpCalc_house
	);
}

function helpCalc_enterprise()
{
	var level=parseInt($('#buildLevel').val());
	if (level<1)
	{
		$('#buildLevel').val('1');
		level=1;
	}
	var basicCost=parseInt($('#buildCost').val());
	var updateCost=parseFloat($('#updateCost').val());
	var basicTime=parseInt($('#buildTime').val());
	var updateTime=parseFloat($('#updateTime').val());

	updateCostAndTime(level,basicCost,basicTime,updateCost,updateTime);

	var summCost=parseFloat($('#allCost').val());
	var clients=parseInt($('#clientsCount').val());
	updatePaybackTimeAndProfit(summCost,clients,level,0);
}
function helpCalcAct_enterprise()
{
	$('#buildLevel').bind
	(
		'change',
		helpCalc_enterprise
	);
}

function helpCalc_social()
{
	var level=parseInt($('#buildLevel').val());
	if (level<1)
	{
		$('#buildLevel').val('1');
		level=1;
	}
	var basicCost=parseInt($('#buildCost').val());
	var updateCost=parseFloat($('#updateCost').val());
	var basicTime=parseInt($('#buildTime').val());
	var updateTime=parseFloat($('#updateTime').val());

	updateCostAndTime(level,basicCost,basicTime,updateCost,updateTime);

	var basicBonus=parseFloat($('#basicBonus').val());
	var updateBonus=parseFloat($('#updateBonus').val());
	var bonus=basicBonus*Math.pow(updateBonus,level-1);

	$('#bonusForClients').val(bonus.toFixed(2));
}
function helpCalcAct_social()
{
	$('#buildLevel').bind
	(
		'change',
		helpCalc_social
	);
}

function updatePaybackTimeAndProfit(summCost,clients,level,bonus)
{
	var basicDoxod=parseFloat($('#basicDoxod').val());
	var updateDoxod=parseFloat($('#updateDoxod').val());

	var summProfit=basicDoxod*Math.pow(updateDoxod,level-1)*clients;
	if (bonus>0)
	{
		summProfit=summProfit+summProfit*bonus/100;
	}
	
	var paymentPersent=parseFloat($('#paymentPersent').val());
	var paybackTime=Math.ceil(summCost/(summProfit*clients*paymentPersent));
	$('#clientsProfit').val(summProfit.toFixed(2));
	$('#paybackTime').val(paybackTime);
}

function updateCostAndTime(level,basicCost,basicTime,updateCost,updateTime)
{
	var costLevelUp=basicCost*Math.pow(updateCost,level-1);
	var allCostLevelUp=basicCost;
	for(var i=1;i<level;i++)
	{
		allCostLevelUp+=basicCost*Math.pow(updateCost,level-1);
	}


	var timeLevelUp=basicTime*Math.pow(updateTime,level-1);
	var allTimeLevelUp=basicTime;
	for(var i=1;i<level;i++)
	{
		allTimeLevelUp+=basicTime*Math.pow(updateTime,level-1);
	}

	if (level==1)
	{
			costLevelUp=0;
			timeLevelUp=0;
	}

	$('#levelUpCost').val(costLevelUp.toFixed(2));
	$('#allCost').val(allCostLevelUp.toFixed(2));
	$('#levelUpTime').val(timeLevelUp.toFixed(0));
	$('#allTime').val(allTimeLevelUp.toFixed(0));
}

function library_init()
{
	$('#selectTypeCalcedHouse').bind
	(
		'change',
		function()
		{
			var buildType=parseInt($(this).val());
			if (!isNaN(buildType) && buildType!=0)
			{
				$.post
				(
					"ajax/calcForms.php",
					{
						type: buildType
					},
					function(data)
					{
						if (data.result!='ok')
						{
							errorHanding(data.result);
							return ;
						}
						$('#calcedHouseForm').html(data.calcForm);
						$('#calcedHouseResult').html(data.resForm);

						eval('helpCalcAct_'+data.type+'();');

					},
					"json"
				);
			}
				else
				{
					$('#calcedHouseForm').html('');
					$('#calcedHouseResult').html('');
				}
		}
	);

	$('#selectTypeCalcedAuc').bind
	(
		'change',
		function()
		{
			calcAuc();
		}
	);

	$('#oneHourDoxod').bind
	(
		'keyup',
		function()
		{
			calcAuc();
		}
	);
}

function calcAuc()
{
	var auc=parseInt($('#selectTypeCalcedAuc').val());
	var aucPersent=parseFloat($('#aucPer_'+auc).html());
	var oneHourDoxod=parseFloat($('#oneHourDoxod').val());

	if (!isNaN(auc) && !isNaN(auc) && !isNaN(aucPersent))
	{
		auc=auc*-1;
		var doxod=8*oneHourDoxod+oneHourDoxod/5*2;
		doxod=auc+doxod*aucPersent*3;
		$('#aucCalcDoxod').val(doxod.toFixed(2));
	}
		else
		{
			$('#aucCalcDoxod').val('0');
		}
}
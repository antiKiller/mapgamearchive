function getUserProfile(ids)
{
	if (!ids)
	{
		ids=null;
	}
	$.post
	(
		'ajax/getUserProfile.php',
		{
			profile:ids
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			setMessage(data.userProfile);

			$('#newUserName').unbind('keyup');
			$('#newUserName').bind
			(
				'keyup',
				function()
				{
					var len=$(this).val().length;
					$('#nowUserNameLength').removeClass('red')
					$('#nowUserNameLength').html(len);
					if (len>60)
					{
						$('#nowUserNameLength').addClass('red');
					}
				}
			);

			$('#changeUserName').unbind('click');
			$('#changeUserName').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/changeUserName.php',
						{
							newName:$('#newUserName').val()
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							setMessage('Ваше имя было успешно изменено!');
						},
						'json'
					);
				}
			);

			$('#isBlinkedChat').unbind('change');
			$('#isBlinkedChat').bind
			(
				'change',
				function()
				{
					$.post
					(
						'ajax/changeChatBlink.php',
						{
							chatBlink:$(this).is(":checked")
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}
function userPayments_init()
{
	var timer=null;
	$('#messageWindow').undelegate('#rublesOutput');
	$('#messageWindow').delegate
	(
		'#rublesOutput',
		'keyup',
		function()
		{
			clearTimeout(timer);
			var _this=$(this);
			var f=function()
			{
				var kurse=parseFloat($('#outKurs').html());
				var rubles=parseFloat(_this.val());
				if (rubles>40)
				{
					rubles=40;
					_this.val(40);
				}
				if (rubles<0)
				{
					rubles=0;
					_this.val(0);
				}
				var allMoney=number_format(rubles*kurse,2,'.',' ');
				$('#outputMoneyCost').html(allMoney);
				_this=null;
				delete _this;
			}
			timer=setTimeout(f,200);
		}
	);

	$('#messageWindow').undelegate('#outputPayment');
	$('#messageWindow').delegate
	(
		'#outputPayment',
		'click',
		function()
		{
			$.post
			(
				'ajax/userPayment.php',
				{
					rubles:$('#rublesOutput').val(),
					account:$('#yandexMoneyAccount').val()
				},
				function(data)
				{
					if (data.result!='ok')
					{
						errorHanding(data.result);
						return ;
					}
					$('#userData').trigger('click');
				},
				'json'
			);
		}
	);
}
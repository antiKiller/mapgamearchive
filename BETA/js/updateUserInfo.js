var intervalUpdate=null;

function getUserMoney()
{
	if(!windowIsFocused)
	{
		intervalUpdate=setTimeout("getUserMoney()", 10000);
		return ;
	}
	$.post
	(
		'ajax/getUserMoney.php',
		{
			nowPage:detectNowPage(true,true)
		},
		function(data)
		{
			intervalUpdate=setTimeout("getUserMoney()", 10000);
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			$('#userMoneys').html(data.money);
			$('#userCredits').html(data.credits);
			$('#userKapital').html(data.kap);
			if (data.confirm)
			{
				setMessage(data.confirm);
			}

			if (parseInt(data.badAuc)!=0)
			{
				$('#badAuc').html(' ('+data.badAuc+')');
			}
				else
				{
					$('#badAuc').html('');
				}

			if (data.blinkChat)
			{
				startBlinkChat();
			}

			data=null;
			delete data;
		},
		"json"
	);
}

function startGetUserMoneyAndChat()
{
	if (!$('#userMoneys').html())
	{
		return ;
	}
	if (intervalUpdate==null)
	{
		intervalUpdate=setTimeout("getUserMoney()", 1);
		$('#gameChat').css('display','block');
		$('#chatIndicator').bind
		(
			'click',
			function()
			{
				if (!chatIsOpen)
				{
					openChat();
				}
					else
					{
						closeChat();
					}
			}
		);
	}
}

function stopGetUserMoneyAndChat()
{
	clearInterval(intervalUpdate);
	intervalUpdate=null;
	closeChat();
	$('#gameChat').css('display','none');
	$('#chatIndicator').unbind('click');
}
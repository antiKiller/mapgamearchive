var chatIsOpen=false;
var chatUpdateInterval=null;
var chatBlinkInterval=null;
var nowMaxId=0;
var chatMaxRight;
var blinkedChat=false;

function openChat()
{
	stopBlinkChat();
	chatIsOpen=true;
	clearInterval(chatUpdateInterval);
	chatUpdateInterval=null;
	chatUpdateInterval=setTimeout("chatUpdate()", 10);
	$('#gameChat').animate
	(
		{
			right: 0
		},
		1000
	);
	$('#chatIndicator').removeClass('closed');
	$('#sendChatMessageButton').bind
	(
		'click',
		function ()
		{
			sendChatMess();
		}
	);
	$('#chatMessageText').bind
	(
		'keydown',
		function (e)
		{
			if (e.ctrlKey && e.keyCode===13)
			{
				sendChatMess();
			}
		}
	);
	$('#chatMessages').animate({ scrollTop: $('#chatMessages').prop("scrollHeight") }, 200);
}

function closeChat()
{
	chatIsOpen=false;
	clearInterval(chatUpdateInterval);
	chatUpdateInterval=null;
	$('#gameChat').animate
	(
		{
			right: chatMaxRight
		},
		1000
	);
	$('#chatIndicator').addClass('closed');
	$('#sendChatMessageButton').unbind('click');
	$('#chatMessageText').unbind('keydown');
}

function chatUpdate()
{
	if (!chatIsOpen)
	{
		closeChat();
		return ;
	}
	$.post
	(
		'ajax/updateChat.php',
		{
			fromIds:nowMaxId
		},
		function(data)
		{
			chatUpdateInterval=setTimeout("chatUpdate()", 1500);
			if (data.result!='ok')
			{
				errorHanding(data.result);
				closeChat();
				return ;
			}
			nowMaxId=data.fromIds;
			$('#chatMessages').append(data.messages);
			$('#chatUsers').html(data.users);
			if (data.messages && data.messages!='')
			{
				$('#chatMessages').animate({ scrollTop: $('#chatMessages').prop("scrollHeight") }, 200);
		  }
			stopBlinkChat();
		},
		"json"
	);
}

function sendChatMess()
{
	var nowText=$('#chatMessageText').val();
	$('#chatMessageText').val('');
	$.post
	(
		'ajax/sendChatMessage.php',
		{
			message:nowText
		},
		function(data)
		{
			if (data.result!='ok')
			{
				$('#chatMessageText').val(nowText);
				errorHanding(data.result);
				return ;
			}
			nowText=null;
		},
		'json'
	);
}

function startBlinkChat()
{
	if (!blinkedChat)
	{
		chatBlinkAnimate();
		chatBlinkInterval=setInterval('chatBlinkAnimate();', 5000);
		blinkedChat=true;
	}
}

function stopBlinkChat()
{
	if (blinkedChat)
	{
		clearInterval(chatBlinkInterval);
		blinkedChat=false;
	}
}

function chatBlinkAnimate()
{
	if(!windowIsFocused)
	{
		return ;
	}
	
	if (blinkedChat)
	{
		$('#chatIndicator')
			.animate({bottom: '85px'}, 250)
			.animate({bottom: '3px'},  180)
			.animate({bottom: '60px'}, 200)
			.animate({bottom: '3px'},  100)
			.animate({bottom: '25px'}, 150)
			.animate({bottom: '3px'},  100);
	}
}
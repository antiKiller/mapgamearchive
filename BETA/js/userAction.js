function userAuthorize(uLoginToken)
{
	$.post
	(
		'ajax/userAuthorize.php',
		{token:uLoginToken},
		function(data)
		{
			if (data.res=='ok')
			{
				updateInterface();
			}
				else
				{
					if(data.res=='banned')
					{
						$('#messageError .content').html('<h1>К сожалению вы были забаннены в игре!</h1><h2>За подробностями обратитесь к администратору в личку: <a href="http://vk.com/webforever" target="_blank">Стас Тельнов</a>.</h2>');
						$('#messageError').show(300);
					}
						else
						{
							errorHanding(data.res);
						}
				}
		},
		'json'
	);
}

function updateInterface()
{
	$.post
	(
		'ajax/getPage.php',
		{name:'authorize'},
		function(data)
		{
			$('#authBlock').html(data);
			if ($('#uLogind91b6c88')[0])
			{
				uLogin.customInit("uLogind91b6c88");
			}
		}
	);
	$.post
	(
		'ajax/getPage.php',
		{name:'mainMenu'},
		function(data)
		{
			$('#mainMenu').html(data);
			setNavigation();
			startGetUserMoneyAndChat();
		}
	);
}

function initComments()
{
	if($('#vk_comments')[0])
	{
		var f=function ()
		{
			VK.Widgets.Comments("vk_comments", {limit: 10, attach: "*"},detectNowPage(true));
		}
		setTimeout(f,250);
	}
}
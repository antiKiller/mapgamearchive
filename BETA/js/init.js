var authBlockClosed=false;
var windowIsFocused=true;

function readyFunc()
{
	var startPosContent=$('#mainPageContainer').offset().top;
	var maxPos=$('#copyrightBlock').offset().top;
	var maxWidth=$('#mainMenu').width();
	var leftWidth=0;
	var dopHeight=20;
	if ($('#leftBlock').html())
	{
		leftWidth=$('#authBlock').outerWidth();
		dopHeight=0;
	}

	$('#mainPageContainer').css('height',maxPos-startPosContent+'px');
	$('#mainPageContent').css('height',maxPos-startPosContent-36+dopHeight+'px');
	$('#mainPageContent').css('width',maxWidth-leftWidth-32+'px');

	$('#gameChat').css('width',maxWidth-24+'px');
	chatMaxRight=-maxWidth+24;
	$('#gameChat').css('right',chatMaxRight+'px');

	if (leftWidth>0)
	{
		var f=function ()
		{
			VK.init({apiId: 2331763, onlyWidgets: true});
			VK.Widgets.Group("vk_groups", {mode: 2, width: "300", height: (maxPos-155)}, 54660744);
			initComments();
		}
		setTimeout(f,250);
	}
		else
		{
			authBlockClosed=true;
			$('#authSlider').unbind('click');
			$('#authSlider').bind
			(
				'click',
				function()
				{
					if (authBlockClosed)
					{
						authBlockClosed=false;
						$('#authBlock').animate
						(
							{
								left: -5
							},
							500
						);
					}
						else
						{
							authBlockClosed=true;
							$('#authBlock').animate
							(
								{
									left: -238
								},
								500
							);
						}
				}
			)
		}

	$('#authBlock').delegate
	(
	  '#deathorizeButton',
	  'click',
		function()
		{
			$.get
			(
				'ajax/exitUser.php',
				function()
				{
					stopGetUserMoneyAndChat();
					updateInterface();
					window.location.pathname='/';
				}
			);
		}
	);

	$('#mainMenu').delegate
	(
	  '#userData',
	  'click',
		function()
		{
			$.post
			(
				'ajax/getBankruptInfo.php',
				function(data)
				{
					if (data.result!='ok')
					{
						errorHanding(data.result);
						return ;
					}

					setMessage(data.content);
					$('#creditsBunkrupt').unbind('click');
					$('#creditsBunkrupt').bind
					(
						'click',
						function()
						{
							closeMessage();
							$.post
							(
								'ajax/bankruptGamer.php',
								{
									type:'optimize'
								},
								function(data)
								{
									if (data.result!='ok')
									{
										errorHanding(data.result);
										return ;
									}
									window.location.reload();
								},
								'json'
							);
						}
					);

					$('#fullBunkrupt').unbind('click');
					$('#fullBunkrupt').bind
					(
						'click',
						function()
						{
							closeMessage();
							$.post
							(
								'ajax/bankruptGamer.php',
								{
									type:'full'
								},
								function(data)
								{
									if (data.result!='ok')
									{
										errorHanding(data.result);
										return ;
									}
									window.location.reload();
								},
								'json'
							);
						}
					);
				},
				'json'
			);
		}
	);

	setNavigation();

	$('#messageWindow .closeMessageButton').click
	(
		function()
		{
			closeMessage();
		}
	);

	$('#messageWindow').width(maxWidth-260);
	$('#messageWindow').height($('#mainPageContainer').height()-150);
	startGetUserMoneyAndChat();

	$('#messageError .closeErrorButton').bind
	(
		'click',
		function()
		{
			$('#messageError .content').html('');
			$('#messageError').hide(300);
		}
	);

	userPayments_init();

	$('body').delegate
	(
		'.openUserProfile',
		'click',
		function()
		{
			getUserProfile($(this).attr('userId'));
		}
	);

	$('body').delegate
	(//переход к карте
		'.goToMap',
		'click',
		function()
		{
			goToMapPoint($(this).attr('coords'),$(this).attr('zoom'));
		}
	);



	$(window).focus
	(
		function()
		{
			windowIsFocused=true;
		}
	);

	$(window).blur
	(
		function()
		{
			windowIsFocused=false;
		}
	);
}
$(document).ready(readyFunc);

function setMessage(content)
{
	$('#message .content').html(content);
	$('#message').show(300);
}

function closeMessage()
{
	setTimeout("$('#message .content').html('');",300);
	$('#message').hide(300);
}

function strip_tags( str ){	// Strip HTML and PHP tags from a string
	//
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

	return str.replace(/<\/?[^>]+>/gi, '');
}

function errorHanding(text)
{
	if (text=='unknown_user')
	{
		document.write('<h1>Ошибка авторизации. Через 3 секунды вы будете переведены на главную страницу игры.</h1>');
		setTimeout("window.location.pathname='/';",3000);
		return ;
	}
	if (text=='banned')
	{
		document.write('<h1>К сожалению вы были забаннены в игре!</h1><h2>За подробностями обратитесь к администратору в личку: <a href="http://vk.com/webforever" target="_blank">Стас Тельнов</a>.</h2><h3>Через 10 секунды вы будете переведены на главную страницу игры.</h3>');
		setTimeout("window.location.pathname='/';",10500);
		return ;
	}
	$('#messageError .content').html(text);
	$('#messageError').show(300);
}

function number_format( number, decimals, dec_point, thousands_sep )
{	// Format a number with grouped thousands
	//
	// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +	 bugfix by: Michael White (http://crestidg.com)

	var i, j, kw, kd, km;

	// input sanitation & defaults
	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


	return km + kw + kd;
}

function house_init()
{
	closeMap();
	$("#housesBlock").jqGrid
	(
		{
			url:'ajax/getHousesList.php',
			datatype: "json",
			mtype: "POST",
			colNames:['Адрес', 'Тип', 'Доход','Уровень', 'Клиентов', 'Бонус', 'Конкуренция', 'Управляющий', 'Время окончания', 'Действие'],
			colModel:
 			[
 				{name:'address', width:300, index:'address', searchoptions:{sopt:['cn','nc','eq','ne','bw','bn','ew','en']}},
				{name:'name',index:'name', align:"center", stype:'select', searchoptions: { value: $('#listOfHouseTypes').html(),sopt:['eq','ne'] }},
				{name:'doxod',index:'doxod', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.'}, summaryType:'sum', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'level',index:'level', align:"center",searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'clients',index:'clients', align:"center", sorttype:"inteter", formatter:"integer", formatoptions:{decimalSeparator:'.'}, summaryType:'sum', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'bonus',index:'bonus', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.',suffix:'%'}, searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'competition',index:'competition', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.',suffix:'%'}, searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{name:'isManaged',index:'isManaged', align:"center", stype:'select', searchoptions: { value: ':Все;Да:Да;Нет:Нет;Необслуживаемое:Необслуживаемое',sopt:['eq','ne'] }},
				{name:'time',index:'time', align:"center", search:false},
				{name:'action', width:180, index:'action', align:"center", sortable:false, search:false}
			],
 			rowNum:parseInt($('#countHousesOnPage').html()),
			rowList:[50,100,500,1000,2500,5000,7500,10000],
			pager: '#housesBlockPager',
			viewrecords: true,
			grouping:true,
			multiSort:true,
			sortname: 'name',
			sortorder: 'asc',
			multiselect: true,
			multipleSearch:true,
			groupingView:
			{
				groupField:['name'],
				groupColumnShow:[true],
				groupText : ['<b>{0}</b>, кол-во: {1} '],
				groupSummary : [true],
				groupCollapse : false,
				showSummaryOnHide: true
			},
			width: $('#mainPageContent').width(),
			height: $('#mainPageContent').height()-145,
			loadComplete: function()
			{
				$("#housesBlock tr td").removeAttr("title");
			}
		}
	);
	$("#housesBlock").jqGrid('filterToolbar',{stringResult:true, searchOperators: true, operandTitle:"Кликните для выбора типа условия"});
	$("#housesBlock").jqGrid('navGrid','#housesBlockPager',{edit:false,add:false,del:false,search:false});
	updateManagers();

	{
		$('#housesBlock').undelegate('.buildObject','click');
		$('#housesBlock').delegate
		(//строительство
			'.buildObject',
			'click',
			function()
			{
				buildOnHouses($(this).attr('ids'));
			}
		);
	
		$('#housesBlock').undelegate('.recalcObject','click');
		$('#housesBlock').delegate
		(//пересчёт
			'.recalcObject',
			'click',
			function()
			{
				recalcHouse([$(this).attr('ids')]);
			}
		);

		$('#housesBlock').undelegate('.speedUpAction','click');
		$('#housesBlock').delegate
		(//ускорение
			'.speedUpAction',
			'click',
			function()
			{
				speedUpHouse([$(this).attr('ids')]);
			}
		);

		$('#housesBlock').undelegate('.canselSellBuilding','click');
		$('#housesBlock').delegate
		(//снятие с продажи
			'.canselSellBuilding',
			'click',
			function()
			{
				cancelSellHouse([$(this).attr('ids')]);
			}
		);

		$('#housesBlock').undelegate('.destroyObject','click');
		$('#housesBlock').delegate
		(//снос
			'.destroyObject',
			'click',
			function()
			{
				destroyHouse([$(this).attr('ids')]);
			}
		);

		$('#housesBlock').undelegate('.rentObject','click');
		$('#housesBlock').delegate
		(//аренда
			'.rentObject',
			'click',
			function()
			{
				rentHouse([$(this).attr('ids')]);
			}
		);

		$('#housesBlock').undelegate('.sellObject','click');
		$('#housesBlock').delegate
		(//продажа
			'.sellObject',
			'click',
			function()
			{
				saleHouse($(this).attr('ids'));
			}
		);

		$('#housesBlock').undelegate('.impObject','click');
		$('#housesBlock').delegate
		(//улучшение
			'.impObject',
			'click',
			function()
			{
				levelUpHouse([$(this).attr('ids')]);
			}
		);

	}
	$('#managAllHouses').unbind('click');
	$('#managAllHouses').bind
	(
		'click',
		function()
		{
			managAllHouses($("#housesBlock").jqGrid('getGridParam','selarrrow'),$('#actionsOfAllHouses input[name="modeOfAction"]:checked').val());
		}
	);

	$('#impAllHouses').unbind('click');
	$('#impAllHouses').bind
	(
		'click',
		function()
		{
			levelUpHouse($("#housesBlock").jqGrid('getGridParam','selarrrow'),$('#actionsOfAllHouses input[name="modeOfAction"]:checked').val());
		}
	);

	$('#rentAllHouses').unbind('click');
	$('#rentAllHouses').bind
	(
		'click',
		function()
		{
			rentHouse($("#housesBlock").jqGrid('getGridParam','selarrrow'),$('#actionsOfAllHouses input[name="modeOfAction"]:checked').val());
		}
	);

	$('#buildAllHouses').unbind('click');
	$('#buildAllHouses').bind
	(
		'click',
		function()
		{
			buildOnHouses($("#housesBlock").jqGrid('getGridParam','selarrrow'),$('#actionsOfAllHouses input[name="modeOfAction"]:checked').val());
		}
	);

	$('#destroyAllHouses').unbind('click');
	$('#destroyAllHouses').bind
	(
		'click',
		function()
		{
			destroyHouse($("#housesBlock").jqGrid('getGridParam','selarrrow'),$('#actionsOfAllHouses input[name="modeOfAction"]:checked').val());
		}
	);
}

function buildOnHouses(objId,modeVal)
{
	if (!objId)
	{
		objId=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/getBuildresForObject.php',
		{
			ids:objId.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			setMessage(data.dataList);

			$('#messageWindow .spoilerSwitcher abbr').unbind('click');
			$('#messageWindow .spoilerSwitcher abbr').bind
			(
				'click',
				function()
				{
					var content=$($(this).parent('.spoilerSwitcher').next('.spoilerContent')[0]);
					if (!content.is(':visible'))
					{
						$('#messageWindow .spoilerContent').slideUp(300);
					}
					content.slideToggle(300);
				}
			);

			$('.geoObjectInfo div.button').unbind('click');
			$('.geoObjectInfo div.button').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/confimBuildOnObject.php',
						{
							obj:$('#messageWindow .geoObjectInfo').attr('objid').toString(),
							type:$(this).attr('typeid')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							setMessage(data.content);
							$('#buildAllNow').unbind('click');
							$('#buildAllNow').bind
							(
								'click',
								function()
								{
									closeMessage();
									$.post
									(
										'ajax/buildOnObject.php',
										{
											obj:$(this).attr('objid'),
											type:$(this).attr('typeid')
										},
										function(data)
										{
											if (data.result!='ok')
											{
												errorHanding(data.result);
												return ;
											}
											$("#housesBlock").trigger('reloadGrid');
										},
										'json'
									);
								}
							);
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}

function recalcHouse(objId,modeVal)
{
	if (!objId)
	{
		objId=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	$.post
	(
		'ajax/reCalcHouse.php',
		{
			ids:objId.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
		},
		'json'
	);
}

function saleHouse(objId,modeVal)
{
	if (!objId)
	{
		objId=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/getSaleHousesInfo.php',
		{
			ids:objId.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			setMessage(data.message);

			$('#selledCostMoney').unbind('keyup');
			$('#selledCostMoney').bind
			(
				'keyup',
				function()
				{
					var val=$(this).val();
					var tmp=parseFloat(val);
					if (!isNaN(tmp))
					{
						$(this).val(tmp.toFixed(0));
					}
				}
			);

			$('#selledCostCredit').unbind('keyup');
			$('#selledCostCredit').bind
			(
				'keyup',
				function()
				{
					var val=$(this).val();
					var tmp=parseFloat(val);
					if (!isNaN(tmp))
					{
						$(this).val(tmp.toFixed(0));
					}
				}
			);

			$('#sendBuildingToExchange').unbind('click');
			$('#sendBuildingToExchange').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/houseToExchange.php',
						{
							ids:$(this).attr('ids'),
							money:$('#selledCostMoney').val(),
							credits:$('#selledCostCredit').val()
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							closeMessage();
							$("#housesBlock").trigger('reloadGrid');
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}

function levelUpHouse(objId,modeVal)
{
	if (!objId)
	{
		objId=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/getLevelUpCost.php',
		{
			ids:objId.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			setMessage(data.message);
			$('#startLevelUp').unbind('click');
			$('#startLevelUp').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/levelUp.php',
						{
							ids:$(this).attr('ids'),
							mode:$(this).attr('mode')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							closeMessage();
							$("#housesBlock").trigger('reloadGrid');
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}

function destroyHouse(objIdsList,modeVal)
{
	if (!objIdsList)
	{
		objIdsList=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/getDestroyInfo.php',
		{
			ids:objIdsList.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			setMessage(data.message);
			$('#destroyOnly').unbind('click');
			$('#destroyOnly').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/destroyObjects.php',
						{
							obj:$(this).attr('objid')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							closeMessage();
							$("#housesBlock").trigger('reloadGrid');
						},
						'json'
					);
				}
			);

			$('#fullDestroy').unbind('click');
			$('#fullDestroy').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/fullDestroy.php',
						{
							obj:$(this).attr('objid')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							closeMessage();
							$("#housesBlock").trigger('reloadGrid');
						},
						'json'
					);
				}
			);

			$('#messageWindow .spoilerSwitcher abbr').unbind('click');
			$('#messageWindow .spoilerSwitcher abbr').bind
			(
				'click',
				function()
				{
					var content=$($(this).parent('.spoilerSwitcher').next('.spoilerContent')[0]);
					if (!content.is(':visible'))
					{
						$('#messageWindow .spoilerContent').slideUp(300);
					}
					content.slideToggle(300);
				}
			);

			$('.geoObjectInfo div.button').unbind('click');
			$('.geoObjectInfo div.button').bind
			(
				'click',
				function()
				{
					closeMessage();
					var idsList=[$('#messageWindow .geoObjectInfo').attr('objid')].toString();
					var typeId=$(this).attr('typeid');
					$.post
					(
						'ajax/destroyObjects.php',
						{
							obj:idsList
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							$.post
							(
								'ajax/buildOnObject.php',
								{
									obj:idsList,
									type:typeId
								},
								function(data)
								{
									$("#housesBlock").trigger('reloadGrid');
									if (data.result!='ok')
									{
										errorHanding(data.result);
										return ;
									}
								},
								'json'
							);
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}

function rentHouse(objIdsList,modeVal)
{
	if (!objIdsList)
	{
		objIdsList=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/getRentTimes.php',
		{
			ids:objIdsList.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			setMessage(data.message);
			$('#idsListForRent .rentToTime').unbind('click');
			$('#idsListForRent .rentToTime').bind
			(
				'click',
				function()
				{
					closeMessage();
					$.post
					(
						'ajax/rent.php',
						{
							ids:$('#idsListForRent').attr('ids'),
							period:$(this).attr('timeHours'),
							mode:modeVal
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
							}
							$("#housesBlock").trigger('reloadGrid');
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}

function speedUpHouse(objIdsList,modeVal)
{
	if (!objIdsList)
	{
		objIdsList=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/confirmSpeedUp.php',
		{
			ids:objIdsList.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			setMessage(data.content);
			$('#speedUpConfirmed').unbind('click');
			$('#speedUpConfirmed').bind
			(
				'click',
				function()
				{
					closeMessage();
					$.post
					(
						'ajax/speedUp.php',
						{
							ids:$(this).attr('ids')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
							}
							$("#housesBlock").trigger('reloadGrid');
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}

function cancelSellHouse(objIdsList,modeVal)
{
	if (!objIdsList)
	{
		objIdsList=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/canselSellHouse.php',
		{
			ids:objIdsList.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			$("#housesBlock").trigger('reloadGrid');
		},
		'json'
	);
}

function managAllHouses(objIdsList,modeVal)
{
	if (!objIdsList)
	{
		objIdsList=new Array();
	}
	if (!modeVal)
	{
		modeVal='onlyChecked';
	}
	
	$.post
	(
		'ajax/managAllHouses.php',
		{
			ids:objIdsList.toString(),
			mode:modeVal
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			$("#housesBlock").trigger('reloadGrid');
		},
		'json'
	);
}

function updateManagers()
{
	$.post
	(
		'ajax/getManagerInfo.php',
		{},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
			}
			$('#userHouseManagerText').html(data.manager);
			$('#userHouseRentTask').html(data.tax);
			$('#userTaxOpitmizator').html(data.taxManager);

			$('#buyUserManager').unbind('click');
			$('#buyUserManager').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/getBuyManagerCosts.php',
						{},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
							}
							setMessage(data.message);
							$('#tableForBuyManager input[type="button"]').unbind('click');
							$('#tableForBuyManager input[type="button"]').bind
							(
								'click',
								function()
								{
									closeMessage();
									$.post
									(
										'ajax/buyUserManager.php',
										{
											ids:$(this).attr('managType')
										},
										function(data)
										{
											if (data.result!='ok')
											{
												errorHanding(data.result);
											}
											updateManagers();
										},
										'json'
									);
								}
							);
						},
						'json'
					);
				}
			);

			$('#buyUserAccountant').unbind('click');
			$('#buyUserAccountant').bind
			(
				'click',
				function()
				{
					$.post
					(
						'ajax/getTaxManagerCosts.php',
						{},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
							}
							setMessage(data.message);
							$('#tableForTaxManager input[type="button"]').unbind('click');
							$('#tableForTaxManager input[type="button"]').bind
							(
								'click',
								function()
								{
									closeMessage();
									$.post
									(
										'ajax/buyTaxManager.php',
										{
											ids:$(this).attr('managType')
										},
										function(data)
										{
											if (data.result!='ok')
											{
												errorHanding(data.result);
											}
											updateManagers();
										},
										'json'
									);
								}
							);
						},
						'json'
					);
				}
			);
		},
		'json'
	);
}
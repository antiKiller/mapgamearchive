function rating_init()
{
	closeMap();
	getRating();

	$('#ratingTabs abbr').unbind('click');
  $('#ratingTabs abbr').bind
	(
		'click',
		function()
		{
			var _this=$(this);
			if (!$(this).hasClass('selected'))
			{
				$('#ratingTabs abbr').removeClass('selected');
				_this.addClass('selected');
				$('#ratingNowPage').html(1);
				getRating();
			}
		}
	);

	$('#toFirstRatingPage').unbind('click');
	$('#toFirstRatingPage').bind
	(
		'click',
		function()
		{
			getRating('first');
		}
	);

	$('#toPrevRatingPage').unbind('click');
	$('#toPrevRatingPage').bind
	(
		'click',
		function()
		{
			if ( parseInt($('#ratingNowPage').html())>1 )
			{
				getRating('prev');
			}
		}
	);

	$('#toNextRatingPage').unbind('click');
	$('#toNextRatingPage').bind
	(
		'click',
		function()
		{
			if ( parseInt($('#ratingNowPage').html())<parseInt($('#ratingPagesCount').html()) )
			{
				getRating('next');
			}
		}
	);

	$('#toLastRatingPage').unbind('click');
	$('#toLastRatingPage').bind
	(
		'click',
		function()
		{
			getRating('last');
		}
	);

	$('#nowUserRatingPos').unbind('click');
	$('#nowUserRatingPos').bind
	(
		'click',
		function()
		{
			getRating($(this).attr('page'));
		}
	);
}

function getRating(goToPage)
{
	if (!goToPage)
	{
		goToPage=false;
	}
	$.post
	(
		'ajax/getRating.php',
		{
			ratingType:$('#ratingTabs abbr.selected').attr('name'),
			nowPage:$('#ratingNowPage').html(),
			toPage:goToPage,
			maxPage:$('#ratingPagesCount').html()
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			$('#ratingPeoples').html(data.peoples);
			$('#displayedRatingsUsers').html(data.displayerUsers);
			$('#ratingDescription').html(data.descr);
			$('#ratingNowPage').html(data.page);
			$('#nowUserRatingPos').html(data.nowUserPos);
			$('#nowUserRatingPos').attr('page',data.nowUserPage);
		},
		'json'
	);
}
var isAuctionAlredyInited=false;

function auction_init()
{
	closeMap();
	$("#auctionBlock").jqGrid
	(
		{
			url:'ajax/getAuctionsList.php',
			datatype: "json",
			mtype: "POST",
			colNames:['Адрес', 'Тип', 'Доход', 'Владелец', 'Ставка', 'Претендент', 'Окончание', 'Действия'],
			colModel:
 			[
 				{name:'address',index:'address', width:400, searchoptions:{sopt:['cn','nc','eq','ne','bw','bn','ew','en']}},
				{name:'name',index:'name', align:"center", stype:'select', searchoptions: { value: $('#listOfAuctionTypes').html(),sopt:['eq','ne'] }},
				{
					name:'doxod',index:'doxod', align:"center", sorttype:"float", formatter:"currency", summaryType:'sum',
					formatoptions:{decimalSeparator:'.'},
					searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}
				},
				{
					name:'userOwner',index:'userOwner', align:"center", search:false,
					cellattr: function(rowId, val, rawObject)
					{
						if (val!='Вы' && val!='')
						{
							return "class='ui-state-error-text ui-state-error'";
						}
					}
				},
				{name:'stavka',index:'stavka', align:"center", sorttype:"float", formatter:"currency", formatoptions:{decimalSeparator:'.'}, summaryType:'sum',searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
				{
					name:'userBid',index:'userBid', align:"center", search:false,
					cellattr: function(rowId, val, rawObject)
					{
						if (val!='Вы' && val!='')
						{
							return "class='ui-state-error-text ui-state-error'";
						}
					}
				},
				{name:'time',index:'time', align:"center", search:false},
				{name:'actions',index:'actions', align:"center", sortable:false, search:false}
			],
 			rowNum:1000000,
			viewrecords: true,
			pager: '#auctionBlockPager',
			grouping:true,
			multiSort:true,
			sortname: 'name',
			sortorder: 'asc',
			caption:'Аукционов: <span id="countAucAll">0</span>. &nbsp; Общий доход: <span id="allAucDoxod">0</span>. &nbsp; Сумма ставок: <span id="allAucBids">0</span>',
			hidegrid:false,
			multipleSearch:true,
			groupingView:
			{
				groupField:['name'],
				groupColumnShow:[true],
				groupText: ['<b>{0}</b>, кол-во: {1} '],
				groupSummary: [true],
				groupCollapse: false,
				showSummaryOnHide: true
			},
			width: $('#mainPageContent').width(),
			height: $('#mainPageContent').height()-100,
			loadComplete: function()
			{
				$.post
				(
					'ajax/allAucData.php',
					{},
					function(data)
					{
						if (data.result!='ok')
						{
							errorHanding(data.result);
							return ;
						}
						$('#countAucAll').html(data.countAucAll);
						$('#allAucDoxod').html(data.allAucDoxod);
						$('#allAucBids').html(data.allAucBids);
					},
					'json'
				);
				$("#auctionBlock tr td").removeAttr("title");
			}
		}
	);

	$("#auctionBlock").jqGrid('filterToolbar',{stringResult:true, searchOperators: true, operandTitle:"Кликните для выбора типа условия"});
	$("#auctionBlock").jqGrid('navGrid','#auctionBlockPager',{edit:false,add:false,del:false,search:false});


	{
		$('#auctionBlock').delegate
		(
			'.auctionActionBid',
			'click',
			function()
			{
				var ids=$(this).attr('ids');
				var addr=$("#auctionBlock").getCell(ids,0);
				var nowBid=parseFloat(strip_tags($("#auctionBlock").getCell(ids,4)).replace(/ /gm,''));
				var type=$("#auctionBlock").getCell(ids,1);
				auctionBid(ids,type,addr,nowBid);
			}
		);

		$('#auctionBlock').delegate
		(
			'.auctionActionClose',
			'click',
			function()
			{
				auctionDrop($(this).attr('ids'));
			}
		);
	}
}

function auctionBid(idAuc,type,addr,nowBid)
{
	var minBid=(nowBid*1.1).toFixed(2);
	var content='<p>Вы собираетесь сделать ставку на аукцион типа <b>'+type+'</b> по адресу <i>'+addr+'</i></p>'+
					    '<p>Минимальная ставка составляет '+minBid+' <img class="moneyIndicator" src="images/money.png"></p>'+
							'Ваша ставка: <input type=text value="'+minBid+'" id="userBidToAuc"> &nbsp; <input type="button" value="Cделать ставку" id="doingBid">';
	setMessage(content);
	$('#doingBid').click
	(
		function()
		{
			$.post
			(
				'ajax/bid.php',
				{
					bid:$('#userBidToAuc').val(),
					ids:idAuc
				},
				function(data)
				{
					if (data.result!='ok')
					{
						errorHanding(data.result);
						return ;
					}
					closeMessage();
					$("#auctionBlock").trigger('reloadGrid');
				},
				'json'
			);
		}
	);
}

function auctionDrop(idAuc)
{
	$.post
	(
		'ajax/aucDrop.php',
		{
			ids:idAuc
		},
		function(data)
		{
			if (data.result!='ok')
			{
				errorHanding(data.result);
				return ;
			}
			$("#auctionBlock").trigger('reloadGrid');
		},
		'json'
	);
}








function bid_auc(id,stavka)
{
    var vis = getClientSTop()
    if(!document.getElementById('message'))
    {
    var message = document.createElement('div');
    message.style.position="absolute";
    message.style.top="50%";
    message.style.left="50%";
	message.style.marginLeft="-301px";
	message.style.marginTop="-82px";
    message.style.width="600px";
    message.style.backgroundColor="#FFFFFF";
    message.style.border="1px solid #777777";
    //message.style.textAlign="left";
    message.setAttribute("id", "message");
    document.body.appendChild(message);
    var text =  document.createElement('div');
    var input = document.createElement('div');
	var header =  document.createElement('div');
	header.style.height="25px";
	header.style.backgroundColor="#5a7da3";
	header.innerHTML="<font color='#FFFFFF'>Ставка</font>";
	message.appendChild(header);
    input.style.height="27px";
	input.style.textAlign="right";
    text.borderBottom="1px solid #777777";
    message.appendChild(text);
    text.style.padding="5px";
	text.style.height="100px";
	stavka-=0;
	stavka=parseInt(stavka*1.1,10);
	otvet="<font>Ставка: </font><input type='text' name='stavka' id='stavka' style='color: #000000; font-size:13px; font-family:Arial;margin-left:25px;width:150px;' value='"+parseInt(stavka,10)+"'/><br><div id='soob'></div>";
    text.innerHTML=otvet;
    var button = document.createElement('input');
    button.setAttribute("type", "button");
    button.setAttribute("value", "Поставить");
    button.style.height="27px";
	button.style.textAlign="center";
    button.style.border="0px";
    button.style.backgroundColor="#5a7da3";
    button.style.color="#fff";
    button.style.cursor="pointer";
	button.style.left="30px";
	button.style.marginLeft="5px";
	button.style.borderRadius="5px";
    button.onclick = function()
    {
		var stavka0=document.getElementById("stavka").value;

			stavka1=parseInt(stavka0,10);
			if (stavka1>=stavka)
			{
				s=bid_a(id,stavka1);
				if (s=="ok")
				{
					input.removeChild(butcan);
					input.removeChild(button);
					message.removeChild(input);
					message.removeChild(text);
					document.body.removeChild(message);
				}
				else
				{
					alert(s);
				}
			}
			else
			{
				document.getElementById("soob").innerHTML="<font id='error' style='color:#FF0000'>Слишком маленькая ставка.<br>Минимальная ставка "+stavka+"</font>";
			}
	}
	input.appendChild(button);
	var butcan = document.createElement('input');
    butcan.setAttribute("type", "button");
    butcan.setAttribute("value", "Отмена");
    butcan.style.height="27px";
	butcan.style.width="60px";
	butcan.style.textAlign="center";
    butcan.style.border="0px";
    butcan.style.backgroundColor="#5a7da3";
    butcan.style.color="#fff";
    butcan.style.cursor="pointer";
	butcan.style.left="30px";
	butcan.style.marginLeft="5px";
	butcan.style.borderRadius="5px";
    butcan.onclick = function()
    {
		input.removeChild(butcan);
        input.removeChild(button);
        message.removeChild(input);
        message.removeChild(text);
        document.body.removeChild(message);
	}
    input.appendChild(butcan);
    message.appendChild(input);
    }

}
function getClientSTop()
{
    return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop)
}
window.onscroll = function  () {
    if(document.getElementById('message'))
    {
        var message = document.getElementById('message');
        var vis = getClientSTop();
        message.style.top="40%";
    }
}
function bid_a(id,stavka) {

    var req = new XMLHttpRequest();
	document.body.removeChild(document.getElementById("message"));
	req.open('GET', 'bid.php?ids=' + id + "&value=" + stavka, false);
	req.send(null);
	return req.responseText;

}
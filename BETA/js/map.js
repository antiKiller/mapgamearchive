var isMapAlredyInited=false;

var deln=false;
var renc="";
var buyOnOneClick=false;
var st="";
var str="";
var strk="";
var balloon;
var newBounds;
var kol;
var myMap;
var myRectangle;
var myCircle;
var myClickCircle;

var startPos=[55.754674,37.621543];
var startZoom=12;

var myObjectsManagerOnScreen;
var visibleObjsArray=new Array();
var visibleType="all";
var visibleOnlyCosted=0;
var allMapObjsArray=new Array();

	function mapStart()
	{
		var maxHeight=$('#mainPageContent').height();
		$("#YMapsID").height(maxHeight+'px');

		var pos=
			{
				lat:startPos[0],
				lon:startPos[1],
				zoom:startZoom
			};
		myMap=new ymaps.Map
		(
			'YMapsID',
			{
        center: [pos.lat,pos.lon],
        zoom: pos.zoom,
        behaviors: ['default', 'scrollZoom']
      },
			{balloonMaxHeight: 9000}
		);

		myObjectsManagerOnScreen=new ObjectManager();
		myMap.geoObjects.add(myObjectsManagerOnScreen);

		myMap.controls.add(new ymaps.control.MapTools());
		myMap.controls.add(new ymaps.control.ZoomControl());
		myMap.controls.add('searchControl');
		myMap.controls.add('typeSelector',{top: 5, right: 100});

		var oneClickBut=new ymaps.control.Button
		(
			{
				data:
				{
					image: 'images/oneClick.png'
				}
			},
			{
				selectOnClick: true,
				layout: ymaps.templateLayoutFactory.createClass('<img class="mapIconsButons oneClickMapButton" title="Покупка в один клик" src="$[data.image]">')
			}
		);
		oneClickBut.events.add
		(
			'click',
			function (event)
			{
				buyOnOneClick=!event.originalEvent.target.isSelected();
				$('.oneClickMapButton').removeClass('selected');
				if (buyOnOneClick)
				{
					$('.oneClickMapButton').addClass('selected');
				}
			}
		);
		myMap.controls.add(oneClickBut, {top: 5, right: 50});

		var settings=new ymaps.control.Button
		(
			{
				data:
				{
					image: 'images/icons/settings.png'
				}
			},
			{
				layout: ymaps.templateLayoutFactory.createClass('<img class="mapIconsButons" title="Настройка видимости обьектов на карте" src="$[data.image]">')
			}
		);
		settings.events.add
		(
			'click',
			function (event)
			{
				var message='Выберите обьекты, которые следует отображать на карте.'+
				'<form id="selectUnvisible">'+
				'<input type="button" value="Отметить все" id="checkAllObjects"> &nbsp; &nbsp; <input type="button" value="Снять отметку со всех" id="uncheckAllObjects">'+
				'<table>';
				for (var objPos in allMapObjsArray)
				{
					var obj=allMapObjsArray[objPos];
					if ((objPos%3)==0)
					{
						message+='</tr><tr>';
					}
					message+='<td><input type="checkbox" value="'+obj.ids+'" name="visibleGeoObjs" id="objsId_'+obj.ids+'"><label for="objsId_'+obj.ids+'"> ';

					if (obj.img!='')
					{
						message+='<img src="/images/'+obj.img+'.png" width="12px" height="12px"> '
					}
					message+=obj.name+'</label></td>';
				}

				var checkedAll='';
				var checkedMy='';
				var checkedEnemy='';

				if (visibleType=='all')
				{
					checkedAll='checked="checked"';
				}
				if (visibleType=='my')
				{
					checkedMy='checked="checked"';
				}
				if (visibleType=='enemy')
				{
					checkedEnemy='checked="checked"';
				}
				message+='</table><br><br>'+
					'Отображать обьекты: '+
					'<input type="radio" name="displayType" value="all" id="displayType_all" '+checkedAll+'><label for="displayType_all">Все &nbsp;</label> '+
					'<input type="radio" name="displayType" value="my" id="displayType_my" '+checkedMy+'><label for="displayType_my">Только мои &nbsp;</label> '+
					'<input type="radio" name="displayType" value="enemy" id="displayType_enemy" '+checkedEnemy+'><label for="displayType_enemy">Только чужие </label><br>'+
					'<input type="checkbox" value="onlyCosted" id="onlyCosted"><label for="onlyCosted">Только выставленые на продажу </label><br><br>'+
					'<center><input type="button" value="Применить" id="changeVisibleObjs"></center>'+
					'</form>';
				setMessage(message);
				for (var objPos in visibleObjsArray)
				{
					var objId=visibleObjsArray[objPos];
					$('#selectUnvisible input[value='+objId+']').filter('[name="visibleGeoObjs"]').attr('checked','checked');
				}
				$('#checkAllObjects').unbind('click');
				$('#checkAllObjects').bind
				(
					'click',
					function()
					{
						var arr=$('#selectUnvisible input:checkbox').filter('[name="visibleGeoObjs"]');
						arr.each
						(
							function(index)
							{
								this.checked=true;
							}
						);
					}
				);

				$('#uncheckAllObjects').unbind('click');
				$('#uncheckAllObjects').bind
				(
					'click',
					function()
					{
						$('#selectUnvisible input:checkbox').filter('[name="visibleGeoObjs"]').attr('checked',false);
					}
				);

				$('#changeVisibleObjs').unbind('click');
				$('#changeVisibleObjs').bind
				(
					'click',
					function()
					{
						var arr=$('#selectUnvisible input:checkbox').filter(':checked').filter('[name="visibleGeoObjs"]');
						visibleObjsArray=new Array();
						arr.each
						(
							function(index)
							{
								visibleObjsArray.push($(this).val());
							}
						);
						visibleType=$('#selectUnvisible input:radio').filter(':checked').val();
						if ($('#selectUnvisible #onlyCosted').is(':checked'))
						{
							visibleOnlyCosted=1;
						}
							else
							{
								visibleOnlyCosted=0;
							}
						getInfoOfAreaObjects();
						closeMessage();
					}
				);
			}
		);
		myMap.controls.add(settings, {top: 5, right: 5});

		newBounds = myMap.getBounds();
		zooom=myMap.getZoom();
		myMap.events.add
		(
			'boundschange',
			function (e)
			{
				clearStaticCircle();
				getInfoOfAreaObjects();
			}
		);

		myMap.events.add
		(
			'click',
			function(e)
			{
				getInfOnMapClick(e.get('coordPosition'),openBallonForContent);
		});

		getInfoOfAreaObjects();
		if (document.location.hash=='#map/toPoint' || document.location.hash=='map/toPoint')
		{
			document.location.hash='map';
		}
	}

  function openBallonForContent(pos,text)
	{
		balloon=myMap.balloon.open
		(
			pos,
			{
				content: text.toString()
      }
    );
		balloon.events.add
		(
			'close',
			function (event)
			{
				clearStaticCircle();
				clearClickStaticCircle();
			}
		);

		if (!$('#YMapsID .baloonInfoContent').hasClass('fullShow'))
		{
			$('#YMapsID .baloonInfoContent').css('height',$('#YMapsID .baloonInfoContent').height());
		}
	}

	function goToMapPoint(coords,zoom)
	{
		var coordsArr=coords.split(',');

		startPos=[parseFloat(coordsArr[0]),parseFloat(coordsArr[1])];
		startZoom=parseInt(zoom);

		document.location.hash='map/toPoint';
		updateNowPage();
	}

	function map_init()
	{
		$('#mainPageContent').css('opacity','1');
		buyOnOneClick=false;
		visibleObjsArray=new Array();
		allMapObjsArray=new Array();
		var objectsList=$('#mapsBuildingsTypes').html().split(';');
		for(var objStr in objectsList)
		{
			var objInfo=objectsList[objStr].split(':');
			var obj={ids:objInfo[0],name:objInfo[1],img:objInfo[2]};
			allMapObjsArray.push(obj);
			visibleObjsArray.push(obj.ids);
		}
    ymaps.ready(mapStart);
		//if (!isMapAlredyInited)
		{
			isMapAlredyInited=true;

			$('#YMapsID').undelegate('.buyGeoObjectButton');
			$('#YMapsID').delegate
			(
				'.buyGeoObjectButton',
				'click',
				function()
				{
					$.post
					(
						'ajax/buyObjectByPoint.php',
						{
							id: $(this).attr('ids')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
								return ;
							}
							getInfoOfAreaObjects();
							if (balloon)
							{
								balloon.close();
							}
						},
						'json'
					);
				}
			);

			$('#YMapsID').undelegate('.geoObjectInfo');
			$('#YMapsID').delegate
			(
				'.geoObjectInfo',
				'click',
				function()
				{
					var _this=$(this);
					if (_this.hasClass('nonCollapsed') || $('#YMapsID .baloonInfoContent').hasClass('fullShow'))
					{
						_this=null;
						delete _this;
						return ;
					}

					var isOpen=false;
					if (!_this.find('.dopInfo').is(':hidden'))
						isOpen=true;

					$('#YMapsID .geoObjectInfo .dopInfo').slideUp
					(
						500
					);

					if (!isOpen)
						_this.find('.dopInfo').slideDown(500);
				}
			);

			$('#YMapsID').undelegate('.recalcObject','click');
			$('#YMapsID').delegate
			(//пересчёт
				'.recalcObject',
				'click',
				function()
				{
					recalcHouse([$(this).attr('ids')]);
					if (balloon)
					{
						balloon.close();
					}
				}
			);

			$('#YMapsID').undelegate('.buildedOnObject div.button');
			$('#YMapsID').delegate
			(
				'.buildedOnObject div.button',
				'click',
				function()
				{
					$.post
					(
						'ajax/buildOnObject.php',
						{
							obj:[$('#YMapsID .geoObjectInfo').attr('objid')].toString(),
							type:$(this).attr('typeid')
						},
						function(data)
						{
							if (data.result!='ok')
							{
								errorHanding(data.result);
							}
							getInfoOfAreaObjects();
							if (balloon)
							{
								balloon.close();
							}
						},
						'json'
					);
				}
			);

			$('#YMapsID').undelegate('.spoilerSwitcher abbr');
			$('#YMapsID').delegate
			(
				'.spoilerSwitcher abbr',
				'click',
				function()
				{
					var content=$($(this).parent('.spoilerSwitcher').next('.spoilerContent')[0]);
					if (!content.is(':visible'))
					{
						$('#YMapsID .spoilerContent').slideUp(300);
					}
					content.slideToggle(300);
				}
			);

			$('#YMapsID').undelegate('.impObject','click');
			$('#YMapsID').delegate
			(//улучшение
			 '.impObject',
			 'click',
				function()
				{
					levelUpHouse([$(this).attr('ids')]);
					if (balloon)
					{
						balloon.close();
					}
				}
			);

			$('#YMapsID').undelegate('.rentObject','click');
			$('#YMapsID').delegate
			(//аренда
				'.rentObject',
				'click',
				function()
				{
					rentHouse([$(this).attr('ids')]);
					if (balloon)
					{
						balloon.close();
					}
				}
			);

			$('#YMapsID').undelegate('.sellObject','click');
			$('#YMapsID').delegate
			(//продажа
				'.sellObject',
				'click',
				function()
				{
					saleHouse($(this).attr('ids'));
					if (balloon)
					{
						balloon.close();
					}
				}
			);

			$('#YMapsID').undelegate('.destroyObject','click');
			$('#YMapsID').delegate
			(//снос
				'.destroyObject',
				'click',
				function()
				{
					destroyHouse([$(this).attr('ids')]);
					if (balloon)
					{
						balloon.close();
					}
				}
			);

			$('#YMapsID').undelegate('.canselSellBuilding','click');
			$('#YMapsID').delegate
			(//убрать с продажи
				'.canselSellBuilding',
				'click',
				function()
				{
					cancelSellHouse([$(this).attr('ids')]);
					if (balloon)
					{
						balloon.close();
					}
				}
			);

			$('#YMapsID').undelegate('.speedUpAction','click');
			$('#YMapsID').delegate
			(//ускорение
				'.speedUpAction',
				'click',
				function()
				{
					speedUpHouse([$(this).attr('ids')]);
					if (balloon)
					{
						balloon.close();
					}
				}
			);

			$('#YMapsID').undelegate('.auctionActionBid','click');
			$('#YMapsID').delegate
			(//ставка на аукцион
				'.auctionActionBid',
				'click',
				function()
				{
					var idAuc=$(this).attr('ids');
					var addr=$(this).attr('addr');
					var nowBid=parseFloat($(this).attr('bid'));
					var type=$(this).attr('typename');
					auctionBid(idAuc,type,addr,nowBid);
					if (balloon)
					{
						balloon.close();
					}
				}
			);
		}
	}

	var geoSearchedObjects=new Array();

	function getInfOnMapClick(coords, callback)
	{
		geoSearchedObjects=new Array();

		var geoSearch=ymaps.geocode(coords,{boundedBy:myMap.getBounds(),strictBounds:buyOnOneClick});
		geoSearch.then
		(
			function (res)
			{
				var i=0;
				var nowGeoObject;
				while(nowGeoObject=res.geoObjects.get(i))
				{
					i++;
					var props=nowGeoObject.properties;
					var metaData=props.get('metaDataProperty').GeocoderMetaData;
					var tmpObj=
						{
							kind:metaData.kind,
							address:props.get('text'),
							coords:nowGeoObject.geometry.getCoordinates(),
							country:null,
							administrative:null,
							subadministrative:null,
							locality:null,
							street:null,
							number:null
						};
					switch(metaData.kind)
					{
						case 'house':
							{
								var addr=metaData.AddressDetails;
								tmpObj.country=addr.Country.CountryName;
								if (addr.Country.AdministrativeArea)
								{
									tmpObj.administrative=addr.Country.AdministrativeArea.AdministrativeAreaName;
									if (addr.Country.AdministrativeArea.SubAdministrativeArea)
									{
										tmpObj.subadministrative=addr.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName;
										if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality)
										{
											tmpObj.locality=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
											if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare)
											{
												tmpObj.street=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
												if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.Premise)
												{
													tmpObj.number=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.Premise.PremiseNumber;
												}
											}
											if (!tmpObj.street && addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality)
											{
												if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare)
												{
													tmpObj.street=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName;
													if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare.Premise)
													{
														tmpObj.number=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare.Premise.PremiseNumber;
													}
												}
											}
										}
									}
									if (addr.Country.AdministrativeArea.Locality)
									{
										tmpObj.locality=addr.Country.AdministrativeArea.Locality.LocalityName;
										if (addr.Country.AdministrativeArea.Locality.Thoroughfare)
										{
											tmpObj.street=addr.Country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
											if (addr.Country.AdministrativeArea.Locality.Thoroughfare.Premise)
											{
												tmpObj.number=addr.Country.AdministrativeArea.Locality.Thoroughfare.Premise.PremiseNumber;
											}
										}
										if (!tmpObj.street && addr.Country.AdministrativeArea.Locality.DependentLocality)
										{
											if (addr.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare)
											{
												tmpObj.street=addr.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName;
												if (addr.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.Premise)
												{
													tmpObj.number=addr.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.Premise.PremiseNumber;
												}
											}
										}
									}
								}
								break;
							}
							case 'street':
							{
								var addr=metaData.AddressDetails;
								tmpObj.country=addr.Country.CountryName;
								if (addr.Country.AdministrativeArea)
								{
									tmpObj.administrative=addr.Country.AdministrativeArea.AdministrativeAreaName;
									if (addr.Country.AdministrativeArea.SubAdministrativeArea)
									{
										tmpObj.subadministrative=addr.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName;
										if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality)
										{
											tmpObj.locality=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
											if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare)
											{
												tmpObj.street=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
											}
											if (!tmpObj.street && addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality)
											{
												if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare)
												{
													tmpObj.street=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName;
												}
											}
										}
									}
									if (addr.Country.AdministrativeArea.Locality)
									{
										tmpObj.locality=addr.Country.AdministrativeArea.Locality.LocalityName;
										if (addr.Country.AdministrativeArea.Locality.Thoroughfare)
										{
											tmpObj.street=addr.Country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
										}
										if (!tmpObj.street && addr.Country.AdministrativeArea.Locality.DependentLocality)
										{
											if (addr.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare)
											{
													tmpObj.street=addr.Country.AdministrativeArea.Locality.DependentLocality.Thoroughfare.ThoroughfareName;
											}
										}
									}
								}
								break;
							}
							case 'locality':
							{
								var addr=metaData.AddressDetails;
								tmpObj.country=addr.Country.CountryName;
								if (addr.Country.AdministrativeArea)
								{
									tmpObj.administrative=addr.Country.AdministrativeArea.AdministrativeAreaName;
									if (addr.Country.AdministrativeArea.SubAdministrativeArea)
									{
										tmpObj.subadministrative=addr.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName;
										if (addr.Country.AdministrativeArea.SubAdministrativeArea.Locality)
										{
											tmpObj.locality=addr.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
										}
									}
									if (addr.Country.AdministrativeArea.Locality)
									{
										tmpObj.locality=addr.Country.AdministrativeArea.Locality.LocalityName;
									}
								}
								break;
							}

							case 'area':
							{
								var addr=metaData.AddressDetails;
								tmpObj.country=addr.Country.CountryName;
								if (addr.Country.AdministrativeArea)
								{
									tmpObj.administrative=addr.Country.AdministrativeArea.AdministrativeAreaName;
									if (addr.Country.AdministrativeArea.SubAdministrativeArea)
									{
										tmpObj.subadministrative=addr.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName;
									}
								}
								break;
							}
							case 'province':
							{
								var addr=metaData.AddressDetails;
								tmpObj.country=addr.Country.CountryName;
								if (addr.Country.AdministrativeArea)
								{
									tmpObj.administrative=addr.Country.AdministrativeArea.AdministrativeAreaName;
									if (addr.Country.AdministrativeArea.SubAdministrativeArea)
									{
										tmpObj.subadministrative=addr.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName;
									}
								}
								break;
							}
							case 'country':
							{
								var addr=metaData.AddressDetails;
								tmpObj.country=addr.Country.CountryName;
								break;
							}
							default: break;
					}

					geoSearchedObjects.push(tmpObj);
				}

				$.post
				(
					'ajax/getInfoOfPoint.php',
					{
						objsList: JSON.stringify(geoSearchedObjects),
						buyOnClick:buyOnOneClick
					},
					function(data)
					{
						if (data.result!='ok')
						{
							errorHanding(data.result);
							return ;
						}
						if (balloon)
						{
							balloon.close();
						}
						if (!buyOnOneClick)
						{
							callback(coords,data.content);
						}
					},
					"json"
				);
			},
			function (err)
			{
				alert('Ошибка: картографический сервис вернул ошибку в ответ на попытку поиска обьекта');
			}
		);
	}


	function getInfoOfAreaObjects()
	{
		$.post
		(
			'ajax/getInfoOfAreaObjects.php',
			{
				area: myMap.getBounds(),
				zoom: myMap.getZoom(),
				center: myMap.getCenter(),
				visibleObjs: getVisibleObjects(),
				types:visibleType,
				onlyCosted:visibleOnlyCosted
			},
			function(data)
			{
				if (data.result!='ok')
				{
					errorHanding(data.result);
					return ;
				}

				myObjectsManagerOnScreen.removeAll();
				var geoObject;
				var len=data.objsList.length;
				for (var i=0;i<len;i++)
				{
					geoObject=data.objsList[i];

					var dom=new ymaps.Placemark
					(
						[parseFloat(geoObject.pos1), parseFloat(geoObject.pos2)],
						{
							objIds:geoObject.id,
							isAuc:geoObject.isAuction,
							radius:geoObject.radius,
							objCircle:null
						},
						{
							iconImageHref: geoObject.img,
							iconImageSize: [16,16],
							iconImageOffset: [-8,-8]
						}
					);
					dom.events.add
					(
						'click',
						function (event)
						{
							var objId=event.get('target').properties.get('objIds');
							var isAuction=event.get('target').properties.get('isAuc');
							var coords=event.get('coords');
							$.post
							(
								'ajax/getInfoOfObject.php',
								{
									ids: objId,
									isAuc: isAuction
								},
								function(data)
								{
									clearClickStaticCircle();
									if (data.result!='ok')
									{
										errorHanding(data.result);
										return ;
									}
									if (data.radius!=null)
									{
										data.coord[0]=parseFloat(data.coord[0]);
										data.coord[1]=parseFloat(data.coord[1]);
										myClickCircle=new ymaps.Circle
										(
											[
												data.coord,
												data.radius
											],
											{},
											{
													fillColor: "#DB709377",
													strokeColor: "#990066",
													strokeOpacity: 0.7,
													strokeWidth: 2
											}
										);

										myMap.geoObjects.add(myClickCircle);
									}
									openBallonForContent(coords,data.content);

									$('#YMapsID .buyHouse').unbind('click');
									$('#YMapsID .buyHouse').bind
									(
										'click',
										function()
										{
											$.post
											(
												'ajax/confirmBuyHouse.php',
												{
													ids:$(this).attr('ids')
												},
												function (data)
												{
													if (data.result!='ok')
													{
														errorHanding(data.result);
														return ;
													}
													setMessage(data.content);
													$('#confirmedByHouse').unbind('click');
													$('#confirmedByHouse').bind
													(
														'click',
														function ()
														{
															$.post
															(
																'ajax/buyHouseOnExchange.php',
																{
																	ids:$(this).attr('ids')
																},
																function(data)
																{
																	if (data.result!='ok')
																	{
																		errorHanding(data.result);
																		return ;
																	}
																	closeMessage();
																	if (balloon)
																	{
																		balloon.close();
																	}
																},
																'json'
															);
														}
													);
												},
												'json'
											);
										}
									);
								},
								"json"
							);
						}
					);
					if (geoObject.radius!=null)
					{
						dom.events.add
						(
							'mouseenter',
							function (event)
							{
								var target=event.get('target');
								clearStaticCircle();
								myCircle=new ymaps.Circle
								(
									[
										target.geometry.getCoordinates(),
										target.properties.get('radius')
									],
									{},
									{
											fillColor: "#DB709377",
											strokeColor: "#990066",
											strokeOpacity: 0.8,
											strokeWidth: 3
									}
								);

								myMap.geoObjects.add(myCircle);
							}
						);
						dom.events.add
						(
							'mouseleave',
							function (event)
							{
								clearStaticCircle();
							}
						);
					}
					myObjectsManagerOnScreen.add(dom);
				}
			},
			"json"
		);
	}

function getVisibleObjects()
{
	return visibleObjsArray;
}

function clearStaticCircle()
{
	if (myCircle!=null)
	{
		myMap.geoObjects.remove(myCircle);
		myCircle=null;
	}
}

function clearClickStaticCircle()
{
	if (myClickCircle!=null)
	{
		myMap.geoObjects.remove(myClickCircle);
		myClickCircle=null;
	}
}

function closeMap()
{
	if (myMap)
	{
		myMap.destroy();
		myMap=null;
		$('#mainPageContent').css('opacity','0.9');
	}
}
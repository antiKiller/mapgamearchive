var NOW_PAGE_LOADED=false;

function loadPage(pageName,pageParam)
{
	if (!pageParam)
	{
		pageParam=false;
	}
	$.post
	(
		'ajax/getPage.php',
		{
			name:'content_'+pageName,
			param:pageParam
		},
		function(data)
		{
			$('#mainPageContent').html(data);
			$('#mainMenu a').removeClass('selected');
			$('#mainMenu a[href$=\'#'+pageName+'\']').addClass('selected');
			initComments();
			if (!NOW_PAGE_LOADED)
			{
//				if (window.location.pathname!='/' && window.location.pathname!='/index.php' && window.location.pathname!='')
				{
					var anchor=pageName+'_init';
					var evalStr=' if ('+anchor+') { '+anchor+'(); }';
					eval(evalStr);
				}
			}
			NOW_PAGE_LOADED=true;
		}
	);
	$('#mainMenu a[href$=\'#'+pageName+'\']').addClass('selected');
}

function detectNowPage(onlyReturn,withChat)
{
	if (!withChat)
	{
		withChat=false;
	}
	if (withChat && chatIsOpen)
	{
		return 'chat';
	}
	var anchor=document.location.hash;
	if (anchor)
	{
		anchor=anchor.substring(1);
		var param=false;
		var subAncor=anchor.split('/');
		if (subAncor.length==2)
		{
			anchor=subAncor[0];
			param=subAncor[1];
		}
		if (!onlyReturn)
		{
			loadPage(anchor,param);
		}
	}
		else
		{
			anchor='index';
			var page=document.location.search.toString();
			if (page.length>0)
			{
				anchor=page.substring(6);
			}
			if (!onlyReturn)
				$('#mainMenu a[href$=\'#'+anchor+'\']').addClass('selected');
		}
	return anchor;
}

function setNavigation()
{
	$('#mainMenu a').each
	(
		function (i,obj)
		{
			var href=$(obj).attr('href').toString();
			href=href.replace(/\?page=/i,'#');
			$(obj).attr('href',href);
		}
	);

	var clickToHref=function()
	{
		var name=$(this).attr('href');
		var position=name.indexOf('#')+1;
		name=name.substring(position);
		NOW_PAGE_LOADED=false;
		loadPage(name);
		return true;
	}
	$('#mainMenu a').on
	(
	  'click',
		clickToHref
	);

	return detectNowPage();
}

function updateNowPage()
{
	NOW_PAGE_LOADED=false;
	detectNowPage();
}
<?
$game_in_construction=false;
if ($game_in_construction)
{
	echo '<h2>Идут восстановительные работы. Через несколько часов всё будет.</h2>
		<div style="text-align:center;"><img src="images/under_construction.png"></div>';
	exit();
}

include 'globalFuncs.php';

mysql_connect('localhost','USER','PASS') or die('Не удалось подключиться к базе данных.');
mysql_select_db('NAME') or die('Не удалось подключиться к базе данных.');
mysql_set_charset('utf8');

define('ONE_HOUR', 3600);
define('AUC_BID_DEPRECIATION', 0.8);
define('AUC_NEW_BID_PERSENT', 0.1);
define('AUC_BID_RETURN_BROKEN', 0.6);
define('AUC_MIN_DURATION', 8);

define('RENT_PERSENT_STREET', 0.25);
define('RENT_PERSENT_LOCALITY', 0.2);
define('RENT_PERSENT_AREA', 0.15);
define('RENT_PERSENT_REGION', 0.1);
define('RENT_PERSENT_COUNTRY', 0.05);

define('PROFIT_TAX', 0.2);

define('SPEED_UP_REMAINING', 0.1);
define('SPEED_UP_COST', 50);

define('MONTH_USER_PAYMENTS_LIMIT', 300);
define('ONE_INTERVAL_USER_PAYMENTS_DAYS', 5);
define('ONE_INTERVAL_USER_PAYMENTS_LIMIT', 40);

define('ONE_DAY_COST_MANAGER_FIRST', 340);
define('ONE_DAY_COST_MANAGER_SECOND', 310);
define('ONE_DAY_COST_MANAGER_THIRD', 260);
define('ONE_DAY_COST_MANAGER_FOURTH', 210);
define('ONE_DAY_COST_MANAGER_FIFTH', 160);
define('ONE_DAY_COST_MANAGER_SIXTH', 100);

define('COST_MANAGER_DISCOUNT_FIRST', 100);
define('COST_TAXMANAGER_DISCOUNT_FIRST', 50);
define('COST_MANAGER_DISCOUNT_SECOND', 1000);
define('COST_TAXMANAGER_DISCOUNT_SECOND', 500);

define('ONE_DAY_COST_TAXMANAGER_FIRST', 140);
define('ONE_DAY_COST_TAXMANAGER_SECOND', 100);
define('ONE_DAY_COST_TAXMANAGER_THIRD', 80);
define('ONE_DAY_COST_TAXMANAGER_FOURTH', 60);
define('ONE_DAY_COST_TAXMANAGER_FIFTH', 40);
define('ONE_DAY_COST_TAXMANAGER_SIXTH', 18);


define('IS_NEWBIE_DAYS', 25);
define('IS_NEWBIE_TIME', 86400*IS_NEWBIE_DAYS);//3600*24*IS_NEWBIE_DAYS

define('RENT_PROFIT_BASIC_FIRST', 3.5);
define('RENT_PROFIT_BASIC_SECOND', 3);
define('RENT_PROFIT_BASIC_THIRD', 2);
define('RENT_PROFIT_BASIC_FOURTH', 1.15);
define('RENT_PROFIT_BASIC_FIFTH', 0.8);
define('RENT_PROFIT_BASIC_SIXTH', 0.5);

define('RENT_PROFIT_NEWBIE_FIRST', 5);
define('RENT_PROFIT_NEWBIE_SECOND', 4);
define('RENT_PROFIT_NEWBIE_THIRD', 3);
define('RENT_PROFIT_NEWBIE_FOURTH', 2.5);
define('RENT_PROFIT_NEWBIE_FIFTH', 2);
define('RENT_PROFIT_NEWBIE_SIXTH', 1);
?>
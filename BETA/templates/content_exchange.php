<?
$getCounts="SELECT
							(SELECT count(`id`) FROM `exchange` WHERE `credits`>0) countExcange,
							(SELECT count(`id`) FROM `house` WHERE `isCosted`=1) countBuildings";
$restGetCount=mysql_query($getCounts) or die(handleError('Не удалось получить информацию о лотах на бирже.',__FILE__,false,$getCounts,$nowUserInfo['id']));
$counts=mysql_fetch_assoc($restGetCount);
$exchange=$counts['countExcange'];
$buildings=$counts['countBuildings'];
?>


<div id="exchangeBlock">
	<div id="exchangeTabs">
		<abbr class="selected" name="credits" title="Здесь можно купить или продать свои кредиты">Биржа кредитов <b id="exchangeLotsCount">(<?echo $exchange;?>)</b></abbr>
		<abbr name="buildings" title="Здесь можно продать или купить свои здания">Биржа зданий <b id="buildingLotsCount">(<?echo $buildings;?>)</b></abbr>
	</div>
	<div class="gameTextBlock">
		<b>Правила:</b>
		<ul>
			<li>При размещении заявки на покупку кредитов с вас единовременно снимается 5% от указанного вами курса.</li>
			<li>В случае отмены заявки на покупку или продажу кредитов вам возвращается только 75% от сделанной вами ставки.</li>
			<li>В случае, если ваша заявка не была исполнена в течении 3-х месяцев, то она автоматически отменяется, но при этом вам возвращается только 20% от оставшейся суммы.</li>
			<li>При покупке строения с вас снимается дополнительный налог в 13%, помимо указанной стоимости строения продавцом.</li>
		</ul>
	</div>

	<div id="creditsExchangeActions">
		<input type="button" id="newCreditsBuy" value="Купить кредиты на бирже">
		<input type="button" id="buyCreditsButton" value="Купить кредиты за рубли">
		<input type="button" id="newCreditsSell" value="Продать кредиты на бирже">
	</div>
	<div id="exchangeContent">
		<div id="creditsExchangeBlock">
			<table id="buyerBlock"></table>
			<table id="sellerBlock"></table>

			<div id="buyerBlockPager"></div>
			<div id="sellerBlockPager"></div>
		</div>

		<div id="buildsExchangeBlock">
			<table id="buildsBlock"></table>
			<div id="buildsBlockPager"></div>
		</div>

	</div>
</div>

<?
$getTypes="SELECT DISTINCT `name` FROM `buildingsUsers` WHERE `isCosted`=1 ORDER BY `order`";
$res=mysql_query($getTypes);
$str=':Все;';
while($typeLog=mysql_fetch_assoc($res))
{
	$str.=$typeLog['name'].':'.$typeLog['name'].';';
}
$str=trim($str,';');
?>
<span class="hidden" id="listOfCostedHouseTypes"><? echo $str;?></span>
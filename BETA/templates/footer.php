<div id="copyrightBlock">© 2013-<?echo date('Y');?> "Война олигархов"</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter22649320 = new Ya.Metrika({id:22649320,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    trackHash:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/22649320" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

		<div id="messageError">
			<img src="images/close.png" class="closeErrorButton">
			<h1>Ошибка!</h1>
			<div class="content"></div>
		</div>

		<div id="gameChat">
			<div id="chatIndicator" class="closed"></div>
			<div id="chatWindows">
				<div id="chatUsers"></div>

				<div id="chatMessages"></div>

				<div id="chatSendMessage">
					<div id="rightChatEnterBlock">
						<input type="button" id="sendChatMessageButton" value="Отправить"><br>или нажмите ctrl+enter для отправки сообщения
					</div>
					<textarea id="chatMessageText"></textarea>
				</div>
			</div>
		</div>
	</body>
</html>
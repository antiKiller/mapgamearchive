<div id="actionsOfAllHouses" class="gameTextBlock">
	<div class="actions">
		Действия над строениями:
		<img class="managObject" title="Здания к управляющему" id="managAllHouses" src="images/manager.png">
		<img class="destroyObject" title="Снести здания" id="destroyAllHouses" src="images/destroy.png">
		<img class="buildObject" title="Построить на зданиях" id="buildAllHouses" src="images/build.png">
		<img class="impObject" title="Улучшить здания" id="impAllHouses" src="images/imp.png">
		<img class="rentObject" title="Сдать здания в аренду" id="rentAllHouses" src="images/rent.png">
		<br>
		Применить: <input type="radio" name="modeOfAction" checked="checked" value="forAll" id="modeOfAction_forAll"><label for="modeOfAction_forAll"> ко всем&nbsp;</label>
		<input type="radio" name="modeOfAction" value="onlyChecked" id="modeOfAction_onlyChecked"><label for="modeOfAction_onlyChecked"> к выделенным&nbsp;</label>
		<input type="radio" name="modeOfAction" value="exeptChecked" id="modeOfAction_exeptChecked"><label for="modeOfAction_exeptChecked"> к не выделенным&nbsp;</label>
	</div>
	<div class="title">Управляющий: <span id="userHouseManagerText"></span><br>Подоходный налог (0.2% за каждые 100 зданий): <span id="userHouseRentTask">0</span>%
		                 Бухгалтер:<span id="userTaxOpitmizator"></span></div>
</div>

<table id="housesBlock"></table>
<div id="housesBlockPager"></div>

<?
$getTypes="SELECT DISTINCT `name` FROM `buildingsUsers` WHERE `user`='".$nowUserInfo['id']."' ORDER BY `order`";
$res=mysql_query($getTypes) or die(handleError('Не удалось получить список зданий игрока.',__FILE__,false,$getTypes,$nowUserInfo['id']));
$str=':Все;';
while($typeLog=mysql_fetch_assoc($res))
{
	$str.=$typeLog['name'].':'.$typeLog['name'].';';
}
$str=trim($str,';');
?>
<span class="hidden" id="listOfHouseTypes"><? echo $str;?></span>
<span class="hidden" id="countHousesOnPage"><? echo $nowUserInfo['rowOnHousesPage'];?></span>
<a href="#library/top" class="toTopContainer">&uarr; НАВЕРХ</a>

<div id="library/top">
	<ul>
		<li><a href="#library/rules">Правила</a></li>
		<li><a href="#library/FAQ">F.A.Q.</a></li>
		<li>
			<a href="#library/buildings">Строения</a>
			<ul>
				<li><a href="#library/buildings_levelUp">Строительство и улучшения</a></li>
				<li><a href="#library/buildings_calc">Калькулятор доходности</a></li>
			</ul>
		</li>
		<li>
			<a href="#library/auctions">Аукционы</a>
			<ul>
				<li><a href="#library/auctions_calc">Калькулятор доходности</a></li>
			</ul>
		</li>
		<li><a href="#library/credits">Биржа и кредиты</a></li>
		<li><a href="#library/payments">Выплаты игрокам</a></li>
	</ul>
</div>

<h1 id="library/rules">Правила</h1>
<ol>
	<li>В игре запрещаются какие-либо оскорбления или нецензурные выражения (в нике игрока, в сообщениях чата и прочее). За нарушение - бан от нескольких часов до нескольких дней.</li>
	<li>В игре разрешено иметь несколько аккаунтов.</li>
	<li>Запрещено использовать какие-либо средства автоматизации действий, не предусмотреные игрой. Например, использовать ботов, специальные скрипты, другое ПО, которые совершают или позволяют совершать какие-либо действия в игре в обход установленных игрой правил и ограничений. За нарушение бан от нескольких дней, до вечного бана. В некоторых случаях возможно частичное или полное обнуление аккаунта нарушителя. За сообщение о подобных скриптах, ссылки на загрузку таких скриптов и представление их исходных кодов, игроки могут получить вознаграждение. Подробнее о вознаграждениях <a href="https://vk.com/topic-54660744_29384573" target="_blank">в нашей группе Вконтакте</a>.</li>
	<li>Запрещено использовать какие-либо либо ошибки и уязвимости игры (эксплойты, хаки, читерство). В случае обнаружения ошибки или возможности эксплуатации необходимо сообщить руководству игры об этом ошибке. За тихую эксплуатацию ошибок и уязвимостей игры возможен бан от нескольких часов, до нескольких суток. В некоторых случаях возможно частичное или полное обнуление аккаунта нарушителя. За сообщение о ошибках и уязвимостях игроки могут получить вознаграждение. Подробнее о вознаграждениях <a href="https://vk.com/topic-54660744_29384573" target="_blank">в нашей группе Вконтакте</a>.</li>
	<li>В игре запрещено публиковать свою или чужую рекламу сайтов или страниц сайтов, с описанием и ссылками на боты, хаки, эксплойны, вирусы и прочее, а так же сообщение о способах обхода существующих в игре правил и ограничений. Это влечёт за собой бан от нескольких дней до вечного.</li>
</ol>

<h1 id="library/FAQ">F.A.Q.</h1>
<div>
	<b>Q</b>: Что нужно чтобы начать играть?<br>
	<b>A</b>: Ничего - достаточно иметь аккаунт в одном из сервисов: <a href="https://vk.com/" target="_blank">Вконтакте</a>, <a href="https://facebook.com/" target="_blank">Facebook</a>, <a href="http://odnoklassniki.ru/" target="_blank">Одноклассники</a>, <a href="http://my.mail.ru/" target="_blank">Мой Мир</a>, <a href="https://yandex.ru/" target="_blank">Яндекс</a>, <a href="https://google.com/" target="_blank">Google</a> или <a href="https://twitter.com/" target="_blank">Twitter</a>. При первом входе в игру вы будете автоматически зарегистрированы.
</div>
<br>
<div>
	<b>Q</b>: В чём суть игры, как играть?<br>
	<b>A</b>: Игроку, необходимо покупать на реальной карте покупать участки в собственность, при этом участки являются реально существующими домами. Затем на этих участках можно стоить различные дома, коммерческие и промышленные предприятия, получая доход от периодической сдачи этих строений в аренду. Так же доступны аукционы - вы можете приобрести улицы, город, район, область или даже целую страну. Доход с аукционов получается путём собирания налогов со сдачи в аренду (то есть улица, на которой нет ни одной принадлежащей игрокам постройке не будет приносить никакого дохода).
	<!--Подробнее вы можете посмотреть процесс игры и некоторые особенности игрового процесса в <a href="http://youtube.com/ссылка_на_записанное_видео" target="_blank">этом видео</a>.-->
</div>
<br>
<div>
	<b>Q</b>: Чьи это карты, ваши или нет?<br>
	<b>A</b>: Карты для игры предоставляются сервисом <a href="https://maps.yandex.ru" target="_blank">Яндекс.Карты</a>, руководство игры никоим образом не может влиять на то, что именно расположено на карте, какие именно города, улицы, дома доступны, а какие нет.
</div>
<br>
<div>
	<b>Q</b>: Я нашел на карте свой дом/улицу/город/прочее. Но не могу их купить, в чём дело?<br>
	<b>A</b>: Карты для игры предоставляются сервисом <a href="https://maps.yandex.ru" target="_blank">Яндекс.Карты</a>, и, в данном случае Яндекс не знает ничего об этом обьекте. К сожалению, такое возможно даже если на доме стоит номер, а на улице написано её название - то что вы видите на карте это лишь картинка, действительные данные о обьектах на карте не всегда в точности соответствуют изображению на экране. Ничего с этим поделать нельзя, возможно когда-то в будущем эти обьекты появятся на карте.
</div>
<br>
<div>
	<b>Q</b>: Играть можно только на этом сайте?<br>
	<b>A</b>: Играть можно через этот сайт или через приложение Вконтакте. По просьбам некоторых игроков и для их удобства, существуюет <a href="http://vk.com/app2331763">приложение игры во Вконтакте</a>. Однако, рекомендуется играть именно через сайт. В принципе разницы никакой нет, за исключением размеров окна - в приложении Вконтакте размеры окна непосредственно с игрой ограничены.
</div>
<br>
<div>
	<b>Q</b>: Могу ли я иметь несколько аккаунтов?<br>
	<b>A</b>: Да, в игре "Война олигархов" не запрещено одному игроку иметь несколько игровых аккаунтов.
</div>
<br>
<div>
	<b>Q</b>: Игра платная?<br>
	<b>A</b>: Нет, игра бесплатна. Но в игре можно приобрести специальную валюту - кредиты <img class="moneyIndicator" src="images/baks.png">, которые можно продать другим игрокам на бирже, потратить на более выгодные здания, нанять управляющего или бухгалтера.
</div>
<br>
<div>
	<b>Q</b>: Могу ли я как то заработать в этой игре?<br>
	<b>A</b>: Да, это возможно. В игре вы можете обменять игровую валюту на российские рубли и получить выплату на свой кошелёк в системе <a href="https://money.yandex.ru" target="_blank">Яндекс.Деньги</a>. Однако сообщаю, что есть серьезные ограничения по суммам и срокам  вывода средств. Подробнее <a href="#library/payments">смотрите в соответствующем разделе</a>.
</div>
<br>
<div>
	<b>Q</b>: Как мне обменять мои монеты на рубли и как получить рубли?<br>
	<b>A</b>: Для подробностей вам нужно кликнуть на свой балланс в верхнем правом углу. Если у вас более 50 монет, то вы увидите форму с подробными правилами заказа выплат в рублях.
</div>
<br>
<div>
	<b>Q</b>: Я вот хочу заказать выплату, но у меня нет Яндекс.кошелька, что делать?<br>
	<b>A</b>: Только завести его. В настоящий момент выплаты осуществляются только на яндекс.кошелек и других способов выплаты в обозримом будущем не планируется.
</div>
<br>
<div>
	<b>Q</b>: Что за фигня, почему у меня отрицательные балланс?<br>
	<b>A</b>: Скорее всего дело в аукционах. Аукционы каждые 3 дня начинаются заново и вы должны делать ставку не меньше номинала. То есть, если на момент окончания аукциона улицы у вас было 100 монет, аукцион начался заново и была сделана номинальная ставка (500 монет для улицы) - ваш балланс станет -400 монет. Этого никак не избежать. Нужно покупать только доходные аукционы (здания на которых приносят существенные доход). Или перекупить этот аукцион с другого своего аккаунта до лучших времен.
</div>
<br>
<div>
	<b>Q</b>: Я совсем ушел в минус и не могу оттуда выбраться, что делать?<br>
	<b>A</b>: При количестве монет менее 50 доступна процедура банкротства. Для этого надо кликнуть на свой балланс в правом верхнем углу - появится форма с возможностью оптимизации активов (за кредиты) или банкротства (фактически начать игру заново).
</div>
<br>
<div>
	<b>Q</b>: Я уезжаю в отпуск/к бабушке в деревню/улетаю на Марс на месяц. Что будет с моими домиками и аукционами?<br>
	<b>A</b>: Все дома, в случае если вы не заходили в игру более 2-х месяцев, выставляются на продажу автоматически. А аукционы, так и останутся вашими, пока их кто-нибудь не перекупит.
</div>
<br>
<div>
	<b>Q</b>: VIP здания улучшаются за кредиты?<br>
	<b>A</b>: Нет, за кредиты происходит только покупка, улучшение происходит за монеты.
</div>
<br>
<div>
	<b>Q</b>: VIP здания улучшаются за кредиты?<br>
	<b>A</b>: Нет, за кредиты происходит только покупка, улучшение происходит за монеты.
</div>
<br>
<div>
	<b>Q</b>: В игре присутствует конкуренция между одинаковыми предприятиями. На каком принципе она основана и можно ли ее допускать или она всегда убыточна?<br>
	<b>A</b>: Да, конкуренция присутствует. И она высчитывается как процент клиентов, обслуживающийся не только этим предприятием, но и другим. С такого клиента, который обслуживается ещё и другим аналогичным предприятием, берётся только половина от его дохода. То есть здание со 100% конкуренции будет приносит только половину от своего дохода.
</div>
<br>
<div>
	<b>Q</b>: Существует ли ограничение на количество клиентов у предприятий и социальных учреждений?<br>
	<b>A</b>: Нет, ограничений нет - сколько попадёт в радиус действия - столько клиентов и будет.
</div>
<br>
<div>
	<b>Q</b>: При большом количестве зданий снимается существенный налог. Как этот налог действует на аукционы?<br>
	<b>A</b>: Прямо на аукционы, он не действует. Только косвено. На аукцион начисляется процент от дохода за вычетом налога. То есть, если ваш доход 1000 монет, а налог 50%, то на аукцион будет начисляться доход с 500 монет, а не с 1000.
</div>
<br>
<div>
	<b>Q</b>: Я купил город (район, область и т.д.), на которых есть мои дома, но прибыли он мне не несет. Что делать?<br>
	<b>A</b>: Скорее всего это ошибка. Писать об этом в чате игры, в <a href="http://vk.com/topic-54660744_28302767" target="_blank">группе игры во вконтакте</a>.
</div>


<h1 id="library/buildings">Строения</h1>
<p>Основой игры является покупка участков на карте, строительство на этих участках определенных зданий и последующая сдача этих зданий в аренду. Чем меньше период сдачи в аренду, тем больше доход. Минимальный срок сдачи в аренду - 1 час, максимальный - 1 сутки.</p>
<p>В игре есть особое здание - строй-прощадка. Когда вы покупаете участок на карте - он становится строй-площадкой. Это здание не приносит никакого дохода, но на месте строй-прощадки можно строить другие здания.</p>
Здания бывают следующих видов:
<ol>
	<li><b>Жилые дома.</b> Дома с жителями, являющиеся основой для всех наиболее доходных зданий в игре. При этом сами, как правило, приносят небольшой доход. Различаются вместимостью жителей и доходом с одного жителя.</li>
	<li><b>Предприятия.</b> Этот тип здания получает доход, как правило, за счет жителей в жилых домах. Для работы некоторых предприятий зачастую необходимы другие предприятия. В частности для работы магазина, необходим склад, а склад не может работать без завода. Так же все предприятия имеют радиус своего действия.</li>
	<li><b>Социальные учреждения.</b> Эти здания не приносят дохода сами по себе, но они увеличивают доходность жилых домов на определеный процент. Действуют на жилые дома в определенном радиусе.</li>
</ol>
<p>Есть отдельная категория здания - vip здания. Эти здания стоят дороже, улучшаются дольше своих аналогов, но и являются гораздо более выгодными в плане доходности. К тому же с улучшением у этих зданий доходность растет чуть быстрее, чем затраты, но улучшение занимает больше времени.</p>
<p>Социальные учреждения хоть и дают бонусы всем жилым зданиям в радиусе, но эти бонусы не суммируются, а берётся наибольший. То есть, если в районе жилого дома есть 2 школы 2 и 3 уровней, и 2 больницы 4 и 5 уровней, то дом будет получать бонус только от школы 3 уровня и больницы 5 уровня - другие будут игнорироваться.</p>
<p><b>Описания зданий</b></p>
<?
$getBuildedObjects="SELECT * FROM `geoObjectsTypes` WHERE `isAuction`=0 ORDER BY `order`";
$resBuildedObjects=mysql_query($getBuildedObjects) or die(handleError('Не удалось получить список строений.',__FILE__,false,$getBuildedObjects));
$sendedHouse=false;
$sendedEnterprise=false;
$sendedSocail=false;
$optionsList='';
$content='<table id="helpObjsInfo">
	<tr>
		<th>Здание</th>
		<th>Стоимость</th>
		<th>Описание</th>
		<th>Жителей</th>
		<th>Доход с клиента или бонус к доходу</th>
		<th>Радиус</th>
		<th>Время строительства (мин)</th>
		<th>Свойства</th>
		<th>Требуется</th>
		<th>Клиенты</th>
	</tr>';
while($rowBuildedObjects=mysql_fetch_assoc($resBuildedObjects))
{
	if (!$sendedHouse && $rowBuildedObjects['isHouse'])
	{
		$sendedHouse=true;
		$content.='<tr><th colspan="10"><br>Жилые дома</th></tr>';
	}
	if (!$sendedEnterprise && $rowBuildedObjects['isEnterprise'])
	{
		$sendedEnterprise=true;
		$content.='<tr><th colspan="10"><br>Предприятия</th></tr>';
	}
	if (!$sendedSocail && $rowBuildedObjects['isSocial'])
	{
		$sendedSocail=true;
		$content.='<tr><th colspan="10"><br>Социальные учреждения</th></tr>';
	}

	$dopFileds='';

	if ($rowBuildedObjects['fixClients']>0)
	{
		$dopFileds.='<td>'.$rowBuildedObjects['fixClients'].'</td>';
	}
		else
		{
			$dopFileds.='<td>-</td>';
		}

	if(!$rowBuildedObjects['isSocial'])
	{
		$dopFileds.='<td>'.$rowBuildedObjects['basicDoxod'].' <img class="moneyIndicator" src="images/money.png">  (x'.$rowBuildedObjects['updateDoxod'].' каждый уровень)</td>';
	}
		elseif(!$rowBuildedObjects['isMap'])
		{
			$dopFileds.='<td>'.$rowBuildedObjects['basicBonus'].'%</td>';
		}
			else
			{
				$dopFileds.='<td>-</td>';
			}

	if ($rowBuildedObjects['radius'])
	{
		$dopFileds.='<td>'.$rowBuildedObjects['radius'].'</td>';
	}
		else
		{
			$dopFileds.='<td>-</td>';
		}

	if(!$rowBuildedObjects['isMap'])
	{
		$dopFileds.='<td>'.$rowBuildedObjects['basicTime'].'</td>';
	}
		else
		{
			$dopFileds.='<td>-</td>';
		}

	$info=[];


	if ($rowBuildedObjects['updateCost']<$rowBuildedObjects['updateDoxod'])
	{
		$info[]='<abbr class="green" title="При увеличении уровня доходность растёт быстрее расходов.">Сверхэффективное</abbr>';
	}

	if ($rowBuildedObjects['updateCost']>=1.25)
	{
		$info[]='<abbr class="red" title="Стоимость повышения уровня растёт очень быстро">Дорогое улучшение (x'.$rowBuildedObjects['updateCost'].' каждый уровень)</abbr>';
	}
		elseif ($rowBuildedObjects['updateCost']<=1.15)
		{
			$info[]='<abbr class="green" title="Стоимость повышения уровня растёт очень медленно">Дешевое улучшение (x'.$rowBuildedObjects['updateCost'].' каждый уровень)</abbr>';
		}
			else
			{
				$info[]='<abbr title="Стоимость повышения уровня растёт очень умеренно">Умеренная стоимость улучшения (x'.$rowBuildedObjects['updateCost'].' каждый уровень)</abbr>';
			}


	if ($rowBuildedObjects['updateTime']>1.1)
	{
		$info[]='<abbr class="red" title="При каждом повышении уровня здание будет очень долго улучшаться">Долгое улучшение (x'.$rowBuildedObjects['updateTime'].' каждый уровень)</abbr>';
	}
		elseif ($rowBuildedObjects['updateTime']<=1.05)
		{
			$info[]='<abbr class="green" title="Время улучшения с каждым уровнем растёт лишь немного">Быстрое улучшение (x'.$rowBuildedObjects['updateTime'].' каждый уровень)</abbr>';
		}
			else
			{
				$info[]='<abbr title="Время улучшения здания с каждым уровнем растёт не сильно">Умеренная скорость улучшения (x'.$rowBuildedObjects['updateTime'].' каждый уровень)</abbr>';
			}

	if($rowBuildedObjects['isSocial'])
	{
		if ($rowBuildedObjects['updateBonus']>=1.25)
		{
			$info[]='<abbr class="green" title="Бонус к доходу растёт хорошими темпами при увеличении уровня здания">Хороший бонус (x'.$rowBuildedObjects['updateBonus'].' каждый уровень)</abbr>';
		}
			elseif ($rowBuildedObjects['updateBonus']<=1.10)
			{
				$info[]='<abbr class="red" title="Бонус растёт низкими темпами при увеличении уровня здания">Низкий бонус (x'.$rowBuildedObjects['updateBonus'].' каждый уровень)</abbr>';
			}
				else
				{
					$info[]='<abbr title="Бонус растёт умеренными темпами при увеличении уровня здания">Средний бонус (x'.$rowBuildedObjects['updateBonus'].' каждый уровень)</abbr>';
				}
	}

	if ($rowBuildedObjects['isMap'])
	{
		$info=['-'];
	}

	$dopFileds.='<td>'.implode('<br>',$info).'</td>';

	$getInfoOfNeededBuilding="SELECT a.`name`,a.`isMap`,a.`img` FROM `geoObjectsTypes` a, `needsForBuildings` b WHERE b.`building`=".$rowBuildedObjects['id']." and a.`id`=b.`needBuilding`";
	$resGetInfoOfNeededBuilding=mysql_query($getInfoOfNeededBuilding);
	$infoOfNeededBuilding=mysql_fetch_assoc($resGetInfoOfNeededBuilding);

	if ($infoOfNeededBuilding)
	{
		$dopFileds.='<td class="nowrap"><img class="geoObjIcon" src="images/'.$infoOfNeededBuilding['img'].'.png"> '.$infoOfNeededBuilding['name'].'</td>';
	}
		else
		{
			$dopFileds.='<td>-</td>';
		}

	$clientsList=[];
	$getClientsInfo="SELECT a.`name`,a.`img`, b.`withHisClients` FROM `geoObjectsTypes` a, `clientsForBuildngs` b WHERE a.`id`=b.`clientObj` and b.`parentObj`=".$rowBuildedObjects['id'];
	$resGetClientsInfo=mysql_query($getClientsInfo);
	while($clients=mysql_fetch_assoc($resGetClientsInfo))
	{
		$str='<img class="geoObjIcon" src="images/'.$clients['img'].'.png"> '.$clients['name'];
		if ($clients['withHisClients'])
		{
			$str='<abbr class="withHisClients" title="Вместе со всеми его клиентами">'.$str.'</abbr>';
		}
		$clientsList[]=$str;
	}

	if ($clientsList)
	{
		$dopFileds.='<td>'.implode('<br>',$clientsList).'</td>';
	}
		else
		{
			$dopFileds.='<td>-</td>';
		}

	$cost=number_format($rowBuildedObjects['basicCost'],0,'.',' ').' <img class="moneyIndicator" src="images/money.png">';
	if ($rowBuildedObjects['creditsCost']>0)
	{
		$cost.='<br>'.$rowBuildedObjects['creditsCost'].' <img class="moneyIndicator" src="images/baks.png">';
	}
	$content.='
		<tr>
			<td class="nowrap"><img src="images/'.$rowBuildedObjects['img'].'.png" class="objectTypeImg"> '.$rowBuildedObjects['name'].'</td>
			<td>'.$cost.'</td>
			<td>'.$rowBuildedObjects['description'].'</td>
					'.$dopFileds.'
		</tr>';

	if ($rowBuildedObjects['isMap']==0)
	{
		$optionsList.='<option value="'.$rowBuildedObjects['id'].'">'.$rowBuildedObjects['name'].'</option>';
	}
}
$content.='</table>';
echo $content;
?>

<h3 id="library/buildings_levelUp">Строительство и улучшения</h3>
<ol>
	<li>За каждые 100 зданий у игрока с дохода снимается сумма, равная <?echo PROFIT_TAX;?>%. То есть, до 100 зданий, налога не будет, при 100-199 зданиях - налог будет <?echo PROFIT_TAX;?>%, при 200-299 - налог <?echo PROFIT_TAX*2;?>%</li>
	<li>Все здания можно улучшать, с повышением уровня увеличивается доходность зданий, но вместе с тем повышение уровня начинает стоить дороже и происходит гораздо дольше. Причем процент, на который увеличивается доходность, меньше процента, на которые увеличивается стоимость и время улучшения.</li>
	<li>Здания находящиеся на улучшении - теряют свою клиентуру и свои связи, во время улучшения не видны другим зданиям, не обслуживают другие здания и формально не существуют. То есть, если есть некий завод, обсуживающий склады, уйдет в улучшение, то склады не будут видеть этот завод и не будут приносить доход.</li>
	<li>После строительства или улучшения зданиям требуется время, чтобы найти клиентов, обрасти партнёрскими связями и начать полноценно работать. Этот процесс может занимать от пары минут до нескольких часов.</li>
	<li>Никаких искусственных ограничений на уровень зданий - нет. Повышать уровень зданий можно до бесконечности.</li>
	<li>Все жилые здания получают +1 жителя за каждые 10 уровней.</li>
	<li>Все жилые здания получают +1 жителя за каждые 20 уровней обслуживающего их социального учреждения.</li>
	<li>Любое действие над строением (постройка, улучшение, аренда) может быть ускорено на <?echo 100-SPEED_UP_REMAINING*100;?>% за <?echo SPEED_UP_COST;?> кредитов.</li>
	<li>Если вы не заходили в игру более 2-х месяцев, то ваши дома выставляются на продажу, по цене на 10% ниже номинальной (то есть на 10% ниже чем суммарная цена постройки и улучшения до текущего уровня).</li>
</ol>

<h3 id="library/buildings_calc">Калькулятор доходности</h3>
Выберите тип здания: <select id="selectTypeCalcedHouse"><option value="0">Не выбрано</option><?echo $optionsList;?></select>
<div id="calcedHouseForm"></div>
<div id="calcedHouseResult"></div>



<h1 id="library/auctions">Аукционы</h1>
<p>Со строительства зданий хорошо начинать, постройки обеспечивают стабильный полноценный доход. Но в игре так же существуют аукционы - сущности имеющие менее стабильный, но и, зачастую, более высокий доход. Свой доход аукционы получают за счёт налогов, получаемых от расположенных на них зданий.</p>
<p>Аукционы имеют временное право собственности, которое можно оспорить. Каждые 3 дня аукцион может изменить своего собственника - новым собственником становится игрок, чья ставка на аукционе на момент его окончания.</p>
Правила работы аукционов следующие:
<ol>
	<li>С момента покупки (когда аукцион ставится чьим-то), аукцион переходит в собственность игрока длится 3 дня.</li>
	<li>В момент окончания ауциона, аукцион переходит в собственность того игрока, чья ставка на текущий момент на аукционе.</li>
	<li>В момент перехода к новому (или в случае если аукцион остаётся у старого игрока) с игрока снимается новая ставка.</li>
	<li>Если ставка на аукцион была выше номинальной, то новая ставка будет составлять <?echo AUC_BID_DEPRECIATION*100;?>% от предыдущей, иначе - остётся номинальной.</li>
	<li>При ставке на аукцион, перебивающая ставка должна быть не менее чем на <?echo AUC_NEW_BID_PERSENT*100;?>% больше текущей.</li>
	<li>При перебивании вашей ставке на аукционе, вам возвращается <?echo AUC_BID_RETURN_BROKEN*100;?>% от вашей перебитой ставки.</li>
	<li>Минимальная длительность аукциона - <?echo AUC_MIN_DURATION;?> часов с момента последней ставки. Если ставка была, когда аукцион должен был длиться менее <?echo AUC_MIN_DURATION;?> часов, то аукцион продлевается до <?echo AUC_MIN_DURATION;?> часов.</li>
</ol>

<h3 id="library/auctions_calc">Калькулятор доходности</h3>
Выберите тип аукциона:
<select id="selectTypeCalcedAuc">
	<option value="0">Не выбрано</option>
	<?
		$hiddensPersents='';
		$getAucObjects="SELECT `basicCost`,`name`,`kind` FROM `geoObjectsTypes` WHERE `isAuction`=1 ORDER BY `basicCost`";
		$resAucObjects=mysql_query($getAucObjects) or die(handleError('Не удалось получить список строений.',__FILE__,false,$getAucObjects));
		while($auc=mysql_fetch_assoc($resAucObjects))
		{
			echo '<option value="'.$auc['basicCost'].'">'.$auc['name'].' ('.$auc['basicCost'].' монет)</option>';


			switch ($auc['kind'])
			{
				case 'street':
					$persent=RENT_PERSENT_STREET;
					break;
				case 'locality':
					$persent=RENT_PERSENT_LOCALITY;
					break;
				case 'area':
					$persent=RENT_PERSENT_AREA;
					break;
				case 'province':
					$persent=RENT_PERSENT_REGION;
					break;
				case 'country':
					$persent=RENT_PERSENT_COUNTRY;
					break;

				default:
					$persent=0;
					break;
			}

			$hiddensPersents.='<span class="hidden" id="aucPer_'.$auc['basicCost'].'">'.$persent.'</span>';
		}
	?>
</select><br><?echo $hiddensPersents;?>
Доход с домов в час на этом аукционе: <input type="number" value="0" id="oneHourDoxod"><br>
Ваш <b>примерный</b> доход за 3 дня: <input type="text" disabled="disabled" value="0" id="aucCalcDoxod">, при условии что вы будете сдавать дома каждый день 10 раз на 1 час и один раз на 8 часов.<br>


<h1 id="library/credits">Биржа и кредиты</h1>
<p>Биржа состоит из 3-х условных частей и предоставляет следующий набор возможностей:</p>
<ol>
	<li>Приобритенине кредитов за российские рубли.</li>
	<li>Биржа кредитов, на которой игроки могут обменить кредиты на монеты и обратно.</li>
	<li>Биржа зданий, на которой игроки могут продавать свои здания и приобретать чужие.</li>
</ol>

<p>Покупка кредитов за рубли функционирует по следующим правилам:</p>
<ol>
	<li>Цена 1 кредита составляет 50 копеек.</li>
	<li>Минимальный обьем для покупки - 10 кредитов (5 рублей), максимальный - 50 000 кредитов (10 000).</li>
	<li>Чем больше кредитов вы приобретаете, тем дешевле цена кредита. За каждые 1500 кредитов цена уменьшается на 1 копейку.</li>
	<li>Оплата возможна только в российских рублях.</li>
	<li>Оплата возможно 3-мя способами: через <a href="http://w1.ru/" target="_black">Единый кошелёк</a>, <a href="http://sprypay.ru/" target="_black">Система приема платежей SpryPay</a> и <a href="http://money.yandex.ru/" target="_black">Яндекс.Деньги</a></li>
	<li>Сервисы оплаты, указаные выше, могут брать комиссию за перевод средств. Руководство игры никоим образом не может влиять на этот размер комиссий, равно как и отменять эти комиссии.</li>
	<li>Зачисление кредитов, как правило, происходит в течении нескольких минут после оплаты, но в отдельных случаях может достигать суток.</li>
</ol>

<p>Биржа кредитов функционирует по следующим правилам:</p>
<ol>
	<li>Любой игрок может разместить на бирже заявку на покупку или продажу кредитов за игровые монеты.</li>
	<li>Любой игрок может обменять свои кредиты на монеты, по существующей заявке игрока, равно как и приобрести кредиты за монеты, по заявке игрока.</li>
	<li>При размещении заявки на покупку кредитов необходимо указать курс - цену одного кредита в монетах. Курс не может быть ниже 100 монет.</li>
	<li>При размещении заявки на покупку кредитов с вас единовременно снимается 5% от от общей цены кредитов по указанному курсу.</li>
	<li>При размещении заявки на продажу кредитов курс не ограничен и никаких дополнительных налогов не снимается.</li>
	<li>В случае отмены заявки на покупку или продажу кредитов вам возвращается только 75% от сделанной вами ставки.</li>
	<li>В случае, если ваша заявка не была исполнена в течении 3-х месяцев, то она автоматически отменяется, но при этом вам возвращается только 20% от оставшейся суммы.</li>
	<li>При покупке строения с вас снимается дополнительный налог в 13%, помимо указанной стоимости строения продавцом.</li>
</ol>

<p>Биржа зданий функционирует по следующим правилам:</p>
<ol>
	<li>Любой игрок может выставить на продажу своё здание.</li>
	<li>Здание может быть выставлено на продажу, только в случае если в настоящий момент оно не находится в стадии строительства, улучшения или аренды.</li>
	<li>Цена при продаже не может быть больше 3-х базовых цен этого здания. Базовой ценой считается стоимость строительства + общая сумма затраченная на улучшеение до текущего уровня.</li>
	<li>При покупке здания, покупатель платит налог в 13% с цены каждого здания.</li>
	<li>Минимальная цена здания не ограничена.</li>
	<li>Перечисленныее выше ограничения касаются только цены здания в монетах, цена здания в кредитах не ограничена.</li>
	<li>При покупке строения с покупателя снимается дополнительный налог в 13%, от указанной стоимости строения продавцом.</li>
</ol>


<h1 id="library/payments">Выплаты игрокам</h1>
<p>В игре любой игрок может обменять игровые монеты на российские рубли с выводом на указанный игроком кошелёк в системе Яндекс.Деньги. Данная возможность появляется при баллансе игрока более 50 монет. Для открытия формы запроса вывода средств, необходимо кликнуть на блок с вашим баллансом.</p>
<p>При этом игра "Война олигархов" не является, так называемой "инвестиционной игрой", нет необходимости для продуктивной и интересной игры вкладывать свои средства. Но и заработать полноценно на выплатах невозможно - эта функция всего лишь для поощрения особо эффективных игроков.</p>
<p>Выплата игрокам основана на следующих правилах:</p>
<ol>
	<li>Максимальная сумма выплат игрокам за фактический месяц составляет <?echo MONTH_USER_PAYMENTS_LIMIT;?> рублей. То есть, если за последние 30 дней игроки суммарно запросили выплату на <?echo MONTH_USER_PAYMENTS_LIMIT;?> рублей, то выплата преостанавливается до тех пор, пока запрошенная за последние 30 дней сумма не станет меньше <?echo MONTH_USER_PAYMENTS_LIMIT;?></li>

	<li>Существует лимит - <?echo ONE_INTERVAL_USER_PAYMENTS_DAYS;?> дней для каждого игрока. В течении этого лимита игрок может запросить выплату максимум на <?echo ONE_INTERVAL_USER_PAYMENTS_LIMIT;?> рублей.</li>
	<li>Курс обмена монет к рублям динамический и рассчитывается по формуле: K=X/Y, где<br>
		K - курс обмена,<br>
		X - все имеющиеся в наличии монеты у всех игроков, которые хоть раз за последний месяц заходили в игру,<br>
		Y - текущий остаток от месячного лимита (т.е. если за последний месяц никто не запрашивал выплат - Y=<?echo ONE_INTERVAL_USER_PAYMENTS_LIMIT;?>, если за текущий месяц запросили выплат на 50 рублей, то Y=<?echo ONE_INTERVAL_USER_PAYMENTS_LIMIT-50;?>).</li>
</ol>
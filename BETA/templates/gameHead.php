<!DOCTYPE html>
<html>
	<head>
		<title>Война олигархов - игра на реальной карте</title>
		<meta http-equiv="content-type" content="text/html;charset=utf8"/>
		<link rel="stylesheet" type="text/css" media="screen" href="//code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="/getCss.php?game=true">

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
		(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
						try {
								w.yaCounter22649320 = new Ya.Metrika({id:22649320,
												webvisor:true,
												clickmap:true,
												trackLinks:true,
												accurateTrackBounce:true,
												trackHash:true});
						} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="//mc.yandex.ru/watch/22649320" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		<!--[if lte IE 8]>
			<script type="text/javascript" src="//yandex.st/jquery/1.9.1/jquery.min.js"></script>
			<script type="text/javascript" src="//yandex.st/jquery-ui/1.9.0/jquery-ui.min.js"></script>
		<![endif]-->
		<script type="text/javascript" src="//yandex.st/jquery/2.0.3/jquery.min.js"></script>
		<script type="text/javascript" src="//yandex.st/jquery-ui/1.10.4/jquery-ui.min.js"></script>
		<script src="//api-maps.yandex.ru/2.0-stable/?load=package.map,package.controls,package.search,package.geoObjects&lang=ru-RU" type="text/javascript"></script>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?96"></script>
		<script src="//ulogin.ru/js/ulogin.js"></script>
		<script src="getJs.php?game=true"></script>

		<script src="js/jquery.jqGrid/js/jquery.jqGrid.src.js" type="text/javascript"></script>
		<script src="js/jquery.jqGrid/js/i18n/grid.locale-ru.js" type="text/javascript"></script>

<!--		<link rel="stylesheet" type="text/css" media="screen" href="js/jquery.jqGrid/css/ui.jqgrid.css" />-->
	</head>
	<body>
		<?  include 'logoHeader.php';?>

		<div id="mainMenu">
			<?  include 'mainMenu.php';?>
		</div>

		<div id="message">
			<div id="messageWindow">
				<img src="images/close.png" class="closeMessageButton">
				<div class="content"></div>
			</div>
		</div>
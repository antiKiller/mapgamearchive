<?php
require '../configUsers.php';

$getManager="SELECT `period`, UNIX_TIMESTAMP(`buyedDate`) buyedDate, UNIX_TIMESTAMP(`endDate`) endDate FROM `manager` WHERE `user`=".$nowUserInfo['id']." and `endDate`>CURRENT_TIMESTAMP";
$resGetManager=mysql_query($getManager) or die(handleError('Не удалось получить информацию об управляющем.',__FILE__,false,$getManager,$nowUserInfo['id']));
$manager=mysql_fetch_assoc($resGetManager);
$managerText='<input type="button" value="Нанять управляющего" id="buyUserManager">';
if ($manager)
{
	$period=$manager['period'];
	if($period==1)
	{
		$period='каждый 1 час';
	}

	if($period==2 || $period==4 || $period==24)
	{
		$period='каждые '.$period.' часа';
	}

	if($period==8 || $period==12)
	{
		$period='каждые '.$period.' часов';
	}

	$leftDays=number_format(round(($manager['endDate']-time())/60/60/24,3),1,'.',' ');

	$managerText='сдаёт дома '.$period.'. Работает до '.date('H:i:s d.m.Y', $manager['endDate']).' ('.$leftDays.' дней)';
}

$getManager="SELECT `discount`, UNIX_TIMESTAMP(`startDate`) startDate, UNIX_TIMESTAMP(`endDate`) endDate FROM `taxManager` WHERE `user`=".$nowUserInfo['id']." and `endDate`>CURRENT_TIMESTAMP";
$resGetManager=mysql_query($getManager) or die(handleError('Не удалось получить информацию об бухгалтере.',__FILE__,false,$getManager,$nowUserInfo['id']));
$manager=mysql_fetch_assoc($resGetManager);
$taxManagerText='<input type="button" value="Нанять бухгалтера" id="buyUserAccountant">';
if ($manager)
{
	$leftDays=number_format(round(($manager['endDate']-time())/60/60/24,3),1,'.',' ');

	$taxManagerText='снижает налог на '.$manager['discount'].'%. Работает до '.date('H:i:s d.m.Y', $manager['endDate']).' ('.$leftDays.' дней)';
}

$countOfHouses="SELECT count(`id`) housesCount FROM `house` WHERE `user`=".$nowUserInfo['id'];
$resCountOfHouse=mysql_query($countOfHouses) or die(handleError('Не удалось получить кол-во ваших строений.',__FILE__,false,$countOfHouses,$nowUserInfo['id']));
$countHouses=mysql_fetch_assoc($resCountOfHouse);
$tax=floor($countHouses['housesCount']/100)*0.2;

if ($manager['discount']>0)
{
	$tax=number_format($tax-$tax*$manager['discount']/100,2,'.','');
}

echo json_encode(['result'=>'ok','tax'=>number_format($tax,2,'.',' '),'manager'=>$managerText,'taxManager'=>$taxManagerText]);
?>
<?
require "../configUsers.php";

/*
 * Дубли для домов
SELECT *
FROM `geoObjects` WHERE `country`='Беларусь' and `type` in (1,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34) and `id` not in
(
	SELECT `id` FROM `geoObjects`
	WHERE `type` in (1,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34)
	GROUP BY `address`
)

SELECT COUNT(`address`), COUNT(DISTINCT `address`) FROM `geoObjects`
WHERE `type` in (1,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34)


SELECT COUNT(`address`), COUNT(DISTINCT `address`) FROM 
(
  SELECT * FROM `geoObjects`
  WHERE `type` in (1,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,34)
  ORDER BY `address`
) a


 * Дубли для ауков
SELECT count(*)
FROM `geoObjects` WHERE `id` not in
(
	SELECT `id` FROM `geoObjects`
	WHERE `type`=2 GROUP BY `address`
) and `type`=2
*/

$objsList=json_decode($_POST["objsList"]);
$resultHtml='';
$ids='';
$buyOnClick=false;
$tmpArr=[];
if (!is_array($objsList))
{
	handleError('Некорректный список географических обьектов.',__FILE__);
}
if ($_POST['buyOnClick']=='true')
{
	$buyOnClick=true;
}
foreach ($objsList as $geoObject)
{
	if ($buyOnClick && $geoObject->kind!='house')
	{
		continue;
	}
	if (
				!$geoObject->country ||
				($geoObject->kind=='house' && $geoObject->number=='') ||
				($geoObject->kind=='street' && $geoObject->street=='') ||
				($geoObject->kind=='locality' && $geoObject->locality=='') ||
				($geoObject->kind=='area' && $geoObject->administrative=='' && $geoObject->subadministrative=='') ||
				($geoObject->kind=='province' && $geoObject->administrative=='' && $geoObject->subadministrative=='')
		 )
	{
		continue;
	}
	$getType="SELECT `id`,`name`,`isAuction`,`basicCost` FROM `geoObjectsTypes` WHERE `kind`='".$geoObject->kind."' and `isMap`=1";
	$rowType=mysql_fetch_assoc(mysql_query($getType));
	if (!$rowType)
	{
		continue;
	}

	$geoObject->number=htmlentities($geoObject->number, ENT_QUOTES, 'UTF-8');
	$geoObject->street=htmlentities($geoObject->street, ENT_QUOTES, 'UTF-8');
	$geoObject->locality=htmlentities($geoObject->locality, ENT_QUOTES, 'UTF-8');
	$geoObject->administrative=htmlentities($geoObject->administrative, ENT_QUOTES, 'UTF-8');
	$geoObject->subadministrative=htmlentities($geoObject->subadministrative, ENT_QUOTES, 'UTF-8');
	$geoObject->country=htmlentities($geoObject->country, ENT_QUOTES, 'UTF-8');
	$geoObject->address=htmlentities($geoObject->address, ENT_QUOTES, 'UTF-8');

	if (
				strpos($geoObject->address,'Крым')!==false ||
				strpos($geoObject->address,'крым')!==false ||
				strpos($geoObject->address,'Севастополь')!==false ||
				strpos($geoObject->address,'севастополь')!==false
		 )
	{
		for ($i=0;$i<count($objsList);$i++)
		{
			$objsList[$i]->country='Россия';
			if ($objsList[$i]->kind=='country')
			{
				$objsList[$i]->coords[0]=61.698653;
				$objsList[$i]->coords[1]=99.505405;
			}
			$objsList[$i]->address=str_replace('Украина', 'Россия', $objsList[$i]->address);
		}
	}

	$addNewobject=false;
	$searchObjectCoord="SELECT * FROM `geoObjects` WHERE `coords`=POINT(".$geoObject->coords[0].",".$geoObject->coords[1].") and `type`=".$rowType['id'];
	$searchObjectRes=mysql_query($searchObjectCoord) or die(handleError('Ошибка выполнения запроса поиска обьекта.',__FILE__,false,$searchObjectCoord,$nowUserInfo['id']));
	$rowObject=mysql_fetch_assoc($searchObjectRes);

	mysql_query("START TRANSACTION");
	if (!$rowObject)
	{
		$searchDistance=" and getDistBeforePoints(POINT(".$geoObject->coords[0].",".$geoObject->coords[1]."),`coords`)<";
		$isTypeSearch="`type`=".$rowType['id']." and";
		if ($geoObject->kind=='house')
		{
			$isTypeSearch='';
			$searchDistance.=1000;
			$searchOptions="and
											UPPER(`locality`)=UPPER('".$geoObject->locality."') and
											UPPER(`street`)=UPPER('".$geoObject->street."') and
											UPPER(`number`)=UPPER('".$geoObject->number."')";
		}

		if ($geoObject->kind=='street' || $geoObject->kind=='district')
		{
			$searchDistance.=15000;
			$searchOptions="and
											UPPER(`locality`)=UPPER('".$geoObject->locality."') and
											UPPER(`street`)=UPPER('".$geoObject->street."')";
		}

		if ($geoObject->kind=='locality')
		{
			$searchDistance.=35000;
			$searchOptions="and UPPER(`locality`)=UPPER('".$geoObject->locality."')";
		}

		if ($geoObject->kind=='province' || $geoObject->kind=='area')
		{
			$searchDistance.=200000;
			$searchOptions="and
											(
												( UPPER(`administrative`)=UPPER('".$geoObject->administrative."') and UPPER(`subadministrative`)=UPPER('".$geoObject->subadministrative."') ) or
												( UPPER(`subadministrative`)=UPPER('".$geoObject->administrative."') and UPPER(`administrative`)=UPPER('".$geoObject->subadministrative."') )
											)";
		}

		if ($geoObject->kind=='country')
		{
			$searchOptions='';
			$searchDistance='';
		}


		$searchObjectInfo="SELECT *
											 FROM `geoObjects`
											 WHERE ".$isTypeSearch."
														 (
															(
																UPPER(`country`)=UPPER('".$geoObject->country."')
																".$searchOptions."
														 	)
														 	or
														 	(
																UPPER(`address`)=UPPER('".$geoObject->address."')
															)
														 )
														 ".$searchDistance;
		$tmpArr[]=$searchObjectInfo;
		$searchObjectRes2=mysql_query($searchObjectInfo) or die(handleError('Ошибка выполнения запроса поиска обьекта.',__FILE__,false,$searchObjectInfo,$nowUserInfo['id'],true));
		$rowObject2=mysql_fetch_assoc($searchObjectRes2);

		if (!$rowObject2)
		{
			$addNewobject=true;
		}
			else
			{
				$updateCoods="UPDATE `geoObjects`
												SET `coords`=POINT(".$geoObject->coords[0].",".$geoObject->coords[1]."),
														`country`='".$geoObject->country."',
														`administrative`='".$geoObject->administrative."',
														`subadministrative`='".$geoObject->subadministrative."',
														`locality`='".$geoObject->locality."',
														`street`='".$geoObject->street."',
														`number`='".$geoObject->number."'
											WHERE `id`=".$rowObject2['id'];
				mysql_query($updateCoods) or die(handleError('Ошибка обновления координат обьекта.',__FILE__,false,$updateCoods,$nowUserInfo['id'],true));
				$rowObject=$rowObject2;
			}
	}
		elseif
			(
				$rowObject['administrative']!=$geoObject->administrative ||
				$rowObject['subadministrative']!=$geoObject->subadministrative ||
				$rowObject['locality']!=$geoObject->locality ||
				$rowObject['street']!=$geoObject->street
			)
		{
			$oldAdministrative=$rowObject['administrative'];
			$oldSubadministrative=$rowObject['subadministrative'];
			$oldLocality=$rowObject['locality'];
			$oldStreet=$rowObject['street'];
			$updateInfo="UPDATE `geoObjects` SET `administrative`='".$geoObject->administrative."'
											  WHERE `country`='".$rowObject['country']."' and
													`administrative`='".$oldAdministrative."'";
			mysql_query($updateInfo) or die(handleError('Ошибка выполнения обновления информации обьекта',__FILE__,false,$updateInfo,$nowUserInfo['id'],true));

			$updateInfo="UPDATE `geoObjects` SET `subadministrative`='".$geoObject->subadministrative."'
											  WHERE `country`='".$rowObject['country']."' and
													`administrative`='".$geoObject->administrative."' and
													`subadministrative`='".$oldSubadministrative."'";
			mysql_query($updateInfo) or die(handleError('Ошибка выполнения обновления информации обьекта.',__FILE__,false,$updateInfo,$nowUserInfo['id'],true));

			$updateInfo="UPDATE `geoObjects` SET `locality`='".$geoObject->locality."'
											  WHERE `country`='".$rowObject['country']."' and
													`administrative`='".$geoObject->administrative."' and
													`subadministrative`='".$geoObject->subadministrative."' and
													`locality`='".$oldLocality."'";
			mysql_query($updateInfo) or die(handleError('Ошибка выполнения обновления информации обьекта.',__FILE__,false,$updateInfo,$nowUserInfo['id'],true));

			$updateInfo="UPDATE `geoObjects` SET `street`='".$geoObject->street."'
											  WHERE `country`='".$rowObject['country']."' and
													`administrative`='".$geoObject->administrative."' and
													`subadministrative`='".$geoObject->subadministrative."' and
													`locality`='".$geoObject->locality."' and
													`street`='".$oldStreet."'";
			mysql_query($updateInfo) or die(handleError('Ошибка выполнения обновления информации обьекта.',__FILE__,false,$updateInfo,$nowUserInfo['id'],true));
		}
	$class='nonCollapsed';
	$isBuyed=false;
	if ($addNewobject)
	{
		//сначала проверяем на уже наличие такого дубля
		$searhTypes='='.$rowType['id'];
		if ($geoObject->kind=='house')
		{
			$searhTypes=" in (SELECT `id` FROM `geoObjectsTypes` WHERE `isAuction`=0 or `id`=1) ";
		}
		
		$checkDuble="SELECT `id`,`type` FROM `geoObjects` WHERE `type`".$searhTypes." and `address`='".$geoObject->address."'";
		$resCheckDubles=mysql_query($checkDuble) or die(handleError('Ошибка поиска дубля текущего обьекта. ',__FILE__,false,$checkDuble,$nowUserInfo['id'],true));
		$dublesSearched=mysql_fetch_assoc($resCheckDubles);
		
		if ($dublesSearched)
		{
			$updateGeoObj="UPDATE `geoObjects` SET
												`coords`=POINT(".$geoObject->coords[0].",".$geoObject->coords[1]."),
												`country`='".$geoObject->country."',
												`administrative`='".$geoObject->administrative."',
												`subadministrative`='".$geoObject->subadministrative."',
												`locality`='".$geoObject->locality."',
												`street`='".$geoObject->street."',
												`number`='".$geoObject->number."'
										 WHERE `id`=".$dublesSearched['id'];
			mysql_query($updateGeoObj) or die(handleError('Ошибка обновления сведений об обнаруженном дублей геообьекта. ',__FILE__,false,$updateGeoObj,$nowUserInfo['id'],true));
			$ids=$dublesSearched['id'];
		}
			else
			{
				$saveObject="INSERT INTO `geoObjects`
										 (
											`type`,
											`address`,
											`coords`,
											`country`,
											`administrative`,
											`subadministrative`,
											`locality`,
											`street`,
											`number`
										 )
										 VALUES

											(
												 ".$rowType['id'].",
												'".$geoObject->address."',
												 POINT(".$geoObject->coords[0].",".$geoObject->coords[1]."),
												'".$geoObject->country."',
												'".$geoObject->administrative."',
												'".$geoObject->subadministrative."',
												'".$geoObject->locality."',
												'".$geoObject->street."',
												'".$geoObject->number."'
											 )";
				mysql_query($saveObject) or die(handleError('Ошибка добавления в БД сведений о новых геообьектах. ',__FILE__,false,$saveObject,$nowUserInfo['id'],true));
				$ids=mysql_insert_id();
			}
		if ($buyOnClick)
		{
			if ($nowUserInfo['money']<$rowType['basicCost'])
			{
				echo json_encode(['result'=>'Недостаточно монет! Надо '.$rowType['basicCost'].', а у вас только '.$nowUserInfo['money']]);
				mysql_query("ROLLBACK");
				exit();
			}
			$buyQuery="INSERT INTO `house`
												(
													`user`,
													`geoObjectId`
												)
											VALUES
												(
													".$nowUserInfo['id'].",
													".$ids."
												)";
			mysql_query($buyQuery) or die(handleError('Ошибка покупки обьекта.',__FILE__,false,$buyQuery,$nowUserInfo['id'],true));

			$updateMoney="UPDATE `user` SET `money`=`money`-".$rowType['basicCost']." WHERE `id`=".$nowUserInfo['id'];
			mysql_query($updateMoney) or die(handleError('Ошибка списания монет с вашего счёта.',__FILE__,false,$updateMoney,$nowUserInfo['id'],true));

			$logEnter="INSERT INTO `log`
										(`user`,`text`,`geoObjectId`,`money`,`type`)
								 VALUES
										(".$nowUserInfo['id'].",'Покупка строения типа <i>".$rowType['name']."</i>',".$ids.",".($rowType['basicCost']*-1).",'Покупка строения')";
			mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));

			mysql_query("COMMIT");
			echo json_encode(['result'=>'ok']);
			exit();
		}
	}
		else
		{
			$ids=$rowObject['id'];
			if ($rowObject['address']!=$geoObject->address)
			{
				//сначала проверяем на уже наличие такого дубля
				$searhTypes='='.$rowType['id'];
				if ($geoObject->kind=='house')
				{
					$searhTypes=" in (SELECT `id` FROM `geoObjectsTypes` WHERE `isAuction`=0 or `id`=1) ";
				}

				$checkDuble="SELECT `id`,`type` FROM `geoObjects` WHERE `type`".$searhTypes." and `address`='".$geoObject->address."'";
				$resCheckDubles=mysql_query($checkDuble) or die(handleError('Ошибка поиска дубля текущего обьекта. ',__FILE__,false,$checkDuble,$nowUserInfo['id'],true));
				$dublesSearched=mysql_fetch_assoc($resCheckDubles);

				if ($dublesSearched)
				{
					$updateGeoObj="UPDATE `geoObjects` SET
														`coords`=POINT(".$geoObject->coords[0].",".$geoObject->coords[1]."),
														`country`='".$geoObject->country."',
														`administrative`='".$geoObject->administrative."',
														`subadministrative`='".$geoObject->subadministrative."',
														`locality`='".$geoObject->locality."',
														`street`='".$geoObject->street."',
														`number`='".$geoObject->number."'
												 WHERE `id`=".$dublesSearched['id'];
					mysql_query($updateGeoObj) or die(handleError('Ошибка обновления сведений об обнаруженном дублей геообьекта. ',__FILE__,false,$updateGeoObj,$nowUserInfo['id'],true));
					$ids=$dublesSearched['id'];
				}
					else
					{
						$updateInfo="UPDATE `geoObjects` SET `address`='".$geoObject->address."'
																WHERE `id`=".$ids;
						mysql_query($updateInfo) or die(handleError('Ошибка обновления адреса обьекта.',__FILE__,false,$updateInfo,$nowUserInfo['id'],true));
					}
			}

			if ($rowType['isAuction']==true)
			{
				$getAucUserInfo="SELECT a.*, b.`address`, d.`id` userIds, d.`name` userName, c.`name` nameType
												 FROM `auction` a,
															`geoObjects` b,
															`geoObjectsTypes` c,
															`user` d
												 WHERE b.`id`=".$rowObject['id']." and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and a.`user`=d.`id`";
				$resAucUser=mysql_query($getAucUserInfo) or die(handleError('Ошибка поиска владельца обьекта.',__FILE__,false,$getAucUserInfo,$nowUserInfo['id'],true));
				$rowAucUser=mysql_fetch_assoc($resAucUser);
				if ($rowAucUser)
				{
					$resultHtml.=getInfoOfBuildedObject($rowAucUser,true,$nowUserInfo);
					$isBuyed=true;
				}
			}
				else
				{
					$getAucUserInfo="SELECT a.*,
																	a.`id` houseId,
																	c.`isBuilded`,
												 					b.`id` objId,
												 					b.`address`,
												 					d.`id` userIds,
												 					d.`name` userName,
												 					c.`name` nameType,
												 					UNIX_TIMESTAMP(`time`) `time`,
												 					c.`radius`,
												 					X(b.`coords`) `pos1`,
												 					Y(b.`coords`) `pos2`,
												 					c.`basicDoxod`,
												 					c.`isHouse`,
												 					c.`isEnterprise`,
												 					c.`isSocial`
													 FROM `house` a,
											 					`geoObjects` b,
											 					`geoObjectsTypes` c,
											 					`user` d
													 WHERE b.`id`=".$rowObject['id']." and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and a.`user`=d.`id`";
					$resAucUser=mysql_query($getAucUserInfo) or die(handleError('Ошибка поиска владельца обьекта.',__FILE__,false,$getAucUserInfo,$nowUserInfo['id'],true));
					$rowAucUser=mysql_fetch_assoc($resAucUser);

					if ($rowAucUser)
					{
						$resultHtml.=getInfoOfBuildedObject($rowAucUser,false,$nowUserInfo);
						$isBuyed=true;
					}
						else
						{
							if ($buyOnClick)
							{
								if ($nowUserInfo['money']<$rowType['basicCost'])
								{
									echo json_encode(['result'=>'Недостаточно монет! Надо '.$rowType['basicCost'].', а у вас только '.$nowUserInfo['money']]);
									mysql_query("ROLLBACK");
									exit();
								}
								$buyQuery="INSERT INTO `house`
																	(
																		`user`,
																		`geoObjectId`
																	)
																VALUES
																	(
																		".$nowUserInfo['id'].",
																		".$ids."
																	)";
								mysql_query($buyQuery) or die(handleError('Ошибка покупки обьекта.',__FILE__,false,$buyQuery,$nowUserInfo['id'],true));

								$updateMoney="UPDATE `user` SET `money`=`money`-".$rowType['basicCost']." WHERE `id`=".$nowUserInfo['id'];
								mysql_query($updateMoney) or die(handleError('Ошибка списания монет с вашего счёта.',__FILE__,false,$updateMoney,$nowUserInfo['id'],true));

								$logEnter="INSERT INTO `log`
												(`user`,`text`,`geoObjectId`,`money`,`type`)
										 VALUES
												(".$nowUserInfo['id'].",'Покупка строения типа <i>".$rowType['name']."</i>',".$ids.",".($rowType['basicCost']*-1).",'Покупка строения')";
								mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));

								mysql_query("COMMIT");
								echo json_encode(['result'=>'ok']);
								exit();
							}
						}
				}
		}
	mysql_query("COMMIT");

	if (!$isBuyed)
	{
		$buyButton='<div class="buyGeoObjectButton" ids="'.$ids.'">'.$rowType['basicCost'].' <img class="moneyIndicator" src="images/money.png"></div>';
		$resultHtml.='
			<div class="geoObjectInfo '.$class.'">
				'.$buyButton.'
				<div class="geoObjAddr">'.$geoObject->address.' (<span class="geoObjType">'.$rowType['name'].'</span>)</div>
			</div>';
	}

}

$resultHtml='<div class="baloonInfoContent">'.$resultHtml.'</div>';

echo json_encode(['result'=>'ok','content'=>$resultHtml,'qwe'=>$checkDuble]);
?>
<?php
if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	echo json_encode(['result'=>'Неверный список идентификаторов строений!']);
	exit();
}

require "../configUsers.php";
$ids=(int)$_POST['ids'];

$getBuilding="SELECT `id`
							FROM `house`
							WHERE `geoObjectId`=".$ids." and `isCosted`=1 and `user`=".$nowUserInfo['id'];
$resBuilding=mysql_query($getBuilding) or die(handleError('Не удалось получить информацию о здании в продаже.',__FILE__,false,$getBuilding,$nowUserInfo['id']));
$building=mysql_fetch_assoc($resBuilding);

if (!$building)
{
	echo json_encode(['result'=>'Такое строение выставленное на продажу, не найдено']);
	exit();
}

$updateHouse="UPDATE `house` SET `isCosted`=0 WHERE `geoObjectId`=".$ids." and `user`=".$nowUserInfo['id'];
$updateLog="INSERT INTO `log` (`user`,`geoObjectId`,`text`,`type`) VALUES (".$nowUserInfo['id'].", ".$ids.", 'Отмена выставления здания на продажу', 'Выставление здания на продажу')";

mysql_query("START TRANSACTION");
mysql_query($updateHouse) or die(handleError('Не удалось обновить данные строения.',__FILE__,false,$updateHouse,$nowUserInfo['id'],true));
mysql_query($updateLog) or die(handleError('Не удалось записать данные в журнал.',__FILE__,false,$updateLog,$nowUserInfo['id'],true));
mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
<?php
require "../configUsers.php";

if (!is_numeric($_POST['fromIds']))
{
	handleError('Неверный идентификатор сообщения чата.',__FILE__);
}

$fromIds=(int)$_POST['fromIds'];

$usersList='';
$getUsersList="SELECT `id`,`name`,`photo`,`lastPage` FROM `user` WHERE `lastEnter`>(CURRENT_TIMESTAMP - INTERVAL 1 MINUTE) ORDER BY `id`";
$resGetUsersList=mysql_query($getUsersList) or die(handleError('Не удалось получить список игроков в чате.',__FILE__,false,$getUsersList,$nowUserInfo['id']));
while ($chatUser=mysql_fetch_assoc($resGetUsersList))
{
	$usersList.='<div class="chatUser">
								<span class="openUserProfile" userId="'.$chatUser['id'].'"> <img src="'.$chatUser['photo'].'" class="chatUserImg"> &nbsp;'.$chatUser['name'].'</span>
								<span class="viewNowPage">('.$chatUser['lastPage'].')</span>
							 </div>';
}

$messagesList='';
$getMessagesList="SELECT a.`id`, a.`message`, UNIX_TIMESTAMP(a.`time`) time,
												 b.`id` userId, b.`name`, b.`photo`
									FROM `chat` a,`user` b
									WHERE a.`user`=b.`id` and a.`id`>".$fromIds."
									ORDER BY `id` DESC
									LIMIT 30";
$resGetMessagesList=mysql_query($getMessagesList) or die(handleError('Не удалось получить список сообщений чата.',__FILE__,false,$getMessagesList,$nowUserInfo['id']));
while ($chatMessage=mysql_fetch_assoc($resGetMessagesList))
{
	if ($fromIds<$chatMessage['id'])
	{
		$fromIds=$chatMessage['id'];
	}
	$chatMessage['message']=str_replace("\n", '<br>', $chatMessage['message']);
	$messagesList='<div class="chatMessageBlock">
										<div class="chatMessageTime">'.date('H:i:s d.m.Y', $chatMessage['time']).'</div>
										<span class="openUserProfile" userId="'.$chatMessage['userId'].'"><img src="'.$chatMessage['photo'].'" class="chatUserMessageImg"> '.$chatMessage['name'].'</span>
										<div class="chatMessage">'.$chatMessage['message'].'</div>
									</div>'.$messagesList;
}

if ($fromIds>$nowUserInfo['lastViewedChat'])
{
	$updateUser="UPDATE `user` SET lastViewedChat=".$fromIds." WHERE `id`=".$nowUserInfo['id'];
	mysql_query($updateUser) or die(handleError('Не удалось обновить список просмотренных сообщений чата.',__FILE__,false,$updateUser,$nowUserInfo['id']));
}

echo json_encode(['result'=>'ok','fromIds'=>$fromIds,'users'=>$usersList,'messages'=>$messagesList]);
?>
<?php
if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный тип управляющего!',__FILE__);
}

require '../configUsers.php';

$costs=[ONE_DAY_COST_MANAGER_FIRST,ONE_DAY_COST_MANAGER_SECOND,ONE_DAY_COST_MANAGER_THIRD,ONE_DAY_COST_MANAGER_FOURTH,ONE_DAY_COST_MANAGER_FIFTH,ONE_DAY_COST_MANAGER_SIXTH];
$nowTime=time();
$ids=(int)$_POST['ids'];

$oneDay=3600*24;
$sevenDay=$oneDay*7;
$mouthDay=$oneDay*30;

switch ($ids)
{
	case 1:
		{
			$period=1;
			$cost=$costs[0];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 2:
		{
			$period=2;
			$cost=$costs[1];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 3:
		{
			$period=4;
			$cost=$costs[2];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 4:
		{
			$period=8;
			$cost=$costs[3];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 5:
		{
			$period=12;
			$cost=$costs[4];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 6:
		{
			$period=24;
			$cost=$costs[5];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 7:
		{
			$period=1;
			$cost=$costs[0]*7-COST_MANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 8:
		{
			$period=2;
			$cost=$costs[2];
			$cost=$costs[1]*7-COST_MANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 9:
		{
			$period=4;
			$cost=$costs[2]*7-COST_MANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 10:
		{
			$period=8;
			$cost=$costs[3]*7-COST_MANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 11:
		{
			$period=12;
			$cost=$costs[4]*7-COST_MANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 12:
		{
			$period=24;
			$cost=$costs[5]*7-COST_MANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 13:
		{
			$period=1;
			$cost=$costs[0]*30-COST_MANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 14:
		{
			$period=2;
			$cost=$costs[1]*30-COST_MANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 15:
		{
			$period=4;
			$cost=$costs[2]*30-COST_MANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 16:
		{
			$period=8;
			$cost=$costs[3]*30-COST_MANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 17:
		{
			$period=12;
			$cost=$costs[4]*30-COST_MANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 18:
		{
			$period=24;
			$cost=$costs[5]*30-COST_MANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}


	default:
		handleError('Неверный тип управляющего!',__FILE__);
}

if($nowUserInfo['credits']<$cost)
{
	handleError('Недостаточно кредитов! Для покупки выбранного типа управляющего надо '.$cost.' кредитов, а у вас только '.$nowUserInfo['credits'],__FILE__);
}

$insertToManager="INSERT INTO `manager` (`user`, `period`, `endDate`) VALUES (".$nowUserInfo['id'].",".$period.",FROM_UNIXTIME(".$endTime."))";
$userUpdate="UPDATE `user` SET `credits`=`credits`-".$cost." WHERE `id`=".$nowUserInfo['id'];

if($period==1)
{
	$period='каждый 1 час';
}

if($period==2 || $period==4 || $period==24)
{
	$period='каждые '.$period.' часа';
}

if($period==8 || $period==12)
{
	$period='каждые '.$period.' часов';
}

$days=round(($endTime-$nowTime)/60/60/24,3);

$toLog="INSERT INTO `log`
					(`user`,`text`,`money`,`credits`,`type`)
											VALUES
					(".$nowUserInfo['id'].",'Вы приобрели управляющего, который будет сдавать ваши дома ".$period." в течении ".$days." дней',0,".($cost*-1).",'Управляющий')";

mysql_query("START TRANSACTION");

mysql_query($insertToManager) or die(handleError('Не удалось обновить данные управляющего.',__FILE__,false,$insertToManager,$nowUserInfo['id'],true));
mysql_query($userUpdate) or die(handleError('Не удалось обновить ваш счёт.',__FILE__,false,$userUpdate,$nowUserInfo['id'],true));
mysql_query($toLog) or die(handleError('Не удалось записать данные в журнал.',__FILE__,false,$toLog,$nowUserInfo['id'],true));

mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
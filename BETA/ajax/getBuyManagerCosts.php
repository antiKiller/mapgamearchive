<?php
require "../configUsers.php";

$costs=[ONE_DAY_COST_MANAGER_FIRST,ONE_DAY_COST_MANAGER_SECOND,ONE_DAY_COST_MANAGER_THIRD,ONE_DAY_COST_MANAGER_FOURTH,ONE_DAY_COST_MANAGER_FIFTH,ONE_DAY_COST_MANAGER_SIXTH];

$message='Вы можете нанять управляющего на ближайщие дни! Управляющий будет сдавать ваши дома так, как будто это делаете вы.<br>
	<table id="tableForBuyManager">
		<tr>
			<th>Период сдачи в аренду</th>
			<th>Цена <img class="moneyIndicator" src="images/baks.png"></th>
			<th>Время работы управляющего</th>
			<th>Нанять</th>
		<tr>

		<tr>
			<td>1 час</td>
			<td>'.$costs[0].' <img class="moneyIndicator" src="images/baks.png"></td>
			<td>1 день</td>
			<td><input type="button" value="Нанять" managType="1"></td>
		<tr>
		<tr>
			<td>2 часа</td>
			<td>'.$costs[1].' <img class="moneyIndicator" src="images/baks.png"></td>
			<td>1 день</td>
			<td><input type="button" value="Нанять" managType="2"></td>
		<tr>
		<tr>
			<td>4 часа</td>
			<td>'.$costs[2].' <img class="moneyIndicator" src="images/baks.png"></td>
			<td>1 день</td>
			<td><input type="button" value="Нанять" managType="3"></td>
		<tr>
		<tr>
			<td>8 часов</td>
			<td>'.$costs[3].' <img class="moneyIndicator" src="images/baks.png"></td>
			<td>1 день</td>
			<td><input type="button" value="Нанять" managType="4"></td>
		<tr>
		<tr>
			<td>12 часов</td>
			<td>'.$costs[4].' <img class="moneyIndicator" src="images/baks.png"></td>
			<td>1 день</td>
			<td><input type="button" value="Нанять" managType="5"></td>
		<tr>
		<tr>
			<td>24 часа</td>
			<td>'.$costs[5].' <img class="moneyIndicator" src="images/baks.png"></td>
			<td>1 день</td>
			<td><input type="button" value="Нанять" managType="6"></td>
		<tr>

		<tr>
			<td>1 час</td>
			<td>'.($costs[0]*7-COST_MANAGER_DISCOUNT_FIRST).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_FIRST.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>7 дней</td>
			<td><input type="button" value="Нанять" managType="7"></td>
		<tr>
		<tr>
			<td>2 часа</td>
			<td>'.($costs[1]*7-COST_MANAGER_DISCOUNT_FIRST).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_FIRST.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>7 дней</td>
			<td><input type="button" value="Нанять" managType="8"></td>
		<tr>
		<tr>
			<td>4 часа</td>
			<td>'.($costs[2]*7-COST_MANAGER_DISCOUNT_FIRST).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_FIRST.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>7 дней</td>
			<td><input type="button" value="Нанять" managType="9"></td>
		<tr>
		<tr>
			<td>8 часов</td>
			<td>'.($costs[3]*7-COST_MANAGER_DISCOUNT_FIRST).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_FIRST.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>7 дней</td>
			<td><input type="button" value="Нанять" managType="10"></td>
		<tr>
		<tr>
			<td>12 часов</td>
			<td>'.($costs[4]*7-COST_MANAGER_DISCOUNT_FIRST).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_FIRST.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>7 дней</td>
			<td><input type="button" value="Нанять" managType="11"></td>
		<tr>
		<tr>
			<td>24 часа</td>
			<td>'.($costs[5]*7-COST_MANAGER_DISCOUNT_FIRST).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_FIRST.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>7 дней</td>
			<td><input type="button" value="Нанять" managType="12"></td>
		<tr>

		<tr>
			<td>1 час</td>
			<td>'.($costs[0]*30-COST_MANAGER_DISCOUNT_SECOND).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_SECOND.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>30 дней</td>
			<td><input type="button" value="Нанять" managType="13"></td>
		<tr>
		<tr>
			<td>2 часа</td>
			<td>'.($costs[1]*30-COST_MANAGER_DISCOUNT_SECOND).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_SECOND.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>30 дней</td>
			<td><input type="button" value="Нанять" managType="14"></td>
		<tr>
		<tr>
			<td>4 часа</td>
			<td>'.($costs[2]*30-COST_MANAGER_DISCOUNT_SECOND).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_SECOND.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>30 дней</td>
			<td><input type="button" value="Нанять" managType="15"></td>
		<tr>
		<tr>
			<td>8 часов</td>
			<td>'.($costs[3]*30-COST_MANAGER_DISCOUNT_SECOND).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_SECOND.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>30 дней</td>
			<td><input type="button" value="Нанять" managType="16"></td>
		<tr>
		<tr>
			<td>12 часов</td>
			<td>'.($costs[4]*30-COST_MANAGER_DISCOUNT_SECOND).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_SECOND.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>30 дней</td>
			<td><input type="button" value="Нанять" managType="17"></td>
		<tr>
		<tr>
			<td>24 часа</td>
			<td>'.($costs[5]*30-COST_MANAGER_DISCOUNT_SECOND).' <img class="moneyIndicator" src="images/baks.png"> (СКИДКА '.COST_MANAGER_DISCOUNT_SECOND.' <img class="moneyIndicator" src="images/baks.png">)</td>
			<td>30 дней</td>
			<td><input type="button" value="Нанять" managType="18"></td>
		<tr>
	</table>';

echo json_encode(['result'=>'ok','message'=>$message]);
?>
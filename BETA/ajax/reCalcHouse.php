<?php
require "../config.php";
$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));

$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$getHousesList="SELECT `id` `houseId`, `geoObjectId`, `user`, `info`, `level`,  `clients`, `doxod`, `radius`, X(`coords`) `pos1`, Y(`coords`) `pos2`, `bonus`,
											 `type`, `name`, `isNeeded`, `address`, `fixClients`, `basicDoxod`, `updateDoxod`, `basicBonus`, `updateBonus`, `isHouse`, `isEnterprise`, `isSocial`
								FROM `buildingsUsers`
								WHERE `geoObjectId` in (".$objIds.")";
$resHousesList=mysql_query($getHousesList) or die(handleError('Ошибка получения списка зданий для пересчёта.',__FILE__,false,$getHousesList,$nowUserInfo['id']));
while($house=mysql_fetch_assoc($resHousesList))
{
	$table='';
	if($house['isHouse'])
	{
		recalcHouse($house,true);
	}
	if($house['isEnterprise'])
	{
		recalcEnterprise($house,true);
	}
	
	if ($house['isSocial'])
	{
		$toRecalcSql="INSERT INTO `reCalcHouses` (`houseId`)
											SELECT `id`
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and getDistBeforePoints(POINT(".$house['pos1'].", ".$house['pos2']."),`coords`)<=".$house['radius'];
		mysql_query($toRecalcSql) or die(handleError('Не удалось поставить дома в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id']));
		
		if ($house['level']>1)
		{
			$bonus=$house['basicBonus']*pow($house['updateBonus'],$house['level']);
		}
			else
			{
				$bonus=$house['basicBonus'];
			}
		if ($bonus!=$house['bonus'])
		{
			$updateSocial="UPDATE `house` SET `bonus`=".$bonus."  WHERE `id`=".$house['houseId'];
			mysql_query($updateSocial) or die(handleError('Не удалось обновить данные строения.',__FILE__,false,$updateSocial,$nowUserInfo['id']));
		}
	}
}

echo json_encode(['result'=>'ok']);
?>
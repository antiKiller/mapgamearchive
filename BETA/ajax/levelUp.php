<?php
require "../configUsers.php";

$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));
$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$dopQuery='';
if ($objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	if ($mode=='onlyChecked')
	{
		$operator='in';
	}
		else
		{
			$operator='not in';
		}
	$dopQuery="and `geoObjectId` ".$operator." (".$objIds.")";
}

$getAllCost="SELECT round(sum(`basicCost`*pow(`updateCost`,`level`)),2) allCost
						 FROM `buildingsUsers`
						 WHERE `user`=".$nowUserInfo['id']." and `isUpdated`=1 and `time`<=CURRENT_TIMESTAMP ".$dopQuery;
$resGetAllCost=mysql_query($getAllCost) or die(handleError('Ошибка получения общей суммы улучшения.',__FILE__,false,$getAllCost,$nowUserInfo['id']));
$allCost=mysql_fetch_assoc($resGetAllCost);
if (!$allCost || !$allCost['allCost'] || $allCost['allCost']<=0)
{
	handleError('Ошибка получения общей суммы улучшения!',__FILE__);
}

if ($allCost['allCost']>$nowUserInfo['money'])
{
	handleError('Недостаточно монет для улучшения зданий! У вас '.number_format($nowUserInfo['money'], 2, '.', ' ').' , а нужно '.number_format($allCost['allCost'], 2, '.', ' '),__FILE__);
}


$toLog=[];
$nowObject="SELECT `id`, `geoObjectId`, `level`, `clients`, `oneClientDoxod`, `bonus`, `doxod`, `competition`, X(`coords`) `pos1`, Y(`coords`) `pos2`, `type`, `name`,
									 `radius`, `basicDoxod`, `basicCost`, `basicTime`, `basicBonus`, `updateBonus`, `updateCost`, `updateDoxod`, `updateTime`, `isHouse`, `isEnterprise`, `isSocial`
						FROM `buildingsUsers`
						WHERE `user`=".$nowUserInfo['id']." and `isUpdated`=1 and `time`<=CURRENT_TIMESTAMP ".$dopQuery;
$resNowObject=mysql_query($nowObject) or die(handleError('Ошибка выборки строения.',__FILE__,false,$nowObject,$nowUserInfo['id']));

mysql_query("START TRANSACTION");
while($nowObjInfo=mysql_fetch_assoc($resNowObject))
{
	$level=$nowObjInfo['level']+1;

	if ($nowObjInfo['isHouse'])
	{
		$oneClientDoxod=$nowObjInfo['basicDoxod']*pow($nowObjInfo['updateDoxod'],$nowObjInfo['level']);
		$dopClients=floor($level/10);
		$nowObjInfo['clients']+=$dopClients;
		$doxodBasic=round($nowObjInfo['clients']*$oneClientDoxod,2);
		$doxod=round($doxodBasic+$doxodBasic*$nowObjInfo['bonus']/100,2);
		$time=$nowObjInfo['basicTime']*pow($nowObjInfo['updateTime'],$nowObjInfo['level']);
		$cost=$nowObjInfo['basicCost']*pow($nowObjInfo['updateCost'],$nowObjInfo['level']);
		$bonus=$nowObjInfo['bonus'];
		$tableRecalc='reCalcHouses';
	}

	if ($nowObjInfo['isSocial'])
	{
		$oneClientDoxod=0;
		$doxodBasic=0;
		$doxod=0;
		$time=$nowObjInfo['basicTime']*pow($nowObjInfo['updateTime'],$nowObjInfo['level']);
		$cost=$nowObjInfo['basicCost']*pow($nowObjInfo['updateCost'],$nowObjInfo['level']);
		$bonus=$nowObjInfo['basicBonus']*pow($nowObjInfo['updateBonus'],$nowObjInfo['level']);
	}

	if ($nowObjInfo['isEnterprise'])
	{
		$oneClientDoxod=$nowObjInfo['basicDoxod']*pow($nowObjInfo['updateDoxod'],$nowObjInfo['level']);
		$potencialDoxod=round($nowObjInfo['clients']*$oneClientDoxod,2);
		$doxod=round($potencialDoxod-$potencialDoxod*$nowObjInfo['competition']/100,2);
		$time=$nowObjInfo['basicTime']*pow($nowObjInfo['updateTime'],$nowObjInfo['level']);
		$cost=$nowObjInfo['basicCost']*pow($nowObjInfo['updateCost'],$nowObjInfo['level']);
		$bonus=0;
		$tableRecalc='reCalc';
	}

	$timeEnd=time()+round($time*60);
	$updateBuilding="UPDATE `house`
										SET `doxod`=".$doxod.",
												`clients`=".$nowObjInfo['clients'].",
												`oneClientDoxod`=".$oneClientDoxod.",
												`level`=".$level.",
												`bonus`=".$bonus.",
												`action`='Улучшение',
												`time`=FROM_UNIXTIME(".$timeEnd.")
									 WHERE `id`=".$nowObjInfo['id'];
	mysql_query($updateBuilding) or die(handleError('Ошибка сохранения данных здания.',__FILE__,false,$updateBuilding,$nowUserInfo['id'],true));

	if (!$nowObjInfo['isSocial'])
	{
		$toRecalcSql="INSERT INTO `".$tableRecalc."` (`houseId`,`notRecalcUntil`)
										VALUES
											(".$nowObjInfo['id'].",CURRENT_TIMESTAMP),
											(".$nowObjInfo['id'].",FROM_UNIXTIME(".($timeEnd+5)."))";
		mysql_query($toRecalcSql) or die(handleError('Не удалось поставить здание в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));

		$getClientsTypes="SELECT GROUP_CONCAT(`clientObj`) `clientObj` FROM `clientsForBuildngs` WHERE `parentObj`=".$nowObjInfo['type'];
		$resClientsQuery=mysql_query($getClientsTypes);
		$clientsQuery=mysql_fetch_assoc($resClientsQuery);
		$clientsTypes=$clientsQuery['clientObj'];

		if ($clientsTypes && $nowObjInfo['radius'])
		{
			$deleteLast="DELETE FROM `reCalcHouses` WHERE `id` IN  (SELECT `id`
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'].")";
			mysql_query($deleteLast) or die(handleError('Не удалось удалить лишние дома из очереди пересчета.',__FILE__,false,$deleteLast,$nowUserInfo['id'],true));

			$toRecalcSql="INSERT INTO `reCalcHouses` (`houseId`)
											SELECT `id`
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить дома в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));

			$toRecalcSql="INSERT INTO `reCalcHouses` (`houseId`,`notRecalcUntil`)
											SELECT `id`,FROM_UNIXTIME(".($timeEnd+5).")
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить дома в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));


			$deleteLast="DELETE FROM `reCalc` WHERE `id` IN  (SELECT `id`
											FROM `buildingsUsers`
											WHERE `isEnterprise`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'].")";
			mysql_query($deleteLast) or die(handleError('Не удалось удалить лишние дома из очереди пересчета.',__FILE__,false,$deleteLast,$nowUserInfo['id'],true));

			$toRecalcSql="INSERT INTO `reCalc` (`houseId`)
											SELECT `id`
											FROM `buildingsUsers`
											WHERE `isEnterprise`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить предприятия в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));

			$toRecalcSql="INSERT INTO `reCalc` (`houseId`,`notRecalcUntil`)
											SELECT `id`,FROM_UNIXTIME(".($timeEnd+5).")
											FROM `buildingsUsers`
											WHERE `isEnterprise`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить предприятия в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));
		}
	}
		else
		{
			$deleteLast="DELETE FROM `reCalcHouses` WHERE `id` IN  (SELECT `id`
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'].")";
			mysql_query($deleteLast) or die(handleError('Не удалось удалить лишние дома из очереди пересчета.',__FILE__,false,$deleteLast,$nowUserInfo['id'],true));

			$toRecalcSql="INSERT INTO `reCalcHouses` (`houseId`)
											SELECT `id`
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить дома в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));

			$toRecalcSql="INSERT INTO `reCalcHouses` (`houseId`,`notRecalcUntil`)
											SELECT `id`,FROM_UNIXTIME(".($timeEnd+5).")
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and getDistBeforePoints(POINT(".$nowObjInfo['pos1'].", ".$nowObjInfo['pos2']."),`coords`)<=".$nowObjInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить дома в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));
		}

	$toLog[]="(".$nowUserInfo['id'].",'Улучшения строения типа <i>".$nowObjInfo['name']."</i> до уровня <b>".$level."</b>',".$nowObjInfo['geoObjectId'].",".($cost*-1).",'Улучшение')";
}
$updateUser="UPDATE `user` SET `money`=`money`-".$allCost['allCost'].' WHERE `id`='.$nowUserInfo['id'];
mysql_query($updateUser) or die(handleError('Ошибка снятия монет за улучшение.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));

$logEnter="INSERT INTO `log`
							(`user`,`text`,`geoObjectId`,`money`,`type`)
					 VALUES ".implode(',', $toLog);
mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));

mysql_query("COMMIT");
echo json_encode(['result'=>'ok']);
?>
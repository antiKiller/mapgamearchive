<?php
require "../configUsers.php";

$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));
$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$dopQuery='';
if ($objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	if ($mode=='onlyChecked')
	{
		$operator='in';
	}
		else
		{
			$operator='not in';
		}
	$dopQuery="and `geoObjectId` ".$operator." (".$objIds.")";
}

$newIds=[];
$allCost=0;

$message='';

$getHouse="SELECT `level`, `geoObjectId`, `address`, `name`, `basicCost`, `updateCost`
					 FROM `buildingsUsers`
					 WHERE `user`=".$nowUserInfo['id']." and `time`<=CURRENT_TIMESTAMP and `isCosted`=0 and `isMap`=0 ".$dopQuery;
$resHouse=mysql_query($getHouse) or die(handleError('Ошибка получения списка домов.',__FILE__,false,$getHouse,$nowUserInfo['id']));
$housesDestroy='';
while($house=mysql_fetch_assoc($resHouse))
{
	$newIds[]=$house['geoObjectId'];
	$desctroyCost=round($house['basicCost']*pow($house['updateCost'],$house['level']-1)/3,2);
	$allCost+=$desctroyCost;
	$housesDestroy.='<b>'.$house['name'].' '.$house['level'].' уровня</b> по адресу <i>'.$house['address'].' </i>. Стоимость сноса: '.number_format($desctroyCost, 2, ',', ' ').' <img class="moneyIndicator" src="images/money.png"><br>';
}
if (count($newIds)==0)
{
	echo json_encode(['result'=>'Среди выбранных зданий нет доступных для сноса! Сносить можно только здания, не находящиеся в аренде, строительстве, улучшении и не выставленные на продажу на бирже.']);
	exit();
}
$allCost=round($allCost,2);
$newIdsList=implode(',',$newIds);

$message.='Вы можете просто снести указанные здания или построить на их месте что-то другое.<br><br>
	Стоимость сноса одного здания составляет 1/3 от стоимости улучшения до текущего уровня.<br>
	Итого стоимость сноса всех зданий ('.count($newIds).' штук) будет равна: '.number_format($allCost, 2, ',', ' ').' <img class="moneyIndicator" src="images/money.png"><br>
	<div>Здания на которые будут снесены:
		<div>'.$housesDestroy.'</div>
	</div>
	<br>
	Вы можете полностью уничтожить здания, после чего они превратятся в строй-площадку, перестанут быть вашей собственностью и любой игрок сможет купить данную строй-прощадку. Если вы хотите уничтожить здания, то нажмите на кнопку: <input type="button" value="Уничтожить постройки и отказаться от права собственности на них" id="fullDestroy" objid="'.$newIdsList.'"><br>
	Если вы хотите только снести здания, превратив их в принадлежащие вам строй-прощадки, то нажмите на эту кнопку: <input type="button" value="Снос" id="destroyOnly" objid="'.$newIdsList.'">
	<hr>
Но в дополнение вы можете на всех этих здания сразу любое другое. При этом стоимость постройки составит '.number_format($allCost, 2, ',', ' ').' + стоимость выбранного здания.<br>';


$message.=getBuildingsForObjects($objIds,$nowUserInfo);

echo json_encode(['result'=>'ok','message'=>$message]);
?>
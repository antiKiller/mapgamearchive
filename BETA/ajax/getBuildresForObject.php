<?php
require "../configUsers.php";

$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));
$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$dopQuery='';
if ($objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	if ($mode=='onlyChecked')
	{
		$operator='in';
	}
		else
		{
			$operator='not in';
		}
	$dopQuery="and `geoObjectId` ".$operator." (".$objIds.")";
}

$newIds=[];

$checkObject="SELECT `isBuilded`, `geoObjectId`
							FROM `buildingsUsers`
							WHERE `user`=".$nowUserInfo['id']." and `isBuilded`=1 ".$dopQuery;

$resCheckObject=mysql_query($checkObject) or die(handleError('Ошибка проверки обьекта на возможность постройки.',__FILE__,false,$checkObject,$nowUserInfo['id']));
while($rowCheckObject=mysql_fetch_assoc($resCheckObject))
{
	$newIds[]=$rowCheckObject['geoObjectId'];
}

if (count($newIds)==0)
{
	echo json_encode(['result'=>'Нет пригодных зданий для строительства!']);
	exit();
}

$ids=implode(',',$newIds);

$resultsList=getBuildingsForObjects($ids,$nowUserInfo);

echo json_encode(['result'=>'ok','dataList'=>$resultsList]);
?>
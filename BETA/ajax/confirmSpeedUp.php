<?php
if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	echo json_encode(['result'=>'Неверный идентификатор строения.']);
	exit();
}
require '../configUsers.php';

$ids=(int)$_POST['ids'];

if ($nowUserInfo['credits']<50)
{
	echo json_encode(['result'=>'Недостаточно кредитов! Для ускорения здания необходимо 50 кредитов, а у вас только '.$nowUserInfo['credits']]);
	exit();
}

$getHouse="SELECT a.`level`, UNIX_TIMESTAMP(a.`time`) `endTime`,
									b.`address`,
									c.`name`
					 FROM `house` a,
								`geoObjects` b,
								`geoObjectsTypes` c
					 WHERE b.`id`=".$ids." and `user`=".$nowUserInfo['id']." and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and a.`time`>CURRENT_TIMESTAMP";
$resGetHouse=mysql_query($getHouse) or die(handleError('Не удалось получить информацию о ускоряемом здании.',__FILE__,false,$getHouse,$nowUserInfo['id']));
$house=mysql_fetch_assoc($resGetHouse);
if(!$house)
{
	echo json_encode(['result'=>'Дом для ускрокения не найден.']);
	exit();
}

$nowTime=time();
$nowEndTime=$house['endTime']-$nowTime;
$nowEnd_min=round($nowEndTime/60,3);
$nowEnd_hour=round($nowEnd_min/60,3);
$nowEnd_days=round($nowEnd_hour/24,3);

$afterEndTime=ceil($nowEndTime*0.1);
$afterEnd_min=round($afterEndTime/60,3);
$afterEnd_hour=round($afterEnd_min/60,3);
$afterEnd_days=round($afterEnd_hour/24,3);

$content='Вы уверены что хотите сократить на 90% время работы здания <b>'.$house['name'].' '.$house['level'].' уровня</b> по адресу <i>'.$house['address'].'</i> за 50 <img class="moneyIndicator" src="images/baks.png"><br><br>
	Сейчас до конца действия осталось '.$nowEnd_min.' минут ('.$nowEnd_hour.' часов, '.$nowEnd_days.' дней) и закончится '.date('H:i:s d.m.Y',$house['endTime']).'<br>
	После улучшения действие действие окончится через '.$afterEnd_min.' минут ('.$afterEnd_hour.' часов, '.$afterEnd_days.' дней) и закончится '.date('H:i:s d.m.Y',$nowTime+$afterEndTime).'.<br>
	<input type="button" ids="'.$ids.'" id="speedUpConfirmed" value="Да, ускорить">';

echo json_encode(['result'=>'ok','content'=>$content]);

?>
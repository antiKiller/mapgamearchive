<?php
require "../configUsers.php";
if (!$_POST['id'] || !is_numeric($_POST['id']))
{
	handleError('Неверный идентификатор строения.',__FILE__);
}

$id=$_POST['id'];

$getobjInfo="SELECT a.*,b.`basicCost`,b.`isAuction`,b.`name`
						 FROM `geoObjects` a,
									`geoObjectsTypes` b
						 WHERE a.`id`='".$id."' and a.`type`=b.`id` and b.`isMap`=1";
$resObjInfo=mysql_query($getobjInfo) or die(handleError('Ошибка поиска информации о геообьекте.',__FILE__,false,$getobjInfo,$nowUserInfo['id']));
$rowObjInfo=mysql_fetch_assoc($resObjInfo);
if (!$rowObjInfo)
{
	handleError('Такого обьекта не было найдено.',__FILE__);
}

if ($rowObjInfo['isAuction']==true)
{
	$table='auction';
	$buedType='аукциона';
	$insertObjectSql="INSERT INTO `auction`
												(
													`user`,
													`user_bid`,
													`geoObjectId`,
													`stavka`,
													`time`
												)
											VALUES
												(
													".$nowUserInfo['id'].",
													".$nowUserInfo['id'].",
													".$rowObjInfo['id'].",
													".$rowObjInfo['basicCost'].",
													FROM_UNIXTIME(".(time()+3600*24*3).")
												)";
}
	else
	{
		$table='house';
		$buedType='строения';
		$insertObjectSql="INSERT INTO `house`
												(
													`user`,
													`geoObjectId`
												)
											VALUES
												(
													".$nowUserInfo['id'].",
													".$rowObjInfo['id']."
												)";
	}
$searchObjInBuyed="SELECT `id` FROM `".$table."` WHERE `geoObjectId`=".$rowObjInfo['id'];
$resSearchBuyed=mysql_query($searchObjInBuyed) or die(handleError('Ошибка поиска геообьекте среди купленных.',__FILE__,false,$searchObjInBuyed,$nowUserInfo['id']));
$rowSearchBuyed=mysql_fetch_assoc($resSearchBuyed);
if ($rowSearchBuyed)
{
	handleError('Кто-то уже купил этот обьект.',__FILE__);
}

if ($nowUserInfo['money']<$rowObjInfo['basicCost'])
{
	handleError('Недостаточно монет! Надо '.$rowObjInfo['basicCost'].', а у вас только '.$nowUserInfo['money'],__FILE__);
}

mysql_query("START TRANSACTION");
mysql_query($insertObjectSql) or die(handleError('Ошибка привязки геообьекта к вашему аккаунту!',__FILE__,false,$insertObjectSql,$nowUserInfo['id'],true));
if ($rowObjInfo['isAuction'])
{
	$insertIntoUser="INSERT INTO `auction_user` (`user`,`auc`) SELECT `user`,`id` FROM `auction` WHERE `geoObjectId`=".$rowObjInfo['id'];
	mysql_query($insertIntoUser) or die(handleError('Ошибка создания аукциона!',__FILE__,false,$insertIntoUser,$nowUserInfo['id'],true) );
}
$updateMoney="UPDATE `user` SET `money`=`money`-".$rowObjInfo['basicCost']." WHERE `id`=".$nowUserInfo['id'];
mysql_query($updateMoney) or die(handleError('Ошибка списания монет с вашего счёта!',__FILE__,false,$updateMoney,$nowUserInfo['id'],true));


$logEnter="INSERT INTO `log`
							(`user`,`text`,`geoObjectId`,`money`,`type`)
					 VALUES
							(".$nowUserInfo['id'].",'Покупка ".$buedType." типа <i>".$rowObjInfo['name']."</i>',".$rowObjInfo['id'].",".($rowObjInfo['basicCost']*-1).",'Покупка ".$buedType."')";
mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));
mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
<?php
if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный идентификатор строения!',__FILE__);
}
require '../configUsers.php';

$ids=(int)$_POST['ids'];

if ($nowUserInfo['credits']<SPEED_UP_COST)
{
	handleError('Недостаточно кредитов! Для ускорения здания необходимо 50 кредитов, а у вас только '.$nowUserInfo['credits'],__FILE__);
}

$getHouse="SELECT a.`level`, UNIX_TIMESTAMP(a.`time`) `endTime`,
									b.`address`,
									c.`name`
					 FROM `house` a,
								`geoObjects` b,
								`geoObjectsTypes` c
					 WHERE b.`id`=".$ids." and `user`=".$nowUserInfo['id']." and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and a.`time`>CURRENT_TIMESTAMP";
$resGetHouse=mysql_query($getHouse) or die(handleError('Не удалось получить информацию о ускоряемом здании здании.',__FILE__,false,$getHouse,$nowUserInfo['id']));
$house=mysql_fetch_assoc($resGetHouse);
if(!$house)
{
	handleError('Строение для ускрокения не найдено!',__FILE__);
}

$nowTime=time();
$nowEndTime=$house['endTime']-$nowTime;
$afterEndTime=$nowTime+ceil($nowEndTime*SPEED_UP_REMAINING);

$updateHouse="UPDATE `house` SET `time`=FROM_UNIXTIME(".$afterEndTime.") WHERE `geoObjectId`=".$ids." and `user`=".$nowUserInfo['id'];
$updateUser="UPDATE `user` SET `credits`=`credits`-".SPEED_UP_COST." WHERE `id`=".$nowUserInfo['id'];
$toLog="INSERT INTO `log`
					(`user`,`geoObjectId`,`type`,`credits`,`money`,`text`)
							VALUES
					(".$nowUserInfo['id'].",".$ids.",'Ускорение действия',-50,0,'Вы ускорили на 90% действие над зданием.')";
mysql_query("START TRANSACTION");
mysql_query($updateHouse) or die(handleError('Ошибка обновления времени окончания работ в здании здания.',__FILE__,false,$updateHouse,$nowUserInfo['id'],true));
mysql_query($updateUser) or die(handleError('Ошибка снятия средств за ускорение.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));
mysql_query($toLog) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$toLog,$nowUserInfo['id'],true));
mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);

?>
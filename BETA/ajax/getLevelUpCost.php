<?php
require "../configUsers.php";

$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));
$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$dopQuery='';
if ($objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	if ($mode=='onlyChecked')
	{
		$operator='in';
	}
		else
		{
			$operator='not in';
		}
	$dopQuery="and `geoObjectId` ".$operator." (".$objIds.")";
}

$summCost=0;
$obsjListText='';

$nowObject="SELECT `level`, `address`, `name`, `basicCost`, `basicTime`, `updateCost`, `updateTime`,
									 (`basicCost`*pow(`updateCost`,`level` )) updateCost
						FROM `buildingsUsers`
						WHERE `user`=".$nowUserInfo['id']." and `isUpdated`=1 and `time`<=CURRENT_TIMESTAMP ".$dopQuery;
$resNowObject=mysql_query($nowObject) or die(handleError('Ошибка получения списка обьектов для улучшения.',__FILE__,false,$nowObject,$nowUserInfo['id']));

while($nowObjInfo=mysql_fetch_assoc($resNowObject))
{
	$newIds[]=$objId;
	$cost=$nowObjInfo['updateCost'];
	$time=round($nowObjInfo['basicTime']*pow($nowObjInfo['updateTime'],$nowObjInfo['level']));
	$summCost+=$cost;
	$obsjListText.='<div>
								<b>'.$nowObjInfo['name'].'</b> по адресу <i>'.$nowObjInfo['address'].'</i> до уровня <b>'.($nowObjInfo['level']+1).'</b> за <b>'.number_format($cost, 2, '.', ' ').'</b> <img class="moneyIndicator" src="images/money.png">, потратив на это '.$time.' минут ('.number_format($time/60, 3, '.', '').' часов, '.number_format($time/60/24, 3, '.', '').' дней)
							</div>';
}
if ($summCost<=0)
{
	handleError('Нет зданий для улучшения!',__FILE__);
}

$summCost=round($summCost,2);
$message='<p>Вы уверены что хотите улучшить '.count($newIds).' зданий за '.number_format($summCost, 2, '.', ' ').' <img class="moneyIndicator" src="images/money.png">?</p>
					<center><input type="button" ids="'.$objIds.'" mode="'.$mode.'" id="startLevelUp" value="Улучшить"></center><br>
					<p>Улучшению подвергнутся следующие здания:</p>
					'.$obsjListText;
echo json_encode(['result'=>'ok','message'=>$message]);
?>
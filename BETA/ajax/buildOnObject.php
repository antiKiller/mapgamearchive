<?php
if (!$_POST['obj'])
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}
$_POST['obj']=preg_replace('/[,\ ]+/i', ',', trim($_POST['obj'],' ,'));

if (!$_POST['type'])
{
	handleError('Неверный тип постройки.',__FILE__);
}

require "../configUsers.php";

$objectIdList=explode(',',$_POST['obj']);
$typeId=(int)$_POST['type'];

$newTypeInfo="SELECT *
							FROM `geoObjectsTypes`
							WHERE `id`=".$typeId." and `isMap`=0 and `canBuyedBefore`>CURRENT_TIMESTAMP";
$resTypeInfo=mysql_query($newTypeInfo) or die(handleError('Ошибка поиска типа постройки.',__FILE__,false,$newTypeInfo,$nowUserInfo['id']));
$rowTypeInfo=mysql_fetch_assoc($resTypeInfo);
if (!$rowTypeInfo)
{
	handleError('Не найден тип здания, которое вы хотите построить!',__FILE__);
}


$allCostMoney=round($rowTypeInfo['basicCost']*count($objectIdList),2);
$allCostCredits=round($rowTypeInfo['creditsCost']*count($objectIdList));

if ($nowUserInfo['money']<$allCostMoney)
{
	handleError('У вас недостаточно монет для строительства! Необходимо '.number_format($allCostMoney, 2, '.', ' ').' монет, а у вас только '.number_format($nowUserInfo['money'], 2, '.', ' '),__FILE__);
}

if ($nowUserInfo['credits']<$allCostCredits)
{
	handleError('У вас недостаточно кредитов для строительства! Необходимо '.number_format($allCostCredits, 2, '.', ' ').' кредитов, а у вас только '.number_format($nowUserInfo['credits'], 2, '.', ' '),__FILE__);
}

mysql_query('START TRANSACTION');
$updates=[];
$toLogVals=[];
foreach ($objectIdList as $key=>$objectId)
{
	$checkCanBuild="SELECT *, X(`coords`) `pos1`, Y(`coords`) `pos2`
									FROM `buildingsUsers`
									WHERE `geoObjectId`=".$objectId." and `isBuilded`=1";
	$resCheckObject=mysql_query($checkCanBuild) or die(handleError('Ошибка проверки обьекта на возможность постройки.',__FILE__,false,$checkCanBuild,$nowUserInfo['id'],true));
	$rowCheckObject=mysql_fetch_assoc($resCheckObject);
	if (!$rowCheckObject)
	{
		handleError('Место для строительства не найдено!',__FILE__,true,'','',true);
	}

	$update1="UPDATE `geoObjects` SET `type`=".$rowTypeInfo['id']." WHERE `id`=".$rowCheckObject['geoObjectId'];
	mysql_query($update1) or die(handleError('Не удалось обновить тип строения. Вероятнее всего обнаружен дубль.',__FILE__,false,$update1,$nowUserInfo['id'],true));

	$doxod=0;
	if ($rowTypeInfo['fixClients']>0)
	{
		$doxod=$rowTypeInfo['fixClients']*$rowTypeInfo['basicDoxod'];
	}
	$timeEnd=time()+round($rowTypeInfo['basicTime']*60);
	$update2="UPDATE `house`
						SET
								`level`=1,
								`doxod`=".$doxod.",
								`clients`=".$rowTypeInfo['fixClients'].",
								`oneClientDoxod`='".$rowTypeInfo['basicDoxod']."',
								`bonus`='".$rowTypeInfo['basicBonus']."',
								`action`='Строительство',
								`time`=FROM_UNIXTIME(".$timeEnd.")
						WHERE `id`=".$rowCheckObject['id'];
	$updates[]=$update2;
	mysql_query($update2) or die(handleError('Не удалось обновить информацию о строении.',__FILE__,false,$update2,$nowUserInfo['id'],true));

	if ($rowTypeInfo['isHouse'])
	{
		$tableRecalc='reCalcHouses';
	}
		elseif ($rowTypeInfo['isEnterprise'])
		{
			$tableRecalc='reCalc';
		}

	if (!$rowTypeInfo['isSocial'])
	{
		$toRecalcSql="INSERT INTO `".$tableRecalc."` (`houseId`,`notRecalcUntil`)
										VALUES
											(".$rowCheckObject['id'].",FROM_UNIXTIME(".($timeEnd+5)."))";
		mysql_query($toRecalcSql) or die(handleError('Не удалось поставить здание в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));


		$getClientsTypes="SELECT GROUP_CONCAT(`clientObj`) `clientObj` FROM `clientsForBuildngs` WHERE `parentObj`=".$rowTypeInfo['id'];
		$resClientsQuery=mysql_query($getClientsTypes);
		$clientsQuery=mysql_fetch_assoc($resClientsQuery);
		$clientsTypes=$clientsQuery['clientObj'];

		if ($clientsTypes && $rowTypeInfo['radius'])
		{

			$toRecalcSql="INSERT INTO `reCalcHouses` (`houseId`,`notRecalcUntil`)
											SELECT `id`,FROM_UNIXTIME(".($timeEnd+5).")
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$rowCheckObject['pos1'].", ".$rowCheckObject['pos2']."),`coords`)<=".$rowTypeInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить дома в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));


			$toRecalcSql="INSERT INTO `reCalc` (`houseId`,`notRecalcUntil`)
											SELECT `id`,FROM_UNIXTIME(".($timeEnd+5).")
											FROM `buildingsUsers`
											WHERE `isEnterprise`=1 and `geoObjectId` in (".$clientsTypes.") and
														getDistBeforePoints(POINT(".$rowCheckObject['pos1'].", ".$rowCheckObject['pos2']."),`coords`)<=".$rowTypeInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить предприятия в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));
		}
	}
		else
		{
			$toRecalcSql="INSERT INTO `reCalcHouses` (`houseId`,`notRecalcUntil`)
											SELECT `id`,FROM_UNIXTIME(".($timeEnd+5).")
											FROM `buildingsUsers`
											WHERE `isHouse`=1 and getDistBeforePoints(POINT(".$rowCheckObject['pos1'].", ".$rowCheckObject['pos2']."),`coords`)<=".$rowTypeInfo['radius'];
			mysql_query($toRecalcSql) or die(handleError('Не удалось поставить дома в очередь на пересчёт.',__FILE__,false,$toRecalcSql,$nowUserInfo['id'],true));
		}

	$toLogVals[]="(".$nowUserInfo['id'].",'Постройка <i>".$rowTypeInfo['name']."</i>',".$rowCheckObject['geoObjectId'].",".($rowTypeInfo['basicCost']*-1).",".($rowTypeInfo['creditsCost']*-1).",'Строительство')";
}

if (count($toLogVals)>0)
{
	$logEnter="INSERT INTO `log`
									(`user`,`text`,`geoObjectId`,`money`,`credits`,`type`)
							 VALUES ".implode(',',$toLogVals);
	mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));

	$update3="UPDATE `user`
						SET `money`=`money`-".$allCostMoney.",
								`credits`=`credits`-".$allCostCredits."
					  WHERE `id`=".$nowUserInfo['id']." ";
	mysql_query($update3) or die(handleError('Не удалось снять монеты с вашего счёта.',__FILE__,false,$update3,$nowUserInfo['id'],true));
}

mysql_query('COMMIT');

echo json_encode(['result'=>'ok']);
?>
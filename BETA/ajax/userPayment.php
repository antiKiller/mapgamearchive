<?php
include '../configUsers.php';

if (!$_POST['rubles'] || !is_numeric($_POST['rubles']) || $_POST['rubles']<=0)
{
	handleError('Некорректная сумма была запрошена вами!',__FILE__);
}

if ($_POST['rubles']>40)
{
	handleError('Единоразовая выплата не может быть более 40 рублей!',__FILE__);
}

if (!$_POST['account'] || !is_numeric($_POST['account']))
{
	handleError('Введён некорректный кошелёк Яндекс.Денег!',__FILE__);
}

if (!$_SESSION['kurs'] || $_SESSION['kurs']<=0)
{
	handleError('Некорректный курс обмена!',__FILE__);
}

$nowTime=time();
if (!$_SESSION['kursCreated'] || ($nowTime-$_SESSION['kursCreated'])>120)
{
	handleError('Курс уже устарел! Курс действителен только в течении 2-х минут с момента открытия формы запроса выплаты.',__FILE__);
}

$rubles=$_POST['rubles'];
$account=$_POST['account'];

$mustMoney=$_SESSION['kurs']*$rubles;

if ($nowUserInfo['money']<$mustMoney)
{
	handleError('У вас недостаточно <img class="moneyIndicator" src="images/money.png"> для выплаты! Необходимо '.number_format($mustMoney,2,'.',' ').' <img class="moneyIndicator" src="images/money.png">, а у вас только '.number_format($nowUserInfo['money'],2,'.',' ').'!',__FILE__);
}

$maxSumm=MONTH_USER_PAYMENTS_LIMIT;
$alreadyOutput="SELECT SUM(`summ`) output FROM `moneyOutput` WHERE `date`>(CURRENT_TIMESTAMP - INTERVAL 1 MONTH)";
$resOutput=mysql_query($alreadyOutput) or die(handleError('Ошибка получения суммы уже произведённых выплат.',__FILE__,false,$alreadyOutput,$nowUserInfo['id']));
$output=mysql_fetch_assoc($resOutput);
if ($output['output']>0)
{
	$maxSumm-=$output['output'];
}

if ($maxSumm<=0)
{
	handleError('Извините, но лимит выплат в '.MONTH_USER_PAYMENTS_LIMIT.' рублей за этот месяц уже выполнен. Попробуйте запросить выплату позже!',__FILE__);
}

$alreadyOutput="SELECT SUM(`summ`) output FROM `moneyOutput` WHERE `user`=".$nowUserInfo['id']." and `date`>(CURRENT_TIMESTAMP - INTERVAL 5 DAY)";
$resOutput=mysql_query($alreadyOutput) or die(handleError('Ошибка получения суммы уже произведённых выплат.',__FILE__,false,$alreadyOutput,$nowUserInfo['id']));
$output=mysql_fetch_assoc($resOutput);
if ($output['output']>40)
{
	handleError('Ваш текущий 5-ти дневный лимит выплат - 40 рублей, уже превышен. Попробуйте запросить выплату завтра.',__FILE__);
}

if ($output['output']>0 && ($rubles+$output['output'])>40)
{
	handleError('Ваш текущий дневной лимит выплат - 40 рублей в день. За прошедшие 5 дней вы уже запросили выплату на '.$output['output'].' рублей. Итого, сегодня вам доступно для запроса только '.(40-$output['output']).' рублей, а вы запросили '.$rubles.' рублей!',__FILE__);
}

mysql_query('START TRANSACTION');

$updateUser="UPDATE `user` SET `money`=`money`-".$mustMoney." WHERE `id`=".$nowUserInfo['id'];
mysql_query ($updateUser) or die(handleError('Ошибка снятия монет с вашего аккаунта!',__FILE__,false,$updateUser,$nowUserInfo['id'],true));

$paymentInsert="INSERT INTO `moneyOutput` (`user`,`summ`,`kurs`,`purse`)
																						VALUES
																					(".$nowUserInfo['id'].",".$rubles.",".$_SESSION['kurs'].",'".$account."')";
mysql_query ($paymentInsert) or die(handleError('Ошибка записи информации о выплате!',__FILE__,false,$paymentInsert,$nowUserInfo['id'],true));

$logEnters="INSERT INTO `log` (`user`,`type`,`money`,`text`)
																VALUES
															(".$nowUserInfo['id'].",'Заказ выплаты',".($mustMoney*-1).",'Запрос выплаты ".$rubles." рублей на кошелёк ".$account." Яндекс.Денег')";
mysql_query ($logEnters) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnters,$nowUserInfo['id'],true));

mysql_query('COMMIT');

echo json_encode(['result'=>'ok']);
?>
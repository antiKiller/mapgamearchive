<?php
require "../configUsers.php";

if (!$_SESSION['payment'])
{
	echo json_encode(['result'=>'Устаревшие данные оплаты, попробуйте ещё раз!']);
	exit();
}

switch ($_POST['service'])
{
	case 'w1':
		$form='<form method="post" action="https://merchant.w1.ru/checkout/default.aspx" accept-charset="UTF-8" target="_blank" id="paymentForm">
	<input name="WMI_MERCHANT_ID"  type="hidden"   value="WMI_MERCHANT_ID"/>
	<input name="WMI_PAYMENT_AMOUNT" type="hidden"  value="'.$_SESSION['payment']['value'].'"/>
	<input name="WMI_CURRENCY_ID"  type="hidden"   value="643"/>
	<input name="WMI_PAYMENT_NO"  type="hidden"   value="'.$_SESSION['payment']['id'].'"/>
	<input name="WMI_DESCRIPTION"  type="hidden"  value="Покупка '.$_SESSION['payment']['credits'].' кредитов в игре Битва олигархов"/></form>';

		break;
	case 'sp':
		$form='<form action="http://sprypay.ru/sppi/" method="POST" accept-charset="utf-8" target="_blank" id="paymentForm">
  <input type="hidden" name="spShopId" value="shop_id">
  <input type="hidden" name="spShopPaymentId" value="'.$_SESSION['payment']['id'].'">
  <input type="hidden" name="spCurrency" value="rur">
  <input type="hidden" name="spPurpose" value="Покупка '.$_SESSION['payment']['credits'].' кредитов в игре Битва олигархов">
  <input type="hidden" name="spAmount" value="'.$_SESSION['payment']['value'].'">
  <input type="hidden" name="spUserDataUserId" value="'.$nowUserInfo['id'].'">
</form>';
		break;
	
	default:
		echo json_encode(['result'=>'Некорректный сервис оплаты.']);
		exit();
}

$form.='<script>$("#paymentForm").submit();</script>';

echo json_encode(['result'=>'ok','form'=>$form]);

?>

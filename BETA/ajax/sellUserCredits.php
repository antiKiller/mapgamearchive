<?php
require '../configUsers.php';

if (!is_numeric($_POST['sellId']))
{
	handleError('Неверный идентификатор ордера на покупку кредитов!',__FILE__);
}
$ids=(int)$_POST['sellId'];

if (!$_POST['credits'] || !is_numeric($_POST['credits']))
{
	handleError('Неверная идентификатор ордера на покупку кредитов.!',__FILE__);
}
$credits=(int)$_POST['credits'];

if ($credits<=0)
{
	handleError('Количество кредитов, которое вы хотите продать, должно быть больше нуля!',__FILE__);
}

if ($nowUserInfo['credits']<$credits)
{
	handleError('У вас всего '.$nowUserInfo['credits'].' кредитов, а вы пытаетесь продать '.$credits.'. Нельзя продать больше кредитов чем у вас есть!',__FILE__);
}

if ($ids!=0)
{
	$getSelledRow="SELECT * FROM `exchange` WHERE `id`=".$ids." and `type`='buy'";
	$resGetSelledRow=mysql_query($getSelledRow) or die(handleError('Не удалось получить список продавцов кредитов.',__FILE__,false,$getSelledRow,$nowUserInfo['id']));
	$selledRow=mysql_fetch_assoc($resGetSelledRow);
	if (!$selledRow)
	{
		handleError('Такая заявка на бирже не найдена!',__FILE__);
	}

	if ($selledRow['credits']<$credits)
	{
		handleError('Максимум вы можете продать '.$selledRow['credits'].', а вы пытаетесь продать '.$credits,__FILE__);
	}

	$profit=round($credits*$selledRow['kurs'],2);

	$updateUser="UPDATE `user` SET `credits`=`credits`-".$credits.", `money`=`money`+".$profit." WHERE `id`=".$nowUserInfo['id'];
	$updateOtherUser="UPDATE `user` SET `credits`=`credits`+".$credits." WHERE `id`=".$selledRow['user'];
	$toLog="INSERT INTO `log` (`user`,`otherUser`,`text`,`money`,`credits`,`type`)
											VALUES
														(".$nowUserInfo['id'].",".$selledRow['user'].",'Вы продали свои кредиты на бирже',".$profit.",".($credits*-1).",'Продажа кредитов на бирже'),
														(".$selledRow['user'].",".$nowUserInfo['id'].",'По вашей заявке куплены кредиты на бирже',0,".$credits.",'Заявки на бирже')";
	$updateExchange="UPDATE `exchange` SET `credits`=`credits`-".$credits." WHERE `id`=".$selledRow['id'];

	mysql_query("START TRANSACTION");

	mysql_query($updateUser) or die(handleError('Не удалось обновить данные игрока.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));
	mysql_query($updateOtherUser) or die(handleError('Не удалось обновить данные игрока.',__FILE__,false,$updateOtherUser,$nowUserInfo['id'],true));
	mysql_query($toLog) or die(handleError('Не удалось записать данные в журнал.',__FILE__,false,$toLog,$nowUserInfo['id'],true));
	mysql_query($updateExchange) or die(handleError('Не обновить данные биржи.',__FILE__,false,$updateExchange,$nowUserInfo['id'],true));

	mysql_query("COMMIT");
}
	else
	{
		$profit=round($credits*100,2);

		$updateUser="UPDATE `user` SET `credits`=`credits`-".$credits.", `money`=`money`+".$profit." WHERE `id`=".$nowUserInfo['id'];
		$toLog="INSERT INTO `log` (`user`,`text`,`money`,`credits`,`type`)
												VALUES
															(".$nowUserInfo['id'].",'Вы продали свои кредиты на бирже ZOG`у',".$profit.",".($credits*-1).",'Продажа кредитов на бирже')";

		mysql_query("START TRANSACTION");

		mysql_query($updateUser) or die(handleError('Не удалось обновить данные игрока.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));
		mysql_query($toLog) or die(handleError('Не удалось записать данные в журнал.',__FILE__,false,$toLog,$nowUserInfo['id'],true));

		mysql_query("COMMIT");
	}

echo json_encode(['result'=>'ok','sql'=>$updateUser]);
?>
<?
require "../configUsers.php";

$area=$_POST['area'];
$zoom=$_POST['zoom'];
$center=$_POST['center'];
$visibleObjs=$_POST['visibleObjs'];
$visibleType=$_POST['types'];
$onlyCosted=$_POST['onlyCosted'];
if (!$visibleObjs || count($visibleObjs)<=0)
{
	echo json_encode(['result'=>'ok','objsList'=>[]]);
	exit();
}
$types='';
$typesArr=[];
$objectsArr=[];
$usersList=[];
if ($zoom<15)
{
	if ($zoom<=4)
	{
		$typesArr[]=7;//страна
	}

	if ($zoom>4 && $zoom<=6)
	{
		$typesArr[]=7;//страна
		$typesArr[]=6;//область
	}

	if ($zoom>6 && $zoom<=8)
	{
		$typesArr[]=7;//страна
		$typesArr[]=6;//область
		$typesArr[]=5;//регион
		$typesArr[]=4;//город
	}

	if ($zoom>8 && $zoom<=12)
	{
		$typesArr[]=7;//страна
		$typesArr[]=6;//область
		$typesArr[]=5;//регион
		$typesArr[]=4;//город
		$typesArr[]=3;//район
	}

	if ($zoom>12)
	{
		$typesArr[]=7;//страна
		$typesArr[]=6;//область
		$typesArr[]=5;//регион
		$typesArr[]=4;//город
		$typesArr[]=3;//район
		$typesArr[]=2;//улица
	}
	$types="and d.`id` in (".implode(',', $typesArr).")";
}

switch ($visibleType)
{
	case 'my':
		$dopHouses='and c.`user`='.$nowUserInfo['id'];
		$dopAuc='and b.`user`='.$nowUserInfo['id'];
		break;

	case 'enemy':
		$dopHouses='and c.`user`!='.$nowUserInfo['id'];
		$dopAuc='and b.`user`!='.$nowUserInfo['id'];
		break;

	default:
		$dopHouses='';
		$dopAuc='';
		break;
}

if ($onlyCosted)
{
	$dopHouses.=' and c.`isCosted`=1';
}

$objectsList="
SELECT a.`id`, x(a.`coords`) pos1, y(a.`coords`) pos2, b.`user` aucUser, b.`user_bid` aucUserBid, null houseUser, d.`isAuction`,d.`img`, d.`radius`
FROM `geoObjects` a,
	  `auction` b,
	  `geoObjectsTypes` d
WHERE WITHIN(`coords` ,GeomFromText('".poligonFromPoints($area[0],$area[1])."')) and d.`id` in (".implode(',',$visibleObjs).") and
	     a.`id`=b.`geoObjectId` and d.`isAuction`=1 and a.`type`=d.`id` ".$dopAuc." ".$types."

UNION ALL

SELECT a.`id`, x(a.`coords`) pos1, y(a.`coords`) pos2, null aucUser, null aucUserBid, c.`user` houseUser, d.`isAuction`,d.`img`, d.`radius`
FROM `geoObjects` a,
	  `house` c,
	  `geoObjectsTypes` d
WHERE WITHIN(`coords` ,GeomFromText('".poligonFromPoints($area[0],$area[1])."')) and d.`id` in (".implode(',',$visibleObjs).") and
	     a.`id`=c.`geoObjectId` and d.`isAuction`=0 and a.`type`=d.`id` ".$dopHouses." ".$types;

$resObjectsList=mysql_query($objectsList) or die(handleError('Ошибка выборки списка обьектов на карте.',__FILE__,false,$objectsList,$nowUserInfo['id']));

while ($object=mysql_fetch_assoc($resObjectsList))
{
	$nowObj=$object;
	if (!$nowObj['img'])
	{
		if ($nowObj['isAuction'])
		{
			$userId=$nowObj['aucUser'];
		}
			else
			{
				$userId=$nowObj['houseUser'];
			}
		if (!$usersList[$userId])
		{
			$getUserInfo="SELECT `name`,`photo` FROM `user` WHERE `id`=".$userId;
			$resUserInfo=mysql_query($getUserInfo) or die(handleError('Ошибка пользователя, которому принадлежит обьект.',__FILE__,false,$getUserInfo,$nowUserInfo['id']));
      $usersList[$userId]=mysql_fetch_assoc($resUserInfo);
		}
		$nowObj['userId']=$userId;
		$nowObj['userName']=$usersList[$userId]['name'];
		$nowObj['img']=$usersList[$userId]['photo'];
		$nowObj['isThisUser']=false;
		if ($userId==$nowUserInfo['id'])
		{
			$nowObj['isThisUser']=true;
		}
	}
		else
		{
			$nowObj['img']='/images/'.$nowObj['img'].'.png';
		}
	$objectsArr[]=$nowObj;
}

$updateLastPos="UPDATE `user` SET `lastZoom`=".$zoom.", `lastMapCenter`=POINT(".$center[0].",".$center[1].") WHERE `id`=".$nowUserInfo['id'];
mysql_query($updateLastPos) or die(handleError('Не удалось обновить данные о последнем положении пользователя на карте.',__FILE__,false,$updateLastPos,$nowUserInfo['id']));

echo json_encode(['result'=>'ok','objsList'=>$objectsArr,'sql'=>$objectsList]);
?>
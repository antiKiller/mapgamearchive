<?php
require "../configUsers.php";

if (!$_POST['newName'])
{
	handleError('Имя не может быть пустым!',__FILE__);
}

$name=$_POST['newName'];

if (mb_strlen($name,'UTF-8')>60)
{
	handleError('Имя слишком длинное! Максимум 60 символов, а у вас '.mb_strlen($name,'UTF-8'),__FILE__);
}

$updateName="UPDATE `user` SET `name`='".htmlentities($name, ENT_QUOTES, 'UTF-8')."' WHERE `id`=".$nowUserInfo['id'];
mysql_query($updateName) or die(handleError('Не удалось обновить ваше имя.',__FILE__,false,$updateName,$nowUserInfo['id']));

echo json_encode(['result'=>'ok']);
?>
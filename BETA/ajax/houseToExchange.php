<?php
require "../configUsers.php";

if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный список идентификаторов строений!',__FILE__);
}

if (!is_numeric($_POST['money']) || $_POST['credits']<0)
{
	handleError('Неверное количество монет!',__FILE__);
}

if (!is_numeric($_POST['credits']) || $_POST['credits']<0)
{
	handleError('Неверное количество кредитов!',__FILE__);
}


$ids=(int)$_POST['ids'];
$monets=(int)$_POST['money'];
$credits=(int)$_POST['credits'];

if ($monets==0 && $credits==0)
{
	handleError('Строение должно сколько-то стоить в монетах или кредитах, отдавать за "просто так" нельзя :)',__FILE__);
}

$getInfoOfHouse="SELECT `level`, `address`, `name`, `basicCost`, `creditsCost`, `creditsCost`, `updateCost`
								 FROM `buildingsUsers`
								 WHERE `geoObjectId`=".$ids." and `isCosted`=0 and `user`=".$nowUserInfo['id'];
$resInfoOfHouse=mysql_query($getInfoOfHouse) or die(handleError('Ошибка получения информации о строении.',__FILE__,false,$getInfoOfHouse,$nowUserInfo['id']));
$house=mysql_fetch_assoc($resInfoOfHouse);

if (!$house)
{
	handleError('Неверный список идентификаторов строений!',__FILE__);
}

$moneyCost=$house['basicCost'];
$creditsCost=$house['creditsCost'];
if ($house['level']>1)
{
	for ($i=2;$i<$house['level'];$i++)
	{
		$moneyCost+=$house['basicCost']*pow($house['updateCost'],$house['level']);
	}
}

$moneyCost=round($moneyCost);

$maxMoneyCost=ceil($moneyCost*3);

if ($monets>$maxMoneyCost)
{
	handleError('Максимально допустимая цена в <b>'.number_format($maxMoneyCost, 0, '.', ' ').' <img class="moneyIndicator" src="images/money.png"></b>, а вы пытаетесь продать за '.number_format($monets, 0, '.', ' ').' <img class="moneyIndicator" src="images/money.png">!',__FILE__);
}

$monets=round($monets*1.13,2);

$updateHouse="UPDATE `house` SET `isCosted`=1, `cost`=".$monets.", `costCredits`=".$credits." WHERE `geoObjectId`=".$ids." and `user`=".$nowUserInfo['id'];
$updateLog="INSERT INTO `log` (`user`,`geoObjectId`,`text`,`type`) VALUES (".$nowUserInfo['id'].", ".$ids.",'Выставление на продажу здания за ".$monets." <img class=\"moneyIndicator\" src=\"images/money.png\"> и ".$credits." <img class=\"moneyIndicator\" src=\"images/baks.png\">', 'Выставление здания на продажу')";

mysql_query("START TRANSACTION");
mysql_query($updateHouse) or die(handleError('Не удалось обновить данные строения.',__FILE__,false,$updateHouse,$nowUserInfo['id'],true));
mysql_query($updateLog) or die(handleError('Не удалось записать информацию в лог.',__FILE__,false,$updateLog,$nowUserInfo['id'],true));
mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
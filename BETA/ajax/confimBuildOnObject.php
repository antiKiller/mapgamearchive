<?php
$_POST['obj']=preg_replace('/[,\ ]+/i', ',', trim($_POST['obj'],' ,'));
if (!$_POST['obj'])
{
	echo json_encode(['result'=>'Неверный список идентификаторов строений.']);
	exit();
}
if (!$_POST['type'])
{
	echo json_encode(['result'=>'Неверный тип постройки.']);
	exit();
}

require "../configUsers.php";

$objectIdList=explode(',',$_POST['obj']);
$typeId=(int)$_POST['type'];

$newTypeInfo="SELECT `name`, `basicCost`, `creditsCost`
							FROM `geoObjectsTypes`
							WHERE `id`=".$typeId." and `isMap`=0 and `canBuyedBefore`>CURRENT_TIMESTAMP";
$resTypeInfo=mysql_query($newTypeInfo) or die(handleError('Ошибка поиска типа постройки.',__FILE__,false,$newTypeInfo,$nowUserInfo['id']));
$rowTypeInfo=mysql_fetch_assoc($resTypeInfo);
if (!$rowTypeInfo)
{
	echo json_encode(['result'=>'Не найден тип здания, которое вы хотите построить!']);
	exit();
}


$addreses='';
$newIds=[];
foreach ($objectIdList as $key=>$objectId)
{
	$checkCanBuild="SELECT b.`address`
									FROM `geoObjectsTypes` a,
											 `geoObjects` b
									WHERE a.`id`=b.`type` and b.`id`=".$objectId." and `isBuilded`=1 and `canBuyedBefore`>CURRENT_TIMESTAMP";
	$resCheckObject=mysql_query($checkCanBuild) or die(handleError('Ошибка проверки обьекта на возможность постройки.',__FILE__,false,$checkCanBuild,$nowUserInfo['id']));
	$rowCheckObject=mysql_fetch_assoc($resCheckObject);
	if (!$rowCheckObject)
	{
		continue ;
	}
	$newIds[]=$objectId;
	$addreses.=$rowCheckObject['address'].'<br>';
}

$allCostMoney=round($rowTypeInfo['basicCost']*count($objectIdList),2);
$allCostCredits=round($rowTypeInfo['creditsCost']*count($objectIdList));

if ($nowUserInfo['money']<$allCostMoney)
{
	echo json_encode(['result'=>'У вас недостаточно монет для строительства! Необходимо '.number_format($allCostMoney, 2, '.', ' ').' монет, а у вас только '.number_format($nowUserInfo['money'], 2, '.', ' ')]);
	exit();
}

if ($nowUserInfo['credits']<$allCostCredits)
{
	echo json_encode(['result'=>'У вас недостаточно кредитов для строительства! Необходимо '.number_format($allCostCredits, 2, '.', ' ').' кредитов, а у вас только '.number_format($nowUserInfo['credits'], 2, '.', ' ')]);
	exit();
}

$content='Для строительства '.count($objectIdList).' построек типа <b>'.$rowTypeInfo['name'].'</b> вам потребуется '.number_format($allCostMoney, 2, '.', ' ').' <img class="moneyIndicator" src="images/money.png"> и '.number_format($allCostCredits, 2, '.', ' ').' <img class="moneyIndicator" src="images/baks.png">.<br>
	<input type="button" value="Построить" objId="'.implode(',',$objectIdList).'" typeId="'.$typeId.'" id="buildAllNow"><br>
	Строительство здания "'.$rowTypeInfo['name'].'" произойдёт по адресам:<br><b>'.$addreses.'</b>';

echo json_encode(['result'=>'ok','content'=>$content]);
?>
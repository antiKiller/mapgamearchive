<?php
require '../configUsers.php';

if (!$_POST['nowPage'] || !is_numeric($_POST['nowPage']) || !$_POST['maxPage'] || !is_numeric($_POST['maxPage']) || $_POST['nowPage']>$_POST['maxPage'] || $_POST['nowPage']<=0)
{
	echo json_encode(['result'=>'Неверная тип рейтинга.']);
	exit();
}

$ratingsList=['oligarchs','managers','auctioneers','patrons','industrialists','kapitalization'];
if (!in_array($_POST['ratingType'], $ratingsList))
{
	echo json_encode(['result'=>'Неверный тип рейтинга.']);
	exit();
}

$page=$_POST['nowPage'];
$ratingType='`'.$_POST['ratingType'].'`';
$toPage=$_POST['toPage'];
$maxPage=$_POST['maxPage'];

switch ($toPage)
{
	case 'next': $page++; break;
	case 'prev': $page--; break;
	case 'first': $page=1; break;
	case 'last': $page=$maxPage; break;
	default:{
						if(is_numeric($toPage) && $toPage>0 && $toPage<=$maxPage)
						{
							$page=(int)$toPage;
						}
						break;
					}
}

$getNowUserPos="SELECT *
								FROM
								(
									SELECT `id`, @i:=@i+1 AS `rownumber`
									FROM `user`,
												(
													SELECT @i:=0
												) AS `RowNumberTable`
									ORDER BY ".$ratingType." desc, `id` desc
								) q
								WHERE `id`=".$nowUserInfo['id'];
$resNowUserPos=mysql_query($getNowUserPos) or die(handleError('Ошибка получения вашей позиции в рейтинге.',__FILE__,false,$getNowUserPos,$nowUserInfo['id']));
$nowUserNumber=mysql_fetch_assoc($resNowUserPos);
$nowUserPage=ceil($nowUserNumber['rownumber']/10);


$startLimit=($page-1)*10;
$nowPos=$startLimit+1;
$displayedRatingsUsers=$nowPos.'-'.($startLimit+10);
$peoples='';

$getUsers="SELECT `id`,`name`, ".$ratingType." koeff, `photo_big`, UNIX_TIMESTAMP(`lastEnter`) lastEnter, `lastPage`
					 FROM `user`
					 ORDER BY ".$ratingType." desc, `id` desc
					 LIMIT ".$startLimit.", 10";
$resGetUsers=mysql_query($getUsers) or die(handleError('Ошибка получения списка игроков по рейтингу.',__FILE__,false,$getUsers,$nowUserInfo['id']));
while($ratingUser=mysql_fetch_assoc($resGetUsers))
{
	if ($ratingUser['lastEnter']>=(time()-60))
	{
		$isOnline='Online ('.$ratingUser['lastPage'].')';
	}
		else
		{
			$isOnline='Заходил в игру '.date('H:i:s d.m.Y',$ratingUser['lastEnter']);
		}
	$peoples.='<div class="nowGamer">
							<div class="ratingPos">'.($nowPos++).'</div>
							<div class="gamerName openUserProfile" userId="'.$ratingUser['id'].'"><img src="'.$ratingUser['photo_big'].'"> '.$ratingUser['name'].'</div>
							<div class="onlineStatus">'.$isOnline.'</div>
							<div class="ratingValue">'.$ratingUser['koeff'].'</div>
						 </div>';
}

switch ($_POST['ratingType'])
{
	case 'oligarchs':
		$descr='По сути это рейтинг по капитализации. Значение рейтинга высчитывается по формуле:<br>
			(<br>
			&nbsp; (кол-во монет + кол-во купленных кредитов + ставки на аукционы) * 0.25 +<br>
			&nbsp; (кол-во кредитов + кол-во проданных кредитов) * 0.2 +<br>
			&nbsp; (доход от зданий + доход от аукционов) * 0.3 +<br>
			&nbsp; номинальная стоимость ваших зданий * 0.25
			) /<br>
			1000';
		break;

	case 'managers':
		$descr='Это рейтинг по эффективности. Учитывает доходы и расходы за последнее время. Значение рейтинга высчитывается по формуле:<br>
			(<br>
			&nbsp; (доход в монетах за последние 7 дней + доход в кредитах за последние 7 дней) /<br>
			&nbsp; ((расходы в монетах за последние 7 дней + расходы в кредитах за последние 7 дней) * -1)<br>
			) *<br>
			 100000';
		break;

	case 'auctioneers':
		$descr='Рейтинг игроков, предпочитающих аукционы строительству и чей доход зависит от доходности аукционов больше чем от домов и предприятий. Значение рейтинга высчитывается по формуле:<br>
			&nbsp; сумма ставок на аукционы / количество ставок на аукционы * 0.3 +<br>
			&nbsp; доход от аукционов / кол-во ваших аукционов * 0.25 +<br>
			&nbsp; кол-во ваших аукционов / кол-во ваших зданий * 0.2 +<br>
			&nbsp; доход от ваших аукционов / доход от ваших зданий * 0.25';
		break;

	case 'patrons':
		$descr='Рейтинг по получаемым и даваемым бонусам социалных зданий (школ, больниц, памятников). Значение рейтинга высчитывается по формуле:<br>
			&nbsp; (кол-во жителей в домах + кол-во социальных зданий) / кол-во клиентов предприятий * 0.2 +<br>
			&nbsp; доход от зданий с жителями / доход от всех зданий * 0.3 +<br>
			&nbsp; бонус от социальных зданий * кол-во социальных зданий * 0.3 +<br>
			&nbsp; (номинальная стоимость домов с жителями + номинальная стоимость социальных зданий) / стоимость предприятий * 0.2)';
		break;

	case 'industrialists':
		$descr='Рейтинг по получаемому доходу от предприятий. По сути обратен социальному рейтингу. Значение рейтинга высчитывается по формуле:<br>
			&nbsp; кол-во клиентов предприятий / (кол-во жителей в домах + кол-во социальных зданий) * 0.2 +<br>
			&nbsp; доход от предприятий / доход от всех зданий * 0.3 +<br>
			&nbsp; кол-во клиентов предприятий * доход от предприятий * 0.3 +<br>
			&nbsp; стоимость предприятий / (номинальная стоимость домов с жителями + номинальная стоимость социальных зданий) * 0.2)';
		break;

	case 'kapitalization':
		$descr='Рейтинг по капитализации. По сути, это игроки, которые имеют наибольшее количество свободных денег. Капитализация учитывает только монеты и рассчитывается по формуле:<br>
			&nbsp; сумма монет на счету +<br>
			&nbsp; сумма монет на ставках на аукционы +<br>
			&nbsp; стоимость всех зданий (учитывается только стоимость улучшения до текущего уровня) +<br>
			&nbsp; занятые на бирже в качестве заявки на покупку кредитов монеты';
		break;

	default:
		break;
}

echo json_encode(['result'=>'ok','peoples'=>$peoples,'displayerUsers'=>$displayedRatingsUsers,'descr'=>$descr,'page'=>$page,'nowUserPos'=>$nowUserNumber['rownumber'],'nowUserPage'=>$nowUserPage]);
?>
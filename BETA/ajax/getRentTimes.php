<?php
require "../configUsers.php";
$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));

$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$dopQuery='';
if ($objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	if ($mode=='onlyChecked')
	{
		$operator='in';
	}
		else
		{
			$operator='not in';
		}
	$dopQuery="and `geoObjectId` ".$operator." (".$objIds.")";
}

$getDoxod="SELECT SUM(`doxod`) profit,
									COUNT(`id`) houses
					 FROM `buildingsUsers`
					 WHERE `user`=".$nowUserInfo['id']." and `time`<=CURRENT_TIMESTAMP and `doxod`>0 and
								 (`isHouse`=1 or `isEnterprise`=1) and `isCosted`=0 ".$dopQuery;
$resDoxod=mysql_query($getDoxod) or die(handleError('Ошибка получения суммы дохода.',__FILE__,false,$getDoxod,$nowUserInfo['id']));
$doxod=mysql_fetch_assoc($resDoxod);
if (!$doxod)
{
	handleError('Не найдено ни одного дома, доступного для сдачи в аренду.',__FILE__);
}

$profit=$doxod['profit'];

$countOfHouses="SELECT count(`id`) housesCount FROM `house` WHERE `user`=".$nowUserInfo['id'];
$resCountOfHouse=mysql_query($countOfHouses) or die(handleError('Не удалось получить кол-во ваших строений.',__FILE__,false,$countOfHouses,$nowUserInfo['id'],true));
$countHouses=mysql_fetch_assoc($resCountOfHouse);
$persent=floor($countHouses['housesCount']/100)*0.2;

$tax=0;
if ($persent>0)
{
	$taxManagerSql="SELECT `discount` FROM `taxManager` WHERE `user`=".$nowUserInfo['id']." and `endDate`>CURRENT_TIMESTAMP";
	$resTaxManagerSql=mysql_query($taxManagerSql) or die(handleError('Не удалось информацию о бухгалтере.',__FILE__,false,$taxManagerSql,$nowUserInfo['id']));
	$taxManager=mysql_fetch_assoc($resTaxManagerSql);
	if ($taxManager['discount']>0)
	{
		$persent=number_format($persent-$persent*$taxManager['discount']/100,2,'.','');
	}
}

$persent=number_format($persent, 2, '.', ' ');
$subMessage='';

$profitBonuses=[RENT_PROFIT_BASIC_FIRST,RENT_PROFIT_BASIC_SECOND,RENT_PROFIT_BASIC_THIRD,RENT_PROFIT_BASIC_FOURTH,RENT_PROFIT_BASIC_FIFTH,RENT_PROFIT_BASIC_SIXTH];
if ($nowUserInfo['registredTime']>(time()-IS_NEWBIE_TIME) && $nowUserInfo['kapitalization']<100000)
{
	$profitBonuses=[RENT_PROFIT_NEWBIE_FIRST,RENT_PROFIT_NEWBIE_SECOND,RENT_PROFIT_NEWBIE_THIRD,RENT_PROFIT_NEWBIE_FOURTH,RENT_PROFIT_NEWBIE_FIFTH,RENT_PROFIT_NEWBIE_SIXTH];
	$subMessage='<p>Вы - новичок в "Войне Олигархов"! И поэтому у вас бонусная доходность при сдаче зданий в аренду!<br>Бонус прекратит своё действие как только с момента вашего первого входа в игру пройдут '.IS_NEWBIE_DAYS.' дней или когда ваша капитализация превысит 100 000 <img class="moneyIndicator" src="images/money.png">!</p>';
}

$message='
<div id="idsListForRent" ids="'.$objIds.'" mode="'.$mode.'">
	В аренду будет передано строений: <b>'.$doxod['houses'].'</b><br>
	Выберите время, на которое вы хотите сдать в аренду строения:<br>
	'.$subMessage.'
	<div class="rentToTime" timeHours="1">
		<img class="hourIndicator" src="images/h1.png">
		<div class="infoOfRent">
			Длительность: <b>1 час</b><br>
			Доходность: <b>'.number_format($profitBonuses[0]*100,0,'.',' ').'%</b>
		</div>
		<div class="rentProfit">
			'.number_format($profit*$profitBonuses[0],2,'.',' ').' <img class="moneyIndicator" src="images/money.png"> в час.<br>
			Итого: '.number_format(($profit*$profitBonuses[0]*1)-round(($profit*$profitBonuses[0]*1)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">. Подоходный налог '.$persent.'% ('.number_format(round(($profit*$profitBonuses[0]*1)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">) учтён.
		</div>
	</div>

	<div class="rentToTime" timeHours="2">
		<img class="hourIndicator" src="images/h2.png">
		<div class="infoOfRent">
			Длительность: <b>2 часа</b><br>
			Доходность: <b>'.number_format($profitBonuses[1]*100,0,'.',' ').'%</b>
		</div>
		<div class="rentProfit">
			'.number_format($profit*$profitBonuses[1],2,'.',' ').' <img class="moneyIndicator" src="images/money.png"> в час.<br>
			Итого: '.number_format(($profit*$profitBonuses[1]*2)-round(($profit*$profitBonuses[1]*2)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">. Подоходный налог '.$persent.'% ('.number_format(round(($profit*$profitBonuses[1]*2)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">) учтён.
		</div>
	</div>

	<div class="rentToTime" timeHours="4">
		<img class="hourIndicator" src="images/h4.png">
		<div class="infoOfRent">
			Длительность: <b>4 часа</b><br>
			Доходность: <b>'.number_format($profitBonuses[2]*100,0,'.',' ').'%</b>
		</div>
		<div class="rentProfit">
			'.number_format($profit*$profitBonuses[2],2,'.',' ').' <img class="moneyIndicator" src="images/money.png"> в час.<br>
			Итого: '.number_format(($profit*$profitBonuses[2]*4)-round(($profit*$profitBonuses[2]*4)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">. Подоходный налог '.$persent.'% ('.number_format(round(($profit*$profitBonuses[2]*4)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">) учтён.
		</div>
	</div>

	<div class="rentToTime" timeHours="8">
		<img class="hourIndicator" src="images/h6.png">
		<div class="infoOfRent">
			Длительность: <b>8 часов</b><br>
			Доходность: <b>'.number_format($profitBonuses[3]*100,0,'.',' ').'%</b>
		</div>
		<div class="rentProfit">
			'.number_format($profit*$profitBonuses[3],2,'.',' ').' <img class="moneyIndicator" src="images/money.png"> в час.<br>
			Итого: '.number_format(($profit*$profitBonuses[3]*8)-round(($profit*$profitBonuses[3]*8)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">. Подоходный налог '.$persent.'% ('.number_format(round(($profit*$profitBonuses[3]*8)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">) учтён.
		</div>
	</div>

	<div class="rentToTime" timeHours="12">
		<img class="hourIndicator" src="images/h12.png">
		<div class="infoOfRent">
			Длительность: <b>12 часов</b><br>
			Доходность: <b>'.number_format($profitBonuses[4]*100,0,'.',' ').'%</b>
		</div>
		<div class="rentProfit">
			'.number_format($profit*$profitBonuses[4],2,'.',' ').' <img class="moneyIndicator" src="images/money.png"> в час.<br>
			Итого: '.number_format(($profit*$profitBonuses[4]*12)-round(($profit*$profitBonuses[4]*12)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">. Подоходный налог '.$persent.'% ('.number_format(round(($profit*$profitBonuses[4]*12)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">) учтён.
		</div>
	</div>

	<div class="rentToTime" timeHours="24">
		<img class="hourIndicator" src="images/h24.png">
		<div class="infoOfRent">
			Длительность: <b>24 часа</b><br>
			Доходность: <b>'.number_format($profitBonuses[5]*100,0,'.',' ').'%</b>
		</div>
		<div class="rentProfit">
			'.number_format($profit*$profitBonuses[5],2,'.',' ').' <img class="moneyIndicator" src="images/money.png"> в час.<br>
			Итого: '.number_format(($profit*$profitBonuses[5]*24)-round(($profit*$profitBonuses[5]*24)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">. Подоходный налог '.$persent.'% ('.number_format(round(($profit*$profitBonuses[5]*24)*$persent/100,2),2,'.',' ').' <img class="moneyIndicator" src="images/money.png">) учтён.
		</div>
	</div>
</div>';

echo json_encode(['result'=>'ok','message'=>$message]);
?>
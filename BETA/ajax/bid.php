<?
require "../configUsers.php";

if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный идентификатор аукциона!',__FILE__);
}

if (!$_POST['bid'] || !is_numeric($_POST['bid']))
{
	handleError('Неверное значение ставки на аукцион!',__FILE__);
}

$ids=(int)$_POST['ids'];
$bid=(float)$_POST['bid'];

$getInfoOfAuc="SELECT a.`user`, a.`user_bid`, a.`stavka`, UNIX_TIMESTAMP(a.`time`) time, b.`id` geoObjId, c.`name`, c.`basicCost`
							 FROM `auction` a,
										`geoObjects` b,
										`geoObjectsTypes` c
							 WHERE a.`id`=".$ids." and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isAuction`=1";
$resGetInfoOfAuc=mysql_query($getInfoOfAuc) or die(handleError('Не удалось получить информацию по аукциону.',__FILE__,false,$getInfoOfAuc,$nowUserInfo['id']));
$infoOfAuc=mysql_fetch_assoc($resGetInfoOfAuc);

if (!$infoOfAuc)
{
	handleError('Аукцион на который вы хотитете сделать ставку не найден!',__FILE__);
}

if ($infoOfAuc['user_bid']==$nowUserInfo['id'])
{
	handleError('На аукционе уже ваша ставка!',__FILE__);
}

$minBid=round($infoOfAuc['stavka']*(1+AUC_NEW_BID_PERSENT),2);
if ($bid<$minBid)
{
	handleError('Ставка '.number_format($bid, 2, '.', ' ').' слишком мала! Ставка должна быть больше '.number_format($minBid, 2, '.', ' '),__FILE__);
}

if ($bid>$nowUserInfo['money'])
{
	handleError('У вас недостаточно монет для такой ставки! Ваша ставка '.number_format($bid, 2, '.', ' ').', в то время когда монет всего '.number_format($nowUserInfo['money'], 2, '.', ' '),__FILE__);
}

$returnOfBid=$infoOfAuc['stavka']*AUC_BID_RETURN_BROKEN;
$timeDifference=$infoOfAuc['time']-time();

mysql_query('START TRANSACTION');

$returnBid="UPDATE `user` SET `money`=`money`+".$returnOfBid." WHERE `id`=".$infoOfAuc['user_bid'];
mysql_query($returnBid) or die(handleError('Не удалось вернуть игроку его процент от ставки.',__FILE__,false,$returnBid,$nowUserInfo['id'],true));

$userSubtractionBid="UPDATE `user` SET `money`=`money`-".$bid." WHERE `id`=".$nowUserInfo['id'];
mysql_query($userSubtractionBid) or die(handleError('Не удалось снять с вашего счёта сумму ставки.',__FILE__,false,$userSubtractionBid,$nowUserInfo['id'],true));

$dopUpdate='';
$minDuration=ONE_HOUR*AUC_MIN_DURATION;
if ($timeDifference<$minDuration)
{
	$dopUpdate=', `time`=FROM_UNIXTIME('.(time()+$minDuration).')';
}
$updateAuction="UPDATE `auction` SET `stavka`=".$bid.", `user_bid`=".$nowUserInfo['id'].$dopUpdate."  WHERE `id`=".$ids;
mysql_query($updateAuction) or die(handleError('Не удалось обновить данные аукциона.',__FILE__,false,$updateAuction,$nowUserInfo['id'],true));

$checkAucForUser="SELECT `id` FROM `auction_user` WHERE `auc`=".$ids." and `user`=".$nowUserInfo['id'];
$resCheckAuc=mysql_query($checkAucForUser) or die(handleError('Не удалось получить данные аукциона.',__FILE__,false,$checkAucForUser,$nowUserInfo['id'],true));
$checkAuc=mysql_fetch_assoc($resCheckAuc);
if (!$checkAuc)
{
	$inserUserAuc="INSERT INTO `auction_user` (`user`,`auc`) VALUES (".$nowUserInfo['id'].", ".$ids.")";
	mysql_query($inserUserAuc) or die(handleError('Не удалось добавить соотвествие аукциона и вашего аккаунта.',__FILE__,false,$inserUserAuc,$nowUserInfo['id'],true));
}

$logEnter="INSERT INTO `log`
							(`user`,`otherUser`,`text`,`geoObjectId`,`money`,`type`)
					 VALUES
							(".$nowUserInfo['id'].", ".$infoOfAuc['user_bid'].", 'Ставка на аукцион типа <i>".$infoOfAuc['name']."</i>',".$infoOfAuc['geoObjId'].",".($bid*-1).",'Ставка в аукционе'),
							(".$infoOfAuc['user_bid'].", ".$nowUserInfo['id'].", 'Перебита ваша ставка на аукционе типа <i>".$infoOfAuc['name']."</i>',".$infoOfAuc['geoObjId'].",0,'Перебивание ставки'),
							(".$infoOfAuc['user_bid'].", ".$nowUserInfo['id'].", 'Возврат перебитой ставки за аукцион типа <i>".$infoOfAuc['name']."</i>',".$infoOfAuc['geoObjId'].",".$returnOfBid.",'Возврат ставки')";
mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));

mysql_query('COMMIT');

echo json_encode(['result'=>'ok']);
?>
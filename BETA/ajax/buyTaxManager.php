<?php
require '../configUsers.php';

if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный тип бухгалтера!',__FILE__);
}

$nowTime=time();
$ids=(int)$_POST['ids'];

$countOfHouses="SELECT count(`id`) housesCount FROM `house` WHERE `user`=".$nowUserInfo['id'];
$resCountOfHouse=mysql_query($countOfHouses) or die(handleError('Не удалось получить кол-во ваших строений.',__FILE__,false,$countOfHouses,$nowUserInfo['id']));
$countHouses=mysql_fetch_assoc($resCountOfHouse);
$persent=floor($countHouses['housesCount']/100)*0.2;

$discronts=[90,75,55,40,30,25];
$costs=[ONE_DAY_COST_TAXMANAGER_FIRST,ONE_DAY_COST_TAXMANAGER_SECOND,ONE_DAY_COST_TAXMANAGER_THIRD,ONE_DAY_COST_TAXMANAGER_FOURTH,ONE_DAY_COST_TAXMANAGER_FIFTH,ONE_DAY_COST_TAXMANAGER_SIXTH];

$oneDay=3600*24;
$sevenDay=$oneDay*7;
$mouthDay=$oneDay*30;

switch ($ids)
{
	case 1:
		{
			$userDiscont=$discronts[0];
			$cost=$costs[0];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 2:
		{
			$userDiscont=$discronts[1];
			$cost=$costs[1];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 3:
		{
			$userDiscont=$discronts[2];
			$cost=$costs[2];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 4:
		{
			$userDiscont=$discronts[3];
			$cost=$costs[3];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 5:
		{
			$userDiscont=$discronts[4];
			$cost=$costs[4];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 6:
		{
			$userDiscont=$discronts[5];
			$cost=$costs[5];
			$endTime=$nowTime+$oneDay;
			break;
		}

	case 7:
		{
			$userDiscont=$discronts[0];
			$cost=$costs[0]*7-COST_TAXMANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 8:
		{
			$userDiscont=$discronts[1];
			$cost=$costs[2];
			$cost=$costs[1]*7-COST_TAXMANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 9:
		{
			$userDiscont=$discronts[2];
			$cost=$costs[2]*7-COST_TAXMANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 10:
		{
			$userDiscont=$discronts[3];
			$cost=$costs[3]*7-COST_TAXMANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 11:
		{
			$userDiscont=$discronts[4];
			$cost=$costs[4]*7-COST_TAXMANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 12:
		{
			$userDiscont=$discronts[5];
			$cost=$costs[5]*7-COST_TAXMANAGER_DISCOUNT_FIRST;
			$endTime=$nowTime+$sevenDay;
			break;
		}

	case 13:
		{
			$userDiscont=$discronts[0];
			$cost=$costs[0]*30-COST_TAXMANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 14:
		{
			$userDiscont=$discronts[1];
			$cost=$costs[1]*30-COST_TAXMANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 15:
		{
			$userDiscont=$discronts[2];
			$cost=$costs[2]*30-COST_TAXMANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 16:
		{
			$userDiscont=$discronts[3];
			$cost=$costs[3]*30-COST_TAXMANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 17:
		{
			$userDiscont=$discronts[4];
			$cost=$costs[4]*30-COST_TAXMANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}

	case 18:
		{
			$userDiscont=$discronts[5];
			$cost=$costs[5]*30-COST_TAXMANAGER_DISCOUNT_SECOND;
			$endTime=$nowTime+$mouthDay;
			break;
		}


	default:
		handleError('Неверный тип бухгалтера!',__FILE__);
}

if($nowUserInfo['credits']<$cost)
{
	handleError('Недостаточно кредитов! Для покупки выбранного типа бухгалтера надо '.$cost.' кредитов, а у вас только '.$nowUserInfo['credits'],__FILE__);
}

$insertToManager="INSERT INTO `taxManager` (`user`, `discount`, `endDate`) VALUES (".$nowUserInfo['id'].",".$userDiscont.",FROM_UNIXTIME(".$endTime."))";
$userUpdate="UPDATE `user` SET `credits`=`credits`-".$cost." WHERE `id`=".$nowUserInfo['id'];

if($period==1)
{
	$period='каждый 1 час';
}

if($period==2 || $period==4 || $period==24)
{
	$period='каждые '.$period.' часа';
}

if($period==8 || $period==12)
{
	$period='каждые '.$period.' часов';
}

$days=round(($endTime-$nowTime)/60/60/24,3);

$toLog="INSERT INTO `log`
					(`user`,`text`,`money`,`credits`,`type`)
											VALUES
					(".$nowUserInfo['id'].",'Вы приобрели бухгалтера, который будет уменьшать ваш налог на ".$userDiscont."% в течении ".$days." дней',0,".($cost*-1).",'Бухгалтер')";

mysql_query("START TRANSACTION");

mysql_query($insertToManager) or die(handleError('Не удалось обновить данные бухгалтера.',__FILE__,false,$insertToManager,$nowUserInfo['id'],true));
mysql_query($userUpdate) or die(handleError('Не удалось обновить ваш счёт.',__FILE__,false,$userUpdate,$nowUserInfo['id'],true));
mysql_query($toLog) or die(handleError('Не удалось записать данные в журнал.',__FILE__,false,$toLog,$nowUserInfo['id'],true));

mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
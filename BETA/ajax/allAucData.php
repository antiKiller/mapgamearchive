<?php
require "../configUsers.php";

$getAucData="SELECT
							(SELECT count(DISTINCT a.`id`)
							FROM `auction` a,
									 `auction_user` d
							WHERE a.`id`=d.`auc` and (a.`user`=".$nowUserInfo['id']." OR a.`user_bid`=".$nowUserInfo['id']." OR d.`user`=".$nowUserInfo['id'].")) countAucAll,

							(SELECT sum(`doxod`)
							FROM
							(SELECT a.`doxod`
							FROM `auction` a,
									 `auction_user` d
							WHERE a.`id`=d.`auc` and (a.`user`=".$nowUserInfo['id']." OR a.`user_bid`=".$nowUserInfo['id']." OR d.`user`=".$nowUserInfo['id'].") GROUP BY a.`id`)a ) allAucDoxod,

							(SELECT sum(`stavka`)
							FROM
							(SELECT a.`stavka`
							FROM `auction` a,
									 `auction_user` d
							WHERE a.`id`=d.`auc` and (a.`user`=".$nowUserInfo['id']." OR a.`user_bid`=".$nowUserInfo['id']." OR d.`user`=".$nowUserInfo['id'].") GROUP BY a.`id`)a ) allAucBids";

$res=mysql_query($getAucData) or die(handleError('Не удалось обновить данные управляющего.',__FILE__,false,$getAucData,$nowUserInfo['id']));
$data=mysql_fetch_assoc($res);

echo json_encode(['result'=>'ok','countAucAll'=>$data['countAucAll'],'allAucDoxod'=>number_format($data['allAucDoxod'], 2, '.', ' '),'allAucBids'=>number_format($data['allAucBids'], 2, '.', ' ')]);
?>
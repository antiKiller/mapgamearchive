<?
require "../configUsers.php";

$nowPage=$_POST['nowPage'];
if (!$_POST['nowPage'])
{
	$nowPage='Неизвестная страница';
}

switch($nowPage)
{
	case 'index': $nowPage='Главная';
								break;
	case 'map': $nowPage='Карта';
								break;
	case 'house': $nowPage='Здания';
								break;
	case 'auction': $nowPage='Аукционы';
								break;
	case 'log': $nowPage='Журнал';
								break;
	case 'rating': $nowPage='Рейтинг';
								break;
	case 'profile': $nowPage='Профиль';
								break;
	case 'library': $nowPage='Информация';
								break;
	case 'chat': $nowPage='Чат';
								break;
	case 'exchange': $nowPage='Биржа';
								break;
	default: break;
}

if ($_SERVER['HTTP_HOST']=='test.map-game.ru')
{
	$nowPage.=' - тест.сервер';
}

$confirm=false;
$dopUpdate='';
if ($nowUserInfo['money']<=50 && $nowUserInfo['isConfirmBankrupt']==0)
{
	$dopUpdate=', isConfirmBankrupt=1';
	$confirm='<p>Ваш балланс меньше или 50!</p>
		<p>Как же это могло произойти? Вероятно во всём винованы аукционы! Каждый раз, когда вы побеждаете в аукционе, вы должны сделать новую ставку и сумма этой ставки снимается с вашего счёта. Но если на вашем счету недостаточно монет, то вы уходите в минус. По всей вероятности, именно это и произошло.</p>
		<p>Если ваши доходы от сдачи домов и аукционов за последние 3 дня превосходят ваши расходы на аукционы, то вы можете не волноваться - скоро вы отобьете свои убытки сдачей домов в аренду.</p>
		<p>В противном же случае, выши убытки будут только расти и вы ничего не сможете с этим поделать.<br>
		<b>Для того чтобы избежать этого, вы можете пройти процедуру банкротства. Для получения сведений об этой процедуре, кликните на блок, где отображается ваш балланс, в верхнем правом углу.</b></p><br><br>
		<center><big><b>Данное сообщение больше не будет появляться!</b></big></center>';
}

if($nowPage!=$nowUserInfo['lastPage'])
{
	$dopUpdate.=", `lastPage`='".$nowPage."'";
}

$blinkChat=false;
if ($dopUpdate || (time()-$nowUserInfo['lastEnterTime'])>40)
{
	$updateUser="UPDATE `user` SET `lastEnter`=CURRENT_TIMESTAMP ".$dopUpdate." WHERE `id`=".$nowUserInfo['id'];
	mysql_query($updateUser) or die(handleError('Не удалось обновить информацию о последнем посещении.',__FILE__,false,$updateUser,$nowUserInfo['id']));
	if ($nowUserInfo['isBlinkedChat'] && $nowPage!='Чат')
	{
		$getLastIds="SELECT MAX(`id`) `lastChatId` FROM `chat`";
		$resGetLastIds=mysql_query($getLastIds) or die(handleError('Не удалось обновить информацию о последнем посещении.',__FILE__,false,$getLastIds,$nowUserInfo['id']));
		$lastIds=mysql_fetch_assoc($resGetLastIds);
		if ($lastIds['lastChatId']>$nowUserInfo['lastViewedChat'])
		{
			$blinkChat=true;
		}
	}
}

$getBadAuc="SELECT count(`id`) `badAuc` FROM `auctionsUsers` WHERE `user`=".$nowUserInfo['id']." and `user_bid`!=".$nowUserInfo['id'];
$resBadAuc=mysql_query($getBadAuc) or die(handleError('Не удалось обновить информацию о последнем посещении.',__FILE__,false,$getBadAuc,$nowUserInfo['id']));
$badAuc=mysql_fetch_assoc($resBadAuc);
$badAuc=$badAuc['badAuc'];

$money=number_format($nowUserInfo['money'], 2, '.', ' ');
if ($nowUserInfo['money']<=0)
{
	$money='<span class="red">'.$money.'</span>';
}
echo json_encode(['result'=>'ok','badAuc'=>$badAuc,'money'=>$money,'credits'=>number_format($nowUserInfo['credits'], 0, '.', ' '),'kap'=>number_format($nowUserInfo['kapitalization'], 0, '.', ' '),'confirm'=>$confirm,'blinkChat'=>$blinkChat]);
?>
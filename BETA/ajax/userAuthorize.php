<?php
session_start();
include '../config.php';
$token=$_POST['token'];
$url='http://ulogin.ru/token.php?token='.$token.'&host='.$_SERVER['HTTP_HOST'];
$content=@file_get_contents('http://ulogin.ru/token.php?token='.$token.'&host='.$_SERVER['HTTP_HOST']);
$userAuthData=@json_decode($content);
if ($userAuthData->error)
{
	echo json_encode(['res'=>'Ошибка авторизации: '.$userAuthData->error]);
	session_destroy();
	exit();
}
if (!$userAuthData->uid || !$userAuthData->network)
{
	echo json_encode(['res'=>'Авторизация не удалась!','file'=>$url,'content'=>$content,'authData'=>$userAuthData]);
	session_destroy();
	exit();
}

$searchUser="SELECT `id`,`pass`,`isBanned`,`photo`,`photo_big` FROM `user` WHERE `ids`='".$userAuthData->uid."' and `network`='".$userAuthData->network."'";
$resSearch=mysql_query($searchUser) or die(handleError('Ошибка поиска пользователя в запросе.',__FILE__,false,$searchUser,$nowUserInfo['id']));
$searchedUser=mysql_fetch_assoc($resSearch);
if ($searchedUser)
{
	if ($searchedUser['isBanned']==1)
	{
		session_destroy();
		echo json_encode(['res'=>'banned']);
		exit();
	}

	$mustUpdate=false;
	$update=[];

	if (strpos($searchedUser['photo'], 'http')!==false)
	{
		$maxSize=1048576*3;
		$fileInfo=pathinfo($searchedUser['photo']);
		$file=file_get_contents($searchedUser['photo']);
		$imgSize=strlen($file);
		$res=false;
		if ($imgSize<=$maxSize)
		{
			$fileInfo['extension']=explode('?', $fileInfo['extension'])[0];
			$fileInfo['extension']=explode('#', $fileInfo['extension'])[0];
			if (!$fileInfo['extension'])
			{
				$fileInfo['extension']='jpg';
			}
			$newName=$searchedUser['id'].'.'.$fileInfo['extension'];
			$img = '../images/usersAvs/'.$newName;
			$res=file_put_contents($img, $file);
		}
		$file=null;
		if(!$res)
		{
			$newName='noImg.png';
		}
		$mustUpdate=true;
		$update[]="`photo`='images/usersAvs/".$newName."'";
	}
	if (!$searchedUser['photo'])
	{
		$update[]="`photo`='images/usersAvs/noImg.png'";
	}

	if (strpos($searchedUser['photo_big'], 'http')!==false)
	{
		$maxSize=1048576*6;
		$fileInfo=pathinfo($searchedUser['photo_big']);
		$file=file_get_contents($searchedUser['photo_big']);
		$imgSize=strlen($file);
		$res=false;
		if ($imgSize<=$maxSize && $imgSize>500)
		{
			$fileInfo['extension']=explode('?', $fileInfo['extension'])[0];
			$fileInfo['extension']=explode('#', $fileInfo['extension'])[0];
			if (!$fileInfo['extension'])
			{
				$fileInfo['extension']='jpg';
			}
			$newName=$searchedUser['id'].'_big.'.$fileInfo['extension'];
			$img = '../images/usersAvs/'.$newName;
			$res=file_put_contents($img, $file);
		}
		$file=null;
		if(!$res)
		{
			$newName='noImg_big.png';
		}
		$mustUpdate=true;
		$update[]="`photo_big`='images/usersAvs/".$newName."'";
	}

	if (!$searchedUser['photo_big'] && $imgSize>500)
	{
		$update[]="`photo`='images/usersAvs/noImg_big.png'";
	}

	$_SESSION['id']=$searchedUser['id'];
	$_SESSION['pass']=$searchedUser['pass'];

	if ($mustUpdate)
	{
		$updateSql="UPDATE `user` SET ".implode(',', $update)." WHERE `id`=".$_SESSION['id'];
		mysql_query($updateSql) or die(handleError('Ошибка обновления данных авторизации.',__FILE__,false,$updateSql,$nowUserInfo['id']));
	}

	$insertEnter="INSERT INTO `enter` (`user`,`ip`,`userAgent`) VALUES (".$_SESSION['id'].",'".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."')";
	mysql_query($insertEnter) or die(handleError('Ошибка записи информации об авторизации.',__FILE__,false,$insertEnter,$nowUserInfo['id']));

	echo json_encode(['res'=>'ok']);
	exit();
}

$name=$userAuthData->first_name;
if ($userAuthData->nickname)
	$name.=' ('.$userAuthData->nickname.')';
if ($userAuthData->last_name)
	$name.=' '.$userAuthData->last_name;

if (!$userAuthData->sex)
	$userAuthData->sex=0;

$pass=md5($userAuthData->uid.$name.time());

$createUser="INSERT INTO `user`
								(
									`ids`,
									`network`,
									`accountLink`,
									`name`,
									`pass`,
									`photo`,
									`photo_big`,
									`sex`,
									`city`,
									`country`,
									`lastBankrupt`
								)
						 VALUES
								(
								 '".$userAuthData->uid."',
								 '".$userAuthData->network."',
								 '".$userAuthData->profile."',
								 '".$name."',
								 '".$pass."',
								 '".$userAuthData->photo."',
								 '".$userAuthData->photo_big."',
									".$userAuthData->sex.",
								 '".$userAuthData->city."',
								 '".$userAuthData->country."',
									 CURRENT_TIMESTAMP
								)";
mysql_query($createUser) or die(handleError('Ошибка создания нового аккаунта.',__FILE__,false,$createUser,$nowUserInfo['id']));
$_SESSION['id']=mysql_insert_id();
$_SESSION['pass']=$pass;

$insertEnter="INSERT INTO `enter` (`user`,`ip`,`userAgent`) VALUES (".$_SESSION['id'].",'".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."')";
mysql_query($insertEnter) or die(handleError('Ошибка записи информации об авторизации.',__FILE__,false,$insertEnter,$nowUserInfo['id']));

echo json_encode(['res'=>'ok']);
?>

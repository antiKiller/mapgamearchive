<?php
if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	echo json_encode(['result'=>'Неверный идентификатор строения.']);
	exit();
}
require '../configUsers.php';

$ids=(int)$_POST['ids'];

$getHouse="SELECT a.`level`, a.`cost`, a.`costCredits`,
									b.`address`,
									c.`name`
					 FROM `house` a,
								`geoObjects` b,
								`geoObjectsTypes` c
					 WHERE b.`id`=".$ids." and a.`isCosted`=1 and a.`geoObjectId`=b.`id` and b.`type`=c.`id`";
$resGetHouse=mysql_query($getHouse) or die(handleError('Не удалось получить информацию о продаваемом здании.',__FILE__,false,$getHouse,$nowUserInfo['id']));
$house=mysql_fetch_assoc($resGetHouse);
if(!$house)
{
	echo json_encode(['result'=>'Дом для продажи не найден.']);
	exit();
}

if ($nowUserInfo['money']<$house['cost'] || $nowUserInfo['credits']<$house['costCredits'])
{
	echo json_encode(['result'=>'Недостаточно средств. Для покупки строения вам надо '.number_format($house['cost'], 2, ',', ' ').' монет и '.number_format($house['costCredits'], 2, ',', ' ').' кредитов, а у вас только '.number_format($nowUserInfo['money'], 2, ',', ' ').' монет и '.number_format($nowUserInfo['credits'], 2, ',', ' ').'!']);
	exit();
}

$content='Вы уверены что хотите купить здание <b>'.$house['name'].'</b> '.$house['level'].' уровня и располагающееся по адресу <i>'.$house['address'].'</i> за '.number_format($house['cost'], 2, ',', ' ').' <img src="images/money.png" class="moneyIndicator"> и '.number_format($house['costCredits'], 2, ',', ' ').' <img src="images/baks.png" class="moneyIndicator">?<br><br>
	<center><input type="button" id="confirmedByHouse" ids="'.$ids.'" value="Да, купить!"></center><br><br>
	<b>Данная цена включает в себя непосредственно цену здания и налог 13%. Причём, налог не распространяется на <img src="images/baks.png" class="moneyIndicator">, только на ту часть цены, которая в <img src="images/money.png" class="moneyIndicator"></b>';

echo json_encode(['result'=>'ok','content'=>$content]);
?>
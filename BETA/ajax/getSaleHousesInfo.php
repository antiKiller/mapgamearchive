<?php
if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный список идентификаторов строений!',__FILE__);
}

require "../configUsers.php";

$ids=(int)$_POST['ids'];

$getInfoOfHouse="SELECT `level`, `address`, `name`, `basicCost`, `creditsCost`, `creditsCost`, `updateCost`
								 FROM `buildingsUsers`
								 WHERE `geoObjectId`=".$ids." and `isCosted`=0 and `user`=".$nowUserInfo['id'];
$resInfoOfHouse=mysql_query($getInfoOfHouse) or die(handleError('Ошибка получения информации о строении.',__FILE__,false,$getInfoOfHouse,$nowUserInfo['id']));
$house=mysql_fetch_assoc($resInfoOfHouse);

if (!$house)
{
	handleError('Неверный список идентификаторов строений!',__FILE__);
}

$moneyCost=$house['basicCost'];
$creditsCost=$house['creditsCost'];
if ($house['level']>1)
{
	for ($i=2;$i<$house['level'];$i++)
	{
		$moneyCost+=$house['basicCost']*pow($house['updateCost'],$house['level']);
	}
}

$moneyCost=round($moneyCost);

$message='Вы хотите выставить здание типа <b>'.$house['name'].'</b> по адресу <i>'.$house['address'].'</i> выставить на продажу на бирже. При этом здание будет отображаться как доступное для покупки всеми игроками, а сами вы с этим зданием не сможете производить никаких манипуляций (в том числе и сдавать его в аренду или улучшать), пока оно находится в стадии продажи.<br>
	Выберите цену, по которой вы хотите продавать это здание:<br>
	<input type="text" id="selledCostMoney" value="'.$moneyCost.'"> <img class="moneyIndicator" src="images/money.png"> Покупатель здания дополнительно заплатит налог - 13% от этой суммы<br>
	<input type="text" id="selledCostCredit" value="'.$creditsCost.'"> <img class="moneyIndicator" src="images/baks.png"><br>
	По-умолчанию цена является затратам на постройку и улучшение этого здания до текущего уровня.<br>
	<center><input type="button" id="sendBuildingToExchange" value="Выставить на продажу" ids="'.$ids.'"></center><br>
	<b>ВНИМАНИЕ! Цена не может быть больше 3-х базовых цены. То есть максимальная цена для этого здания составляет '.number_format(ceil($moneyCost*3), 0, '.', ' ').' <img class="moneyIndicator" src="images/money.png">. Ограничение действует только на цену в <img class="moneyIndicator" src="images/money.png">, цена в <img class="moneyIndicator" src="images/baks.png"> неограничена!</b>';

echo json_encode(['result'=>'ok','message'=>$message]);
?>
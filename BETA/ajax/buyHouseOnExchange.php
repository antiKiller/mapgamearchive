<?php
if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный идентификатор строения.',__FILE__);
}
require '../configUsers.php';

$ids=(int)$_POST['ids'];

$getHouse="SELECT `id`, `user`, `cost`, `costCredits`
					 FROM `house`
					 WHERE `geoObjectId`=".$ids." and `isCosted`=1";
$resGetHouse=mysql_query($getHouse) or die(handleError('Не удалось получить информацию о продаваемом здании.',__FILE__,false,$getHouse,$nowUserInfo['id']));
$house=mysql_fetch_assoc($resGetHouse);
if(!$house)
{
	handleError('Дом для продажи не найден.',__FILE__);
}

if ($nowUserInfo['money']<$house['cost'] || $nowUserInfo['credits']<$house['costCredits'])
{
	handleError('Недостаточно средств. Для покупки строения вам надо '.number_format($house['cost'], 2, ',', ' ').' монет и '.number_format($house['costCredits'], 2, ',', ' ').' кредитов, а у вас только '.number_format($nowUserInfo['money'], 2, ',', ' ').' монет и '.number_format($nowUserInfo['credits'], 2, ',', ' ').'!',__FILE__);
}

$buildCost=round(100*$house['cost']/113,2);
$nalog=round($house['cost']-$buildCost,2);

$updateHouse="UPDATE `house` SET `user`=".$nowUserInfo['id'].", `isCosted`=0 WHERE `geoObjectId`=".$ids;
$updateBuyedUser="UPDATE `user` SET `money`=`money`-".$house['cost'].", `credits`=`credits`-".$house['costCredits']." WHERE `id`=".$nowUserInfo['id'];
$updateSelledUser="UPDATE `user` SET `money`=`money`+".$buildCost.", `credits`=`credits`+".$house['costCredits']." WHERE `id`=".$house['user'];
$toLog="INSERT INTO `log`
								(`user`, `otherUser`, `geoObjectId`, `money`, `credits`, `type`, `text`)
	                   VALUES
								(".$nowUserInfo['id'].", ".$house['user'].", ".$ids.", ".($buildCost*-1).", ".($house['costCredits']*-1).",'Покупка здания', 'Покупка здания на бирже'),
								(".$nowUserInfo['id'].", ".$house['user'].", ".$ids.", ".($nalog*-1).", 0,'Налог', 'Налог 13% при покупке зданий'),
								(".$house['user'].", ".$nowUserInfo['id'].", ".$ids.", ".$buildCost.", ".$house['costCredits'].",'Продажа здания', 'Ваше здание на бирже было продано на бирже')";

mysql_query("START TRANSACTION");
mysql_query($updateHouse) or die(handleError('Ошибка обновления владельца здания.',__FILE__,false,$updateHouse,$nowUserInfo['id'],true));
mysql_query($updateBuyedUser) or die(handleError('Ошибка снятия средств за покупку с вашего счёта.',__FILE__,false,$updateBuyedUser,$nowUserInfo['id'],true));
mysql_query($updateSelledUser) or die(handleError('Ошибка зачисления средств на счёт продавца.',__FILE__,false,$updateSelledUser,$nowUserInfo['id'],true));
mysql_query($toLog) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$toLog,$nowUserInfo['id'],true));
mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
<?
require "../configUsers.php";

$total=0;
$recods=[];
$_POST['sidx']=preg_replace('/name asc/', '', $_POST['sidx'],1);

$usersList=[];

$order=trim($_POST['sidx'],', ');
if ($_POST['sord']!='asc')
	$order.=' '.$_POST['sord'];

$order=trim($order,', ');
if (empty($order))
{
	$order='`name` asc';
}

$order=str_replace('userOwner', 'user', $order);
$order=str_replace('userBid', 'user_bid', $order);

$search='';
if ($_POST['_search']=='true' && $_POST['filters'])
{
	$filtres=json_decode($_POST['filters']);
	foreach ($filtres->rules as $key => $searchRule)
	{
		$searchRule->data=htmlentities($searchRule->data, ENT_QUOTES, 'UTF-8');
		$op='=';
		if ($searchRule->field=='name')
		{
			$searchRule->field='c.`'.$searchRule->field.'`';
		}
		switch ($searchRule->op)
		{
			case 'eq': $op='=';
								 break;

			case 'ne': $op='!=';
								 break;

			case 'cn': $op=' LIKE ';
								 $searchRule->data='%'.$searchRule->data.'%';
								 break;

			case 'nc': $op=' NOT LIKE ';
								 $searchRule->data='%'.$searchRule->data.'%';
								 break;

			case 'bw': $op=' LIKE ';
								 $searchRule->data=$searchRule->data.'%';
								 break;

			case 'bn': $op=' NOT LIKE ';
								 $searchRule->data=$searchRule->data.'%';
								 break;

			case 'ew': $op=' LIKE ';
								 $searchRule->data='%'.$searchRule->data;
								 break;

			case 'en': $op=' NOT LIKE ';
								 $searchRule->data='%'.$searchRule->data;
								 break;

			case 'le': $op='<=';
								 break;

			case 'lt': $op='<';
								 break;

			case 'gt': $op='>';
								 break;

			case 'ge': $op='>=';
								 break;

			default: $op='=';
			break;
		}

		$search.=" and ".$searchRule->field.$op."'".$searchRule->data."'";
	}
}

$getAuctions="SELECT a.*, UNIX_TIMESTAMP(a.`time`) time,
										 b.`address`, X(b.`coords`) pos1, Y(b.`coords`) pos2,
										 c.`id` objTypeId, c.`name`
							FROM `auction` a,
									 `geoObjects` b,
									 `geoObjectsTypes` c,
									 `auction_user` d
							WHERE a.`id`=d.`auc` and (a.`user`=".$nowUserInfo['id']." OR a.`user_bid`=".$nowUserInfo['id']." OR d.`user`=".$nowUserInfo['id'].") and
										a.`geoObjectId`=b.`id` and b.`type`=c.`id` ".$search."
							GROUP BY a.`id`
							ORDER BY ".$order;
$resGetAuctions=mysql_query($getAuctions) or die(handleError('Ошибка поиска аукционов пользователя.',__FILE__,false,$getAuctions,$nowUserInfo['id']));
while($record=mysql_fetch_assoc($resGetAuctions))
{
	$total++;

	if ($nowUserInfo['id']==$record['user'])
	{
		$record['userOwner']='Вы';
	}
		else
		{
			if ($users[$record['user']])
			{
				$record['userOwner']=$users[$record['user']];
			}
				else
				{
					$getUserInfoSql="SELECT `name` FROM `user` WHERE `id`=".$record['user'];
					$resGetUser=mysql_query($getUserInfoSql);
					$userInfo=mysql_fetch_assoc($resGetUser);

					$users[$record['user']]='<span class="openUserProfile" userId="'.$record['user'].'">'.$userInfo['name'].'</span>';
					$record['userOwner']=$users[$record['user']];
				}
		}

	if ($nowUserInfo['id']==$record['user_bid'])
	{
		$record['userBid']='Вы';
	}
		else
		{
			if ($users[$record['user_bid']])
			{
				$record['userBid']=$users[$record['user_bid']];
			}
				else
				{
					$getUserInfoSql="SELECT `name` FROM `user` WHERE `id`=".$record['user_bid'];
					$resGetUser=mysql_query($getUserInfoSql);
					$userInfo=mysql_fetch_assoc($resGetUser);

					$users[$record['user_bid']]='<span class="openUserProfile" userId="'.$record['user_bid'].'">'.$userInfo['name'].'</span>';
					$record['userBid']=$users[$record['user_bid']];
				}
		}

	$record['time']=date('d.m.Y H:i:s', $record['time']);
	$record['actions']='';
	if ($record['user_bid']!=$nowUserInfo['id'])
	{
//		$record['stavka']='<b style="color:red">'.$record['stavka'].'</b>';
		$record['actions'].='<img class="auctionActionBid" ids="'.$record['id'].'" title="Перебить ставку" src="images/check.png"> ';
	}

//	if ($record['user']!=$nowUserInfo['id'])
//	{
//		$record['doxod']='<b style="color:red">'.$record['doxod'].'</b>';
//	}

	if ($record['user_bid']!=$nowUserInfo['id'] && $record['user']!=$nowUserInfo['id'])
	{
		$record['actions'].='<img class="auctionActionClose" ids="'.$record['id'].'" title="Прекратить слежение за аукционом" src="images/close.png"> ';
	}

	if ($record['actions']=='')
	{
		$record['actions']='-';
	}

	switch ($record['objTypeId'])
	{
		case 2:
			$zoom=17;
			break;

		case 3:
			$zoom=16;
			break;

		case 4:
			$zoom=13;
			break;

		case 5:
			$zoom=9;
			break;

		case 6:
			$zoom=8;
			break;

		case 7:
			$zoom=5;
			break;

		default:
			$zoom=17;
			break;
	}

	$record['address'].=' <img src="images/map.png" class="goToMap" title="Перейти к обьекту на карте" coords="'.$record['pos1'].','.$record['pos2'].'" zoom="'.$zoom.'">';
	$recods[]=$record;
}

echo json_encode(['result'=>'ok','total'=>1,'records'=>$total,'rows'=>$recods]);
?>
<?php
require '../configUsers.php';

if (!$_POST['kurs'] || !is_numeric($_POST['kurs']))
{
	handleError('Неверная идентификатор ордера на покупку кредитов.',__FILE__);
}
$kurs=(float)$_POST['kurs'];

if (!$_POST['credits'] || !is_numeric($_POST['credits']))
{
	handleError('Неверная идентификатор ордера на покупку кредитов.',__FILE__);
}
$credits=(int)$_POST['credits'];

if ($kurs<100)
{
	handleError('Курс кредитов не может быть меньше 100 монет.',__FILE__);
}

if ($credits<=0)
{
	handleError('Количество кредитов, которое вы хотите продать, должно быть больше нуля.',__FILE__);
}

$kurs=round($kurs,2);

if ($credits>$nowUserInfo['credits'])
{
	handleError('Для продажи '.$credits.' кредитов вам их сначала надо где-то получить. У вас только '.$nowUserInfo['credits'].' кредитов. Вы не можете продать кредитов больше, чем у вас есть.',__FILE__);
}

$updateUser="UPDATE `user` SET `credits`=`credits`-".$credits." WHERE `id`=".$nowUserInfo['id'];

$toLog="INSERT INTO `log` (`user`,`text`,`credits`,`type`)
											VALUES
														(".$nowUserInfo['id'].",'Вы разместили заявку на продажу ".$credits." <img src=\"images/baks.png\" class=\"moneyIndicator\"> на бирже',".($credits*-1).",'Заявки на бирже')";
$updateExchange="INSERT INTO `exchange` (`user`,`kurs`,`credits`,`startCredits`,`type`)
	                   VALUES (".$nowUserInfo['id'].",".$kurs.",".$credits.",".$credits.",'sell')";

mysql_query("START TRANSACTION");

mysql_query($updateUser) or die(handleError('Не снять кредиты с вашего счёта.',__FILE__,false,$toLog,$nowUserInfo['id'],true));
mysql_query($toLog) or die(handleError('Не удалось записать данные в журнал.',__FILE__,false,$toLog,$nowUserInfo['id'],true));
mysql_query($updateExchange) or die(handleError('Не удалось разместить кредиты на бирже.',__FILE__,false,$updateExchange,$nowUserInfo['id'],true));

mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
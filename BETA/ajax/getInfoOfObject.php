<?php
require "../configUsers.php";

$objId=(int)$_POST['ids'];
$isAuc=(int)$_POST['isAuc'];
$result='ok';
$content='';
$objectRadius=null;
$objectCoords=[];

if ($isAuc)
{
	$selectAuc="SELECT a.*, b.`address`, d.`id` userIds, d.`name` userName, c.`name` nameType
							FROM `auction` a,
									 `geoObjects` b,
									 `geoObjectsTypes` c,
									 `user` d
							WHERE b.`id`=".$objId." and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and a.`user`=d.`id`";
	$resSelectAuc=mysql_query($selectAuc) or die(handleError('Ошибка поиска аукциона.',__FILE__,false,$selectAuc,$nowUserInfo['id']));
	$auc=mysql_fetch_assoc($resSelectAuc);
	if (!$auc)
	{
		echo json_encode(['result'=>'Такой аукцион не найден!']);
		exit();
	}

	$content.=getInfoOfBuildedObject($auc,true,$nowUserInfo);
}
	else
	{
		$selectHouse="SELECT a.*,
												 a.`id` houseId,
												 a.`isCosted`,
												 a.`cost`,
												 a.`costCredits`,
												 c.`isBuilded`,
												 b.`id` objId,
												 b.`address`,
												 d.`id` userIds,
												 d.`name` userName,
												 c.`name` nameType,
												 UNIX_TIMESTAMP(`time`) `time`,
												 c.`radius`,
												 X(b.`coords`) `pos1`,
												 Y(b.`coords`) `pos2`,
												 c.`basicDoxod`,
												 c.`isHouse`,
												 c.`isEnterprise`,
												 c.`isSocial`
									FROM `house` a,
											 `geoObjects` b,
											 `geoObjectsTypes` c,
											 `user` d
									WHERE b.`id`=".$objId." and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and a.`user`=d.`id`";
		$resSelectHouse=mysql_query($selectHouse) or die(handleError('Ошибка поиска строения.',__FILE__,false,$selectHouse,$nowUserInfo['id']));
		$house=mysql_fetch_assoc($resSelectHouse);
		if (!$house)
		{
			echo json_encode(['result'=>'Такой дом не найден!']);
			exit();
		}
		$objectRadius=$house['radius'];
		$objectCoords[0]=$house['pos1'];
		$objectCoords[1]=$house['pos2'];
		$content.=getInfoOfBuildedObject($house,false,$nowUserInfo);
	}


$content='<div class="baloonInfoContent fullShow">'.$content.'</div>';
echo json_encode(['result'=>$result,'content'=>$content,'radius'=>$objectRadius,'coord'=>$objectCoords]);
?>
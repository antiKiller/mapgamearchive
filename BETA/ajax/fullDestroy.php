<?php
require "../configUsers.php";
$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));
$_POST['obj']=explode(',', $_POST['obj']);
if (!$_POST['obj'] || count($_POST['obj'])==0)
{
	handleError('Неверный идентификатор здания!',__FILE__);
}

$objIds=implode(',',$_POST['obj']);


$getHouse="SELECT a.`level`, b.`id`, b.`address`, c.`name`, c.`basicCost`, c.`updateCost`
					 FROM `house` a,
					      `geoObjects` b,
								`geoObjectsTypes` c
					 WHERE a.`user`=".$nowUserInfo['id']." and a.`geoObjectId` in (".$objIds.") and `time`<=CURRENT_TIMESTAMP and
								 a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isMap`=0 and `isCosted`=0";
$resHouse=mysql_query($getHouse) or die(handleError('Ошибка получения списка домов.',__FILE__,false,$getHouse,$nowUserInfo['id']));
$allCost=0;
$newIds=[];
$logEnterVals=[];
while($house=mysql_fetch_assoc($resHouse))
{
	$newIds[]=$house['id'];
	$desctroyCost=round($house['basicCost']*pow($house['updateCost'],$house['level']-1)/3,2);
	$allCost+=$desctroyCost;
	$message.='<b>'.$house['name'].' '.$house['level'].' уровня</b> по адресу <i>'.$house['address'].' </i>. Стоимость сноса составляет 1/3 от стоимости улучшения до текущего уровня и составляет '.number_format($desctroyCost, 2, ',', ' ').' <img class="moneyIndicator" src="images/money.png"><br>';

	$logEnterVals[]="(".$nowUserInfo['id'].",'Уничтожение здания <i>".$house['name']."</i>',".$house['id'].",".($desctroyCost*-1).",'Снос здания')";
}

if (!$newIds)
{
	handleError('Здание, подлежащее сносу не найдено!',__FILE__);
}

$allCost=round($allCost,2);
if ($nowUserInfo['money']<$allCost)
{
	handleError('Недостаточно <img class="moneyIndicator" src="images/money.png"> для сноса зданий!',__FILE__);
}

$updateUser="UPDATE `user` SET `money`=`money`-".$allCost." WHERE `id`=".$nowUserInfo['id'];
$updateHouse="DELETE FROM `house` WHERE `geoObjectId` in (".implode(',',$newIds).")";
$updateGeoObj="UPDATE `geoObjects` SET `type`=1 WHERE `id` in (".implode(',',$newIds).")";
$logEnter="INSERT INTO `log`
								(`user`,`text`,`geoObjectId`,`money`,`type`)
						 VALUES
								".implode(',',$logEnterVals);

mysql_query('START TRANSACTION');

mysql_query($updateUser) or die(handleError('Не удалось обновить данные игрока.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));
mysql_query($updateHouse) or die(handleError('Не удалось обновить данные строения.',__FILE__,false,$updateHouse,$nowUserInfo['id'],true));
mysql_query($updateGeoObj) or die(handleError('Не удалось обновить данные строения.',__FILE__,false,$updateGeoObj,$nowUserInfo['id'],true));
mysql_query($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));

mysql_query('COMMIT');

echo json_encode(['result'=>'ok']);
?>
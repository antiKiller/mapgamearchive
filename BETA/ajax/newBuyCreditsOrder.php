<?php
require '../configUsers.php';

if (!$_POST['kurs'] || !is_numeric($_POST['kurs']))
{
	echo json_encode(['result'=>'Неверная идентификатор ордера на покупку кредитов.']);
	exit();
}
$kurs=(float)$_POST['kurs'];

if (!$_POST['credits'] || !is_numeric($_POST['credits']))
{
	echo json_encode(['result'=>'Неверная идентификатор ордера на покупку кредитов.']);
	exit();
}
$credits=(int)$_POST['credits'];

if ($kurs<100)
{
	echo json_encode(['result'=>'Курс кредитов не может быть меньше 100 монет.']);
	exit();
}

if ($credits<=0)
{
	echo json_encode(['result'=>'Количество кредитов, которое вы хотите купить, должно быть больше нуля.']);
	exit();
}

$kurs=round($kurs,2);
$summ=round($credits*$kurs,2);
$nalog=round($summ*0.05,2);
$itog=round($summ+$nalog,2);

if ($itog>$nowUserInfo['money'])
{
	echo json_encode(['result'=>'Для покупки '.$credits.' кредитов вам нужно '.$itog.' монет ('.$summ.' за '.$credits.' кредитов + 5% налог на создание ставки '.$nalog.'). А у вас есть только '.$nowUserInfo['money'].' монет.']);
	exit();
}

$updateUser="UPDATE `user` SET `money`=`money`-".$itog." WHERE `id`=".$nowUserInfo['id'];
$toLog="INSERT INTO `log` (`user`,`text`,`money`,`type`)
											VALUES
														(".$nowUserInfo['id'].",'Вы разместили заявку на покупку ".$credits." <img src=\"images/baks.png\" class=\"moneyIndicator\"> на бирже',".($itog*-1).",'Заявки на бирже')";
$updateExchange="INSERT INTO `exchange` (`user`,`kurs`,`credits`,`startCredits`,`type`)
	                   VALUES (".$nowUserInfo['id'].",".$kurs.",".$credits.",".$credits.",'buy')";

mysql_query("START TRANSACTION");

mysql_query($updateUser) or die(handleError('Не удалось обновить данные игрока.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));
mysql_query($toLog) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$toLog,$nowUserInfo['id'],true));
mysql_query($updateExchange) or die(handleError('Не обновить данные биржи.',__FILE__,false,$updateExchange,$nowUserInfo['id'],true));

mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
<?php
$_POST['creditsCount']=(int)$_POST['creditsCount'];
require "../configUsers.php";

if (!$_POST['creditsCount'] || !is_numeric($_POST['creditsCount']))
{
	echo json_encode(['result'=>'Неверное количество кредитов']);
	exit();
}

$credits=(int)$_POST['creditsCount'];
if ($credits<10)
{
	echo json_encode(['result'=>'Минимальное количество кредитов для покупки - 10 кредитов.']);
	exit();
}

$kurse=0.5;
$coeff=floor($credits/1500);
if ($coeff>0)
{
	$kurse=round($kurse-($coeff)/100,2);
}
if ($kurse<0.2)
{
	$kurse=0.2;
}
$all=round($credits*$kurse,2);

//УСЛОВИЯ АКЦИИ К КРЕДИТАМ:
//$credits=ceil($credits*1.5);

$insert="INSERT INTO `payment` (`user`,`credits`,`kurs`,`mustSumm`) VALUES (".$nowUserInfo['id'].",".$credits.",".$kurse.",".$all.")";
$res=mysql_query($insert) or die(handleError('Не удалось сохранить информацию о кол-ве покупаемых кредитов.',__FILE__,false,$insert,$nowUserInfo['id']));

if ($res)
{
	$idsSearch="SELECT max(`id`) `ids` FROM `payment` WHERE `user`=".$nowUserInfo['id']." and `credits`=".$credits." and `kurs`=".$kurse." and `mustSumm`=".$all;
	$resSearch=mysql_query($idsSearch) or die(handleError('Не удалось получить идентификатор покупки.',__FILE__,false,$idsSearch,$nowUserInfo['id']));
	$searchIds=mysql_fetch_assoc($resSearch);
	$nowId=$searchIds['ids'];
}

if (!$nowId || !is_numeric($nowId))
{
	echo json_encode(['result'=>'Не удалось записать в базу данных информацию.']);
	exit();
}

$_SESSION['payment']=[];
$_SESSION['payment']['value']=number_format($all,2,'.','');
$_SESSION['payment']['credits']=number_format($credits,2,'.',' ');
$_SESSION['payment']['id']=$nowId;

$content='Вы хотите купить '.$_SESSION['payment']['credits'].' <img class="moneyIndicator" src="images/baks.png"> за '.$_SESSION['payment']['value'].' рублей по курсу '.number_format($kurse,2,'.',' ').' рублей.<br><br>';
$content.='Вы можете оплатить двумя возможными способами: <br>
	<input type="button" class="buyCreditsConfirmed" id="w1" value="Перейти к оплате через \'Единый Кошелёк\'"/><br><br>
  <input type="button" class="buyCreditsConfirmed" id="sp" value="Перейти к оплате через \'SpryPay\'"><br><br>

<div style="position: relative; top: -10px; display: inline-block;">Яндекс.Деньги: </div>
<iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/small.xml?account=ACC_ID&quickpay=small&yamoney-payment-type=on&button-text=01&button-size=s&button-color=white&targets=%D0%9F%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B0+%D0%BA%D1%80%D0%B5%D0%B4%D0%B8%D1%82%D0%BE%D0%B2+%D0%B2+%D0%B8%D0%B3%D1%80%D0%B5+%22%D0%92%D0%BE%D0%B9%D0%BD%D0%B0+%D0%BE%D0%BB%D0%B8%D0%B3%D0%B0%D1%80%D1%85%D0%BE%D0%B2%22&default-sum='.number_format($all*1.005,2,'.','').'" width="126" height="31"></iframe>
<div style="position: relative; top: -10px; display: inline-block;"> <b>Внимание!</b> Задержка зачисления кредитов при оплате через Яндекс.Деньги может достигать одних суток.</div>
<br><br>
Зачисление кредитов обычно происходит в течении нескольких минут после оплаты. Спасибо.';

echo json_encode(['result'=>'ok','content'=>$content]);
?>
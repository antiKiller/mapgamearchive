<?php
require "../configUsers.php";
$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));
$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$setManagedHouses='';
$setUnManagedHouses='';

switch ($mode)
{
	case 'forAll':
	{
		$setManagedHouses="UPDATE `house` SET `isManaged`=1 WHERE `isManaged`=0 and `user`=".$nowUserInfo['id'];
		break;
	}
	
	case 'onlyChecked':
	{
		$setManagedHouses="UPDATE `house` SET `isManaged`=1 WHERE `isManaged`=0 and `user`=".$nowUserInfo['id']." and `geoObjectId` in (".$objIds.")";
		$setUnManagedHouses="UPDATE `house` SET `isManaged`=0 WHERE `isManaged`=1 and `user`=".$nowUserInfo['id']." and `geoObjectId` not in (".$objIds.")";
		break;
	}
	
	case 'exeptChecked':
	{
		$setManagedHouses="UPDATE `house` SET `isManaged`=1 WHERE `isManaged`=0 and `user`=".$nowUserInfo['id']." and `geoObjectId` not in (".$objIds.")";
		$setUnManagedHouses="UPDATE `house` SET `isManaged`=0 WHERE `isManaged`=1 and `user`=".$nowUserInfo['id']." and `geoObjectId` in (".$objIds.")";
		break;
	}
}


mysql_query("START TRANSACTION");

if ($setManagedHouses)
{
	mysql_query($setManagedHouses) or die(handleError('Не удалось обновить данные зданий.',__FILE__,false,$setManagedHouses,$nowUserInfo['id'],true));
}

if ($setUnManagedHouses)
{
	mysql_query($setUnManagedHouses) or die(handleError('Не удалось обновить данные зданий.',__FILE__,false,$setUnManagedHouses,$nowUserInfo['id'],true));
}


mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>

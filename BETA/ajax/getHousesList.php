<?php
require "../configUsers.php";

$total=0;
$recods=[];
$_POST['sidx']=preg_replace('/name asc/', '', $_POST['sidx'],1);

$order=trim($_POST['sidx'],', ');
if ($_POST['sord']!='asc')
	$order.=' '.$_POST['sord'];

$order=trim($order,', ');
if (empty($order))
{
	$order='`name` asc';
}

$search='';
if ($_POST['_search']=='true' && $_POST['filters'])
{
	$filtres=json_decode($_POST['filters']);
	foreach ($filtres->rules as $key => $searchRule)
	{
		$searchRule->data=htmlentities($searchRule->data, ENT_QUOTES, 'UTF-8');
		$op='=';
		switch ($searchRule->op)
		{
			case 'eq': $op='=';
								 break;

			case 'ne': $op='!=';
								 break;

			case 'cn': $op=' LIKE ';
								 $searchRule->data='%'.$searchRule->data.'%';
								 break;

			case 'nc': $op=' NOT LIKE ';
								 $searchRule->data='%'.$searchRule->data.'%';
								 break;

			case 'bw': $op=' LIKE ';
								 $searchRule->data=$searchRule->data.'%';
								 break;

			case 'bn': $op=' NOT LIKE ';
								 $searchRule->data=$searchRule->data.'%';
								 break;

			case 'ew': $op=' LIKE ';
								 $searchRule->data='%'.$searchRule->data;
								 break;

			case 'en': $op=' NOT LIKE ';
								 $searchRule->data='%'.$searchRule->data;
								 break;

			case 'le': $op='<=';
								 break;

			case 'lt': $op='<';
								 break;

			case 'gt': $op='>';
								 break;

			case 'ge': $op='>=';
								 break;

			default: $op='=';
			break;
		}

		if($searchRule->field!='isManaged')
		{
			$search.=" and ".$searchRule->field.$op."'".$searchRule->data."'";
		}
			else
			{
				if ($searchRule->data=='Да')
				{
					$search.=" and ".$searchRule->field.$op."1";
				}
				if ($searchRule->data=='Нет')
				{
					$search.=" and ".$searchRule->field.$op."0";
				}
				if ($searchRule->data=='Необслуживаемое')
				{
					if ($op=='=')
					{
						$search.=" and (`isCosted`=1 or `isBuilded`=1 or `isSocial`=1)";
					}
						else
						{
							$search.=" and (`isCosted`=0 and `isBuilded`=0 and `isSocial`=0)";
						}
				}
			}
	}
}


$page=$_POST['page'];
$numRow=$_POST['rows'];
$startLimit=($page-1)*$numRow;

if (!$page || !$numRow)
{
	session_destroy();
	echo json_encode(['result'=>'unknown_user']);
	exit();
}

if ($numRow!=$nowUserInfo['rowOnHousesPage'])
{
	$updateUser="UPDATE `user` SET `rowOnHousesPage`=".$numRow." WHERE `id`=".$nowUserInfo['id'];
	mysql_query($updateUser) or die(handleError('Не удалось сохранить нужное кол-во домов на странице.',__FILE__,false,$updateUser,$nowUserInfo['id']));
}

$getAllRec="SELECT count(`id`) houses
						FROM `buildingsUsers`
						WHERE `user`=".$nowUserInfo['id'].$search;
$resCount=mysql_query($getAllRec) or die(handleError('Ошибка определения кол-ва домов пользователя.',__FILE__,false,$getAllRec,$nowUserInfo['id']));
$counts=mysql_fetch_assoc($resCount);
$total=$counts['houses'];
$pages=ceil($counts['houses']/$numRow);


$getHouses="SELECT `geoObjectId`, `doxod`, `level`, `clients`, `bonus`, `competition`, UNIX_TIMESTAMP(`time`) `time`, `action`, `isCosted`, `isManaged`,
									 `address`, X(`coords`) `pos1`, Y(`coords`) `pos2`,
									 `name`, `isBuilded`, `img`, `isHouse`, `isEnterprise`, `isSocial`
						FROM `buildingsUsers`
						WHERE `user`=".$nowUserInfo['id'].$search."
						ORDER BY ".$order."
						LIMIT ".$startLimit.",".$numRow;
$resGetHouses=mysql_query($getHouses) or die(handleError('Ошибка поиска домов пользователя.',__FILE__,false,$getHouses,$nowUserInfo['id']));
while($record=mysql_fetch_assoc($resGetHouses))
{
	$record['id']=$record['geoObjectId'];
	if ($record['time']<time())
	{
		if ($record['isBuilded']==1)
		{
			$record['action']='<img src="images/build.png" class="buildObject" ids="'.$record['geoObjectId'].'" title="Построить">';
		}
		  else
		  {
				$record['action']='<img src="images/recalc.png" class="recalcObject" ids="'.$record['geoObjectId'].'" title="Пересчитать">';
				if ($record['isCosted']==0)
				{
					$record['action'].='<img src="images/destroy.png" class="destroyObject" ids="'.$record['geoObjectId'].'" title="Снести"> <img src="images/plus.png" class="sellObject" ids="'.$record['geoObjectId'].'" title="Продать на бирже"> <img src="images/imp.png" class="impObject" ids="'.$record['geoObjectId'].'" title="Улучшить">';
					if ($record['doxod']>0 && ($record['isHouse'] || $record['isEnterprise']))
					{
						$record['action'].=' <img src="images/rent.png" class="rentObject" ids="'.$record['geoObjectId'].'" title="Сдать в аренду">';
					}
				}
					else
					{
						$record['action']='<img src="images/close.png" class="canselSellBuilding" ids="'.$record['geoObjectId'].'" title="Убрать с продажи">';
					}
		  }
			$record['time']=' - ';
	}
		else
		{
			$record['time']=date('H:i:s d.m.Y', $record['time']);
			$record['action'].=' <img src="images/speed.png" class="speedUpAction" ids="'.$record['geoObjectId'].'" title="Сократить срок работ"> <img src="images/recalc.png" class="recalcObject" ids="'.$record['geoObjectId'].'" title="Пересчитать">';
		}

	if ($record['isManaged']==1)
	{
		$record['isManaged']='Да';
	}
		else
		{
			$record['isManaged']='Нет';
		}

	if ($record['isBuilded']==1 || $record['isSocial']==1 || $record['isCosted']==1)
	{
		$record['isManaged']='Необслуживаемое';
	}
	if ($record['isSocial']==1 || $record['isHouse']==1 || $record['isBuilded']==1)
	{
		$record['competition']='0';
	}

	if ($record['isEnterprise']==1 || $record['isBuilded']==1)
	{
		$record['bonus']='0';
	}
	$record['address'].=' <img src="images/map.png" class="goToMap" title="Перейти к обьекту на карте" coords="'.$record['pos1'].','.$record['pos2'].'" zoom="18">';
	$recods[]=$record;
}

echo json_encode(['result'=>'ok','page'=>$page,'total'=>$pages,'records'=>$total,'rows'=>$recods]);
?>
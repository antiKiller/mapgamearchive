<?php
require "../configUsers.php";

if (!$_POST['ids'] || !is_numeric($_POST['ids']))
{
	handleError('Неверный идентификатор аукциона!',__FILE__);
}
$ids=(int)$_POST['ids'];

$dropAuc="DELETE FROM `auction_user` WHERE `auc`=".$ids." and `user`=".$nowUserInfo['id'];
mysql_query($dropAuc) or die(handleError('Не удалось удалить сведения о связанности аукциона с вашим аккаунтом.',__FILE__,false,$dropAuc,$nowUserInfo['id']));

$logEnter="INSERT INTO `log`
							(`user`,`text`,`geoObjectId`,`money`,`type`)
					 VALUES
							(".$nowUserInfo['id'].", 'Прекращение отслеживания аукциона',(SELECT b.`id` FROM `auction` a, `geoObjects` b WHERE a.`id`=".$ids." and a.`geoObjectId`=b.`id`),0,'Отслеживание аукционов')";
mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id']));

echo json_encode(['result'=>'ok']);
?>
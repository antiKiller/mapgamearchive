<?php
require "../configUsers.php";

if (!$_POST['message'])
{
	handleError('Сообщение не должно быть пустым!',__FILE__);
}

$message=htmlentities($_POST['message'], ENT_QUOTES, 'UTF-8');
if (mb_strlen($message)>10000)
{
	handleError('Слишком длинное сообщение! Максимум 5000 символов, а у вас '.mb_strlen($message),__FILE__);
}

$insertMessage="INSERT INTO `chat` (`user`,`message`) VALUES (".$nowUserInfo['id'].",'".$message."')";
mysql_query($insertMessage) or die(handleError('Не удалось сохранить сообщение чата.',__FILE__,false,$getMessagesList,$nowUserInfo['id']));

echo json_encode(['result'=>'ok']);
?>
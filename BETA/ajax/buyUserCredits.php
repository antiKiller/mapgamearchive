<?php
require '../configUsers.php';

if (!$_POST['buyId'] || !is_numeric($_POST['buyId']))
{
	handleError('Неверный идентификатор строения.',__FILE__);
}
$ids=(int)$_POST['buyId'];

if (!$_POST['credits'] || !is_numeric($_POST['credits']))
{
	handleError('Неверная идентификатор ордера на покупку кредитов.',__FILE__);
}
$credits=(int)$_POST['credits'];

if ($credits<=0)
{
	handleError('Количество кредитов, которое вы хотите купить, должно быть больше нуля.',__FILE__);
}

if ($ids!=0)
{
	$getSelledRow="SELECT * FROM `exchange` WHERE `id`=".$ids." and `type`='sell'";
	$resGetSelledRow=mysql_query($getSelledRow) or die(handleError('Не удалось получить список покупателей кредитов.',__FILE__,false,$getSelledRow,$nowUserInfo['id']));
	$selledRow=mysql_fetch_assoc($resGetSelledRow);
	if (!$selledRow)
	{
		handleError('Такая заявка на бирже не найдена.',__FILE__);
	}

	if ($selledRow['credits']<$credits)
	{
		handleError('Максимум вы можете купить '.$selledRow['credits'].', а вы пытаетесь купить '.$credits,__FILE__);
	}

	$profit=round($credits*$selledRow['kurs'],2);
	$nalog=round($profit*0.1,2);
	$fullProfit=round($profit+$nalog,2);
	
	if ($nowUserInfo['money']<$fullProfit)
	{
		handleError('У вас всего '.$nowUserInfo['money'].' монет, а вы пытаетесь купить '.$credits.' кредитов, цена которых составляет '.$fullProfit.' монет.',__FILE__);
	}
	
	$updateUser="UPDATE `user` SET `credits`=`credits`+".$credits.", `money`=`money`-".$fullProfit." WHERE `id`=".$nowUserInfo['id'];
	$updateOtherUser="UPDATE `user` SET `money`=`money`+".$profit." WHERE `id`=".$selledRow['user'];
	$toLog="INSERT INTO `log` (`user`,`otherUser`,`text`,`money`,`credits`,`type`)
											VALUES
														(".$nowUserInfo['id'].",".$selledRow['user'].",'Вы купили кредиты на бирже',".($fullProfit*-1).",".$credits.",'Продажа кредитов на бирже'),
														(".$selledRow['user'].",".$nowUserInfo['id'].",'По вашей заявке продали кредиты на бирже',".$profit.",0,'Заявки на бирже')";
	$updateExchange="UPDATE `exchange` SET `credits`=`credits`-".$credits." WHERE `id`=".$selledRow['id'];
	
	mysql_query("START TRANSACTION");
	
	mysql_query($updateUser) or die(handleError('Не удалось обновить ваши игрока.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));
	mysql_query($updateOtherUser) or die(handleError('Не удалось обновить данные игрока.',__FILE__,false,$updateOtherUser,$nowUserInfo['id'],true));
	mysql_query($toLog) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$toLog,$nowUserInfo['id'],true));
	mysql_query($updateExchange) or die(handleError('Не удалось обновить данные биржи.',__FILE__,false,$updateExchange,$nowUserInfo['id'],true));

	mysql_query("COMMIT");
}

echo json_encode(['result'=>'ok']);
?>
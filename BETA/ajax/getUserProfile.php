<?php
session_start();
include '../config.php';

$nowUserId=$_SESSION['id'];
$nowUserPass=$_SESSION['pass'];

$userId=htmlentities($_POST['profile'], ENT_QUOTES, 'UTF-8');

if ($userId=='false' && (!$nowUserId || !$nowUserPass))
{
	$content='<h3>Авторизуйтесь чтобы посмотреть свой профиль!</h3>';
	echo json_encode(['result'=>'ok','userProfile'=>$content]);
	exit();
}

if ($userId=='false' && $nowUserId && $nowUserPass)
{
	$userId=$nowUserId;
}

$userId=(int)$userId;

$getUser="SELECT *, UNIX_TIMESTAMP(`registred`) regDate, UNIX_TIMESTAMP(`lastEnter`) lastEnterDate FROM `user` WHERE `id`=".$userId;
$resGetUser=mysql_query ($getUser) or die(handleError('Ошибка получения профиля пользователя.',__FILE__,false,$getUser,$nowUserInfo['id']));
$nowUser=mysql_fetch_array($resGetUser);
if (!$nowUser)
{
	$content='<h3>Пользователь не найден!</h3>';
	echo json_encode(['result'=>'ok','userProfile'=>$content]);
	exit();
}

if ($nowUser['id']==$nowUserId)
{
	$checkedField='';
	if ($nowUser['isBlinkedChat'])
	{
		$checkedField='checked="checked"';
	}
	$userName='<input type="text" id="newUserName" value="'.$nowUser['name'].'">
							<span id="lenghtNewUserName">(длина имени символов <span id="nowUserNameLength">'.mb_strlen($nowUser['name'],'UTF-8').'</span> из 60 возможных)</span>
							<input type="button" id="changeUserName" value="Изменить"><br><br>
							Анимировать значок чата, в случае если в чате есть непросмотреные сообщения: <input type="checkbox" id="isBlinkedChat" '.$checkedField.'>';
}
 else
 {
	 $userName='<div id=printedUserName>'.$nowUser['name'].'</div>';
 }

if ($nowUser['lastEnterDate']>(time()-60*2))
{
	$lastEnter='Сейчас в игре и находится на странице: '.$nowUser['lastPage'];
}
	else
	{
		$lastEnter='Последний раз заходил: '.date('H:i:s d.m.Y',$nowUser['lastEnterDate']);
	}

$content='<img src="'.$nowUser['photo_big'].'" id="userProfileImage">
					<a href="'.$nowUser['accountLink'].'" target="_blank" id="userSocialNetworkLink"><img title="Перейти к профилю пользователя в социальной сети" src="images/profile.png"></a>
	        '.$userName.'
					<div id="userDatesInfo">Впервые зашёл в игру: '.date('H:i:s d.m.Y',$nowUser['regDate']).' &nbsp;  &nbsp; '.$lastEnter.'</div>';

echo json_encode(['result'=>'ok','userProfile'=>$content]);
?>
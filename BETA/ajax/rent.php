<?php
require "../configUsers.php";

$_POST['ids']=preg_replace('/[,\ ]+/i', ',', trim($_POST['ids'],' ,'));

$objIds=trim($_POST['ids'],', ');
$mode=trim($_POST['mode'],', ');
$period=(int)$_POST['period'];

if (!$period || !is_numeric($period) || !in_array($period, [1,2,4,8,12,24]))
{
	handleError('Неверный период сдачи в аренду!',__FILE__);
}

if (!in_array($mode,['forAll','onlyChecked','exeptChecked']))
{
	handleError('Неверный тип действия.',__FILE__);
}

if (!$objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	handleError('Неверный список идентификаторов строений.',__FILE__);
}

$dopQuery='';
if ($objIds && in_array($mode,['onlyChecked','exeptChecked']))
{
	if ($mode=='onlyChecked')
	{
		$operator='in';
	}
		else
		{
			$operator='not in';
		}
	$dopQuery="and `geoObjectId` ".$operator." (".$objIds.")";
}

$time=time();
$totime=$time+$period*3600;

$profitBonuses=[RENT_PROFIT_BASIC_FIRST,RENT_PROFIT_BASIC_SECOND,RENT_PROFIT_BASIC_THIRD,RENT_PROFIT_BASIC_FOURTH,RENT_PROFIT_BASIC_FIFTH,RENT_PROFIT_BASIC_SIXTH];
if ($nowUserInfo['registredTime']>(time()-IS_NEWBIE_TIME) && $nowUserInfo['kapitalization']<100000)
{
	$profitBonuses=[RENT_PROFIT_NEWBIE_FIRST,RENT_PROFIT_NEWBIE_SECOND,RENT_PROFIT_NEWBIE_THIRD,RENT_PROFIT_NEWBIE_FOURTH,RENT_PROFIT_NEWBIE_FIFTH,RENT_PROFIT_NEWBIE_SIXTH];
}

switch ($period)
{
	case 1: $profitFactor=$period*$profitBonuses[0];
					$toRentTime='1 час';
					break;
	case 2: $profitFactor=$period*$profitBonuses[1];
					$toRentTime='2 часа';
					break;
	case 4: $profitFactor=$period*$profitBonuses[2];
					$toRentTime='4 часа';
					break;
	case 8: $profitFactor=$period*$profitBonuses[3];
					$toRentTime='8 часов';
					break;
	case 12: $profitFactor=$period*$profitBonuses[4];
					 $toRentTime='12 часов';
					 break;
	case 24: $profitFactor=$period*$profitBonuses[5];
					 $toRentTime='24 часа';
					 break;

	default: handleError('Неверный период сдачи в аренду!',__FILE__);
}

$fullProfit=0;
$toLog=[];
$aucRent=[];
$aucArr=[];

$getHouse="SELECT SUM(`doxod`) `doxod`, `country`, `administrative`, `subadministrative`, `locality`, `street`, `type`
					 FROM `buildingsUsers`
					 WHERE `user`=".$nowUserInfo['id']." and `time`<=CURRENT_TIMESTAMP and `isCosted`=0 and `doxod`>0 and
								 (`isHouse`=1 or `isEnterprise`=1) ".$dopQuery."
					 GROUP BY `street`, `locality`, `subadministrative` , `administrative`, `country`";

$resGetHouseInfo=mysql_query($getHouse) or die(handleError('Ошибка получения списка домов.',__FILE__,false,$getHouse,$nowUserInfo['id']));
$idsList=null;
$getHouse=null;

while ($houseInfo=mysql_fetch_assoc($resGetHouseInfo))
{
	if ($houseInfo['doxod']<=0)
	{
		continue;
	}
	$rentProfit=round($houseInfo['doxod']*$profitFactor,2);
	$fullProfit+=$rentProfit;

	$streetSql="SELECT `id`, `geoObjectId`, `user`, `address`
							FROM `auctionsUsers`
							WHERE `type`=2 and `country`='".$houseInfo['country']."' and `administrative`='".$houseInfo['administrative']."' and `locality`='".$houseInfo['locality']."' and `street`='".$houseInfo['street']."'";
	$resStreet=mysql_query($streetSql) or die(handleError('Ошибка выборки аукционов для получения налога.',__FILE__,false,$streetSql,$nowUserInfo['id'],true));
	while ($streetInf=mysql_fetch_assoc($resStreet))
	{
		$aucRent[$streetInf['geoObjectId']]['rent']+=$rentProfit;
		$aucRent[$streetInf['geoObjectId']]['user']=$streetInf['user'];
		$aucRent[$streetInf['geoObjectId']]['persent']=RENT_PERSENT_STREET;
		$aucRent[$streetInf['geoObjectId']]['id']=$streetInf['id'];
	}

	if (!$houseInfo['locality'])
	{
		$houseInfo['locality']='NOT_LOCALITY';
	}
	$aucArr[$houseInfo['country']][$houseInfo['administrative']][$houseInfo['locality']]['summ']+=$rentProfit;
	$aucArr[$houseInfo['country']][$houseInfo['administrative']]['summ']+=$rentProfit;
	$aucArr[$houseInfo['country']]['summ']+=$rentProfit;
}

if ($aucArr)
{
	foreach ($aucArr as $country=>$countryVals)
	{
		if ($country=='summ')
		{
			continue;
		}
		$countryInf=[];
		if ($country)
		{
			$countrysSql="SELECT `id`, `geoObjectId`, `user`, `address`
										FROM `auctionsUsers`
										WHERE `type`=7 and `country`='".$country."'";
			$resCountry=mysql_query($countrysSql) or die(handleError('Ошибка выборки областей для получения налога.',__FILE__,false,$countrysSql,$nowUserInfo['id'],true));
			$countryInf=mysql_fetch_assoc($resCountry);
			if ($countryInf['geoObjectId'])
			{
				$aucRent[$countryInf['geoObjectId']]['rent']+=$aucArr[$country]['summ'];
				$aucRent[$countryInf['geoObjectId']]['user']=$countryInf['user'];
				$aucRent[$countryInf['geoObjectId']]['persent']=RENT_PERSENT_COUNTRY;
				$aucRent[$countryInf['geoObjectId']]['id']=$countryInf['id'];
			}
		}
		foreach ($aucArr[$country] as $adms=>$admsVals)
		{
			if ($adms=='summ')
			{
				continue;
			}
			foreach ($aucArr[$country][$adms] as $localicy=>$localicyVals)
			{
				if ($localicy=='summ')
				{
					continue;
				}
				if ($localicy)
				{
					$local=$localicy;
					if ($localicy==='NOT_LOCALITY')
					{
						$localicy='';
						$local='NOT_LOCALITY';
					}
						
					$localitySql="SELECT `id`, `geoObjectId`, `user`, `address`
												FROM `auctionsUsers`
												WHERE `type`=4 and `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."'";
					$resLocality=mysql_query($localitySql) or die(handleError('Ошибка выборки городов для получения налога.',__FILE__,false,$localitySql,$nowUserInfo['id'],true));
					while ($localityInf=mysql_fetch_assoc($resLocality))
					{
						$aucRent[$localityInf['geoObjectId']]['rent']+=$aucArr[$country][$adms][$local]['summ'];
						$aucRent[$localityInf['geoObjectId']]['user']=$localityInf['user'];
						$aucRent[$localityInf['geoObjectId']]['persent']=RENT_PERSENT_LOCALITY;
						$aucRent[$localityInf['geoObjectId']]['id']=$localityInf['id'];
					}

					$searchAreas="SELECT `id`, `geoObjectId`, `user`, `address`
												FROM `auctionsUsers` a
												WHERE a.`country`='".$country."' and `administrative`='".$adms."' and a.`type`=5 and
												(
													`subadministrative` in
													(
														SELECT `subadministrative` FROM `geoObjects` b
														WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`subadministrative`!='' and
															(
																b.`subadministrative`=a.`administrative` or
																a.`subadministrative`=b.`administrative` or
																a.`subadministrative`=b.`subadministrative`
															)
														GROUP BY `subadministrative`
													)
														or
													(
														`administrative` in
														(
															SELECT `administrative` FROM `geoObjects` b
															WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	a.`administrative`=b.`administrative`
																)
															GROUP BY `administrative`
														) and a.`subadministrative`=''
													)
												)";
					$resSearchAreas=mysql_query($searchAreas) or die(handleError('Ошибка выборки регионов для получения налога.',__FILE__,false,$searchAreas,$nowUserInfo['id'],true));
					while($areaInf=mysql_fetch_assoc($resSearchAreas))
					{
						$aucRent[$areaInf['geoObjectId']]['rent']+=$aucArr[$country][$adms][$local]['summ'];
						$aucRent[$areaInf['geoObjectId']]['user']=$areaInf['user'];
						$aucRent[$areaInf['geoObjectId']]['persent']=RENT_PERSENT_AREA;
						$aucRent[$areaInf['geoObjectId']]['id']=$areaInf['id'];
					}

					$searchRegions="SELECT `id`, `geoObjectId`, `user`, `administrative`, `subadministrative`, `address`
													FROM `auctionsUsers` a
													WHERE a.`country`='".$country."' and a.`type`=6 and
													(
														`subadministrative` in
														(
															SELECT `subadministrative` FROM `geoObjects` b
															WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`subadministrative`!='' and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	a.`administrative`=b.`administrative`
																)
														GROUP BY `subadministrative`
													)
														or
													(
														`administrative` in
														(
															SELECT `administrative` FROM `geoObjects` b
															WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`administrative`!='' and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	a.`administrative`=b.`administrative`
																)
															GROUP BY `administrative`
														) and a.`subadministrative`=''
													)
														or
													`subadministrative` in
													(
														SELECT `administrative` FROM `geoObjects` b
														WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`subadministrative`!='' and
														 (
															b.`subadministrative`=a.`administrative` or
															a.`subadministrative`=b.`administrative` or
															a.`administrative`=b.`administrative`
														 )
														GROUP BY `administrative`
													 )
														or
													 (
															`administrative` in
															(
															 SELECT `subadministrative` FROM `geoObjects` b
															 WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`administrative`!='' and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	(a.`administrative`=b.`administrative`)
																)
															 GROUP BY `subadministrative`
															)
														)
													)";
					$resSearchRegions=mysql_query($searchRegions) or die(handleError('Ошибка выборки областей для получения налога.',__FILE__,false,$searchRegions,$nowUserInfo['id'],true));
					while($regionInf=mysql_fetch_assoc($resSearchRegions))
					{
						$aucRent[$regionInf['geoObjectId']]['rent']+=$aucArr[$country][$adms][$local]['summ'];
						$aucRent[$regionInf['geoObjectId']]['user']=$regionInf['user'];
						$aucRent[$regionInf['geoObjectId']]['persent']=RENT_PERSENT_REGION;
						$aucRent[$regionInf['geoObjectId']]['id']=$regionInf['id'];

						if ($regionInf['subadministrative']==$adms)
						{
							$subRegionsSql="SELECT `id`, `geoObjectId`, `user`, `address`
															FROM `auctionsUsers`
															WHERE `country`='".$country."' and `type`=6 and `administrative`='".$regionInf['administrative']."' and `subadministrative`=''";
							$resSubRegions=mysql_query($subRegionsSql) or die(handleError('Ошибка выборки областей для получения налога.',__FILE__,false,$subRegionsSql,$nowUserInfo['id'],true));

							while($subRegionInf=mysql_fetch_assoc($resSubRegions))
							{
								$aucRent[$subRegionInf['geoObjectId']]['rent']+=$aucArr[$country][$adms][$local]['summ'];
								$aucRent[$subRegionInf['geoObjectId']]['user']=$subRegionInf['user'];
								$aucRent[$subRegionInf['geoObjectId']]['persent']=RENT_PERSENT_REGION;
								$aucRent[$subRegionInf['geoObjectId']]['id']=$subRegionInf['id'];
							}
						}
					}
				}
			}
		}
	}
}
$aucArr=null;

$usersProfit=[];
$toAucLogs=[];

mysql_query('START TRANSACTION');

foreach ($aucRent as $geoObjId=>$params)
{
	$profit=round($params['rent']*$params['persent'],2);
	$auctionUpdate="UPDATE `auction` SET `doxod`=`doxod`+".$profit." WHERE `geoObjectId`=".$geoObjId;
	mysql_query($auctionUpdate) or die(handleError('Не удалось обновить сведениня о доходности аукциона.',__FILE__,false,$auctionUpdate,$nowUserInfo['id'],true));

	$usersProfit[$params['user']]+=$profit;

	$toAucLogs[]="(".$params['id'].",".$profit.")";
	$toLog[]="(".$params['user'].", 'Рента со сдачи в аренду',".$geoObjId.",".$profit.",'Рента')";
}

foreach ($usersProfit as $user=>$profit)
{
	$userProfitSql="UPDATE `user` SET  `money`=`money`+".$profit." WHERE `id`=".$user;
	mysql_query($userProfitSql) or die(handleError('Не удалось начислить ренту игроку.',__FILE__,false,$userProfitSql,$nowUserInfo['id'],true));
}

$houseUpdate="UPDATE `buildingsUsers` a
								SET `time`=FROM_UNIXTIME(".$totime."), `action`='Аренда'
							WHERE `user`=".$nowUserInfo['id']." and `time`<=CURRENT_TIMESTAMP and `isCosted`=0 and `doxod`>0 and
										(`isHouse`=1 or `isEnterprise`=1) ".$dopQuery;
mysql_query($houseUpdate) or die(handleError('Не удалось обновить время выхода строений из аренды.',__FILE__,false,$houseUpdate,$nowUserInfo['id'],true));

if ($fullProfit>0)
{
	$countOfHouses="SELECT count(`id`) housesCount FROM `house` WHERE `user`=".$nowUserInfo['id'];
	$resCountOfHouse=mysql_query($countOfHouses) or die(handleError('Не удалось получить кол-во ваших строений.',__FILE__,false,$countOfHouses,$nowUserInfo['id'],true));
	$countHouses=mysql_fetch_assoc($resCountOfHouse);
	$persent=floor($countHouses['housesCount']/100)*PROFIT_TAX;

	$toLog[]="(".$nowUserInfo['id'].",'Передача в аренду на <i>".$toRentTime."</i> строений',null,".$fullProfit.",'Аренда')";

	if ($persent>0)
	{
		$taxManagerSql="SELECT `discount` FROM `taxManager` WHERE `user`=".$nowUserInfo['id']." and `endDate`>CURRENT_TIMESTAMP";
		$resTaxManagerSql=mysql_query($taxManagerSql) or die(handleError('Не удалось информацию о бухгалтере.',__FILE__,false,$taxManagerSql,$nowUserInfo['id']));
		$taxManager=mysql_fetch_assoc($resTaxManagerSql);
		if ($taxManager['discount']>0)
		{
			$persent=number_format($persent-$persent*$taxManager['discount']/100,2,'.','');
		}
		$tax=round($fullProfit*$persent/100,2);
		$fullProfit=$fullProfit-$tax;
		$toLog[]="(".$nowUserInfo['id'].",'Подоходный налог ".number_format($persent, 2, '.', '')."% со сдачи строений в аренду ',null,-".$tax.",'Налог')";
	}
	$setUserProfit="UPDATE `user` SET  `money`=`money`+".$fullProfit." WHERE `id`=".$nowUserInfo['id'];
	mysql_query ($setUserProfit) or die(handleError('Не удалось зачислить монеты за аренду игроку.',__FILE__,false,$setUserProfit,$nowUserInfo['id'],true));
}

if (count($toLog)>0)
{
	$logEnter="INSERT INTO `log`
								(`user`,`text`,`geoObjectId`,`money`,`type`)
						 VALUES ".implode(',', $toLog);
	mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));
}

if (count($toAucLogs)>0)
{
	$aucLogUpd="INSERT INTO `auction_log`
								(`auc`,`money`)
							VALUES
								".implode(',', $toAucLogs);
	mysql_query ($aucLogUpd) or die(handleError('Ошибка записи в лог аукционов информации.',__FILE__,false,$aucLogUpd,$nowUserInfo['id'],true));
}
mysql_query('COMMIT');

echo json_encode(['result'=>'ok']);
?>
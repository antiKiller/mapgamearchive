<?php
include '../config.php';

$ids=(int)$_POST['type'];
if (!is_numeric($ids))
{
	handleError("Неверный тип обьекта.", __FILE__);
}

$selInfo="SELECT * FROM `geoObjectsTypes` WHERE `id`='".$ids."' and `isAuction`=0";
$resSelInfo=mysql_query($selInfo) or die(handleError('Не удалось получить данные строения или аукциона.',__FILE__,false,$selInfo));
$rowSelInfo=mysql_fetch_assoc($resSelInfo);
if (!$rowSelInfo)
{
	handleError("Такой тип обьекта не найден", __FILE__);
}

if (!$rowSelInfo['isHouse'] && !$rowSelInfo['isEnterprise'] && !$rowSelInfo['isSocial'])
{
		handleError("Это здание не подлежит рассчёту.", __FILE__);
}

$resultForm='';
$clients=0;
$calcForm='Уровень здания: <span class="aligment"><input id="buildLevel" type="number" value="1"></span><br>';

if ($rowSelInfo['isHouse'])
{
	$type='house';
	$calcForm.='В радиусе есть: <br>';
	$getSocials="SELECT * FROM `geoObjectsTypes` WHERE `isSocial`=1 ORDER BY `order`";
	$resGetSocials=mysql_query($getSocials) or die(handleError('Не удалось получить данные о социальных зданиях.',__FILE__,false,$getSocials));
	while($social=mysql_fetch_assoc($resGetSocials))
	{
		$calcForm.=$social['name'].' <span class="aligment"><input name="bonusForHoses" bonus="'.$social['basicBonus'].'" update="'.$social['updateBonus'].'" type="number" value="0"> уровня</span><br>';
	}


	$resultForm.='Кол-во жителей: <span class="aligment"><input id="clientsCount" readonly="readonly" type="text" value="'.$rowSelInfo['fixClients'].'"></span><br>';
	$clients=$rowSelInfo['fixClients'];
}
	elseif ($rowSelInfo['isEnterprise'])
	{
		$type='enterprise';
		$getClientsSql="SELECT FLOOR(AVG(`clients`)) `avgClients` FROM `buildingsUsers` WHERE `type`='".$ids."' and `doxod`>0 and `clients`>0 and `isEnterprise`=1";
		$resGetClients=mysql_query($getClientsSql) or die(handleError('Не удалось получить данные о среднем кол-ве клиентов этого здания.',__FILE__,false,$getClientsSql));
		$getClients=mysql_fetch_assoc($resGetClients);
		if (!$getClients)
		{
			handleError("Не удалось получить данные о среднем кол-ве клиентов этого здания.", __FILE__);
		}
		$calcForm.='Кол-во клиентов: <span class="aligment"><input id="clientsCount" type="text" value="'.$getClients['avgClients'].'"> (по-умолчанию указано текущее среднее кол-во клиентов)</span><br>';
		$clients=$getClients['avgClients'];
	}
if(!$rowSelInfo['isHouse'])
{
	$resultForm.='Радиус действия: <span class="aligment"><input id="buildRadius" readonly="readonly" type="text" value="'.$rowSelInfo['radius'].'"> (в километрах)</span><br>';
}

$resultForm.='
	Цена строительства: <span class="aligment"><input id="buildCost" type="text" readonly="readonly" value="'.$rowSelInfo['basicCost'].'"> <img class="moneyIndicator" src="images/money.png">
				<input id="buildCostCreds" type="text" readonly="readonly" value="'.$rowSelInfo['creditsCost'].'"> <img class="moneyIndicator" src="images/baks.png"></span>
	<br>
	Стоимость улучшения: <span class="aligment"><input id="levelUpCost" readonly="readonly" type="text" value="0"> (с предыдущего уровня до указанного)</span><br>
	Суммарные затраты: <span class="aligment"><input id="allCost" readonly="readonly" type="text" value="'.$rowSelInfo['basicCost'].'"> (в монетах, строительство + улучшение до текущего уровня)</span><br>
	Время строительства: <span class="aligment"><input id="buildTime" readonly="readonly" type="text" value="'.$rowSelInfo['basicTime'].'"> (в минутах)</span><br>
	Время улучшения: <span class="aligment"><input id="levelUpTime" readonly="readonly" type="text" value="0"> (в минутах, с предыдущего уровня до указанного)</span><br>
	Суммарное время: <span class="aligment"><input id="allTime" readonly="readonly" type="text" value="'.$rowSelInfo['basicTime'].'"> (в минутах, строительство + улучшение до текущего уровня)</span><br>
	Бонус к доходу: <span class="aligment"><input id="profitBonus" readonly="readonly" type="text" value="0"> (в %)</span><br>
	';

if($rowSelInfo['isEnterprise'] || $rowSelInfo['isHouse'])
{
	$resultForm.='
		Доход: <span class="aligment"><input id="clientsProfit" readonly="readonly" type="text" value="'.number_format($rowSelInfo['basicDoxod']*$clients, 2, '.', '').'"></span><br>
		Срок окупаемости: <span class="aligment"><input id="paybackTime" readonly="readonly" type="text" value="'.ceil($rowSelInfo['basicCost']/($clients*$rowSelInfo['basicDoxod']*RENT_PROFIT_BASIC_FIRST)).'"> (часов, при сдаче на 1 час)</span><br>';
}

$calcForm.='
	<input id="updateTime" type="hidden" value="'.$rowSelInfo['updateTime'].'">
	<input id="updateCost" type="hidden" value="'.$rowSelInfo['updateCost'].'">
	<input id="updateDoxod" type="hidden" value="'.$rowSelInfo['updateDoxod'].'">
	<input id="updateBonus" type="hidden" value="'.$rowSelInfo['updateBonus'].'">
	<input id="basicBonus" type="hidden" value="'.$rowSelInfo['basicBonus'].'">
	<input id="basicDoxod" type="hidden" value="'.$rowSelInfo['basicDoxod'].'">
	<input id="fixClients" type="hidden" value="'.$rowSelInfo['fixClients'].'">
	<input id="paymentPersent" type="hidden" value="'.RENT_PROFIT_BASIC_FIRST.'">';

if($rowSelInfo['isSocial'])
{
	$type='social';
	$resultForm.='
		Бонус клиентам: <span class="aligment"><input id="bonusForClients" readonly="readonly" type="text" value="'.$rowSelInfo['basicBonus'].'"></span><br>';
}

echo json_encode(['result'=>'ok','calcForm'=>$calcForm.'<br>','resForm'=>$resultForm,'type'=>$type]);
?>
<?php
$pages=1;
$page=1;
require '../configUsers.php';

$type=$_POST['type'];

$recods=[];

switch ($type)
{
	case 'buyer':
	{
		$getBuyCredits="SELECT a.`id`, a.`credits`, a.`kurs`, a.`user`, UNIX_TIMESTAMP(a.`time` + INTERVAL 3 MONTH) `time`, b.`id` `userId`, b.`name`
										FROM `exchange` a,
												 `user` b
										WHERE a.`user`=b.`id` and a.`type`='buy' and a.`credits`>0
										ORDER BY `kurs` DESC";
		$resBuy=mysql_query($getBuyCredits) or die(handleError('Не удалось получить список покупателей кредитов.',__FILE__,false,$getBuyCredits,$nowUserInfo['id']));
		while ($buyCredits=mysql_fetch_assoc($resBuy))
		{
			$cancelBut='';
			if ($buyCredits['user']==$nowUserInfo['id'])
			{
				$cancelBut='<img src="images/close.png" class="exchangeAction canselOrder" ids="'.$buyCredits['id'].'">';
			}
			$summ=round($buyCredits['kurs']*$buyCredits['credits'],2);
			$buyCredits['actions']='<img src="images/auction.png" class="exchangeAction sellCreds" kurs="'.$buyCredits['kurs'].'" credits="'.$buyCredits['credits'].'" ids="'.$buyCredits['id'].'"> '.$cancelBut;
			$buyCredits['kurs']=number_format($buyCredits['kurs'], 2, '.', ' ');
			$buyCredits['money']=number_format(round($summ,2), 2, '.', ' ');
			$buyCredits['name']='<span class="openUserProfile" userId="'.$buyCredits['userId'].'">'.$buyCredits['name'].'</span>';
			$buyCredits['endTime']=ceil(($buyCredits['time']-time())/60/60/24);
			$recods[]=$buyCredits;
		}
		break;
	}

	case 'seller':
	{
		$getSellCredit="SELECT a.`id`, a.`credits`, a.`kurs`, a.`user`, UNIX_TIMESTAMP(a.`time` + INTERVAL 3 MONTH) `time`, b.`id` `userId`, b.`name`
										FROM `exchange` a,
												 `user` b
										WHERE a.`user`=b.`id` and a.`type`='sell' and a.`credits`>0
										ORDER BY `kurs`";
		$resSell=mysql_query($getSellCredit) or die(handleError('Не удалось получить список покупателей кредитов.',__FILE__,false,$getSellCredit,$nowUserInfo['id']));
		while ($sellCredits=mysql_fetch_assoc($resSell))
		{
			$cancelBut='';
			if ($sellCredits['user']==$nowUserInfo['id'])
			{
				$cancelBut='<img src="images/close.png" class="exchangeAction canselOrder" ids="'.$sellCredits['id'].'">';
			}
			$summ=round($sellCredits['kurs']*$sellCredits['credits'],2);
			$sellCredits['actions']='<img src="images/auction.png" class="exchangeAction buyCreds" kurs="'.$sellCredits['kurs'].'" credits="'.$sellCredits['credits'].'" ids="'.$sellCredits['id'].'"> '.$cancelBut;
			$sellCredits['kurs']=number_format($sellCredits['kurs'], 2, '.', ' ');
			$sellCredits['money']=number_format(round($summ,2), 2, '.', ' ');
			$sellCredits['name']='<span class="openUserProfile" userId="'.$sellCredits['userId'].'">'.$sellCredits['name'].'</span>';
			$sellCredits['endTime']=ceil(($sellCredits['time']-time())/60/60/24);
			$recods[]=$sellCredits;
		}
		break;
	}

	case 'builds':
	{
		$order=trim($_POST['sidx'],', ');
		if ($_POST['sord']!='asc')
			$order.=' '.$_POST['sord'];

		$order=trim($order,', ');
		if (empty($order))
		{
			$order='`user` asc';
		}

		$search='';
		if ($_POST['_search']=='true' && $_POST['filters'])
		{
			$filtres=json_decode($_POST['filters']);
			foreach ($filtres->rules as $key => $searchRule)
			{
				$searchRule->data=htmlentities($searchRule->data, ENT_QUOTES, 'UTF-8');
				$op='=';
				if ($searchRule->field=='name')
				{
					$searchRule->field='a.`'.$searchRule->field.'`';
				}
				if ($searchRule->field=='user')
				{
					$searchRule->field='b.`name`';
				}
				switch ($searchRule->op)
				{
					case 'eq': {$op='=';
										 break;}

					case 'ne': {$op='!=';
										 break;}

					case 'cn': {$op=' LIKE ';
										 $searchRule->data='%'.$searchRule->data.'%';
										 break;}

					case 'nc': {$op=' NOT LIKE ';
										 $searchRule->data='%'.$searchRule->data.'%';
										 break;}

					case 'bw': {$op=' LIKE ';
										 $searchRule->data=$searchRule->data.'%';
										 break;}

					case 'bn': {$op=' NOT LIKE ';
										 $searchRule->data=$searchRule->data.'%';
										 break;}

					case 'ew': {$op=' LIKE ';
										 $searchRule->data='%'.$searchRule->data;
										 break;}

					case 'en': {$op=' NOT LIKE ';
										 $searchRule->data='%'.$searchRule->data;
										 break;}

					case 'le': {$op='<=';
										 break;}

					case 'lt': {$op='<';
										 break;}

					case 'gt': {$op='>';
										 break;}

					case 'ge': {$op='>=';
										 break;}

					default: {$op='=';
										break;}
				}
				$search.=" and ".$searchRule->field.$op."'".$searchRule->data."'";
			}
		}

		$page=$_POST['page'];
		$numRow=$_POST['rows'];
		$startLimit=($page-1)*$numRow;
		$getAllRec="SELECT count(`id`) `count`
								FROM
								(
									SELECT a.`id`
									FROM `buildingsUsers` a,
											 `user` b
									WHERE a.`user`=b.`id` and `isCosted`=1 ".$search."
								) a";
		$resCount=mysql_query($getAllRec) or die(handleError('Ошибка определения кол-ва домов пользователя.',__FILE__,false,$getAllRec,$nowUserInfo['id']));
		$counts=mysql_fetch_assoc($resCount);
		$pages=ceil($counts['count']/$numRow);

		$getBuilds="SELECT b.`id` `userId`, b.`name` `user`,
											 a.`geoObjectId` `id`, a.`address`, a.`name`, a.`level`, a.`clients`, a.`competition`, a.`bonus`, a.`doxod`, a.`cost`, a.`costCredits`,
											 X(a.`coords`) `pos1`, Y(a.`coords`) `pos2`
								FROM `buildingsUsers` a,
										 `user` b
								WHERE a.`user`=b.`id` and `isCosted`=1 ".$search."
								ORDER BY ".$order."
								LIMIT ".$startLimit.",".$numRow;
		$resBuilds=mysql_query($getBuilds) or die(handleError('Ошибка определения кол-ва домов пользователя.',__FILE__,false,$getBuilds,$nowUserInfo['id']));
		while($building=mysql_fetch_assoc($resBuilds))
		{
			if ($building['userId']!=$nowUserInfo['id'])
			{
				$building['actions']='<img class="buyHouse" title="Купить здание" ids="'.$building['id'].'" src="images/plus.png">';
			}
				else
				{
					$building['actions']='<img src="images/close.png" class="canselSellBuilding" ids="'.$building['id'].'" title="Убрать с продажи">';
				}
			$building['user']='<span class="openUserProfile" userId="'.$building['userId'].'">'.$building['user'].'</span>';
			$building['address'].=' <img src="images/map.png" class="goToMap" title="Перейти к обьекту на карте" coords="'.$building['pos1'].','.$building['pos2'].'" zoom="18">';
			$recods[]=$building;
		}

		break;
	}

	default:
		echo json_encode(['result'=>'Неверный тип биржи '.$_POST['type']]);
		exit();
}

echo json_encode(['result'=>'ok','page'=>$page,'total'=>$pages,'records'=>count($recods),'rows'=>$recods,'ord'=>$getBuilds]);
?>
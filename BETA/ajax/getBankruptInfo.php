<?php
include '../configUsers.php';

$deal='человеком';
if ($nowUserInfo['sex']==2)
{
	$deal='мужиком';
}

if ($nowUserInfo['sex']==1)
{
	$deal='сильнее обстоятельств';
}

if ($nowUserInfo['money']<=50)
{
	$content='Банкротство возможно при баллансе равном или менее 50 <img class="moneyIndicator" src="images/money.png">.
		<p>Есть несколько вариантов выхода при кол-ве монет менее 50 <img class="moneyIndicator" src="images/money.png">.</p>
		<ol>
			<li>
				Оптимизация активов (стоимость - 50 <img class="moneyIndicator" src="images/baks.png">)<br>
				При этом у вас
				<ul>
					<li>Будут удалены все аукционы, ставка на которые превышает доход за 3 дня. Если ставка на такой аукцион не ваша, то аукцион просто переходит претенденту. Монеты со ставок не возвращаются.</li>
					<li>
						Имеющиеся у вас в наличии здания будут проданы для покрытия ваших убытков (для достижения не отрицательного кол-ва <img class="moneyIndicator" src="images/money.png"> на вашем счету).<br>
						При этом, если у вас есть здания полностью покрывающую сумму ваших убытков, то будет продано именно оно. Если у вас нет таких зданий, то будет продано ваше самое дорогое здания - и так до тех пор пока ваш балланс не будет больше или раным нулю.<br>
						Если всех ваших зданий будет недостаточно чтобы покрыть убытки, то ваш балланс просто станет равным 0 <img class="moneyIndicator" src="images/money.png">, а все ваши здания будут оттогнуты.<br>
					</li>
					<li>Дополнительно вам будет начилено 3000 <img class="moneyIndicator" src="images/money.png"> - для продолжения развития.</li>
				</ul>
				<input type="button" value="Оптимизировать активы" id="creditsBunkrupt"><br><br>
			</li>
			<li>
				Банкротство<br>
				При этом вы фактически начинаете игру заново - все ваши аукционы и дома будут отторгнуты.
				<ul>
					<li>Все выши аукционы будут отторгнуты в пользу других игроков (если на них есть другие претенденты помимо вас), либо просто станут ничейными.</li>
					<li>Все ваши здания будут снесены и отторгнуты.</li>
					<li>Вам на счёт будет начислено 1500 <img class="moneyIndicator" src="images/money.png"> - для начала новой жизни в игре.</li>
				</ul>
				<input type="button" value="Пройти процедуру банкротства" id="fullBunkrupt"><br><br>
			</li>
			<li>
				Стать выше, быстрее, сильнее и самостоятельно преодолеть финансовые проблемы.<br>
				Всё просто: будь '.$deal.'! Возьми и найти себе бабло!<br>
			</li>
		</ol><br>
	<p>И в будущем, чтобы ситуация не повторилась, - не набирай себе аукционов у которых ставка превышает доход; строй больше зданий и помни, строй-прощадка не приносит дохода, сначала на ней необходимо построить ещё какое-либо здание.</p>';

	$lastBakrupt=$nowUserInfo['lastBankruptTime']+3600*48;
	if (($nowUserInfo['lastBankruptTime']+3600*48)>time())
	{
		$content='Процедура банротства доступна не чаще одного раза в 2 дня. Вы уже использовали процедуру банротства '.date('H:i:s d.m.Y',$nowUserInfo['lastBankruptTime']).', в следующий раз вы сможете ей воспользоваться только после '.date('H:i:s d.m.Y',$lastBakrupt).'!';
	}
}
	else
	{
		$content='К сожалению, механизм выплат игрокам не оправдал себя. На текущий момент данный функционал не доступен.';
//		$maxSumm=MONTH_USER_PAYMENTS_LIMIT;
//
//		$alreadyOutput="SELECT SUM(`summ`) output FROM `moneyOutput` WHERE `date`>(CURRENT_TIMESTAMP - INTERVAL 1 MONTH)";
//		$resOutput=mysql_query($alreadyOutput) or die(handleError('Ошибка получения суммы уже произведённых выплат.',__FILE__,false,$alreadyOutput,$nowUserInfo['id']));
//		$output=mysql_fetch_assoc($resOutput);
//		if ($output['output']>0)
//		{
//			$maxSumm-=$output['output'];
//		}
//
//		if ($maxSumm<=0)
//		{
//			$subContent='<p>Извините, но лимит выплат в '.MONTH_USER_PAYMENTS_LIMIT.' рублей за этот месяц уже выполнен. Попробуйте запросить выплату позже!</p>';
//		}
//			else
//			{
//				$alreadyOutput="SELECT SUM(`summ`) output FROM `moneyOutput` WHERE `user`=".$nowUserInfo['id']." and `date`>(CURRENT_TIMESTAMP - INTERVAL ".ONE_INTERVAL_USER_PAYMENTS_DAYS." DAY)";
//				$resOutput=mysql_query($alreadyOutput) or die(handleError('Ошибка получения суммы уже произведённых выплат.',__FILE__,false,$alreadyOutput,$nowUserInfo['id']));
//				$output=mysql_fetch_assoc($resOutput);
//				if ($output['output']>=ONE_INTERVAL_USER_PAYMENTS_LIMIT)
//				{
//					$subContent='<p>Ваш текущий '.ONE_INTERVAL_USER_PAYMENTS_DAYS.'-дневный лимит выплат - '.ONE_INTERVAL_USER_PAYMENTS_LIMIT.' рублей, уже превышен. Попробуйте запросить выплату позже.</p>';
//				}
//					else
//					{
//						$allMonets="SELECT SUM(`money`) allMoney FROM `user` WHERE `money`>0 and `lastEnter`>(CURRENT_TIMESTAMP - INTERVAL 1 MONTH)";
//						$resAllMonets=mysql_query($allMonets) or die(handleError('Ошибка получения общего кол-ва монет.',__FILE__,false,$allMonets,$nowUserInfo['id']));
//						$monets=mysql_fetch_assoc($resAllMonets);
//						$_SESSION['kurs']=round($monets['allMoney']/$maxSumm,2);
//						$printedKurs=number_format($_SESSION['kurs'],2,'.',' ');
//						$_SESSION['kurs']=number_format($_SESSION['kurs'],2,'.','');
//						$_SESSION['kursCreated']=time();
//						$maxNowOutput=ONE_INTERVAL_USER_PAYMENTS_LIMIT-$output['output'];
//						$subContent='<p>Текущий курс: 1 рубль = '.number_format($_SESSION['kurs'],2,'.',' ').' <img class="moneyIndicator" src="images/money.png"></p>
//							<p><b>Указанный курс действителен в течении 2-х минут.</b></p>
//							<p>Курс рассчитывается как общее кол-во монет у всех активных игроков, делённые на доступное кол-ва вывода за текущий месяц.</p>
//							<p>В этом месяце ещё будет выплачено максимум '.$maxSumm.' руб.</p>
//							<span class="hidden" id="outKurs">'.$_SESSION['kurs'].'</span>
//							<p>Сейчас вы можете запросить выплату максимум в '.$maxNowOutput.' руб.</p>
//								Сколько рублей вы хотите получить: <input type="text" id="rublesOutput" value="0">. Для этого вам потребуется <b id="outputMoneyCost">0</b> <img class="moneyIndicator" src="images/money.png"><br>
//								Ваш счёт в Яндекс.Деньги: <input type="text" id="yandexMoneyAccount"> &nbsp; <input type="button" value="Выплатить!" id="outputPayment"><br><br>
//								<p><b>Внимание!</b> Выплата осуществляется в течении 72 часов с момента подачи заявки.<br>
//								<b>Обязательно проверяйте корректность введённого кошелька Яндекс.Деньги!</b></p>';
//					}
//			}
//
//		$content='Вы можете обменять <img class="moneyIndicator" src="images/money.png"> на российские рубли и получить их на свой кошелёк в Яндекс.Деньги!<br>'.$subContent;
//
//		$listContent='';
//		$paymentsList="SELECT *, UNIX_TIMESTAMP(`date`) `createDate` FROM `moneyOutput` WHERE `user`=".$nowUserInfo['id']." ORDER BY `id` DESC";
//		$resList=mysql_query($paymentsList) or die(handleError('Ошибка получения истории ваших выплат.',__FILE__,false,$paymentsList,$nowUserInfo['id']));
//		while($payment=mysql_fetch_assoc($resList))
//		{
//			$status='Выполнена';
//			if (!$payment['isPayment'])
//			{
//				$status='Ожидается выплата';
//			}
//			$listContent.='<tr>
//											<td>'.$payment['id'].'</td>
//											<td>'.number_format($payment['summ'],2,'.',' ').'</td>
//											<td>'.number_format($payment['kurs'],2,'.',' ').'</td>
//											<td>'.$payment['purse'].'</td>
//											<td>'.date('H:i:s d.m.Y', $payment['createDate']).'</td>
//											<td>'.$status.'</td>
//										 </tr>';
//		}
//		if ($listContent)
//		{
//			$listContent='История ваших выплат:<br>
//				<table id="userPaymentsHistory">
//					<tr>
//						<th>№</td>
//						<th>Сумма, руб.</th>
//						<th>Курс, <img class="moneyIndicator" src="images/money.png"></th>
//						<th>Номер кошелька</th>
//						<th>Дата</th>
//						<th>Статус</th>
//					</tr>
//					'.$listContent.'
//				</table>';
//		}
//			else
//			{
//				$listContent='У вас ещё не было запрошенных выплат!';
//			}
//
//		$content.=$listContent;
	}
echo json_encode(['result'=>'ok','content'=>$content]);
?>
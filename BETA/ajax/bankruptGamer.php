<?php
require "../configUsers.php";

if ($_POST['type']!=='optimize' && $_POST['type']!=='full')
{
	handleError('Неверный тип банкротства!',__FILE__);
}
$type=$_POST['type'];

if ($nowUserInfo['money']>50)
{
	handleError('Процедура банкротства доступна только для тех, у кого балланс <img class="moneyIndicator" src="images/money.png"> меньше или равен 50.',__FILE__);
}

if ($type=='optimize' && $nowUserInfo['credits']<50)
{
	handleError('Для оптимизации расходов вам необхоимо 50 <img class="moneyIndicator" src="images/baks.png">, а у вас только '.$nowUserInfo['credits'].' <img class="moneyIndicator" src="images/baks.png">',__FILE__);
}

mysql_query("START TRANSACTION");
if ($type=='full')
{
	$getHouses="SELECT * FROM `house` WHERE `user`=".$nowUserInfo['id'];
	$resGetHouses=mysql_query($getHouses) or die(handleError('Не удалось получить информацию по вашим домам.',__FILE__,false,$getHouses,$nowUserInfo['id'],true));
	$deletedIds=[];
	while($house=mysql_fetch_assoc($resGetHouses))
	{
		$deletedIds[]=$house['geoObjectId'];
	}
	if (count($deletedIds)>0)
	{
		$updateHouses="UPDATE `geoObjects` SET `type`=1 WHERE `id` in (".implode(',',$deletedIds).")";
		$deleteHouses="DELETE FROM `house` WHERE `geoObjectId` in (".implode(',',$deletedIds).")";
		mysql_query($updateHouses) or die(handleError('Не удалось обновить данные ваших домов.',__FILE__,false,$updateHouses,$nowUserInfo['id'],true));
		mysql_query($deleteHouses) or die(handleError('Не удалось удалить ваши дома.',__FILE__,false,$deleteHouses,$nowUserInfo['id'],true));
	}

	$getAucs="SELECT * FROM `auction` WHERE `user`=".$nowUserInfo['id']." or `user_bid`=".$nowUserInfo['id'];
	$resGetAucs=mysql_query($getAucs) or die(handleError('Не удалось получить информацию по вашим аукционам.',__FILE__,false,$getAucs,$nowUserInfo['id'],true));
	$deletedIds=[];
	while($auct=mysql_fetch_assoc($resGetAucs))
	{
		$mustDelete=true;
		if ($auct['user_bid']==$nowUserInfo['id'] && $auct['user']!=$nowUserInfo['id'])
		{
			$mustDelete=false;
			$updateAuc="UPDATE `auction` SET `user_bid`=".$auct['user']." WHERE `id`=".$auct['id'];
			mysql_query($updateAuc) or die(handleError('Не удалось обновить информацию по аукционам с вашими ставками.',__FILE__,false,$updateAuc,$nowUserInfo['id'],true));
		}

		if ($auct['user_bid']!=$nowUserInfo['id'] && $auct['user']==$nowUserInfo['id'])
		{
			$mustDelete=false;
			$updateAuc="UPDATE `auction` SET `user`=".$auct['user_bid']." WHERE `id`=".$auct['id'];
			mysql_query($updateAuc) or die(handleError('Не удалось обновить информацию по вашим аукционам с чужими ставками.',__FILE__,false,$updateAuc,$nowUserInfo['id'],true));
		}
		if ($mustDelete)
		{
			$deletedIds[]=$auct['id'];
		}
	}

	if (count($deletedIds)>0)
	{
		$deleteHouses="DELETE FROM `auction` WHERE `id` in (".implode(',',$deletedIds).")";
		echo $deleteHouses;
		mysql_query($deleteHouses) or die(handleError('Не удалось удалить ваши дома.',__FILE__,false,$deleteHouses,$nowUserInfo['id'],true));
	}

	$updateUser="UPDATE `user` SET `money`=1500, `lastBankrupt`=CURRENT_TIMESTAMP WHERE `id`=".$nowUserInfo['id'];
	mysql_query($updateUser) or die(handleError('Не удалось обновить ваш балланс.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));

	$logEnter="INSERT INTO `log`
							(`user`,`text`,`type`)
					 VALUES
							(".$nowUserInfo['id'].", 'Вы прошли процедуру полного банкротства.','Банкротство')";
	mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));
}

if ($type=='optimize')
{
	$ballance=$nowUserInfo['money'];
	$toLog=[];
	$toLog[]="(".$nowUserInfo['id'].", 'Вы прошли процедуру оптимизации расходов.',NULL,0,-50,'Банкротство')";
	while ($ballance<0)
	{
		$getBigCostHouse="SELECT (c.`basicCost`*POW(c.`updateCost`,a.`level`-1)) buildingCost,
														 b.`id`
											FROM `house` a,
													 `geoObjects` b,
													 `geoObjectsTypes` c
											WHERE a.`user`=".$nowUserInfo['id']." and  (c.`basicCost`*POW(c.`updateCost`,a.`level`-1))>=".($ballance*-1)." and
														a.`geoObjectId`=b.`id` and b.`type`=c.`id`
											ORDER BY (c.`basicCost`*POW(c.`updateCost`,a.`level`-1)) DESC
											LIMIT 1";
		$resGetHouses=mysql_query($getBigCostHouse) or die(handleError('Не удалось получить информацию по вашим домам, способным полностью покрыть ваши убытки.',__FILE__,false,$getBigCostHouse,$nowUserInfo['id'],true));
		$rowHouse=mysql_fetch_assoc($resGetHouses);
		//print_r($rowHouse);
		if ($rowHouse)
		{
			$ballance+=$rowHouse['buildingCost'];
			$toLog[]="(".$nowUserInfo['id'].", 'Здание продано для покрытия убытков.',".$rowHouse['id'].",".$ballance.",0,'Продажа здания')";
			$updateHouses="UPDATE `geoObjects` SET `type`=1 WHERE `id`=".$rowHouse['id'];
			$deleteHouses="DELETE FROM `house` WHERE `geoObjectId`=".$rowHouse['id'];
			mysql_query($updateHouses) or die(handleError('Не удалось обновить данные ваших домов.',__FILE__,false,$updateHouses,$nowUserInfo['id'],true));
			mysql_query($deleteHouses) or die(handleError('Не удалось удалить ваши дома.',__FILE__,false,$deleteHouses,$nowUserInfo['id'],true));
			break;
		}
		$rowHouse=false;
		$getSmallCostHouse="SELECT (c.`basicCost`*POW(c.`updateCost`,a.`level`-1)) buildingCost,
															 b.`id`
												FROM `house` a,
														 `geoObjects` b,
														 `geoObjectsTypes` c
												WHERE a.`user`=".$nowUserInfo['id']." and  (c.`basicCost`*POW(c.`updateCost`,a.`level`-1))<".($ballance*-1)." and
															a.`geoObjectId`=b.`id` and b.`type`=c.`id`
												ORDER BY (c.`basicCost`*POW(c.`updateCost`,a.`level`-1))
												LIMIT 1";
		$resGetHouses=mysql_query($getSmallCostHouse) or die(handleError('Не удалось получить информацию по вашим домам.',__FILE__,false,$getSmallCostHouse,$nowUserInfo['id'],true));
		$rowHouse=mysql_fetch_assoc($resGetHouses);
		if (!$rowHouse)
		{
			$ballance=0;
			break;
		}
		if ($rowHouse)
		{
			$ballance+=$rowHouse['buildingCost'];
			$updateHouses="UPDATE `geoObjects` SET `type`=1 WHERE `id`=".$rowHouse['id'];
			$deleteHouses="DELETE FROM `house` WHERE `geoObjectId`=".$rowHouse['id'];
			mysql_query($updateHouses) or die(handleError('Не удалось обновить данные ваших домов.',__FILE__,false,$updateHouses,$nowUserInfo['id'],true));
			mysql_query($deleteHouses) or die(handleError('Не удалось удалить ваши дома.',__FILE__,false,$deleteHouses,$nowUserInfo['id'],true));
			$toLog[]="(".$nowUserInfo['id'].", 'Здание продано для покрытия убытков.',".$rowHouse['id'].",".$ballance.",0,'Продажа здания')";
		}
	}


	$getAucs="SELECT * FROM `auction` WHERE `doxod`<`stavka` and (`user`=".$nowUserInfo['id']." or `user_bid`=".$nowUserInfo['id'].")";
	$resGetAucs=mysql_query($getAucs) or die(handleError('Не удалось получить информацию по вашим аукционам.',__FILE__,false,$getAucs,$nowUserInfo['id'],true));
	$deletedIds=[];
	while($auct=mysql_fetch_assoc($resGetAucs))
	{
		$mustDelete=true;
		if ($auct['user_bid']==$nowUserInfo['id'] && $auct['user']!=$nowUserInfo['id'])
		{
			$mustDelete=false;
			$updateAuc="UPDATE `auction` SET `user_bid`=".$auct['user']." WHERE `id`=".$auct['id'];
			mysql_query($updateAuc) or die(handleError('Не удалось обновить информацию по аукционам с вашими ставками.',__FILE__,false,$updateAuc,$nowUserInfo['id'],true));
		}

		if ($auct['user_bid']!=$nowUserInfo['id'] && $auct['user']==$nowUserInfo['id'])
		{
			$mustDelete=false;
			$updateAuc="UPDATE `auction` SET `user`=".$auct['user_bid']." WHERE `id`=".$auct['id'];
			mysql_query($updateAuc) or die(handleError('Не удалось обновить информацию по вашим аукционам с чужими ставками.',__FILE__,false,$updateAuc,$nowUserInfo['id'],true));
		}
		if ($mustDelete)
		{
			$deletedIds[]=$auct['id'];
		}
	}

	if (count($deletedIds)>0)
	{
		$deleteHouses="DELETE FROM `auction` WHERE `id` in (".implode(',',$deletedIds).")";
		mysql_query($deleteHouses) or die(handleError('Не удалось удалить ваши дома.',__FILE__,false,$deleteHouses,$nowUserInfo['id'],true));
	}

	$updateUser="UPDATE `user` SET `money`=3000+".$ballance.", `credits`=`credits`-50, `lastBankrupt`=CURRENT_TIMESTAMP WHERE `id`=".$nowUserInfo['id'];
	mysql_query($updateUser) or die(handleError('Не удалось обновить ваш балланс.',__FILE__,false,$updateUser,$nowUserInfo['id'],true));

	$logEnter="INSERT INTO `log`
							(`user`,`text`,`geoObjectId`,`money`,`credits`,`type`)
					 VALUES ".implode(',', $toLog);
	mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUserInfo['id'],true));
}
mysql_query("COMMIT");

echo json_encode(['result'=>'ok']);
?>
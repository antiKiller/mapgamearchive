<?php
require "../configUsers.php";

$total=0;
$recods=[];

$order=trim($_POST['sidx'],', ');
if ($_POST['sord'])
	$order.=' '.$_POST['sord'];
$order=trim($order,', ');
if (empty($order))
{
	$order='`date` desc';
}

$search='';
if ($_POST['_search']=='true' && $_POST['filters'])
{
	$filtres=json_decode($_POST['filters']);
//	print_r($filtres);
	foreach ($filtres->rules as $key => $searchRule)
	{
		$searchRule->data=htmlentities($searchRule->data, ENT_QUOTES, 'UTF-8');
		$op='=';
		if ($searchRule->field=='type' || $searchRule->field=='money' || $searchRule->field=='credits')
		{
			$searchRule->field='a.`'.$searchRule->field.'`';
		}
		switch ($searchRule->op)
		{
			case 'eq': $op='=';
								 break;

			case 'ne': $op='!=';
								 break;

			case 'cn': $op=' LIKE ';
								 $searchRule->data='%'.$searchRule->data.'%';
								 break;

			case 'nc': $op=' NOT LIKE ';
								 $searchRule->data='%'.$searchRule->data.'%';
								 break;

			case 'bw': $op=' LIKE ';
								 $searchRule->data=$searchRule->data.'%';
								 break;

			case 'bn': $op=' NOT LIKE ';
								 $searchRule->data=$searchRule->data.'%';
								 break;

			case 'ew': $op=' LIKE ';
								 $searchRule->data='%'.$searchRule->data;
								 break;

			case 'en': $op=' NOT LIKE ';
								 $searchRule->data='%'.$searchRule->data;
								 break;

			case 'le': $op='<=';
								 break;

			case 'lt': $op='<';
								 break;

			case 'gt': $op='>';
								 break;

			case 'ge': $op='>=';
								 break;

			default: $op='=';
			break;
		}


		$search.=" and ".$searchRule->field.$op."'".$searchRule->data."'";
	}
}

$page=$_POST['page'];
$numRow=$_POST['rows'];
$startLimit=($page-1)*$numRow;

$mainSql="SELECT a.`text`, a.`money`, a.`credits`, a.`type`, UNIX_TIMESTAMP(a.`date`) `date`,
								 b.`address`, c.`name` userName
					FROM `log` a
									LEFT OUTER JOIN
							 `geoObjects` b
								ON a.`geoObjectId`=b.`id`
									LEFT OUTER JOIN
							 `user` c
								ON a.`otherUser`=c.`id`
						 WHERE a.`user`=".$nowUserInfo['id'].$search."
						 /**ORDER BY ".$order."**/";

$pages=1;
$getTotal="SELECT count(*) totalPages
					 FROM (
									".$mainSql."
								) e";
$resGetTotal=mysql_query($getTotal);
//print_r($getTotal);
$totalRows=mysql_fetch_assoc($resGetTotal);
$pages=ceil($totalRows['totalPages']/$numRow);

$mainSql=str_replace("/**", '', $mainSql);
$mainSql=str_replace("**/", '', $mainSql);
$getLog=$mainSql." LIMIT ".$startLimit.",".$numRow;
$resGetLog=mysql_query($getLog) or die(handleError('Ошибка получения лога пользователя.',__FILE__,false,$getLog,$nowUserInfo['id']));
while($logRow=mysql_fetch_assoc($resGetLog))
{
	$total++;
	$logRow['date']=date('H:i:s d.m.Y', $logRow['date']);
	$logRow['money']=  number_format($logRow['money'], 2, '.', ' ');
	$recods[]=$logRow;
}

echo json_encode(['result'=>'ok','page'=>$page,'total'=>$pages,'records'=>$totalRows['totalPages'],'rows'=>$recods]);
?>
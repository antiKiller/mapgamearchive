<?php
include_once '../config.php';
include_once 'cronFunct.php';

$startTime=time();

//обновление доходности аукционов
$updateProfit="UPDATE `auction` a SET `doxod`=(SELECT sum(`money`) FROM `auction_log` WHERE `auc`=a.`id` and `time`>=(CURRENT_TIMESTAMP - INTERVAL 3 DAY))";
mysql_query($updateProfit) or die(handleError('Ошибка обновления доходности аукциона.',__FILE__,false,$updateProfit));

$aucLodClear="DELETE FROM `auction_log` WHERE `time`<(CURRENT_TIMESTAMP - INTERVAL 3 DAY)";
mysql_query($aucLodClear) or die(handleError('Ошибка очистки лога аукционов.',__FILE__,false,$aucLodClear));


//обновление времени аукционов
$toLog=[];
$getAuc="SELECT `id`, `user_bid`, `stavka`, `address`, `basicCost`
				 FROM `auctionsUsers`
				 WHERE `time`<=CURRENT_TIMESTAMP";
$resAuc=mysql_query($getAuc) or die(handleError('Ошибка получения списка аукционов.',__FILE__,false,$getAuc));

while($reCalcAuc=mysql_fetch_assoc($resAuc))
{
	mysql_query("START TRANSACTION");
	$newBid=round($reCalcAuc['stavka']*AUC_BID_DEPRECIATION,2);
	if ($newBid<$reCalcAuc['basicCost'])
	{
		$newBid=$reCalcAuc['basicCost'];
	}

	$aucUpdate="UPDATE `auction` SET `time`=(CURRENT_TIMESTAMP + INTERVAL 3 DAY), `user`=`user_bid`,`stavka`=".$newBid." WHERE `id`=".$reCalcAuc['id'];
	mysql_query($aucUpdate) or die(handleError('Ошибка обновления времени аукциона.',__FILE__,false,$aucUpdate,$reCalcAuc['user_bid'],true));
	$updateUser="UPDATE `user` SET `money`=`money`-".$newBid." WHERE `id`=".$reCalcAuc['user_bid'];
	mysql_query($updateUser) or die(handleError('Ошибка обновления балланса пользователя.',__FILE__,false,$updateUser,$reCalcAuc['user_bid'],true));

	$toLog[]="(".$reCalcAuc['user_bid'].",'Победа в аукционе <i>".$reCalcAuc['address']."</i>',0,'Аукцион')";
	$toLog[]="(".$reCalcAuc['user_bid'].",'Ставка на ауционе <i>".$reCalcAuc['address']."</i>',".($newBid*-1).",'Аукцион')";

	mysql_query("COMMIT");
}
if (count($toLog)>0)
{
	$insertToLog="INSERT INTO `log` (`user`,`text`,`money`,`type`) VALUES ".implode(',', $toLog);
	mysql_query($insertToLog) or die(handleError('Ошибка записи в лог об обновлениях времени аукциона.',__FILE__,false,$insertToLog));
}

logCron($startTime,__FILE__);
?>
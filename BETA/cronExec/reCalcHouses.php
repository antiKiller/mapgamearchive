<?php
include_once '../config.php';
include_once 'cronFunct.php';
$startTime=time();

$delete="DELETE FROM `reCalcHouses` WHERE `notRecalcUntil`<=(CURRENT_TIMESTAMP - INTERVAL 20 HOUR)";
mysql_query($delete) or die(handleError('Не удалось зависшие в очереди здания.',__FILE__,false,$delete));

$getRecalcList="SELECT * FROM
								(
										SELECT a.`id` `reCalledId`,
													 b.`id` `houseId`,
													 `level`, `user`, `clients`, `doxod`, X(`coords`) pos1, Y(`coords`) pos2, `address`, `fixClients`, `basicDoxod`, `updateDoxod`, `isHouse`,
													 `reCalcedDate`, `notRecalcUntil`
																		FROM `reCalcHouses` a,
																				 `buildingsUsers` b
																		WHERE a.`houseId`=b.`id` and `notRecalcUntil`<=CURRENT_TIMESTAMP
																		LIMIT 1000
								) a
								ORDER BY `reCalcedDate`, `notRecalcUntil`";
$resGetRecalcList=mysql_query($getRecalcList) or die(handleError('Не удалось получить здания для пересчета.',__FILE__,false,$getRecalcList));
$countHouses=0;
$notGoodEnd='Нет';
$mustDeletedId=[];
while ($reCalled=mysql_fetch_assoc($resGetRecalcList))
{
	if ( (time()-$startTime)>170 )
	{
		$notGoodEnd='Да';
		break;
	}
	$countHouses++;

	$mustDeletedId[]=$reCalled['reCalledId'];
	if (!$reCalled['isHouse'])
	{
		continue;
	}

	recalcHouse($reCalled);
}

if (count($mustDeletedId)>0)
{
	$deleteFromReCalc="DELETE FROM `reCalcHouses` WHERE `id` in (".implode(',',$mustDeletedId).")";
	mysql_query($deleteFromReCalc) or die(handleError('Не удалось удалить перeсчитаные обычные дома.',__FILE__,false,$deleteFromReCalc));
}

logCron($startTime,__FILE__,$notGoodEnd,$countHouses);
?>
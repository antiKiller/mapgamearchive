<?php
include_once '../config.php';
include_once 'cronFunct.php';

$startTime=time();
$countUsers=0;

$getUsers="SELECT DISTINCT a.`user`, a.`period`, c.`kapitalization`, UNIX_TIMESTAMP(c.`registred`) `registredTime`
					 FROM `manager` a,
								`house` b,
								`user` c
					 WHERE a.`user`=b.`user` and b.`user`=c.`id` and b.`doxod`>0 and b.`isManaged`=1 and b.`isCosted`=0 and b.`time`<=CURRENT_TIMESTAMP and a.`endDate`>=CURRENT_TIMESTAMP";
$resUsers=mysql_query($getUsers);

$notGoodEnd='Нет';

while($nowUser=mysql_fetch_assoc($resUsers))
{
	if ( (time()-$startTime)>55 )
	{
		$notGoodEnd='Да';
		break;
	}
	$countUsers++;
	
	$profitBonuses=[RENT_PROFIT_BASIC_FIRST,RENT_PROFIT_BASIC_SECOND,RENT_PROFIT_BASIC_THIRD,RENT_PROFIT_BASIC_FOURTH,RENT_PROFIT_BASIC_FIFTH,RENT_PROFIT_BASIC_SIXTH];
	if ($nowUser['registredTime']>(time()-IS_NEWBIE_TIME) && $nowUser['kapitalization']<100000)
	{
		$profitBonuses=[RENT_PROFIT_NEWBIE_FIRST,RENT_PROFIT_NEWBIE_SECOND,RENT_PROFIT_NEWBIE_THIRD,RENT_PROFIT_NEWBIE_FOURTH,RENT_PROFIT_NEWBIE_FIFTH,RENT_PROFIT_NEWBIE_SIXTH];
	}

	switch ($nowUser['period'])
	{
		case 1: $profitFactor=$nowUser['period']*$profitBonuses[0];
						$toRentTime='1 час';
						break;
		case 2: $profitFactor=$nowUser['period']*$profitBonuses[1];
						$toRentTime='2 часа';
						break;
		case 4: $profitFactor=$nowUser['period']*$profitBonuses[2];
						$toRentTime='4 часа';
						break;
		case 8: $profitFactor=$nowUser['period']*$profitBonuses[3];
						$toRentTime='8 часов';
						break;
		case 12: $profitFactor=$nowUser['period']*$profitBonuses[4];
						 $toRentTime='12 часов';
						 break;
		case 24: $profitFactor=$nowUser['period']*$profitBonuses[5];
						 $toRentTime='24 часа';
						 break;

		default: echo '<br>Неверный период сдачи в аренду!<br>';
						 continue;
	}

	$fullProfit=0;
	$toLog=[];
	$aucRent=[];
	$aucArr=[];
	$updatedId=[];


	$totime=time()+$nowUser['period']*3600;

	$getHouse="SELECT SUM(`doxod`) `doxod`, GROUP_CONCAT(`id`) `list`,  `country`, `administrative`, `subadministrative`, `locality`, `street`, `type`
						 FROM `buildingsUsers`
						 WHERE `user`=".$nowUser['user']." and `time`<=CURRENT_TIMESTAMP and
									 (`isHouse`=1 or `isEnterprise`=1) and `isCosted`=0 and `isManaged`=1
									 GROUP BY `street`, `locality`, `subadministrative` , `administrative`, `country`";

	$resGetHouseInfo=mysql_query($getHouse) or die(handleError('Ошибка получения списка домов.',__FILE__,false,$getHouse,$nowUser['user']));
	$idsList=null;
	$getHouse=null;

	while ($houseInfo=mysql_fetch_assoc($resGetHouseInfo))
	{
		if ($houseInfo['doxod']<=0)
		{
			continue;
		}
		$rentProfit=round($houseInfo['doxod']*$profitFactor,2);
		$fullProfit+=$rentProfit;
		if ($houseInfo['list'])
	{
		$updatedId[]=trim($houseInfo['list'],', ');
	}

		$streetSql="SELECT `id`, `geoObjectId`, `user`, `address`
								FROM `auctionsUsers`
								WHERE `type`=2 and `country`='".$houseInfo['country']."' and `administrative`='".$houseInfo['administrative']."' and `locality`='".$houseInfo['locality']."' and `street`='".$houseInfo['street']."'";
		$resStreet=mysql_query($streetSql) or die(handleError('Ошибка выборки аукционов для получения налога.',__FILE__,false,$streetSql,$nowUser['user'],true));
		while ($streetInf=mysql_fetch_assoc($resStreet))
		{
			$aucRent[$streetInf['geoObjectId']]['rent']+=$rentProfit;
			$aucRent[$streetInf['geoObjectId']]['user']=$streetInf['user'];
			$aucRent[$streetInf['geoObjectId']]['persent']=0.25;
			$aucRent[$streetInf['geoObjectId']]['id']=$streetInf['id'];
		}

		$aucArr['notStreets'][$houseInfo['country']][$houseInfo['administrative']][$houseInfo['locality']]['summ']+=$rentProfit;
		$aucArr['notStreets'][$houseInfo['country']][$houseInfo['administrative']]['summ']+=$rentProfit;
		$aucArr['notStreets'][$houseInfo['country']]['summ']+=$rentProfit;

	}

	foreach ($aucArr['notStreets'] as $country=>$countryVals)
	{
		if ($country=='summ')
		{
			continue;
		}
		$countryInf=[];
		if ($country)
		{
			$countrysSql="SELECT `id`, `geoObjectId`, `user`, `address`
										FROM `auctionsUsers`
										WHERE `type`=7 and `country`='".$country."'";
			$resCountry=mysql_query($countrysSql) or die(handleError('Ошибка выборки областей для получения налога.',__FILE__,false,$countrysSql,$nowUser['user'],true));
			$countryInf=mysql_fetch_assoc($resCountry);
			if ($countryInf['geoObjectId'])
			{
				$aucRent[$countryInf['geoObjectId']]['rent']+=$aucArr['notStreets'][$country]['summ'];
				$aucRent[$countryInf['geoObjectId']]['user']=$countryInf['user'];
				$aucRent[$countryInf['geoObjectId']]['persent']=0.05;
				$aucRent[$countryInf['geoObjectId']]['id']=$countryInf['id'];
			}
		}
		foreach ($aucArr['notStreets'][$country] as $adms=>$admsVals)
		{
			if ($adms=='summ')
			{
				continue;
			}
			foreach ($aucArr['notStreets'][$country][$adms] as $localicy=>$localicyVals)
			{
				if ($localicy=='summ')
				{
					continue;
				}
				if ($localicy)
				{
					$localitySql="SELECT `id`, `geoObjectId`, `user`, `address`
												FROM `auctionsUsers`
												WHERE `type`=4 and `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."'";

					$resLocality=mysql_query($localitySql) or die(handleError('Ошибка выборки городов для получения налога.',__FILE__,false,$localitySql,$nowUser['user'],true));
					while ($localityInf=mysql_fetch_assoc($resLocality))
					{
						$aucRent[$localityInf['geoObjectId']]['rent']+=$aucArr['notStreets'][$country][$adms][$localicy]['summ'];
						$aucRent[$localityInf['geoObjectId']]['user']=$localityInf['user'];
						$aucRent[$localityInf['geoObjectId']]['persent']=0.2;
						$aucRent[$localityInf['geoObjectId']]['id']=$localityInf['id'];
					}

					$searchAreas="SELECT `id`, `geoObjectId`, `user`, `address`
												FROM `auctionsUsers` a
												WHERE a.`country`='".$country."' and `administrative`='".$adms."' and a.`type`=5 and
												(
													`subadministrative` in
													(
														SELECT `subadministrative` FROM `geoObjects` b
														WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`subadministrative`!='' and
															(
																b.`subadministrative`=a.`administrative` or
																a.`subadministrative`=b.`administrative` or
																a.`subadministrative`=b.`subadministrative`
															)
														GROUP BY `subadministrative`
													)
														or
													(
														`administrative` in
														(
															SELECT `administrative` FROM `geoObjects` b
															WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	a.`administrative`=b.`administrative`
																)
															GROUP BY `administrative`
														) and a.`subadministrative`=''
													)
												)";

					$resSearchAreas=mysql_query($searchAreas) or die(handleError('Ошибка выборки регионов для получения налога.',__FILE__,false,$searchAreas,$nowUser['user'],true));
					while($areaInf=mysql_fetch_assoc($resSearchAreas))
					{
						$aucRent[$areaInf['geoObjectId']]['rent']+=$aucArr['notStreets'][$country][$adms][$localicy]['summ'];
						$aucRent[$areaInf['geoObjectId']]['user']=$areaInf['user'];
						$aucRent[$areaInf['geoObjectId']]['persent']=0.15;
						$aucRent[$areaInf['geoObjectId']]['id']=$areaInf['id'];
					}

					$searchRegions="SELECT `id`, `geoObjectId`, `user`, `administrative`, `subadministrative`, `address`
													FROM `auctionsUsers` a
													WHERE a.`country`='".$country."' and a.`type`=6 and
													(
														`subadministrative` in
														(
															SELECT `subadministrative` FROM `geoObjects` b
															WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`subadministrative`!='' and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	a.`administrative`=b.`administrative`
																)
														GROUP BY `subadministrative`
													)
														or
													(
														`administrative` in
														(
															SELECT `administrative` FROM `geoObjects` b
															WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`administrative`!='' and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	a.`administrative`=b.`administrative`
																)
															GROUP BY `administrative`
														) and a.`subadministrative`=''
													)
														or
													`subadministrative` in
													(
														SELECT `administrative` FROM `geoObjects` b
														WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`subadministrative`!='' and
														 (
															b.`subadministrative`=a.`administrative` or
															a.`subadministrative`=b.`administrative` or
															a.`administrative`=b.`administrative`
														 )
														GROUP BY `administrative`
													 )
														or
													 (
															`administrative` in
															(
															 SELECT `subadministrative` FROM `geoObjects` b
															 WHERE `country`='".$country."' and `administrative`='".$adms."' and `locality`='".$localicy."' and `type` in (2,4) and b.`administrative`!='' and
																(
																	b.`subadministrative`=a.`administrative` or
																	a.`subadministrative`=b.`administrative` or
																	(a.`administrative`=b.`administrative`)
																)
															 GROUP BY `subadministrative`
															)
														)
													)";
					$resSearchRegions=mysql_query($searchRegions) or die(handleError('Ошибка выборки областей для получения налога.',__FILE__,false,$searchRegions,$nowUser['user'],true));
					while($regionInf=mysql_fetch_assoc($resSearchRegions))
					{
						$aucRent[$regionInf['geoObjectId']]['rent']+=$aucArr['notStreets'][$country][$adms][$localicy]['summ'];
						$aucRent[$regionInf['geoObjectId']]['user']=$regionInf['user'];
						$aucRent[$regionInf['geoObjectId']]['persent']=0.1;
						$aucRent[$regionInf['geoObjectId']]['id']=$regionInf['id'];

						if ($regionInf['subadministrative']==$adms)
						{
							$subRegionsSql="SELECT `id`, `geoObjectId`, `user`, `address`
															FROM `auctionsUsers`
															WHERE `country`='".$country."' and `type`=6 and `administrative`='".$regionInf['administrative']."' and `subadministrative`=''";
							$resSubRegions=mysql_query($subRegionsSql) or die(handleError('Ошибка выборки областей для получения налога.',__FILE__,false,$subRegionsSql,$nowUser['user'],true));

							while($subRegionInf=mysql_fetch_assoc($resSubRegions))
							{
								$aucRent[$subRegionInf['geoObjectId']]['rent']+=$aucArr['notStreets'][$country][$adms][$localicy]['summ'];
								$aucRent[$subRegionInf['geoObjectId']]['user']=$subRegionInf['user'];
								$aucRent[$subRegionInf['geoObjectId']]['persent']=0.1;
								$aucRent[$subRegionInf['geoObjectId']]['id']=$subRegionInf['id'];
							}
						}
					}
				}
			}
		}
	}
	$aucArr=null;

	$usersProfit=[];
	$toAucLogs=[];

	mysql_query('START TRANSACTION');

	foreach ($aucRent as $geoObjId=>$params)
	{
		$profit=round($params['rent']*$params['persent'],2);
		$auctionUpdate="UPDATE `auction` SET `doxod`=`doxod`+".$profit." WHERE `geoObjectId`=".$geoObjId;
		mysql_query($auctionUpdate) or die(handleError('Не удалось обновить сведениня о доходности аукциона.',__FILE__,false,$auctionUpdate,$nowUser['user'],true));

		$usersProfit[$params['user']]+=$profit;

		$toAucLogs[]="(".$params['id'].",".$profit.")";
		$toLog[]="(".$params['user'].", 'Рента со сдачи в аренду',".$geoObjId.",".$profit.",'Рента')";
	}

	foreach ($usersProfit as $user=>$profit)
	{
		$userProfitSql="UPDATE `user` SET  `money`=`money`+".$profit." WHERE `id`=".$user;
		mysql_query($userProfitSql) or die(handleError('Не удалось начислить ренту игроку.',__FILE__,false,$userProfitSql,$nowUser['user'],true));
	}

	if (count($updatedId)>0)
	{
		$houseUpdate="UPDATE `house` SET `time`=FROM_UNIXTIME(".$totime."), `action`='Аренда' WHERE `id` in (".implode(',',$updatedId).")";
		mysql_query($houseUpdate) or die(handleError('Не удалось обновить время выхода строений из аренды.',__FILE__,false,$houseUpdate,$nowUser['user'],true));
	}

	if ($fullProfit>0)
	{
		$countOfHouses="SELECT count(`id`) housesCount FROM `house` WHERE `user`=".$nowUser['user'];
		$resCountOfHouse=mysql_query($countOfHouses) or die(handleError('Не удалось получить кол-во ваших строений.',__FILE__,false,$countOfHouses,$nowUser['user'],true));
		$countHouses=mysql_fetch_assoc($resCountOfHouse);
		$persent=floor($countHouses['housesCount']/100)*0.2;

		$toLog[]="(".$nowUser['user'].",'Передача в аренду на <i>".$toRentTime."</i> строений',null,".$fullProfit.",'Аренда')";

		if ($persent>0)
		{
			$taxManagerSql="SELECT `discount` FROM `taxManager` WHERE `user`=".$nowUser['user']." and `endDate`>CURRENT_TIMESTAMP";
			$resTaxManagerSql=mysql_query($taxManagerSql) or die(handleError('Не удалось информацию о бухгалтере.',__FILE__,false,$taxManagerSql,$nowUser['user']));
			$taxManager=mysql_fetch_assoc($resTaxManagerSql);
			if ($taxManager['discount']>0)
			{
				$persent=number_format($persent-$persent*$taxManager['discount']/100,2,'.','');
			}
			$tax=round($fullProfit*$persent/100,2);
			$fullProfit=$fullProfit-$tax;
			$toLog[]="(".$nowUser['user'].",'Подоходный налог ".$persent."% со сдачи управляющим ".count($updatedId)." строений в аренду ',null,-".$tax.",'Налог')";
		}
		$setUserProfit="UPDATE `user` SET  `money`=`money`+".$fullProfit." WHERE `id`=".$nowUser['user'];
		mysql_query ($setUserProfit) or die(handleError('Не удалось зачислить монеты за аренду игроку.',__FILE__,false,$setUserProfit,$nowUser['user'],true));
	}

	if (count($toLog)>0)
	{
		$logEnter="INSERT INTO `log`
									(`user`,`text`,`geoObjectId`,`money`,`type`)
							 VALUES ".implode(',', $toLog);
		mysql_query ($logEnter) or die(handleError('Ошибка записи в лог информации.',__FILE__,false,$logEnter,$nowUser['user'],true));
	}

	if (count($toAucLogs)>0)
	{
		$aucLogUpd="INSERT INTO `auction_log`
									(`auc`,`money`)
								VALUES
									".implode(',', $toAucLogs);
		mysql_query ($aucLogUpd) or die(handleError('Ошибка записи в лог аукционов информации.',__FILE__,false,$aucLogUpd,$nowUser['user'],true));
	}
	mysql_query('COMMIT');
}

logCron($startTime,__FILE__,$notGoodEnd,$countUsers);
?>

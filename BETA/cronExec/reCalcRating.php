<?php
include_once '../config.php';
include_once 'cronFunct.php';
$startTime=time();

$getUsersList="SELECT `id`, `money`, `credits` FROM `user`";
$resGetUsers=mysql_query($getUsersList);
//echo '<pre>';
$countRatings=0;
while($nowUser=mysql_fetch_assoc($resGetUsers))
{
	$countRatings++;
	$id=$nowUser['id'];
	$getValues="SELECT
								(SELECT count(`id`) FROM `house` WHERE `user`=$id) countBuildings,
								(SELECT sum(`doxod`) FROM `house` WHERE `user`=$id) doxodBuildings,
									
								(SELECT sum(`credits`) FROM `exchange` WHERE `user`=$id and `type`='sell') selledCredits,
								(SELECT sum(`kurs`*`credits`) FROM `exchange` WHERE `user`=$id and `type`='buy') buyedCredits,

								(SELECT sum(c.`basicCost`*POW(c.`updateCost`,a.`level`-1)) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id`) buildingsCost,
								(SELECT sum(c.`basicCost`*POW(c.`updateCost`,a.`level`-1)) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isHouse`=1) housesCost,
								(SELECT sum(c.`basicCost`*POW(c.`updateCost`,a.`level`-1)) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isEnterprise`=1) enterpCost,
								(SELECT sum(c.`basicCost`*POW(c.`updateCost`,a.`level`-1)) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isSocial`=1) socialCost,

								(SELECT sum(a.`clients`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isHouse`=1) clientsHouses,
								(SELECT count(a.`id`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isHouse`=1) countHouses,
								(SELECT sum(`doxod`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isHouse`=1) doxodHouses,
								(SELECT sum(`bonus`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isHouse`=1) bonusHouses,

								(SELECT sum(a.`clients`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isEnterprise`=1) clientsEnterp,
								(SELECT count(a.`id`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isEnterprise`=1) countEnterp,
								(SELECT sum(`doxod`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isEnterprise`=1) doxodEnterp,

								(SELECT count(a.`id`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isSocial`=1) countSocial,
								(SELECT sum(`bonus`) FROM `house` a, `geoObjects` b, `geoObjectsTypes` c WHERE a.`user`=$id and a.`geoObjectId`=b.`id` and b.`type`=c.`id` and c.`isSocial`=1) bonusSocial,

								(SELECT count(`id`) FROM `auction` WHERE `user`=$id) countAuctions,
								(SELECT sum(`doxod`) FROM `auction` WHERE `user`=$id) doxodAuctions,

								(SELECT count(`id`) FROM `auction` WHERE `user_bid`=$id) countBidAuctions,
								(SELECT sum(`stavka`) FROM `auction` WHERE `user_bid`=$id) sumBidAuctions,
									

								(SELECT sum(`credits`*`kurs`) FROM `exchange` WHERE `type`='buy' and `user`=$id and `credits`>0 and `kurs`>0) sumMoneyOnExchange,

								(SELECT sum(`money`) FROM `log` WHERE `user`=$id and `money`>0 and `date`>(CURRENT_TIMESTAMP - INTERVAL 7 DAY)) weekDoxod,
								(SELECT sum(`credits`) FROM `log` WHERE `user`=$id and `credits`>0 and `date`>(CURRENT_TIMESTAMP - INTERVAL 7 DAY)) weekCreditsDoxod,
								(SELECT sum(`money`) FROM `log` WHERE `user`=$id and `money`<0 and `date`>(CURRENT_TIMESTAMP - INTERVAL 7 DAY)) weekCosts,
								(SELECT sum(`credits`) FROM `log` WHERE `user`=$id and `credits`<0 and `date`>(CURRENT_TIMESTAMP - INTERVAL 7 DAY)) weekCreditsCosts";


	$resValues=mysql_query($getValues) or die('<br><br>ошибка выполнения <br><br>'.$getValues.'<br><br>'.mysql_error().'<br><br>'.mysql_query("ROLLBACK"));
	$values=mysql_fetch_assoc($resValues);

	foreach ($values as $key => $value)
	{
		if (is_null($value) || is_infinite($value))
		{
			$values[$key]=0;
		}
	}


	@$oligarchs=($nowUser['money']+$nowUser['selledCredits']+$values['sumBidAuctions'])*0.25+
							($nowUser['credits']+$nowUser['buyedCredits'])*0.2+
							($values['doxodBuildings']+$values['doxodAuctions'])*0.3+
							$values['buildingsCost']*0.25;
	$oligarchs=$oligarchs/1000;

	@$managers= ($values['weekDoxod']+$values['weekCreditsDoxod']) /
							(($values['weekCosts']+$values['weekCreditsCosts'])*-1)*100000;

	@$auctioneers=$values['sumBidAuctions']/$values['countBidAuctions']*0.3+
								$values['doxodAuctions']/$values['countAuctions']*0.25+
								$values['countAuctions']/$values['countBuildings']*0.2+
								$values['doxodAuctions']/$values['doxodBuildings']*0.25;

	@$patrons=($values['clientsHouses']+$values['countSocial'])/$values['clientsEnterp']*0.2+
						$values['doxodHouses']/$values['doxodBuildings']*0.3+
						$values['bonusSocial']*$values['countSocial']*0.3+
						($values['housesCost']+$values['socialCost'])/$values['enterpCost']*0.2;

	@$industrialists= $values['clientsEnterp']/($values['clientsHouses']+$values['countSocial'])*0.2+
										$values['doxodEnterp']/$values['doxodBuildings']*0.3+
										$values['clientsEnterp']*$values['doxodEnterp']*0.3+
										$values['enterpCost']/($values['housesCost']+$values['socialCost'])*0.2;
	
	@$kapitalization=$nowUser['money']+$values['sumBidAuctions']+$values['buildingsCost']+$values['sumMoneyOnExchange'];

//	echo '<br><br>';
//	echo $nowUser['money'].'<br>';
//	echo $values['sumBidAuctions'].'<br>';
//	echo $values['buildingsCost'].'<br>';
//	echo $values['sumMoneyOnExchange'].'<br>';
//	echo '<br><br>';
	$updateUser="UPDATE `user`
								SET `oligarchs`=".round($oligarchs).",
										`managers`=".round($managers).",
										`auctioneers`=".round($auctioneers).",
										`patrons`=".round($patrons).",
										`industrialists`=".round($industrialists).",
										`kapitalization`=".round($kapitalization,2)."
								WHERE `id`=".$id;
	mysql_query($updateUser) or die('<br><br>ошибка выполнения <br><br>'.$updateUser.'<br><br>'.mysql_error().'<br><br>');
//	print_r($updateUser);
//	echo '<hr>';
}

logCron($startTime,__FILE__,'',$countRatings);
?>
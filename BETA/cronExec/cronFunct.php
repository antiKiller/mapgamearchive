<?php
function logCron($startTime,$type,$aheadOfTime='',$countEntitys='')
{
	$entTime=time();
	$timeDiff=$entTime-$startTime;
	
	$logValue[]=date('H:i:s',$startTime);
	$logValue[]=date('H:i:s',$entTime);
	$logValue[]=$timeDiff;
	$logValue[]=$type;
	$logValue[]=$aheadOfTime;
	$logValue[]=$countEntitys;
	$string=implode(';',$logValue).";\r\n";

	$date=date('d.m.Y');
	$f=fopen('../appLogs/cronLog_'.$date.'.log', 'a+');
	fwrite($f, $string);
	fclose($f);
}
?>
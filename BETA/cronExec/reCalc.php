<?php
include_once '../config.php';
include_once 'cronFunct.php';
$startTime=time();

$delete="DELETE FROM `reCalc` WHERE `notRecalcUntil`<=(CURRENT_TIMESTAMP - INTERVAL 20 HOUR)";
mysql_query($delete) or die(handleError('Не удалось зависшие в очереди здания.',__FILE__,false,$delete));

$recalledIds=[];
$getRecalcList="SELECT * FROM
								(
									SELECT a.`id` `reCalledId`, b.`id` `houseId`,
												 `level`, `user`, `info`, `geoObjectId`, X(`coords`) pos1, Y(`coords`) pos2, `address`, `type`, `name`, `radius`, `isNeeded`, `basicDoxod`, `updateDoxod`,
												 `isEnterprise`, `reCalcedDate`, `notRecalcUntil`
									FROM `reCalc` a,
											 `buildingsUsers` b
									WHERE a.`houseId`=b.`id` and `notRecalcUntil`<=CURRENT_TIMESTAMP
									LIMIT 250
							 ) a
							 ORDER BY `reCalcedDate`, `notRecalcUntil`";
$resGetRecalcList=mysql_query($getRecalcList) or die(handleError('Не удалось получить здания для пересчета.',__FILE__,false,$getRecalcList));
$countBildings=0;
$mustDeletedId=[];
$notGoodEnd='Нет';
while ($reCalled=mysql_fetch_assoc($resGetRecalcList))
{
	if ( (time()-$startTime)>165 )
	{
		$notGoodEnd='Да';
		break;
	}
	$countBildings++;

	$mustDeletedId[]=$reCalled['reCalledId'];
	if (!$reCalled['isEnterprise'])
	{
		continue;
	}

	recalcEnterprise($reCalled);
}

if (count($mustDeletedId)>0)
{
	$deleteFromReCalc="DELETE FROM `reCalc` WHERE `id` in (".implode(',',$mustDeletedId).")";
	mysql_query($deleteFromReCalc) or die(handleError('Не удалось удалить перeсчитаные предприятия.',__FILE__,false,$deleteFromReCalc));
}

logCron($startTime,__FILE__,$notGoodEnd,$countBildings);
?>
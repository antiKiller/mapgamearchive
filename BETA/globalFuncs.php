<?php
function handleError($message,$fileName,$onlyPrint=true,$sqlQuery='',$userId='',$withRollback=false)
{
	if (!$onlyPrint)
	{
		$logError[]=date('H:m:s',time());
		$logError[]=$userId;
		$logError[]=str_replace("\n", '', $message);
		$logError[]=$fileName;
		$logError[]=str_replace("\n", '', $sqlQuery);
		$logError[]=mysql_error();
		$string=implode(';',$logError).";";

		$string=str_replace(["\n","	",'  '], ' ', $string)."\n";

		$date=date('d.m.Y');
		$f=@fopen('../appLogs/appErrors_'.$date.'.log', 'a+');
		if (!$f)
		{
			$f=@fopen('appLogs/appErrors_'.$date.'.log', 'a+');
		}
		if ($f)
		{
			fwrite($f, $string);
			fclose($f);
		}
	}
	if ($withRollback)
	{
		mysql_query('ROLLBACK');
	}
	echo json_encode(['result'=>$message]);
	exit();
}

function poligonFromPoints($leftTop,$rightBotom)
{
  return 'POLYGON(( '.$leftTop[0].' '.$leftTop[1].',
					'.$leftTop[0].' '.$rightBotom[1].',
					'.$rightBotom[0].' '.$rightBotom[1].',
					'.$rightBotom[0].' '.$leftTop[1].',
					'.$leftTop[0].' '.$leftTop[1].'))';

 /*
  x1,y1-----------
  |							 |
  |							 |
  -----------x2,y2

  x1,y1------x2,y1
  |							 |
  |							 |
  x1,y2------x2,y2
*/
}

function getInfoOfBuildedObject($object,$isAuc,$nowUser)
{
	if ($isAuc)
	{
		$usersList=" `id`=".$object['user'];

		if ($object['user']!=$object['user_bid'])
		{
			$usersList=" `id` in (".$object['user'].",".$object['user_bid'].") ";
		}

		$getAucUsers="SELECT * FROM `user` WHERE ".$usersList;
		$resAucUsers=mysql_query($getAucUsers) or die(handleError('Ошибка поиска участников аукциона.',__FILE__,false,$getAucUsers,$nowUser['id']));
		while($aucUser=mysql_fetch_assoc($resAucUsers))
		{
			if ($aucUser['id']==$object['user'])
			{
				$userOwner='<span class="openUserProfile" userId="'.$aucUser['id'].'">'.$aucUser['name'].'</span>';
			}

			if ($aucUser['id']==$object['user_bid'])
			{
				$userBidded='<span class="openUserProfile" userId="'.$aucUser['id'].'">'.$aucUser['name'].'</span>';
			}
		}

		$actions='';
		if ($object['user_bid']!=$nowUser['id'])
		{
			$actions='<img class="auctionActionBid" ids="'.$object['id'].'" bid="'.$object['stavka'].'" addr="'.$object['address'].'" typeName="'.$object['nameType'].'" src="images/check.png">';
		}
		$content='
			<div class="geoObjectInfo unCollapsed">
				<div class="geoObjAddr">'.$object['address'].' (<span class="geoObjType">'.$object['nameType'].'</span>)</div>
				<div class="dopInfo">
					<div class="doxod">Доход за последние 3 дня: '.$object['doxod'].' ('.$userOwner.')</div>
					<div class="stavka">Ставка на аукцион: '.$object['stavka'].' ('.$userBidded.')</div>
					<div class="endTime">Окончание аукиона: '.$object['time'].'</div>
					<div class="aucActions">'.$actions.'</div>
				</div>
			</div>';
		return $content;
	}

	//для строений
	$objectRadius=$object['radius'];
	$objectCoords[]=$object['pos1'];
	$objectCoords[]=$object['pos2'];
	$content='';
	if ($object['isBuilded'] && $nowUser['id']==$object['userIds'])
	{
		$content=getBuildingsForObjects($object['objId'],$nowUser,'<div class="geoObjAddr">'.$object['address'].'</div>');
	}
		else
		{
			$clientsName='Клиентов';
			$dopFileds='';
			if ($object['isHouse'])
			{
				$dopFileds.='<div class="doxod">Доход с жителя: '.$object['basicDoxod'].' <img class="moneyIndicator" src="images/money.png"> (без учёта бонусов)</div>';
				$dopFileds.='<div class="doxod">Доход: '.$object['doxod'].' <img class="moneyIndicator" src="images/money.png"></div>';
				$clientsName='Жителей';
			}
				else if($object['isEnterprise'])
				{
					$dopFileds.='<div class="doxod">Доход: '.$object['doxod'].' <img class="moneyIndicator" src="images/money.png"></div>';
				}

			$dopFileds.='<div class="level">Уровень: '.$object['level'].'</div>';
			if (!$object['isSocial'])
			{
				$dopFileds.='<div class="clients">'.$clientsName.': '.$object['clients'].'</div>';
			}
				else
				{
					$dopFileds.='<div class="bonus">Бонус к доходности: '.$object['bonus'].'%</div>';
				}
			if ($object['isEnterprise'])
			{
				$dopFileds.='<div class="competition">Конкуренция: '.$object['competition'].'%</div>';
			}

			$dopFileds.='<div class="info">'.$object['info'].'</div>';
			if ($object['time']>time())
			{
				$dopFileds.='<div>Окончание действия "'.$object['action'].'": '.date('d.m.Y H:i:s', $object['time']);
				if ($nowUser['id']==$object['userIds'])
				{
					$dopFileds.=' <img src="images/speed.png" class="speedUpAction" ids="'.$object['objId'].'" title="Сократить срок работ">
												<img src="images/recalc.png" class="recalcObject" ids="'.$object['objId'].'" title="Пересчитать">';
				}
				$dopFileds.='</div>';
			}
			if ($nowUser['id']==$object['userIds'] && $object['time']<=time())
			{
				if ($object['isCosted']==0)
				{
					$dopFileds.='<div id="housesBlock">
											<img src="images/recalc.png" class="recalcObject" ids="'.$object['objId'].'" title="Пересчитать">
											<img src="images/destroy.png" class="destroyObject" ids="'.$object['objId'].'" title="Снести">
											<img src="images/plus.png" class="sellObject" ids="'.$object['objId'].'" title="Продать">
											<img src="images/imp.png" class="impObject" ids="'.$object['objId'].'" title="Улучшить">';

					if (!$object['isSocial'])
					{
						$dopFileds.='<img src="images/rent.png" class="rentObject" ids="'.$object['objId'].'" title="Сдать в аренду">';
					}
				}
					else
					{
						$record['action']='<img src="images/close.png" class="canselSellBuilding" ids="'.$object['objId'].'" title="Убрать с продажи">';
					}
				$dopFileds.='</div>';
			}

			if ($object['isCosted'] && $nowUser['id']!=$object['userIds'])
			{
				$dopFileds.='<div><img class="buyHouse" title="Купить здание" ids="'.$object['objId'].'" src="images/plus.png">
					('.number_format($object['cost'], 2, '.', ' ').' <img src="images/money.png" class="moneyIndicator"> &nbsp;
					 '.number_format($object['costCredits'], 2, '.', ' ').' <img src="images/baks.png" class="moneyIndicator">)
					</div>';

			}

			$content='
			<div class="geoObjectInfo unCollapsed">
				<div class="geoObjAddr">'.$object['address'].' (<span class="geoObjType">'.$object['nameType'].'</span>)</div>
				<div class="dopInfo">
					<div>Владелец: <span class="openUserProfile" userId="'.$object['userIds'].'">'.$object['userName'].'</span></div>
					'.$dopFileds.'
				</div>
			</div>';
		}
		return $content;
}

function getBuildingsForObjects($objIdsList,$nowUser,$addrLine='')
{
	$content='';
	$getBuildedObjects="SELECT * FROM `geoObjectsTypes` WHERE `isMap`=0 and `canBuyedBefore`>CURRENT_TIMESTAMP ORDER BY `order`";
	$resBuildedObjects=mysql_query($getBuildedObjects);
	$content='<div class="geoObjectInfo nonCollapsed" objId="'.$objIdsList.'">
							'.$addrLine.'
							<div class="dopInfo">';
	$sendedHouse=false;
	$sendedEnterprise=false;
	$sendedSocail=false;
	while($rowBuildedObjects=mysql_fetch_assoc($resBuildedObjects))
	{
		if (!$sendedHouse && $rowBuildedObjects['isHouse'])
		{
			$sendedHouse=true;
			$content.='<h4>Жилые дома</h4>';
		}
		if (!$sendedEnterprise && $rowBuildedObjects['isEnterprise'])
		{
			$sendedEnterprise=true;
			$content.='<h4>Предприятия</h4>';
		}
		if (!$sendedSocail && $rowBuildedObjects['isSocial'])
		{
			$sendedSocail=true;
			$content.='<h4>Социальные учреждения</h4>';
		}
		$dopClass='';
		if ($nowUser['money']<$rowBuildedObjects['basicCost'] || $nowUser['credits']<$rowBuildedObjects['creditsCost'])
		{
			$dopClass='notEnoughMoney';
		}

		$dopFileds='';

		if ($rowBuildedObjects['fixClients']>0)
		{
			$dopFileds.='<div>Предоставляет жителей: <span>'.$rowBuildedObjects['fixClients'].'</span></div>';
		}

		if(!$rowBuildedObjects['isSocial'])
		{
			$dopFileds.='<div>Доход с клиента: <span>'.$rowBuildedObjects['basicDoxod'].' <img class="moneyIndicator" src="images/money.png">  (x'.$rowBuildedObjects['updateDoxod'].' каждый уровень)</span></div>';
		}
			else
			{
				$dopFileds.='<div>Бонус к доходу: <span>'.$rowBuildedObjects['basicBonus'].'</span></div>';
			}

		if ($rowBuildedObjects['radius'])
		{
			$dopFileds.='<div>Радиус: <span>'.$rowBuildedObjects['radius'].'</span></div>';
		}

		$dopFileds.='<div>Время строительства: <span>'.$rowBuildedObjects['basicTime'].' минут</span></div>';
		$dopFileds.='<div>Свойства:<br><span>';

		if ($rowBuildedObjects['updateCost']<$rowBuildedObjects['updateDoxod'])
		{
			$dopFileds.='<abbr class="green" title="При увеличении уровня доходность растёт быстрее расходов.">Сверхэффективное</abbr><br>';
		}

		if ($rowBuildedObjects['updateCost']>=1.25)
		{
			$dopFileds.='<abbr class="red" title="Стоимость повышения уровня растёт очень быстро">Дорогое улучшение (x'.$rowBuildedObjects['updateCost'].' каждый уровень)</abbr><br>';
		}
			elseif ($rowBuildedObjects['updateCost']<=1.15)
			{
				$dopFileds.='<abbr class="green" title="Стоимость повышения уровня растёт очень медленно">Дешевое улучшение (x'.$rowBuildedObjects['updateCost'].' каждый уровень)</abbr><br>';
			}
				else
				{
					$dopFileds.='<abbr title="Стоимость повышения уровня растёт очень умеренно">Умеренная стоимость улучшения (x'.$rowBuildedObjects['updateCost'].' каждый уровень)</abbr><br>';
				}


		if ($rowBuildedObjects['updateTime']>1.1)
		{
			$dopFileds.='<abbr class="red" title="При каждом повышении уровня здание будет очень долго улучшаться">Долгое улучшение (x'.$rowBuildedObjects['updateTime'].' каждый уровень)</abbr><br>';
		}
			elseif ($rowBuildedObjects['updateTime']<=1.05)
			{
				$dopFileds.='<abbr class="green" title="Время улучшения с каждым уровнем растёт лишь немного">Быстрое улучшение (x'.$rowBuildedObjects['updateTime'].' каждый уровень)</abbr><br>';
			}
				else
				{
					$dopFileds.='<abbr title="Время улучшения здания с каждым уровнем растёт не сильно">Умеренная скорость улучшения (x'.$rowBuildedObjects['updateTime'].' каждый уровень)</abbr><br>';
				}

		if($rowBuildedObjects['isSocial'])
		{
			if ($rowBuildedObjects['updateBonus']>=1.25)
			{
				$dopFileds.='<abbr class="green" title="Бонус к доходу растёт хорошими темпами при увеличении уровня здания">Хороший бонус (x'.$rowBuildedObjects['updateBonus'].' каждый уровень)</abbr><br>';
			}
				elseif ($rowBuildedObjects['updateBonus']<=1.10)
				{
					$dopFileds.='<abbr class="red" title="Бонус растёт низкими темпами при увеличении уровня здания">Низкий бонус (x'.$rowBuildedObjects['updateBonus'].' каждый уровень)</abbr><br>';
				}
					else
					{
						$dopFileds.='<abbr title="Бонус растёт умеренными темпами при увеличении уровня здания">Средний бонус (x'.$rowBuildedObjects['updateBonus'].' каждый уровень)</abbr><br>';
					}
		}

		$dopFileds.='</span></div>';

		$getInfoOfNeededBuilding="SELECT a.`name`,a.`img` FROM `geoObjectsTypes` a, `needsForBuildings` b WHERE b.`building`=".$rowBuildedObjects['id']." and a.`id`=b.`needBuilding`";
		$resGetInfoOfNeededBuilding=mysql_query($getInfoOfNeededBuilding);
		$infoOfNeededBuilding=mysql_fetch_assoc($resGetInfoOfNeededBuilding);

		if ($infoOfNeededBuilding)
		{
			$dopFileds.='<div>Для работы этого здания обязательно требуется <img class="geoObjIcon" src="images/'.$infoOfNeededBuilding['img'].'.png"> '.$infoOfNeededBuilding['name'].'</div>';
		}

		$clientsList=[];
		$getClientsInfo="SELECT a.`name`,a.`img`, b.`withHisClients` FROM `geoObjectsTypes` a, `clientsForBuildngs` b WHERE a.`id`=b.`clientObj` and b.`parentObj`=".$rowBuildedObjects['id'];
//		echo $getClientsInfo;
		$resGetClientsInfo=mysql_query($getClientsInfo);
		while($clients=mysql_fetch_assoc($resGetClientsInfo))
		{
			$str='<img class="geoObjIcon" src="images/'.$clients['img'].'.png"> '.$clients['name'];
			if ($clients['withHisClients'])
			{
				$str='<abbr class="withHisClients" title="Вместе со всеми его клиентами">'.$str.'</abbr>';
			}
			$clientsList[]=$str;
		}

		if ($clientsList)
		{
			$dopFileds.='<div>Клиентами этого строения являются: '.implode(',&nbsp; ',$clientsList).'</div>';
		}

		$cost=number_format($rowBuildedObjects['basicCost'],0,'.',' ').' <img class="moneyIndicator" src="images/money.png">';
		if ($rowBuildedObjects['creditsCost']>0)
		{
			$cost.='<br>'.$rowBuildedObjects['creditsCost'].' <img class="moneyIndicator" src="images/baks.png">';
		}
		$content.='
			<div class="buildedOnObject '.$dopClass.'">
				<div class="buildedName">
					<img src="images/'.$rowBuildedObjects['img'].'.png" class="objectTypeImg">
					'.$rowBuildedObjects['name'].'
					<div class="button rightBallonButton" typeId="'.$rowBuildedObjects['id'].'">'.$cost.'</div>
				</div>
				<div class="buildSpoiler">
					<div class="spoilerSwitcher"><abbr title="Кликните чтобы отобразить подробные сведения">Подробнее</abbr></div>
					<div class="spoilerContent">
						<div>'.$rowBuildedObjects['description'].'</div>
						'.$dopFileds.'
					</div>
				</div>
			</div>';
	}
	$content.='	</div>
						</div>';
	return $content;
}

function recalcHouse($reCalled,$manual=false)
{
	if ($manual)
	{
		$delete="DELETE FROM `reCalcHouses` WHERE `houseId`=".$reCalled['houseId']." and `notRecalcUntil`<=(CURRENT_TIMESTAMP + INTERVAL 1 HOUR)";
		mysql_query($delete) or die(handleError('Не удалось здание из очереди пересчёта.',__FILE__,false,$delete));
	}
	$dopClients=floor($reCalled['level']/10);
	$newClients=$reCalled['fixClients']+$dopClients;

	$searchSocials="SELECT * FROM
									(
										SELECT `address`,
													 `geoObjectId`,
													 `name`,
													 `basicBonus`,
													 `updateBonus`,
													 `type`,
													 `img`,
													 `level`,
													 `bonus`
										FROM `buildingsUsers`
										WHERE `isSocial`=1 and `isCosted`=0 and `time`<=CURRENT_TIMESTAMP and
													getDistBeforePoints(POINT(".$reCalled['pos1'].", ".$reCalled['pos2']."),`coords`)<=radius
										ORDER BY `level` DESC
									) a
									GROUP BY `type`";

	$resSearchSocials=mysql_query($searchSocials) or die(handleError('Не удалось найти социалку.',__FILE__,false,$searchSocials));
	$fullPersent=0;
	$info='';
	$oneClientDoxod=$reCalled['basicDoxod']*pow($reCalled['updateDoxod'],$reCalled['level']-1);
	$addPersent=0;
	while ($socialBuild=mysql_fetch_assoc($resSearchSocials))
	{
		$dopClients=floor($socialBuild['level']/20);
		$newClients+=$dopClients;
		$addPersent=$socialBuild['bonus'];
		$info.='<div>Бонус в <b>+'.$addPersent.'%</b> к доходу и +'.$dopClients.' жителей от строения типа <b>'.$socialBuild['name'].'</b> <img class="geoObjIcon" src="images/'.$socialBuild['img'].'.png"> '.$socialBuild['level'].'-ого уровня по адресу <i>'.$socialBuild['address'].'</i>.</div>';
		$fullPersent+=$addPersent;
	}
	if ($fullPersent>0)
	{
		$info.='<br><div>Итого <b>+'.$fullPersent.'%</b> к доходу от всех зданий.</div>';
	}
	$reCalled['doxod']=$newClients*$oneClientDoxod;
	$doxod=round($reCalled['doxod']+$reCalled['doxod']*$fullPersent/100,2);

	$updateHouse="UPDATE `house` SET
														`clients`=".$newClients.",
														`doxod`=".$doxod.",
														`info`='".$info."',
														`bonus`=".$fullPersent.",
														`competition`=0,
														`reCalcedDate`=CURRENT_TIMESTAMP
								WHERE `id`=".$reCalled['houseId'];
	mysql_query($updateHouse);
}

function recalcEnterprise($reCalled,$manual=false)
{
	if ($manual)
	{
		$delete="DELETE FROM `reCalc` WHERE `houseId`=".$reCalled['houseId']." and `notRecalcUntil`<=(CURRENT_TIMESTAMP + INTERVAL 1 HOUR)";
		mysql_query($delete) or die(handleError('Не удалось здание из очереди пересчёта.',__FILE__,false,$delete));
	}
	
	$notClients=false;
	$info=$reCalled['info'];
	if ($reCalled['isNeeded'])
	{
		$isCanWorks="SELECT a.`id`
								 FROM `geoObjectsTypes` a,`needsForBuildings` b, `geoObjects` c, `house` d
								 WHERE c.`id`!=".$reCalled['geoObjectId']." and b.`building`=".$reCalled['type']." and a.`id`=b.`needBuilding` and c.`type`=b.`needBuilding` and
											 d.`geoObjectId`=c.`id` and (d.`time`<=CURRENT_TIMESTAMP or d.`action`='Аренда') and
									     getDistBeforePoints(POINT(".$reCalled['pos1'].", ".$reCalled['pos2']."),c.`coords`)<=a.radius
								 LIMIT 1";
		$resIsCanWorks=mysql_query($isCanWorks) or die(handleError('Не удалось провести проверку здания на наличие необходимого здания.',__FILE__,false,$isCanWorks));
		$isCanWorksBuilding=mysql_fetch_assoc($resIsCanWorks);
		$notClients=true;
		if ($isCanWorksBuilding)
		{
			$notClients=false;
		}
		if ($notClients)
		{
			$getInfoOfNeededBuilding="SELECT a.`name`,a.`img`
																FROM `geoObjectsTypes` a, `needsForBuildings` b
																WHERE b.`building`=".$reCalled['type']." and a.`id`=b.`needBuilding`";
			$resGetInfoOfNeededBuilding=mysql_query($getInfoOfNeededBuilding) or die(handleError('Не удалось получить информацию о необходимом здании.',__FILE__,false,$getInfoOfNeededBuilding));
			$infoOfNeededBuilding=mysql_fetch_assoc($resGetInfoOfNeededBuilding);
			$info='Для работы необходимо построить <b>'.$infoOfNeededBuilding['name'].'</b> <img class="geoObjIcon" src="images/'.$infoOfNeededBuilding['img'].'.png">';
			$allClients=0;
			$concurensePersent=0;
			$profit=0;
		}
	}
	if (!$notClients)
	{
		$info=null;

		$getClientsQuery="SELECT GROUP_CONCAT(`clientObj`) `clientObj`,`withHisClients` FROM `clientsForBuildngs` WHERE `parentObj`=".$reCalled['type']." GROUP BY `withHisClients`";
		$resClientsQuery=mysql_query($getClientsQuery) or die(handleError('Не удалось получить список типов клиентов здания.',__FILE__,false,$getClientsQuery));

		$badClients=0;
		$allClients=0;
		while($clientsQuery=mysql_fetch_assoc($resClientsQuery))
		{
			$clientsTypes=trim(str_replace(',,', ',', $clientsQuery['clientObj']),' ,');

			$clients='SUM(`clients`)';
			if (!$clientsQuery['withHisClients'])
			{
				$clients='COUNT(`clients`)';
			}

			$searchClients="SELECT ".$clients." `clients`, GROUP_CONCAT(`geoObjectId`) `idsList`
											FROM `buildingsUsers`
											WHERE `type` in (".$clientsTypes.") and (`time`<=CURRENT_TIMESTAMP or `action`='Аренда') and
														getDistBeforePoints(POINT(".$reCalled['pos1'].", ".$reCalled['pos2']."),`coords`)<=".$reCalled['radius'];

			$resSearchClients=mysql_query($searchClients) or die(handleError('Не удалось получить список клиентов.',__FILE__,false,$searchClients));
			$clientsCount=mysql_fetch_assoc($resSearchClients);
			$allClients+=$clientsCount['clients'];

			$alreadySearched=[];
			$searchBadClients="SELECT `id`, X(`coords`) pos1, Y(`coords`) pos2
												 FROM `buildingsUsers`
												 WHERE `geoObjectId`!=".$reCalled['geoObjectId']." and `type`=".$reCalled['type']." and (`time`<=CURRENT_TIMESTAMP or `action`='Аренда') and
															 getDistBeforePoints(POINT(".$reCalled['pos1'].", ".$reCalled['pos2']."),`coords`)<=".($reCalled['radius']*2)."
												 LIMIT 500";
			$resSearchBadClients=mysql_query($searchBadClients) or die(handleError('Не удалось получить список конкурирующих зданий.',__FILE__,false,$searchBadClients));
			while($allClients>0 && $building=mysql_fetch_assoc($resSearchBadClients))
			{
				if (count($alreadySearched)>0)
				{
					$dopField="and `id` NOT IN (".implode(',',$alreadySearched).")";
				}
				$clientsCount['idsList']=trim($clientsCount['idsList'],', ');
				$clientsCount['idsList']=  str_replace(',,', ',', $clientsCount['idsList']);
				if ($clientsCount['idsList'])
				{
					$getClientsInOthers="SELECT `id`, `clients`
															 FROM `buildingsUsers`
															 WHERE `type` in (".$clientsTypes.") and `geoObjectId` in (".$clientsCount['idsList'].") ".$dopField." and
																		 (`time`<=CURRENT_TIMESTAMP or `action`='Аренда') and
																		 getDistBeforePoints(POINT(".$building['pos1'].", ".$building['pos2']."),`coords`)<=".$reCalled['radius'];
					$resCount=mysql_query($getClientsInOthers) or die(handleError('Не удалось получить список клиентов у конкурирующих зданий.',__FILE__,false,$getClientsInOthers));
					while($otherBuildings=mysql_fetch_assoc($resCount))
					{
						if (!$clientsQuery['withHisClients'])
						{
							$badClients+=$otherBuildings['clients'];
						}
							else
							{
								$badClients++;
							}
						$alreadySearched[]=$otherBuildings['id'];
					}
				}
			}
		}

		$basDoxod=$reCalled['basicDoxod'];
		if ($reCalled['level']>1)
		{
			$basDoxod=$basDoxod*pow($reCalled['updateDoxod'],$reCalled['level']-1);
		}

		if ($badClients>$allClients)
		{
			$badClients=$allClients;
		}

		if ($badClients>0)
		{
			$profit=round( ($basDoxod*($allClients-$badClients)) + ($basDoxod/2*$badClients), 2);
		}
			else
			{
				$profit=round($basDoxod*$allClients, 2);
			}

		$concurensePersent=0;
		if ($allClients)
		{
			$concurensePersent=$badClients*100/$allClients;
		}

	}
	if ($info!=null)
	{
		$info="'".$info."'";
	}
		else
		{
			$info='null';
		}
	$updateBuildingInfo="UPDATE `house`
											 SET `clients`=".$allClients.",
													 `competition`=".$concurensePersent.",
													 `doxod`=".$profit.",
													 `info`=".$info.",
													 `reCalcedDate`=CURRENT_TIMESTAMP
											 WHERE `id`=".$reCalled['houseId'];
	mysql_query($updateBuildingInfo) or die(handleError('Не удалось обновить данные здания.',__FILE__,false,$updateBuildingInfo));
}

//function recalcSocial()
//{
//	
//}
?>
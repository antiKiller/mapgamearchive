function show_improve(id,pr)
{
    var vis = getClientSTop()
    if(!document.getElementById('message'))
    {
    var message = document.createElement('div');
    message.style.position="absolute";
    message.style.top="50%";
    message.style.left="50%";
	message.style.marginLeft="-301px";
	message.style.marginTop="-68px";
    message.style.width="600px";
    message.style.backgroundColor="#FFFFFF";
    message.style.border="1px solid #777777";
    //message.style.textAlign="left";
    message.setAttribute("id", "message");
    document.body.appendChild(message);
    var text =  document.createElement('div');
    var input = document.createElement('div');
	var header =  document.createElement('div');
	header.style.height="25px";
	header.style.backgroundColor="#5a7da3";
	header.innerHTML="<font color='#FFFFFF' style='padding-left:50%;margin-left:-83px;'>Подтвердите улучшение</font>";
	message.appendChild(header);
    input.style.height="27px";
	input.style.textAlign="right";
    text.borderBottom="1px solid #777777";
    message.appendChild(text);
    text.style.padding="5px";
	text.style.height="80px";
	adr=document.getElementById("t"+id).innerHTML;
    text.innerHTML="Вы действительно хотите улучшить дом <b>"+adr+"</b> за <b>"+pr+"</b><img src='images/money.png' width='16' height='16'>";
	var button = document.createElement('input');
    button.setAttribute("type", "button");
    button.setAttribute("value", "Да");
    button.style.height="27px";
	button.style.width="60px";
	button.style.textAlign="center";
    button.style.border="0px";
	button.style.borderRadius="5px";
    button.style.backgroundColor="#5a7da3";
    button.style.color="#fff";
    button.style.cursor="pointer";
	button.style.left="30px";
    button.onclick = function()
    {
		imp(id);
		input.removeChild(butcan);
        input.removeChild(button);
        message.removeChild(input);
        message.removeChild(text);
        document.body.removeChild(message);
	}
    input.appendChild(button);
	var butcan = document.createElement('input');
    butcan.setAttribute("type", "button");
    butcan.setAttribute("value", "Отмена");
    butcan.style.height="27px";
	butcan.style.width="60px";
	butcan.style.textAlign="center";
    butcan.style.border="0px";
    butcan.style.backgroundColor="#5a7da3";
    butcan.style.color="#fff";
    butcan.style.cursor="pointer";
	butcan.style.left="30px";
	butcan.style.marginLeft="5px";
	butcan.style.borderRadius="5px";
    butcan.onclick = function()
    {
		input.removeChild(butcan);
        input.removeChild(button);
        message.removeChild(input);
        message.removeChild(text);
        document.body.removeChild(message);
	}
    input.appendChild(butcan);
    message.appendChild(input);
    }
	
}
function getClientSTop(){
    return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop)
}
window.onscroll = function  () {
    if(document.getElementById('message'))
    {
        var message = document.getElementById('message');
        var vis = getClientSTop();
        message.style.top="50%";
    }
}
function imp(id) {
 
    var req = new XMLHttpRequest();
	document.body.removeChild(document.getElementById("message"));
	req.open('GET', 'improve.php?ids=' + id, false); 
	req.send(null);
	document.getElementById("ar"+id).innerHTML = req.responseText;
	
}
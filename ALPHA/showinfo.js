function show_info(id)
{
    var vis = getClientSTop()
    if(!document.getElementById('message'))
    {
    var message = document.createElement('div');
    message.style.position="absolute";
    message.style.top="50%";
    message.style.left="50%";
	message.style.marginLeft="-301px";
	message.style.marginTop="-86px";
    message.style.width="600px";
    message.style.backgroundColor="#FFFFFF";
    message.style.border="1px solid #777777";
    //message.style.textAlign="left";
    message.setAttribute("id", "message");
    document.body.appendChild(message);
    var text =  document.createElement('div');
	text.setAttribute("id", "text");
    var input = document.createElement('div');
	var header =  document.createElement('div');
	header.style.height="25px";
	header.style.backgroundColor="#5a7da3";
	header.innerHTML="<font color='#FFFFFF' style='padding-left:40px;'>Детализация</font>";
	message.appendChild(header);
    input.style.height="27px";
	input.style.textAlign="right";
	input.style.backgroundColor="#F0F0F0";
    text.borderBottom="1px solid #777777";
    message.appendChild(text);
    text.style.padding="10 25 10 25";
	//text.style.height="101px";
	var req = new XMLHttpRequest();
	req.open('GET', 'showinfo.php?ids=' + id, false); 
	req.send(null);
	otvet=req.responseText;
    text.innerHTML=otvet;
	var but = document.createElement('input');
    but.setAttribute("type", "button");
    but.setAttribute("value", "Пересчитать");
    but.style.height="26px";
	//but.style.width="100px";
	but.style.textAlign="center";
    but.style.border="0px";
    but.style.backgroundColor="#5a7da3";
    but.style.color="#fff";
    but.style.cursor="pointer";
	but.style.left="30px";
	but.style.margin="0 5 0 0";
    but.onclick = function()
    {
		document.getElementById("text").innerHTML="Идет пересчет...";
		var req2 = new XMLHttpRequest();
		req2.open('GET', 'pereschet.php?ids=' + id, false); 
		req2.send(null);
		
		setTimeout("setval("+id+")",3000);
	}
    input.appendChild(but);
    var button = document.createElement('input');
    button.setAttribute("type", "button");
    button.setAttribute("value", "Закрыть");
    button.style.height="26px";
	//button.style.width="60px";
	button.style.textAlign="center";
    button.style.border="0px";
    button.style.backgroundColor="#5a7da3";
    button.style.color="#fff";
    button.style.cursor="pointer";
	button.style.left="30px";
	button.style.margin="0 5";
    button.onclick = function()
    {
		input.removeChild(but);
        input.removeChild(button);
        message.removeChild(input);
        message.removeChild(text);
        document.body.removeChild(message);
	}
    input.appendChild(button);
    message.appendChild(input);
    }
	
}
function setval(id)
{
	req = new XMLHttpRequest();
	req.open('GET', 'showinfo.php?ids=' + id, false); 
	req.send(null);
	var otvet3=req.responseText;
	document.getElementById("text").innerHTML=otvet3;
	//text.innerHTML=otvet;
}
function getClientSTop()
{
    return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop)
}
window.onscroll = function  () {
    if(document.getElementById('message'))
    {
        var message = document.getElementById('message');
        var vis = getClientSTop();
        message.style.top="40%";
    }
}
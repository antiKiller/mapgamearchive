<?php
$CFG = array (
  'charsets' => 'cp1251 utf8 latin1',
  'lang' => 'ru',
  'time_web' => '1600',
  'time_cron' => '1600',
  'backup_path' => 'backup/',
  'backup_url' => 'backup/',
  'only_create' => 'MRG_MyISAM MERGE HEAP MEMORY',
  'globstat' => 0,
  'my_host' => '127.0.0.1',
  'my_port' => 3306,
  'my_user' => 'DB_USER',
  'my_pass' => 'DB_USER_PASS',
  'my_comp' => 1,
  'my_db' => 'mapGame',
  'auth' => 'mysql cfg',
  'user' => '',
  'pass' => '',
  'confirm' => '6',
  'exitURL' => './',
);
?>
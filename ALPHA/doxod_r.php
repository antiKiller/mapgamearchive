<?
function get_doxod($idh)
{
	require_once "zona.php";
	$doxod=0;
	$kind=0;
	$zona=0;
	$pos1=0;
	$pos2=0;
	$lvl=1;
	$q="SELECT * FROM house where `house`.`id`=$idh";
	$r=mysql_query($q);
	while ($row=mysql_fetch_array($r))
	{
		$kind=$row[kind];
		$pos1=$row[pos1];
		$pos2=$row[pos2];
		$lvl=$row[level];
	}
	$kol=0;
	$nkol=0;
	$s="";
	$info="";
	if ($kind==1)
	{
		$m_d=5;
		$doxod=round($m_d*pow(1.2,($lvl)-1),2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$zona=600;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=6";
		$s.=$q;
		$r=mysql_query($q);
		$max1=0;
		$max2=0;
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max1)
			$max1=$row[doxod];
		}
		$zona=1200;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=7";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max2)
			$max2=$row[doxod];
		}
		$doxod=$doxod*(1+($max1+$max2)/100);
		$doxod=round($doxod,2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=17";
		$s.=$q;
		$r=mysql_query($q);
		$stat=0;
		while ($row=mysql_fetch_array($r))
		{
			$stat+=8;
		}
		$doxod=$doxod*(1+($stat)/100);
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$k=$stat/8;
			$info.="Достопримечательностей около дома: $k";
		}
	}
	if ($kind==2)
	{
		$m_d=40;
		$zona=200;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `id` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=2";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `id` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=2";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `id` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=2";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `id` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=2";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$zona=3000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `id` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=3";
		$result=mysql_query($q);
		$n=(int)mysql_num_rows($result);
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Склад отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть склад</font><br>";
		}

		$doxod=(9/8)*$m_d-1/(0.005*$kol+8/(9*$m_d));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		//$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		$m_doxod=(9/8)*$m_d-1/(0.005*$nkol+8/(9*$m_d));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==3)
	{
		$m_d=200;
		$zona=3000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=2";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=3";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=5";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=3";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((20/$n),3);
			$nkol+=20;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=8";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=3";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((50/$n),3);
			$nkol+=50;
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=4";
		$result=mysql_query($q);
		$n=(int)mysql_num_rows($result);
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Завод отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть завод</font><br>";
		}

		$doxod=(9/8)*$m_d-1/(0.0005*$kol+8/(9*$m_d));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		//$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		$m_doxod=(9/8)*$m_d-1/(0.0005*$nkol+8/(9*$m_d));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==4)
	{
		$m_d=2000;
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=3";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=4";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}

		$doxod=(9/8)*$m_d-1/(0.0000375*$kol+8/(9*$m_d));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		//$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		$m_doxod=(9/8)*$m_d-1/(0.0000375*$nkol+8/(9*$m_d));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==5)
	{
		$m_d=800;
		$zona=800;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=5";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=5";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=5";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=5";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$zona=3000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=3";
		$result=mysql_query($q);
		$n=(int)mysql_num_rows($result);
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Склад отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть склад</font><br>";
		}


		$doxod=((9/8)*$m_d-1/(0.00001*$kol+8/(9*$m_d)))*1.125;
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		//$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		$m_doxod=((9/8)*$m_d-1/(0.00001*$nkol+8/(9*$m_d)))*1.125;
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==6)
	{
		$m_d=5;
		$zona=600;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and (`kind`=1 or `kind`=9 or `kind`=10 or `kind`=12)";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$nkol++;
		}
		$doxod=$m_d*pow(1.2,$lvl-1);
		$doxod=round($doxod,2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$info=$s1."Клиенты: <b>$nkol</b><br>Увеличивает доход дома на <b>$doxod%</b><br><b>$lvl-й</b> уровень здания <b>+$pr%</b>";
	}
	if ($kind==7)
	{
		$m_d=5;
		$zona=1200;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and (`kind`=1 or `kind`=9 or `kind`=10 or `kind`=12)";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$nkol++;
		}
		$doxod=$m_d*pow(1.2,$lvl-1);
		$doxod=round($doxod,2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$info=$s1."Клиенты: <b>$nkol</b><br>Увеличивает доход дома на <b>$doxod%</b><br><b>$lvl-й</b> уровень здания <b>+$pr%</b>";
	}
	if ($kind==8)
	{
		$m_d=2000;
		$zona=2500;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=8";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=8";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=8";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=8";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$zona=3000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=3";
		$result=mysql_query($q);
		$n=(int)mysql_num_rows($result);
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Склад отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть склад</font><br>";
		}

		$doxod=1.1*((9/8)*$m_d-1/(0.0000015*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		//$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		$m_doxod=1.1*((9/8)*$m_d-1/(0.0000015*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==9)
	{
		$m_d=150;
		$doxod=round($m_d*pow(1.2,($lvl)-1),2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$zona=600;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=6";
		$s.=$q;
		$r=mysql_query($q);
		$max1=0;
		$max2=0;
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max1)
			$max1=$row[doxod];
		}
		$zona=1200;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=7";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max2)
			$max2=$row[doxod];
		}
		$doxod=$doxod*(1+($max1+$max2)/100);
		$doxod=round($doxod,2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=17";
		$s.=$q;
		$r=mysql_query($q);
		$stat=0;
		while ($row=mysql_fetch_array($r))
		{
			$stat+=8;
		}
		$doxod=$doxod*(1+($stat)/100);
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$k=$stat/8;
			$info.="Достопримечательностей около дома: $k";
		}
	}
	if ($kind==10)
	{
		$m_d=75;
		$doxod=round($m_d*pow(1.2,($lvl)-1),2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$zona=600;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=6";
		$s.=$q;
		$r=mysql_query($q);
		$max1=0;
		$max2=0;
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max1)
			$max1=$row[doxod];
		}
		$zona=1200;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=7";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max2)
			$max2=$row[doxod];
		}
		$doxod=$doxod*(1+($max1+$max2)/100);
		$doxod=round($doxod,2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=17";
		$s.=$q;
		$r=mysql_query($q);
		$stat=0;
		while ($row=mysql_fetch_array($r))
		{
			$stat+=8;
		}
		$doxod=$doxod*(1+($stat)/100);
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$k=$stat/8;
			$info.="Достопримечательностей около дома: $k";
		}
	}
	if ($kind==11)
	{
		$m_d=30;
		$zona=150;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=11";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=11";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=11";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT * FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=11";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";

		$doxod=(35-1/(0.005*$kol+1/35));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		//$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		$m_doxod=(35-1/(0.005*$nkol+1/35));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==12)
	{
		$m_d=25;
		$doxod=round($m_d*pow(1.2,($lvl)-1),2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$zona=600;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=6";
		$s.=$q;
		$r=mysql_query($q);
		$max1=0;
		$max2=0;
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max1)
			$max1=$row[doxod];
		}
		$zona=1200;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=7";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			if ($row[doxod]>$max2)
			$max2=$row[doxod];
		}
		$doxod=$doxod*(1+($max1+$max2)/100);
		$doxod=round($doxod,2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `doxod` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=17";
		$s.=$q;
		$r=mysql_query($q);
		$stat=0;
		while ($row=mysql_fetch_array($r))
		{
			$stat+=8;
		}
		$doxod=$doxod*(1+($stat)/100);
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$k=$stat/8;
			$info.="Достопримечательностей около дома: $k";
		}
	}
	if ($kind==13)
	{
		$m_d=1800;
		$zona=1500;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=13";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=13";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=13";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=13";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000003*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000003*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==14)
	{
		$m_d=1500;
		$zona=1500;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=14";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=14";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=14";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=14";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000004*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000004*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==15)
	{
		$m_d=4000;
		$zona=5000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=15";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT * FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=15";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=15";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=15";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000001*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000001*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==16)
	{
		$m_d=8000;
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=16";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=16";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT * FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=16";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=16";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000000375*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000000375*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==17)
	{
		$m_d=8;
		$zona=10000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and (`kind`=1 or `kind`=9 or `kind`=10 or `kind`=12)";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$nkol++;
		}
		$doxod=$m_d*pow(1.2,$lvl-1);
		$doxod=round($doxod,2);
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$info=$s1."Клиенты: <b>$nkol</b><br>Увеличивает доход дома на <b>$doxod%</b><br>";
	}
	if ($kind==18)
	{
		$m_d=15000;
		$zona=1000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=18";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=18";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT * FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=18";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=18";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000003*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000003*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==19)
	{
		$m_d=25000;
		$zona=5000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=18";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=19";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=20";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=19";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((20/$n),3);
			$nkol+=20;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=22";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=19";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((20/$n),3);
			$nkol+=50;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.0000057*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000057*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==20)
	{
		$m_d=40000;
		$zona=3000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=20";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=20";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT * FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=20";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=20";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.0000001*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000001*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==21)
	{
		$m_d=70000;
		$zona=25000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=19";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=21";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}

		$s1="";
		$doxod=((9/8)*$m_d-1/(0.0000051*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000051*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	if ($kind==22)
	{
		$m_d=100000;
		$zona=5000;
		$d=$zona/(111110*cos($pos1*M_PI/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=1";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=22";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((1/$n),3);
			$nkol++;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=9";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=22";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((30/$n),3);
			$nkol+=30;
		}
		$q="SELECT * FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=10";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=22";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((15/$n),3);
			$nkol+=15;
		}
		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=12";
		$s.=$q;
		$r=mysql_query($q);
		while ($row=mysql_fetch_array($r))
		{
			$n=0;
			$pos001=$row[pos1]-$b;
			$pos002=$row[pos2]-$d;
			$pos011=$row[pos1]-0+$b;
			$pos012=$row[pos2]-0+$d;

			$q1="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos001 and `house`.`pos2`>$pos002 and `house`.`pos1`<$pos011 and `house`.`pos2`<$pos012 and `house`.`kind`=22";
			$r1=mysql_query($q1);
			$n=mysql_num_rows($r1);
			$kol+=round((5/$n),3);
			$nkol+=5;
		}
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.00000003*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.00000003*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=round((1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=round(100*pow(1.2,($lvl)-1))-100;
		$m_doxod=round($m_doxod,2);
		$doxod=round($doxod,2);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$m_doxod=round($m_doxod*pow(1.2,($lvl)-1),2);
		$doxod=round($doxod*pow(1.2,($lvl)-1),2);
		if ($n==0)
		{
			$doxod=0;
		}
	}
	$doxod=round($doxod,2);
	$q2="UPDATE  `house` SET  `doxod` =  $doxod WHERE  `house`.`id` =$idh;";
	mysql_query($q2);
	$q3="UPDATE  `house` SET  `info` =  '$info' WHERE  `house`.`id` =$idh;";
	$r = mysql_query ($q3) or die("Error in $q3 : ". mysql_error());
	return $s;
}
?>
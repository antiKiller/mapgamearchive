#!/usr/bin/perl

use DBI;
#use bigint;
my $host = "localhost"; # MySQL-сервер нашего хостинга
my $port = "3306"; # порт, на который открываем соединение
my $user = ""; # имя пользователя
my $pass = ""; # пароль
my $db = ""; # имя базы данных

print "Content-type: text/html\n\n";

$dbh = DBI->connect("DBI:mysql:$db:$host:$port",$user,$pass);
$dbh->do("set character set utf8");
$dbh->do("set names utf8");

$pr_house=50;
$pr_shop=150;
$pr_warehouse=1000;
$pr_factory=10000;
$pr_supermarket=3000;
$pr_school=1500;
$pr_hospital=3000;
$pr_tc=15000;
$pr_elite=5000;
$pr_tenement=2500;
$pr_cafe=150;
$pr_cottage=300;
$pr_theatre=13000;
$pr_cinema=10000;
$pr_hotel=35000;
$pr_stadium=80000;

$q="SELECT `id`,`money` FROM `user` order by `id` ASC";
$sth = $dbh->prepare($q);
$sth->execute;
while ($ref = $sth->fetchrow_arrayref)
{
	$id=$$ref[0];
	$money=$$ref[1];
	$kapital=0;
	$kapitalization=0;
	$q2="SELECT kind,level FROM `house` WHERE `user`=$id";
	$sth2 = $dbh->prepare($q2);
	$sth2->execute;
	while ($reff = $sth2->fetchrow_arrayref)
	{
		$kind=$$reff[0];
		$lvl=$$reff[1];
		$p=0;
		if ($kind==1)
		{
			$p=$pr_house;
		}
		if ($kind==2)
		{
			$p=$pr_shop;
		}
		if ($kind==3)
		{
			$p=$pr_warehouse;
		}
		if ($kind==4)
		{
			$p=$pr_factory;
		}
		if ($kind==5)
		{
			$p=$pr_supermarket;
		}
		if ($kind==6)
		{
			$p=$pr_school;
		}
		if ($kind==7)
		{
			$p=$pr_hospital;
		}
		if ($kind==8)
		{
			$p=$pr_tc;
		}
		if ($kind==9)
		{
			$p=$pr_elite;
		}
		if ($kind==10)
		{
			$p=$pr_tenement;
		}
		if ($kind==11)
		{
			$p=$pr_cafe;
		}
		if ($kind==12)
		{
			$p=$pr_cottage;
		}
		if ($kind==13)
		{
			$p=$pr_theatre;
		}
		if ($kind==14)
		{
			$p=$pr_cinema;
		}
		if ($kind==15)
		{
			$p=$pr_hotel;
		}
		if ($kind==16)
		{
			$p=$pr_stadium;
		}
		$kap=sprintf("%.2f",$p*0.7*(1.3**($lvl)-1)/0.3)-0;
		$kapitalization=$kapitalization+$kap;
	}
	$rc = $sth2->finish;
	$q2="SELECT `stavka` FROM `auction` where `user_bid`=$id";
	$sth2 = $dbh->prepare($q2);
	$sth2->execute;
	$stavki=0;
	while ($reff = $sth2->fetchrow_arrayref)
	{
		$stavki=$stavki+$$reff[0];
	}
	$rc = $sth2->finish;

	$kapital=$money+$kapitalization+$stavki;
	if ($id<20)
	{print "$kapitalization  $kapital  $stavki $money\n";}
	$q2="UPDATE `user` SET `kapitalization`=$kapitalization, `kapital`=$kapital WHERE `id`=$id";
	$sth2 = $dbh->prepare($q2);
	$sth2->execute;
	$rc = $sth2->finish;
}
$rc = $sth->finish;
$q="TRUNCATE TABLE `rating`";
$sth = $dbh->prepare($q);
$sth->execute;
$rc = $sth->finish;
$q="SELECT id,kapital,name FROM `user` order by `kapital` DESC";
$sth = $dbh->prepare($q);
$sth->execute;
while ($ref = $sth->fetchrow_arrayref)
{
	$id=$$ref[0];
	$kapital=$$ref[1];
	$name=$$ref[2];
        if ($id!=1)
        {
	        $q2="INSERT INTO `rating` VALUES(0,$id,$kapital,'$name')";
	        $sth2 = $dbh->prepare($q2);
	        $sth2->execute;
	        $rc = $sth2->finish;
        }
}
$rc = $sth->finish;

$rc = $dbh->disconnect;  # закрываем соединение
#!/usr/bin/perl

use DBI;

my $host = "localhost"; # MySQL-сервер нашего хостинга
my $port = "3306"; # порт, на который открываем соединение
my $user = ""; # имя пользователя
my $pass = ""; # пароль
my $db = ""; # имя базы данных

print "Content-type: text/html\n\n";

$dbh = DBI->connect("DBI:mysql:$db:$host:$port",$user,$pass);
$dbh->do("set character set utf8");
$dbh->do("set names utf8");

#$id_per=0;
#$q="SELECT * FROM `per_h` where `id`=1 or `id`=2 or `id`=3 order by `id` ASC";
#$sth = $dbh->prepare($q);
#$sth->execute;
#while ($ref = $sth->fetchrow_arrayref)
#{
#	if (($$ref[1]==0)&&($id_per==0))
#	{
#		$id_per=$$ref[0];
#		$q2="UPDATE `per_h` SET `state`=1 where `id`=$id_per";
#		$sth2 = $dbh->prepare($q2);
#		$sth2->execute;
#		$rc = $sth2->finish;
#	}
#}
#$rc = $sth->finish;
#if ($id_per==0)
#{
#	exit(0);
#}

$time0=time;
while (time<=$time0+60)
{
$doxod=0;
$kind=0;
$zona=0;
$pos1=0;
$pos2=0;
$lvl=1;
$idh = 0;
$ids=0;

$q="SELECT * FROM `pereschet` order by `id` ASC limit 0,1";
$sth = $dbh->prepare($q);
$sth->execute;
while ($ref = $sth->fetchrow_arrayref)
{
	$ids=$$ref[0];
	$idh=$$ref[1];
	$q2="delete FROM `pereschet` where `house`=$idh";
	$sth2 = $dbh->prepare($q2);
	$sth2->execute;
	$rc = $sth2->finish;
}
$rc = $sth->finish;

if ($ids!=0)
{

$q="SELECT kind, pos1,pos2,level FROM house where id=$idh";
$sth = $dbh->prepare($q);
$sth->execute;

while ($ref = $sth->fetchrow_arrayref) {

	$kind=$$ref[0];
	$pos1=$$ref[1];
	$pos2=$$ref[2];
	$lvl=$$ref[3];
}
$rc = $sth->finish;


#print "kind = $kind, pos1=$pos1,  pos2=$pos2, level=$lvl\n";
$kol=0;
$nkol=0;
$s="";
$info="";
if ($kind==1)
	{
		$m_d=5;
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$doxod=sprintf("%.2f",$m_d);
		$zona=600;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$max1=0;
		$max2=0;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=6)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max1)
			{
			$max1=$$ref[0];}
		}
		$rc = $sth->finish;
		$zona=1200;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=7)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max2)
			{
			$max2=$$ref[0];}
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($max1+$max2)/100);
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=17)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$stat=0;
		$k=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$stat=$stat+8;
			$k=$k+1;
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($stat)/100);
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$info.="Достопримечательностей около дома: $k";
		}
}
if ($kind==2)
	{
		$m_d=40;
		$zona=200;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=2";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$zona=3000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=3";
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$n=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=$n+1;
		}
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Склад отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть склад</font><br>";
		}
		$doxod=((9/8)*$m_d-1/(0.005*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.005*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info="$s1Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($n==0)
		{
			$doxod=0;
		}


}
if ($kind==3)
	{
		$m_d=200;
		$zona=3000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=2 or kind=5 or kind=8)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=3";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==2) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==5) {
				$kol=$kol+sprintf("%.3f",(20/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==8) {
				$kol=$kol+sprintf("%.3f",(50/$n));
				$nkol=$nkol+50;
			}
		}

		$rc = $sth->finish;
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=4";
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$n=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=$n+1;
		}
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Завод отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть завод</font><br>";
		}
		$doxod=((9/8)*$m_d-1/(0.0005*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0005*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info="$s1Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($n==0)
		{
			$doxod=0;
		}


}
if ($kind==4)
	{
		$m_d=2000;
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=3)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=4";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}

			$kol=$kol+sprintf("%.3f",(1/$n));
			$nkol=$nkol+1

		}

		$rc = $sth->finish;

		$doxod=((9/8)*$m_d-1/(0.0000375*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000375*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($n==0)
		{
			$doxod=0;
		}


}
if ($kind==5)
	{
		$m_d=800;
		$zona=800;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=5";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$zona=3000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=3";
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$n=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=$n+1;
		}
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Склад отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть склад</font><br>";
		}
		$doxod=((9/8)*$m_d-1/(0.00001*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.00001*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info="$s1Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($n==0)
		{
			$doxod=0;
		}


}
if ($kind==6)
	{
		$m_d=5;
		$zona=600;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$nkol=$nkol+1
		}
		$rc = $sth->finish;
		$s1="";
		$doxod=$m_d;
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;

		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		$info=$s1."Клиенты: <b>$nkol</b><br>Увеличивает доход дома на <b>$doxod%</b><br><b>$lvl-й</b> уровень здания <b>+$pr%</b>";
}
if ($kind==7)
	{
		$m_d=5;
		$zona=1200;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$nkol=$nkol+1
		}
		$rc = $sth->finish;
		$s1="";
		$doxod=$m_d;
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;

		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		$info=$s1."Клиенты: <b>$nkol</b><br>Увеличивает доход дома на <b>$doxod%</b><br><b>$lvl-й</b> уровень здания <b>+$pr%</b>";
}
if ($kind==8)
	{
		$m_d=2000;
		$zona=2500;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=8";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$zona=3000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		$q="SELECT `pos1`,`pos2` FROM house where `house`.`pos1`>$pos01 and `house`.`pos2`>$pos02 and `house`.`pos1`<$pos11 and `house`.`pos2`<$pos12 and `house`.`kind`=3";
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$n=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=$n+1;
		}
		$s1="";
		if ($n==0)
		{
			$s1="<font color=#FF0000>Склад отсутствует</font><br>";
		}
		else
		{
			$s1="<font color=#000000>Есть склад</font><br>";
		}

		$doxod=((9/8)*$m_d-1/(0.0000015*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000015*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info="$s1Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($n==0)
		{
			$doxod=0;
		}


}
if ($kind==9)
	{
		$m_d=150;
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$doxod=sprintf("%.2f",$m_d);
		$zona=600;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$max1=0;
		$max2=0;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=6)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max1)
			{
			$max1=$$ref[0];}
		}
		$rc = $sth->finish;
		$zona=1200;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$max2=0;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=7)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max2)
			{
			$max2=$$ref[0];}
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($max1+$max2)/100);
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=17)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$stat=0;
		$k=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$stat=$stat+8;
			$k=$k+1;
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($stat)/100);
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$info.="Достопримечательностей около дома: $k";
		}
}
if ($kind==10)
	{
		$m_d=75;
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$doxod=sprintf("%.2f",$m_d);
		$zona=600;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$max1=0;
		$max2=0;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=6)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max1)
			{
			$max1=$$ref[0];}
		}
		$rc = $sth->finish;
		$zona=1200;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=7)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max2)
			{
			$max2=$$ref[0];}
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($max1+$max2)/100);
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=17)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$stat=0;
		$k=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$stat=$stat+8;
			$k=$k+1;
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($stat)/100);
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$info.="Достопримечательностей около дома: $k";
		}
}
if ($kind==11)
	{
		$m_d=30;
		$zona=150;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=11";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=(35-1/(0.005*$kol+1/35));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=(35-1/(0.005*$nkol+1/35));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));



}
if ($kind==12)
	{
		$m_d=25;
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$doxod=sprintf("%.2f",$m_d);
		$zona=600;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$max1=0;
		$max2=0;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=6)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max1)
			{
			$max1=$$ref[0];}
		}
		$rc = $sth->finish;
		$zona=1200;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		$max2=0;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=7)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			if ($$ref[0]>$max2)
			{
			$max2=$$ref[0];}
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($max1+$max2)/100);
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$info="<b>$lvl-й</b> уровень здания <b>+$pr%</b><br>";
		if ($max1>0)
		{
			$info.="Есть школа: <b>+$max1%</b><br>";
		}
		else
		{
			$info.="Нет школы<br>";
		}
		if ($max2>0)
		{
			$info.="Есть больница: <b>+$max2%</b><br>";
		}
		else
		{
			$info.="Нет больницы<br>";
		}
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT doxod FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=17)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		$stat=0;
		$k=0;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$stat=$stat+8;
			$k=$k+1;
		}
		$rc = $sth->finish;
		$doxod=$doxod*(1+($stat)/100);
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
		if ($stat==0)
		{
			$info.="Нет достопримечательностей";
		}
		else
		{
			$info.="Достопримечательностей около дома: $k";
		}
}
if ($kind==13)
	{
		$m_d=1800;
		$zona=1500;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=13";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000003*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000003*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));



}
if ($kind==14)
	{
		$m_d=1500;
		$zona=1500;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=14";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000004*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000004*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));



}
if ($kind==15)
	{
		$m_d=4000;
		$zona=5000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=15";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000001*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000001*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));



}
if ($kind==16)
	{
		$m_d=8000;
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=16";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000000375*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000000375*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));



}
if ($kind==17)
	{
		$m_d=8;
		$zona=10000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;
		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$nkol=$nkol+1
		}
		$rc = $sth->finish;
		$s1="";
		$doxod=$m_d;
		$pr=sprintf("%.2f",100*(1.2**($lvl-1)))-100;
		$info=$s1."Клиенты: <b>$nkol</b><br>Увеличивает доход дома на <b>$doxod%</b><br>";
		$doxod=sprintf("%.2f",$doxod*(1.2**($lvl-1)));
}
if ($kind==18)
	{
		$m_d=15000;
		$zona=1000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=18";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.000003*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.000003*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.5**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.5**($lvl-1)));



}
if ($kind==19)
	{
		$m_d=25000;
		$zona=5000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=18 or kind=20 or kind=22)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=19";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==18) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==20) {
				$kol=$kol+sprintf("%.3f",(20/$n));
				$nkol=$nkol+20;
			} elsif($$ref[2]==22) {
				$kol=$kol+sprintf("%.3f",(50/$n));
				$nkol=$nkol+50;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.0000057*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000057*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.5**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.5**($lvl-1)));



}
if ($kind==20)
	{
		$m_d=40000;
		$zona=3000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=20";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.0000001*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000001*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.5**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.5**($lvl-1)));
}
if ($kind==21)
	{
		$m_d=70000;
		$zona=25000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=19)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=21";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}

				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1

		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.0000051*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.0000051*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.5**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.5**($lvl-1)));



}
if ($kind==22)
	{
		$m_d=100000;
		$zona=5000;
		$d=$zona/(111110*cos($pos1*3.1415926535897932384/180));
		$b=$zona/111110;
		$pos01=$pos1-$b;
		$pos02=$pos2-$d;
		$pos11=$pos1-0+$b;
		$pos12=$pos2-0+$d;

		# обычный дом
		$q="SELECT pos1, pos2, kind FROM house where pos1>$pos01 and pos2>$pos02 and pos1<$pos11 and pos2<$pos12 and (kind=1 or kind=9 or kind=10 or kind=12)";
		$s.=$q;
		$sth = $dbh->prepare($q); # готовим запрос
		$sth->execute;
		while ($ref = $sth->fetchrow_arrayref)
		{
			$n=0;
			$pos001=$$ref[0]-$b;
			$pos002=$$ref[1]-$d;
			$pos011=$$ref[0]-0+$b;
			$pos012=$$ref[1]-0+$d;

			$q1="SELECT pos1,pos2 FROM house where pos1>$pos001 and pos2>$pos002 and pos1<$pos011 and pos2<$pos012 and kind=22";
			$sth1 = $dbh->prepare($q1); # готовим запрос
			$sth1->execute;
			while ($ref1 = $sth1->fetchrow_arrayref) {
				$n=$n+1;
			}
			if ($$ref[2]==1) {
				$kol=$kol+sprintf("%.3f",(1/$n));
				$nkol=$nkol+1
			} elsif ($$ref[2]==9) {
				$kol=$kol+sprintf("%.3f",(30/$n));
				$nkol=$nkol+30;
			} elsif($$ref[2]==10) {
				$kol=$kol+sprintf("%.3f",(15/$n));
				$nkol=$nkol+15;
			} elsif ($$ref[2]==12) {
				$kol=$kol+sprintf("%.3f",(5/$n));
				$nkol=$nkol+5;
			}
		}

		$rc = $sth->finish;
		$s1="";
		$doxod=((9/8)*$m_d-1/(0.00000003*$kol+8/(9*$m_d)));
		if ($doxod>$m_d)
		{
			$doxod=$m_d;
		}
		$m_doxod=((9/8)*$m_d-1/(0.00000003*$nkol+8/(9*$m_d)));
		if ($m_doxod>$m_d)
		{
			$m_doxod=$m_d;
		}
		if ($m_doxod>0)
		{
			$konk=sprintf("%.2f",(1-$doxod/$m_doxod)*100);
		}
		else
		{
			$konk=0;
		}
		$pr=sprintf("%.2f",100*(1.5**($lvl-1)))-100;
		$m_doxod=sprintf("%.2f",$m_doxod);
		$doxod=sprintf("%.2f",$doxod);
		$info=$s1."Клиенты: <b>$nkol</b><br>Доход: <b>$m_doxod</b> <img src=images/money.png width=16 height=16><br><b>$lvl-й</b> уровень здания <b>+$pr%</b><br>Конкуренция: <b>-$konk%</b>";
		$doxod=sprintf("%.2f",$doxod*(1.5**($lvl-1)));
}
$q2="UPDATE `house` SET `doxod` = $doxod, `info` = '$info' WHERE `house`.`id` =$idh;";
$sth = $dbh->prepare($q2); # готовим запрос
$sth->execute;
$rc = $sth->finish;
 }
	else
	{sleep(5);}
}

#$qu="UPDATE `per_h` SET `state`=0 where `id`=$id_per";
#$sth20 = $dbh->prepare($qu);
#$sth20->execute;
#$rc20 = $sth20->finish;
$rc = $dbh->disconnect;  # закрываем соединение